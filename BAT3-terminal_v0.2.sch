<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.5.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="no" active="no"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Gehäuse" color="7" fill="1" visible="yes" active="yes"/>
<layer number="102" name="Mittellin" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="fp3" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="yes" active="yes"/>
<layer number="105" name="Beschreib" color="9" fill="1" visible="no" active="yes"/>
<layer number="106" name="BGA-Top" color="4" fill="1" visible="no" active="yes"/>
<layer number="107" name="BD-Top" color="5" fill="1" visible="no" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="no" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="no" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="no" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="no" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="113" name="ReferenceLS" color="7" fill="1" visible="no" active="no"/>
<layer number="114" name="FRNTMAAT1" color="7" fill="1" visible="no" active="yes"/>
<layer number="115" name="FRNTMAAT2" color="7" fill="1" visible="no" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="yes"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="no" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="no"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="no" active="yes"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="no" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="no" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="no" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="no" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="no" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="no"/>
<layer number="201" name="201bmp" color="2" fill="1" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="1" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="no" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="no" active="no"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="no" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="no" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="OrgLBR" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="_Special">
<packages>
<package name="1473005-1">
<wire x1="-37.5" y1="22.4" x2="-32.5" y2="22.4" width="0.2032" layer="51"/>
<wire x1="-32.5" y1="22.4" x2="-32.5" y2="14.7" width="0.2032" layer="51"/>
<wire x1="-32.5" y1="14.7" x2="-29.9" y2="14.7" width="0.2032" layer="51"/>
<wire x1="-29.9" y1="14.7" x2="-29.9" y2="9.3" width="0.2032" layer="51"/>
<wire x1="-29.9" y1="9.3" x2="-34.1" y2="9.3" width="0.2032" layer="51"/>
<wire x1="-34.1" y1="9.3" x2="-34.1" y2="3.6" width="0.2032" layer="21"/>
<wire x1="-34.1" y1="3.6" x2="-32.1" y2="3.6" width="0.2032" layer="21"/>
<wire x1="-32.1" y1="3.2" x2="32.1" y2="3.2" width="0.2032" layer="51"/>
<wire x1="32.1" y1="3.2" x2="32.1" y2="3.6" width="0.2032" layer="51"/>
<wire x1="32.1" y1="3.6" x2="34.1" y2="3.6" width="0.2032" layer="21"/>
<wire x1="34.1" y1="3.6" x2="34.1" y2="9.3" width="0.2032" layer="21"/>
<wire x1="34.1" y1="9.3" x2="29.9" y2="9.3" width="0.2032" layer="51"/>
<wire x1="29.9" y1="9.3" x2="29.9" y2="14.7" width="0.2032" layer="51"/>
<wire x1="29.9" y1="14.7" x2="32.5" y2="14.7" width="0.2032" layer="51"/>
<wire x1="32.5" y1="14.7" x2="32.5" y2="22.4" width="0.2032" layer="51"/>
<wire x1="32.5" y1="22.4" x2="37.5" y2="22.4" width="0.2032" layer="51"/>
<wire x1="37.5" y1="22.4" x2="37.5" y2="22" width="0.2032" layer="51"/>
<wire x1="37.5" y1="22" x2="36" y2="16" width="0.2032" layer="51"/>
<wire x1="36" y1="16" x2="35.6" y2="15.6" width="0.2032" layer="51"/>
<wire x1="35.6" y1="15.6" x2="35.6" y2="5.4" width="0.2032" layer="21"/>
<wire x1="35.6" y1="5.4" x2="35" y2="4.8" width="0.2032" layer="21"/>
<wire x1="35" y1="4.8" x2="35" y2="1.5" width="0.2032" layer="21"/>
<wire x1="35" y1="1.5" x2="35.6" y2="1.5" width="0.2032" layer="21"/>
<wire x1="35.6" y1="1.5" x2="35.6" y2="-3.5" width="0.2032" layer="21"/>
<wire x1="35.6" y1="-3.5" x2="32.1" y2="-3.5" width="0.2032" layer="21"/>
<wire x1="32.1" y1="-3.5" x2="32.1" y2="-3.1" width="0.2032" layer="51"/>
<wire x1="32.1" y1="-3.1" x2="-32.1" y2="-3.1" width="0.2032" layer="51"/>
<wire x1="-32.1" y1="-3.1" x2="-32.1" y2="-3.6" width="0.2032" layer="51"/>
<wire x1="-32.1" y1="-3.6" x2="-35.6" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-35.6" y1="-3.6" x2="-35.6" y2="1.5" width="0.2032" layer="21"/>
<wire x1="-35.6" y1="1.5" x2="-35" y2="1.5" width="0.2032" layer="21"/>
<wire x1="-35" y1="1.5" x2="-35" y2="4.8" width="0.2032" layer="21"/>
<wire x1="-35" y1="4.8" x2="-35.6" y2="5.4" width="0.2032" layer="21"/>
<wire x1="-35.6" y1="5.4" x2="-35.6" y2="15.6" width="0.2032" layer="21"/>
<wire x1="-35.6" y1="15.6" x2="-36" y2="16" width="0.2032" layer="51"/>
<wire x1="-36" y1="16" x2="-37.5" y2="22" width="0.2032" layer="51"/>
<wire x1="-37.5" y1="22" x2="-37.5" y2="22.4" width="0.2032" layer="51"/>
<wire x1="-32.1" y1="3.6" x2="-32.1" y2="3.2" width="0.2032" layer="51"/>
<smd name="1" x="-31.65" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="2" x="-31.35" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="3" x="-31.05" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="4" x="-30.75" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="5" x="-30.45" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="6" x="-30.15" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="7" x="-29.85" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="8" x="-29.55" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="9" x="-29.25" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="10" x="-28.95" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="11" x="-28.65" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="12" x="-28.35" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="13" x="-28.05" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="14" x="-27.75" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="15" x="-27.45" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="16" x="-27.15" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="17" x="-26.85" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="18" x="-26.55" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="19" x="-26.25" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="20" x="-25.95" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="21" x="-25.65" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="22" x="-25.35" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="23" x="-25.05" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="24" x="-24.75" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="25" x="-24.45" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="26" x="-24.15" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="27" x="-23.85" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="28" x="-23.55" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="29" x="-23.25" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="30" x="-22.95" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="31" x="-22.65" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="32" x="-22.35" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="33" x="-22.05" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="34" x="-21.75" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="35" x="-21.45" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="36" x="-21.15" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="37" x="-20.85" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="38" x="-20.55" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="39" x="-20.25" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="40" x="-19.95" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="41" x="-16.05" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="42" x="-15.75" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="43" x="-15.45" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="44" x="-15.15" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="45" x="-14.85" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="46" x="-14.55" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="47" x="-14.25" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="48" x="-13.95" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="49" x="-13.65" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="50" x="-13.35" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="51" x="-13.05" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="52" x="-12.75" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="53" x="-12.45" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="54" x="-12.15" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="55" x="-11.85" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="56" x="-11.55" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="57" x="-11.25" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="58" x="-10.95" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="59" x="-10.65" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="60" x="-10.35" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="61" x="-10.05" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="62" x="-9.75" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="63" x="-9.45" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="64" x="-9.15" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="65" x="-8.85" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="66" x="-8.55" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="67" x="-8.25" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="68" x="-7.95" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="69" x="-7.65" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="70" x="-7.35" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="71" x="-7.05" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="72" x="-6.75" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="73" x="-6.45" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="74" x="-6.15" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="75" x="-5.85" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="76" x="-5.55" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="77" x="-5.25" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="78" x="-4.95" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="79" x="-4.65" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="80" x="-4.35" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="81" x="-4.05" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="82" x="-3.75" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="83" x="-3.45" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="84" x="-3.15" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="85" x="-2.85" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="86" x="-2.55" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="87" x="-2.25" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="88" x="-1.95" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="89" x="-1.65" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="90" x="-1.35" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="91" x="-1.05" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="92" x="-0.75" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="93" x="-0.45" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="94" x="-0.15" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="95" x="0.15" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="96" x="0.45" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="97" x="0.75" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="98" x="1.05" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="99" x="1.35" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="100" x="1.65" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="101" x="1.95" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="102" x="2.25" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="103" x="2.55" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="104" x="2.85" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="105" x="3.15" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="106" x="3.45" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="107" x="3.75" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="108" x="4.05" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="109" x="4.35" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="110" x="4.65" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="111" x="4.95" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="112" x="5.25" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="113" x="5.55" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="114" x="5.85" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="115" x="6.15" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="116" x="6.45" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="117" x="6.75" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="118" x="7.05" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="119" x="7.35" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="120" x="7.65" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="121" x="7.95" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="122" x="8.25" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="123" x="8.55" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="124" x="8.85" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="125" x="9.15" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="126" x="9.45" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="127" x="9.75" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="128" x="10.05" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="129" x="10.35" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="130" x="10.65" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="131" x="10.95" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="132" x="11.25" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="133" x="11.55" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="134" x="11.85" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="135" x="12.15" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="136" x="12.45" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="137" x="12.75" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="138" x="13.05" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="139" x="13.35" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="140" x="13.65" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="141" x="13.95" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="142" x="14.25" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="143" x="14.55" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="144" x="14.85" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="145" x="15.15" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="146" x="15.45" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="147" x="15.75" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="148" x="16.05" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="149" x="16.35" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="150" x="16.65" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="151" x="16.95" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="152" x="17.25" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="153" x="17.55" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="154" x="17.85" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="155" x="18.15" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="156" x="18.45" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="157" x="18.75" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="158" x="19.05" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="159" x="19.35" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="160" x="19.65" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="161" x="19.95" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="162" x="20.25" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="163" x="20.55" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="164" x="20.85" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="165" x="21.15" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="166" x="21.45" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="167" x="21.75" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="168" x="22.05" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="169" x="22.35" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="170" x="22.65" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="171" x="22.95" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="172" x="23.25" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="173" x="23.55" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="174" x="23.85" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="175" x="24.15" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="176" x="24.45" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="177" x="24.75" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="178" x="25.05" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="179" x="25.35" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="180" x="25.65" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="181" x="25.95" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="182" x="26.25" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="183" x="26.55" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="184" x="26.85" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="185" x="27.15" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="186" x="27.45" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="187" x="27.75" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="188" x="28.05" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="189" x="28.35" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="190" x="28.65" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="191" x="28.95" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="192" x="29.25" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="193" x="29.55" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="194" x="29.85" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="195" x="30.15" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="196" x="30.45" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="197" x="30.75" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="198" x="31.05" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="199" x="31.35" y="-4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="200" x="31.65" y="4.1" dx="0.35" dy="2" layer="1" rot="R180"/>
<smd name="M1" x="-32.6" y="12" dx="4.6" dy="4.6" layer="1" rot="R90"/>
<smd name="M2" x="32.6" y="12" dx="4.6" dy="4.6" layer="1" rot="R90"/>
<text x="-30.48" y="0" size="1.778" layer="25" ratio="18" align="center-left">&gt;NAME</text>
<text x="30.48" y="0" size="1.778" layer="27" ratio="10" align="center-right">&gt;VALUE</text>
<hole x="-33.4" y="0" drill="1.6"/>
<hole x="33.4" y="0" drill="1.6"/>
<wire x1="-33.8" y1="37" x2="33.8" y2="37" width="0.127" layer="21" style="shortdash"/>
<wire x1="-33.8" y1="37" x2="-33.8" y2="22.8" width="0.127" layer="21" style="shortdash"/>
<wire x1="33.8" y1="37" x2="33.8" y2="22.8" width="0.127" layer="21" style="shortdash"/>
</package>
<package name="BHSD-2032">
<wire x1="-14.19" y1="7.58" x2="-14.19" y2="3" width="0.127" layer="21"/>
<wire x1="-14.19" y1="3" x2="-14.19" y2="-3" width="0.127" layer="21"/>
<wire x1="-14.19" y1="-3" x2="-14.19" y2="-7.58" width="0.127" layer="21"/>
<wire x1="14.19" y1="7.58" x2="14.19" y2="3" width="0.127" layer="21"/>
<wire x1="14.19" y1="3" x2="14.19" y2="-3" width="0.127" layer="21"/>
<wire x1="14.19" y1="-3" x2="14.19" y2="-7.58" width="0.127" layer="21"/>
<wire x1="-8.24" y1="7.58" x2="-14.19" y2="7.58" width="0.127" layer="21"/>
<wire x1="-8.24" y1="-7.58" x2="-14.19" y2="-7.58" width="0.127" layer="21"/>
<wire x1="8.24" y1="7.58" x2="14.19" y2="7.58" width="0.127" layer="21"/>
<wire x1="8.24" y1="-7.58" x2="14.19" y2="-7.58" width="0.127" layer="21"/>
<wire x1="-14.19" y1="3" x2="-7.7" y2="3" width="0.127" layer="21"/>
<wire x1="-7.7" y1="3" x2="-7.7" y2="-3" width="0.127" layer="21"/>
<wire x1="-7.7" y1="-3" x2="-14.19" y2="-3" width="0.127" layer="21"/>
<wire x1="14.19" y1="3" x2="0.5" y2="3" width="0.127" layer="21"/>
<wire x1="0.5" y1="3" x2="0.5" y2="1" width="0.127" layer="21"/>
<wire x1="0.5" y1="1" x2="6.5" y2="1" width="0.127" layer="21"/>
<wire x1="6.5" y1="1" x2="6.5" y2="-1" width="0.127" layer="21"/>
<wire x1="6.5" y1="-1" x2="0.5" y2="-1" width="0.127" layer="21"/>
<wire x1="0.5" y1="-1" x2="0.5" y2="-3" width="0.127" layer="21"/>
<wire x1="0.5" y1="-3" x2="14.19" y2="-3" width="0.127" layer="21"/>
<circle x="0" y="0" radius="11.085" width="0.127" layer="21"/>
<circle x="1.5" y="2" radius="0.4472" width="0.127" layer="21"/>
<circle x="1.5" y="-2" radius="0.4472" width="0.127" layer="21"/>
<smd name="MINUS" x="-14.65" y="0" dx="2.79" dy="4.06" layer="1"/>
<smd name="PLUS" x="14.65" y="0" dx="2.79" dy="4.06" layer="1"/>
<text x="-4.635" y="11.7725" size="1.778" layer="25">&gt;NAME</text>
<text x="-4.5975" y="-13.7325" size="1.778" layer="27">&gt;VALUE</text>
</package>
<package name="2041021">
<wire x1="-13.2" y1="-9.86" x2="-13.2" y2="15.24" width="0.127" layer="21"/>
<wire x1="-13.2" y1="15.24" x2="10.6" y2="15.24" width="0.127" layer="21"/>
<wire x1="10.6" y1="15.24" x2="10.6" y2="12.74" width="0.127" layer="21"/>
<wire x1="10.6" y1="12.74" x2="13.3" y2="12.74" width="0.127" layer="21"/>
<wire x1="13.3" y1="12.74" x2="13.3" y2="-9.86" width="0.127" layer="21"/>
<wire x1="13.3" y1="-9.86" x2="12.2" y2="-9.86" width="0.127" layer="21"/>
<wire x1="12.2" y1="-9.86" x2="-12.3" y2="-9.86" width="0.127" layer="21"/>
<wire x1="-12.3" y1="-9.86" x2="-13.2" y2="-9.86" width="0.127" layer="21"/>
<smd name="GP" x="-13.8" y="-6.56" dx="2.8" dy="2" layer="1" rot="R90"/>
<smd name="GP2" x="14.1" y="-6.56" dx="2.8" dy="2" layer="1" rot="R90"/>
<smd name="GP4" x="-13.9" y="14.24" dx="2.8" dy="2" layer="1" rot="R90"/>
<smd name="WP" x="-12.2" y="16.14" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="8" x="-9.7" y="16.14" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="7" x="-8.1" y="16.14" dx="2" dy="1.2" layer="1" rot="R90"/>
<smd name="6" x="-5.6" y="16.14" dx="2" dy="1.2" layer="1" rot="R90"/>
<smd name="5" x="-3.1" y="16.14" dx="2" dy="1.2" layer="1" rot="R90"/>
<smd name="2" x="4.4" y="16.14" dx="2" dy="1.2" layer="1" rot="R90"/>
<smd name="4" x="-0.6" y="16.14" dx="2" dy="1.2" layer="1" rot="R90"/>
<smd name="1" x="6.9" y="16.14" dx="2" dy="1.2" layer="1" rot="R90"/>
<smd name="3" x="1.9" y="16.14" dx="2" dy="1.2" layer="1" rot="R90"/>
<smd name="9" x="9.4" y="16.14" dx="2" dy="1.2" layer="1" rot="R90"/>
<smd name="CD" x="-11" y="16.14" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="GP3" x="11.7" y="14.34" dx="2.8" dy="2" layer="1" rot="R90"/>
<hole x="-11.6" y="13.14" drill="1.1"/>
<hole x="9.5" y="13.14" drill="1.6"/>
<wire x1="-10" y1="12.5" x2="10" y2="12.5" width="0.127" layer="21"/>
<wire x1="10" y1="12.5" x2="10" y2="5" width="0.127" layer="21"/>
<wire x1="-10" y1="12.5" x2="-10" y2="2.5" width="0.127" layer="21"/>
<wire x1="-10" y1="2.5" x2="10" y2="2.5" width="0.127" layer="21"/>
<wire x1="10" y1="2.5" x2="10" y2="5" width="0.127" layer="21"/>
<text x="0" y="-2.54" size="1.778" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-6.35" size="1.778" layer="27" align="bottom-center">&gt;VALUE</text>
</package>
<package name="SOLDER_JUMPER2">
<wire x1="1.397" y1="-1.016" x2="-1.397" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.397" y1="1.016" x2="1.651" y2="0.762" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.651" y1="0.762" x2="-1.397" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.651" y1="-0.762" x2="-1.397" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="1.397" y1="-1.016" x2="1.651" y2="-0.762" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="-0.762" x2="1.651" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-1.651" y1="-0.762" x2="-1.651" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-1.397" y1="1.016" x2="1.397" y2="1.016" width="0.1524" layer="21"/>
<wire x1="1.016" y1="0" x2="1.524" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.016" y1="0" x2="-1.524" y2="0" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="-0.127" x2="-0.254" y2="0.127" width="1.27" layer="51" curve="-180" cap="flat"/>
<wire x1="0.254" y1="0.127" x2="0.254" y2="-0.127" width="1.27" layer="51" curve="-180" cap="flat"/>
<smd name="1" x="-0.8" y="0" dx="1.1684" dy="1.6002" layer="1"/>
<smd name="2" x="0.8" y="0" dx="1.1684" dy="1.6002" layer="1"/>
<text x="-1.651" y="1.143" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0.4001" y="0" size="0.02" layer="27">&gt;VALUE</text>
</package>
<package name="3,2MM_HOLE">
<pad name="P$1" x="0" y="0" drill="3.2" diameter="6.4516" thermals="no"/>
<pad name="P$2" x="0" y="2.7325" drill="0.5" diameter="0.254" thermals="no"/>
<pad name="P$3" x="1.377" y="2.373" drill="0.5" diameter="0.254" thermals="no"/>
<pad name="P$4" x="-1.377" y="2.373" drill="0.5" diameter="0.254" thermals="no"/>
<pad name="P$5" x="0" y="-2.7325" drill="0.5" diameter="0.254" thermals="no"/>
<pad name="P$6" x="1.377" y="-2.373" drill="0.5" diameter="0.254" thermals="no"/>
<pad name="P$7" x="-1.377" y="-2.373" drill="0.5" diameter="0.254" thermals="no"/>
<pad name="P$9" x="-2.373" y="-1.377" drill="0.5" diameter="0.254" thermals="no"/>
<pad name="P$10" x="-2.7325" y="0" drill="0.5" diameter="0.254" thermals="no"/>
<pad name="P$11" x="-2.373" y="1.377" drill="0.5" diameter="0.254" thermals="no"/>
<pad name="P$12" x="2.7325" y="0" drill="0.5" diameter="0.254" thermals="no"/>
<pad name="P$13" x="2.373" y="1.377" drill="0.5" diameter="0.254" thermals="no"/>
<pad name="P$14" x="2.373" y="-1.377" drill="0.5" diameter="0.254" thermals="no"/>
</package>
<package name="3,8MM_HOLE">
<pad name="P$1" x="0" y="0" drill="3.8" diameter="6.4516"/>
<pad name="P$2" x="0" y="2.7325" drill="0.5" diameter="0.254"/>
<pad name="P$3" x="1.377" y="2.373" drill="0.5" diameter="0.254"/>
<pad name="P$4" x="-1.377" y="2.373" drill="0.5" diameter="0.254"/>
<pad name="P$5" x="0" y="-2.7325" drill="0.5" diameter="0.254"/>
<pad name="P$6" x="1.377" y="-2.373" drill="0.5" diameter="0.254"/>
<pad name="P$7" x="-1.377" y="-2.373" drill="0.5" diameter="0.254"/>
<pad name="P$9" x="-2.373" y="-1.377" drill="0.5" diameter="0.254"/>
<pad name="P$10" x="-2.7325" y="0" drill="0.5" diameter="0.254"/>
<pad name="P$11" x="-2.373" y="1.377" drill="0.5" diameter="0.254"/>
<pad name="P$12" x="2.7325" y="0" drill="0.5" diameter="0.254"/>
<pad name="P$13" x="2.373" y="1.377" drill="0.5" diameter="0.254"/>
<pad name="P$14" x="2.373" y="-1.377" drill="0.5" diameter="0.254"/>
</package>
<package name="XTALSMD5X3.2">
<wire x1="-2.5" y1="-1.2" x2="-2.5" y2="1.2" width="0.127" layer="21"/>
<wire x1="2.5" y1="-1.2" x2="2.5" y2="1.2" width="0.127" layer="21"/>
<wire x1="-2.1" y1="1.6" x2="2.1" y2="1.6" width="0.127" layer="21"/>
<wire x1="-2.1" y1="-1.6" x2="2.1" y2="-1.6" width="0.127" layer="21"/>
<wire x1="-2.5" y1="1.2" x2="-2.1" y2="1.6" width="0.127" layer="21"/>
<wire x1="2.1" y1="1.6" x2="2.5" y2="1.2" width="0.127" layer="21"/>
<wire x1="2.5" y1="-1.2" x2="2.1" y2="-1.6" width="0.127" layer="21"/>
<wire x1="-2.1" y1="-1.6" x2="-2.5" y2="-1.2" width="0.127" layer="21"/>
<wire x1="-2.2" y1="1.1" x2="-2" y2="1.3" width="0.127" layer="21"/>
<wire x1="-2" y1="1.3" x2="2" y2="1.3" width="0.127" layer="21"/>
<wire x1="2" y1="1.3" x2="2.2" y2="1.1" width="0.127" layer="21"/>
<wire x1="2.2" y1="1.1" x2="2.2" y2="-1.1" width="0.127" layer="21"/>
<wire x1="2.2" y1="-1.1" x2="2" y2="-1.3" width="0.127" layer="21"/>
<wire x1="2" y1="-1.3" x2="-2" y2="-1.3" width="0.127" layer="21"/>
<wire x1="-2" y1="-1.3" x2="-2.2" y2="-1.1" width="0.127" layer="21"/>
<wire x1="-2.2" y1="-1.1" x2="-2.2" y2="1.1" width="0.127" layer="21"/>
<wire x1="-0.3" y1="0.5" x2="0.3" y2="0.5" width="0.127" layer="21"/>
<wire x1="0.3" y1="0.5" x2="0.3" y2="-0.5" width="0.127" layer="21"/>
<wire x1="0.3" y1="-0.5" x2="-0.3" y2="-0.5" width="0.127" layer="21"/>
<wire x1="-0.3" y1="-0.5" x2="-0.3" y2="0.5" width="0.127" layer="21"/>
<wire x1="0.5" y1="0.5" x2="0.5" y2="0" width="0.127" layer="21"/>
<wire x1="0.5" y1="0" x2="0.5" y2="-0.5" width="0.127" layer="21"/>
<wire x1="-0.5" y1="0.5" x2="-0.5" y2="0" width="0.127" layer="21"/>
<wire x1="-0.5" y1="0" x2="-0.5" y2="-0.5" width="0.127" layer="21"/>
<wire x1="0.5" y1="0" x2="1.6" y2="0" width="0.127" layer="21"/>
<wire x1="1.6" y1="0" x2="1.9" y2="0.3" width="0.127" layer="21"/>
<wire x1="1.9" y1="0.3" x2="1.9" y2="0.5" width="0.127" layer="21"/>
<wire x1="-0.5" y1="0" x2="-1.6" y2="0" width="0.127" layer="21"/>
<wire x1="-1.6" y1="0" x2="-1.9" y2="-0.3" width="0.127" layer="21"/>
<wire x1="-1.9" y1="-0.3" x2="-1.9" y2="-0.5" width="0.127" layer="21"/>
<smd name="1" x="-1.9" y="-1.15" dx="1.6" dy="1.3" layer="1"/>
<smd name="2" x="1.9" y="-1.15" dx="1.6" dy="1.3" layer="1"/>
<smd name="3" x="1.9" y="1.15" dx="1.6" dy="1.3" layer="1"/>
<smd name="4" x="-1.9" y="1.15" dx="1.6" dy="1.3" layer="1"/>
<text x="-2.54" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.6767" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="SOLDER_JUMPER3">
<smd name="1" x="0" y="0" dx="1.778" dy="0.9144" layer="1" rot="R90"/>
<smd name="2" x="1.27" y="0" dx="1.778" dy="0.9144" layer="1" rot="R90"/>
<smd name="3" x="2.54" y="0" dx="1.778" dy="0.9144" layer="1" rot="R90"/>
<text x="1.27" y="1.27" size="1.27" layer="21" align="bottom-center">&gt;NAME</text>
<text x="-8.89" y="5.08" size="1.27" layer="21">1</text>
<text x="0" y="-1.27" size="0.6096" layer="51" align="top-center">1</text>
<text x="1.27" y="-1.27" size="0.6096" layer="51" align="top-center">2</text>
<text x="2.54" y="-1.27" size="0.6096" layer="51" align="top-center">3</text>
</package>
</packages>
<symbols>
<symbol name="COLIBRI-AUDIO">
<wire x1="-5.08" y1="20.32" x2="27.94" y2="20.32" width="0.254" layer="94"/>
<wire x1="27.94" y1="20.32" x2="27.94" y2="-20.32" width="0.254" layer="94"/>
<wire x1="27.94" y1="-20.32" x2="-5.08" y2="-20.32" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-20.32" x2="-5.08" y2="20.32" width="0.254" layer="94"/>
<text x="-5.08" y="22.86" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="-22.86" size="1.778" layer="96">&gt;VALUE</text>
<pin name="MIC_IN" x="-7.62" y="-5.08" length="short"/>
<pin name="MIC_GND" x="-7.62" y="-7.62" length="short"/>
<pin name="LINEIN_L" x="-7.62" y="12.7" length="short"/>
<pin name="LINEIN_R" x="-7.62" y="10.16" length="short"/>
<pin name="VSSA_AUDIO1" x="20.32" y="-22.86" length="short" rot="R90"/>
<pin name="VDDA_AUDIO1" x="20.32" y="22.86" length="short" rot="R270"/>
<pin name="VSSA_AUDIO2" x="22.86" y="-22.86" length="short" rot="R90"/>
<pin name="VDDA_AUDIO2" x="22.86" y="22.86" length="short" rot="R270"/>
<pin name="HEADPHONE_GND" x="-7.62" y="2.54" length="short"/>
<pin name="HEADPHONE_L" x="-7.62" y="5.08" length="short"/>
<pin name="HEADPHONE_R" x="-7.62" y="0" length="short"/>
</symbol>
<symbol name="COLIBRI-CORE">
<wire x1="-33.02" y1="33.02" x2="30.48" y2="33.02" width="0.254" layer="94"/>
<wire x1="30.48" y1="33.02" x2="30.48" y2="-33.02" width="0.254" layer="94"/>
<wire x1="30.48" y1="-33.02" x2="-33.02" y2="-33.02" width="0.254" layer="94"/>
<wire x1="-33.02" y1="-33.02" x2="-33.02" y2="33.02" width="0.254" layer="94"/>
<text x="-33.02" y="35.56" size="1.778" layer="95">&gt;NAME</text>
<text x="-33.02" y="-35.56" size="1.778" layer="96">&gt;VALUE</text>
<pin name="AD3" x="-35.56" y="-27.94" length="short"/>
<pin name="AD2" x="-35.56" y="-25.4" length="short"/>
<pin name="AD1" x="-35.56" y="-22.86" length="short"/>
<pin name="AD0" x="-35.56" y="-20.32" length="short"/>
<pin name="!RESET_EXT" x="33.02" y="27.94" length="short" rot="R180"/>
<pin name="GPIO-J.01/L_BIAS" x="33.02" y="12.7" length="short" rot="R180"/>
<pin name="GPIO-V.01/PRDY" x="33.02" y="-2.54" length="short" rot="R180"/>
<pin name="GPIO-CC.00/MMC_CLK" x="-35.56" y="7.62" length="short"/>
<pin name="GPIO-BB.01/MMC_DAT1" x="-35.56" y="2.54" length="short"/>
<pin name="GPIO-BB.02/MMC_DAT2/MMCS0" x="-35.56" y="0" length="short"/>
<pin name="GPIO-BB.03/MMC_DAT3/MMCS1" x="-35.56" y="-2.54" length="short"/>
<pin name="GPIO-B.03/L_PCLK_WR" x="33.02" y="15.24" length="short" rot="R180"/>
<pin name="GPIO-J.03/L_LCLK_A0" x="33.02" y="20.32" length="short" rot="R180"/>
<pin name="GPIO-J.04/L_FCLK_RD" x="33.02" y="17.78" length="short" rot="R180"/>
<pin name="GPIO-I.04/!RESET_OUT" x="33.02" y="25.4" length="short" rot="R180"/>
<pin name="!WE" x="-35.56" y="-10.16" length="short"/>
<pin name="!OE" x="-35.56" y="-12.7" length="short"/>
<pin name="RDWR" x="-35.56" y="-15.24" length="short"/>
<pin name="GPIO-CC.07/!PCE1" x="-35.56" y="27.94" length="short"/>
<pin name="GPIO-I.07/RDY" x="-35.56" y="25.4" length="short"/>
<pin name="GPIO-Z.00/!PCE2" x="-35.56" y="22.86" length="short"/>
<pin name="GPIO-Y.04/!POE" x="-35.56" y="20.32" length="short"/>
<pin name="GPIO-Z.01/!PREG" x="-35.56" y="17.78" length="short"/>
<pin name="GPIO-Z.03/!PWE" x="-35.56" y="15.24" length="short"/>
<pin name="GPIO-X.05/!PXCVREN" x="-35.56" y="12.7" length="short"/>
<pin name="GPIO-Y.07/!PIOW" x="33.02" y="0" length="short" rot="R180"/>
<pin name="GPIO-X.06/!PWAIT" x="33.02" y="2.54" length="short" rot="R180"/>
<pin name="GPIO-Y.06/!PIOR" x="33.02" y="5.08" length="short" rot="R180"/>
<pin name="GPIO-X.07/!IOIS16" x="33.02" y="7.62" length="short" rot="R180"/>
<pin name="GPIO-K.02/!EXT_CSO" x="33.02" y="-20.32" length="short" rot="R180"/>
<pin name="GPIO_K.04/!EXT_CS2" x="33.02" y="-25.4" length="short" rot="R180"/>
<pin name="GPIO-K.03/!EXT_CS1" x="33.02" y="-22.86" length="short" rot="R180"/>
<pin name="GPIO-CC.01/MMC_CMD" x="-35.56" y="-5.08" length="short"/>
<pin name="GPIO-BB.00/MMC_DAT0" x="-35.56" y="5.08" length="short"/>
</symbol>
<symbol name="COLIBRI-ETH">
<wire x1="-7.62" y1="12.7" x2="20.32" y2="12.7" width="0.254" layer="94"/>
<wire x1="20.32" y1="12.7" x2="20.32" y2="-12.7" width="0.254" layer="94"/>
<wire x1="20.32" y1="-12.7" x2="-7.62" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-12.7" x2="-7.62" y2="12.7" width="0.254" layer="94"/>
<text x="-7.62" y="15.24" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="-15.24" size="1.778" layer="96">&gt;VALUE</text>
<pin name="!ETH_LINK_ACT" x="-10.16" y="7.62" length="short"/>
<pin name="!ETH_SPEED_100" x="-10.16" y="5.08" length="short"/>
<pin name="ETH_TXO-" x="-10.16" y="2.54" length="short"/>
<pin name="ETH_TXO+" x="-10.16" y="0" length="short"/>
<pin name="ETH_AGND" x="-10.16" y="-2.54" length="short"/>
<pin name="ETH_RXI-" x="-10.16" y="-5.08" length="short"/>
<pin name="ETH_RXI+" x="-10.16" y="-7.62" length="short"/>
</symbol>
<symbol name="COLIBRI-GPIO">
<pin name="GPIO-B.05" x="15.24" y="71.12" length="short" rot="R180"/>
<pin name="GPIO-A.06" x="15.24" y="68.58" length="short" rot="R180"/>
<pin name="GPIO-B.06" x="-17.78" y="68.58" length="short"/>
<pin name="GPIO-B.04" x="-17.78" y="66.04" length="short"/>
<pin name="GPIO-B.07" x="-17.78" y="63.5" length="short"/>
<pin name="GPIO-DD.06" x="-17.78" y="60.96" length="short"/>
<pin name="GPIO-A.07" x="-17.78" y="58.42" length="short"/>
<pin name="GPIO-DD.05" x="-17.78" y="55.88" length="short"/>
<pin name="GPIO-V.02" x="-17.78" y="53.34" length="short"/>
<pin name="GPIO-S.00" x="-17.78" y="50.8" length="short"/>
<pin name="GPIO-W.05" x="-17.78" y="48.26" length="short"/>
<pin name="GPIO-CC.02" x="-17.78" y="45.72" length="short"/>
<pin name="GPIO-Y.05" x="-17.78" y="43.18" length="short"/>
<pin name="GPIO-C.01" x="-17.78" y="40.64" length="short"/>
<pin name="GPIO-V.03" x="-17.78" y="38.1" length="short"/>
<pin name="GPIO-C.07" x="-17.78" y="71.12" length="short"/>
<pin name="GPIO-J.00" x="15.24" y="15.24" length="short" rot="R180"/>
<pin name="GPIO-J.02" x="15.24" y="12.7" length="short" rot="R180"/>
<pin name="GPIO-I.03" x="15.24" y="10.16" length="short" rot="R180"/>
<pin name="GPIO-I.06" x="15.24" y="7.62" length="short" rot="R180"/>
<wire x1="-15.24" y1="76.2" x2="12.7" y2="76.2" width="0.254" layer="94"/>
<wire x1="12.7" y1="76.2" x2="12.7" y2="-66.04" width="0.254" layer="94"/>
<wire x1="12.7" y1="-66.04" x2="-15.24" y2="-66.04" width="0.254" layer="94"/>
<wire x1="-15.24" y1="-66.04" x2="-15.24" y2="76.2" width="0.254" layer="94"/>
<text x="-5.08" y="-73.66" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="-68.58" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GPIO-U.02" x="15.24" y="35.56" length="short" rot="R180"/>
<pin name="GPIO-J.06" x="-17.78" y="33.02" length="short"/>
<pin name="GPIO-U.03" x="15.24" y="33.02" length="short" rot="R180"/>
<pin name="GPIO-J.05" x="-17.78" y="30.48" length="short"/>
<pin name="GPIO-U.04" x="15.24" y="30.48" length="short" rot="R180"/>
<pin name="GPIO-W.06" x="-17.78" y="27.94" length="short"/>
<pin name="GPIO-U.05" x="15.24" y="27.94" length="short" rot="R180"/>
<pin name="GPIO-W.07" x="-17.78" y="25.4" length="short"/>
<pin name="GPIO-U.06" x="15.24" y="25.4" length="short" rot="R180"/>
<pin name="GPIO-C.00" x="-17.78" y="22.86" length="short"/>
<pin name="GPIO-P.04" x="15.24" y="22.86" length="short" rot="R180"/>
<pin name="GPIO-A.01" x="-17.78" y="20.32" length="short"/>
<pin name="GPIO-P.05" x="15.24" y="20.32" length="short" rot="R180"/>
<pin name="GPIO-U.00" x="-17.78" y="17.78" length="short"/>
<pin name="GPIO-P.06" x="15.24" y="17.78" length="short" rot="R180"/>
<pin name="GPIO-U.01" x="-17.78" y="15.24" length="short"/>
<pin name="GPIO-X.04" x="15.24" y="5.08" length="short" rot="R180"/>
<pin name="GPIO-X.03" x="15.24" y="2.54" length="short" rot="R180"/>
<pin name="GPIO-X.02" x="15.24" y="0" length="short" rot="R180"/>
<pin name="GPIO-X.01" x="15.24" y="-2.54" length="short" rot="R180"/>
<pin name="GPIO-X.00" x="15.24" y="-5.08" length="short" rot="R180"/>
<pin name="GPIO-A.05" x="15.24" y="-7.62" length="short" rot="R180"/>
<pin name="GPIO-A.04" x="15.24" y="-10.16" length="short" rot="R180"/>
<pin name="GPIO-G.00" x="-17.78" y="-15.24" length="short"/>
<pin name="GPIO-K.00" x="15.24" y="-15.24" length="short" rot="R180"/>
<pin name="GPIO-G.01" x="-17.78" y="-17.78" length="short"/>
<pin name="GPIO-K.01" x="15.24" y="-17.78" length="short" rot="R180"/>
<pin name="GPIO-G.02" x="-17.78" y="-20.32" length="short"/>
<pin name="GPIO-B.02" x="15.24" y="-20.32" length="short" rot="R180"/>
<pin name="GPIO-G.03" x="-17.78" y="-22.86" length="short"/>
<pin name="GPIO-Z.02" x="15.24" y="-22.86" length="short" rot="R180"/>
<pin name="GPIO-G.04" x="-17.78" y="-25.4" length="short"/>
<pin name="GPIO-N.05" x="15.24" y="-25.4" length="short" rot="R180"/>
<pin name="GPIO-G.05" x="-17.78" y="-27.94" length="short"/>
<pin name="GPIO-N.04" x="15.24" y="-27.94" length="short" rot="R180"/>
<pin name="GPIO-G.06" x="-17.78" y="-30.48" length="short"/>
<pin name="GPIO-N.06" x="15.24" y="-30.48" length="short" rot="R180"/>
<pin name="GPIO-G.07" x="-17.78" y="-33.02" length="short"/>
<pin name="GPIO-Z.04" x="15.24" y="-33.02" length="short" rot="R180"/>
<pin name="GPIO-H.00" x="-17.78" y="-35.56" length="short"/>
<pin name="GPIO-BB.04" x="15.24" y="-35.56" length="short" rot="R180"/>
<pin name="GPIO-H.01" x="-17.78" y="-38.1" length="short"/>
<pin name="GPIO-BB.05" x="15.24" y="-38.1" length="short" rot="R180"/>
<pin name="GPIO-H.02" x="-17.78" y="-40.64" length="short"/>
<pin name="GPIO-BB.06" x="15.24" y="-40.64" length="short" rot="R180"/>
<pin name="GPIO-H.03" x="-17.78" y="-43.18" length="short"/>
<pin name="GPIO-BB.07" x="15.24" y="-43.18" length="short" rot="R180"/>
<pin name="GPIO-H.04" x="-17.78" y="-45.72" length="short"/>
<pin name="GPIO-N.00" x="15.24" y="-45.72" length="short" rot="R180"/>
<pin name="GPIO-H.05" x="-17.78" y="-48.26" length="short"/>
<pin name="GPIO-N.01" x="15.24" y="-48.26" length="short" rot="R180"/>
<pin name="GPIO-H.06" x="-17.78" y="-50.8" length="short"/>
<pin name="GPIO-N.02" x="15.24" y="-50.8" length="short" rot="R180"/>
<pin name="GPIO-H.07" x="-17.78" y="-53.34" length="short"/>
<pin name="GPIO-N.03" x="15.24" y="-53.34" length="short" rot="R180"/>
<pin name="GPIO-A.03" x="15.24" y="-55.88" length="short" rot="R180"/>
<pin name="GPIO-A.02" x="15.24" y="-58.42" length="short" rot="R180"/>
<pin name="GPIO-P.07" x="15.24" y="-60.96" length="short" rot="R180"/>
</symbol>
<symbol name="COLIBRI-LDD">
<wire x1="-7.62" y1="25.4" x2="17.78" y2="25.4" width="0.254" layer="94"/>
<wire x1="17.78" y1="25.4" x2="17.78" y2="-27.94" width="0.254" layer="94"/>
<wire x1="17.78" y1="-27.94" x2="-7.62" y2="-27.94" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-27.94" x2="-7.62" y2="25.4" width="0.254" layer="94"/>
<text x="-7.62" y="27.94" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="-30.48" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GPIO-E.07/LDD07" x="-10.16" y="2.54" length="short"/>
<pin name="GPIO-F.01/LDD09" x="-10.16" y="-2.54" length="short"/>
<pin name="GPIO-F.03/LDD11" x="-10.16" y="-7.62" length="short"/>
<pin name="GPIO-F.04/LDD12" x="-10.16" y="-10.16" length="short"/>
<pin name="GPIO-F.05/LDD13" x="-10.16" y="-12.7" length="short"/>
<pin name="GPIO-M.00/LDD16" x="-10.16" y="-20.32" length="short"/>
<pin name="GPIO-E.03/LDD03" x="-10.16" y="12.7" length="short"/>
<pin name="GPIO-E.02/LDD02" x="-10.16" y="15.24" length="short"/>
<pin name="GPIO-M.01/LDD17" x="-10.16" y="-22.86" length="short"/>
<pin name="GPIO-F.00/LDD08" x="-10.16" y="0" length="short"/>
<pin name="GPIO-F.07/LDD15" x="-10.16" y="-17.78" length="short"/>
<pin name="GPIO-F.06/LDD14" x="-10.16" y="-15.24" length="short"/>
<pin name="GPIO-E.01/LDD01" x="-10.16" y="17.78" length="short"/>
<pin name="GPIO-E.05/LDD05" x="-10.16" y="7.62" length="short"/>
<pin name="GPIO-F.02/LDD10" x="-10.16" y="-5.08" length="short"/>
<pin name="GPIO-E.00/LDD00" x="-10.16" y="20.32" length="short"/>
<pin name="GPIO-E.04/LDD04" x="-10.16" y="10.16" length="short"/>
<pin name="GPIO-E.06/LDD06" x="-10.16" y="5.08" length="short"/>
</symbol>
<symbol name="COLIBRI-PWR">
<wire x1="-17.78" y1="15.24" x2="17.78" y2="15.24" width="0.254" layer="94"/>
<wire x1="17.78" y1="15.24" x2="17.78" y2="-15.24" width="0.254" layer="94"/>
<wire x1="17.78" y1="-15.24" x2="-17.78" y2="-15.24" width="0.254" layer="94"/>
<wire x1="-17.78" y1="-15.24" x2="-17.78" y2="15.24" width="0.254" layer="94"/>
<text x="10.16" y="-17.78" size="1.778" layer="96">&gt;VALUE</text>
<text x="20.32" y="-2.54" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<pin name="VDD_FAULT" x="10.16" y="17.78" length="short" rot="R270"/>
<pin name="BATT_FAULT" x="12.7" y="17.78" length="short" rot="R270"/>
<pin name="GND" x="-12.7" y="-17.78" length="short" rot="R90"/>
<pin name="VCC_3V3_VCCBATT" x="-12.7" y="17.78" length="short" rot="R270"/>
<pin name="GND@4" x="-10.16" y="-17.78" length="short" rot="R90"/>
<pin name="VCC_3V3" x="-10.16" y="17.78" length="short" rot="R270"/>
<pin name="GND@3" x="-7.62" y="-17.78" length="short" rot="R90"/>
<pin name="VCC_3V3@2" x="-7.62" y="17.78" length="short" rot="R270"/>
<pin name="VCC_3V3@3" x="-5.08" y="17.78" length="short" rot="R270"/>
<pin name="GND@2" x="-5.08" y="-17.78" length="short" rot="R90"/>
<pin name="GND@6" x="-2.54" y="-17.78" length="short" rot="R90"/>
<pin name="VCC_3V3@4" x="-2.54" y="17.78" length="short" rot="R270"/>
<pin name="GND@7" x="0" y="-17.78" length="short" rot="R90"/>
<pin name="VCC_3V3@5" x="0" y="17.78" length="short" rot="R270"/>
<pin name="GND@8" x="2.54" y="-17.78" length="short" rot="R90"/>
<pin name="VCC_3V3@6" x="2.54" y="17.78" length="short" rot="R270"/>
<pin name="GND@9" x="5.08" y="-17.78" length="short" rot="R90"/>
<pin name="VCC_3V3@7" x="5.08" y="17.78" length="short" rot="R270"/>
</symbol>
<symbol name="COLIBRI-SERIAL">
<wire x1="-10.16" y1="33.02" x2="20.32" y2="33.02" width="0.254" layer="94"/>
<wire x1="20.32" y1="33.02" x2="20.32" y2="-35.56" width="0.254" layer="94"/>
<wire x1="20.32" y1="-35.56" x2="-10.16" y2="-35.56" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-35.56" x2="-10.16" y2="33.02" width="0.254" layer="94"/>
<text x="-10.16" y="35.56" size="1.778" layer="95">&gt;NAME</text>
<text x="-10.16" y="-38.1" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GPIO-C.03/UART_C_RXD" x="-12.7" y="5.08" length="short"/>
<pin name="GPIO-C.02/UART_C_TXD" x="-12.7" y="7.62" length="short"/>
<pin name="GPIO-O.00/UART_A_DTR" x="-12.7" y="-12.7" length="short"/>
<pin name="GPIO-O.03/UART_A_CTS" x="-12.7" y="-7.62" length="short"/>
<pin name="GPIO-O.04/UART_A_RTS" x="-12.7" y="-5.08" length="short"/>
<pin name="GPIO-O.07/UART_A_DSR" x="-12.7" y="-15.24" length="short"/>
<pin name="GPIO-O.06/UART_A_DCD" x="-12.7" y="-10.16" length="short"/>
<pin name="GPIO-B.01/UART_B_CTS" x="-12.7" y="-30.48" length="short"/>
<pin name="GPIO-O.02/UART_A_RXD" x="-12.7" y="-2.54" length="short"/>
<pin name="GPIO-K.07/UART_B_RTS" x="-12.7" y="-27.94" length="short"/>
<pin name="GPIO-O.01/UART_A_TXD" x="-12.7" y="0" length="short"/>
<pin name="GPIO-B.00/UART_B_RXD" x="-12.7" y="-25.4" length="short"/>
<pin name="GPIO-O.05/UART_A_RI" x="-12.7" y="-17.78" length="short"/>
<pin name="GPIO-J.07/UART_B_TXD" x="-12.7" y="-22.86" length="short"/>
<pin name="GPIO-Y.03/SSPFRM" x="-12.7" y="12.7" length="short"/>
<pin name="GPIO-Y.02/SSPCLK" x="-12.7" y="15.24" length="short"/>
<pin name="GPIO-Y.01/SSPRXD" x="-12.7" y="17.78" length="short"/>
<pin name="GPIO-Y.00/SSPTXD" x="-12.7" y="20.32" length="short"/>
<pin name="GPIO-C.05/I2C_DATA" x="-12.7" y="25.4" length="short"/>
<pin name="GPIO-C.04/I2C_CLK" x="-12.7" y="27.94" length="short"/>
</symbol>
<symbol name="COLIBRI-TOUCH">
<wire x1="-2.54" y1="7.62" x2="10.16" y2="7.62" width="0.254" layer="94"/>
<wire x1="10.16" y1="7.62" x2="10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="-10.16" x2="-2.54" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-10.16" x2="-2.54" y2="7.62" width="0.254" layer="94"/>
<text x="-2.54" y="10.16" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-12.7" size="1.778" layer="96">&gt;VALUE</text>
<pin name="TSPX" x="-5.08" y="2.54" length="short"/>
<pin name="TSMX" x="-5.08" y="0" length="short"/>
<pin name="TSPY" x="-5.08" y="-2.54" length="short"/>
<pin name="TSMY" x="-5.08" y="-5.08" length="short"/>
</symbol>
<symbol name="COLIBRI-USB">
<wire x1="-10.16" y1="17.78" x2="27.94" y2="17.78" width="0.254" layer="94"/>
<wire x1="27.94" y1="17.78" x2="27.94" y2="-15.24" width="0.254" layer="94"/>
<wire x1="27.94" y1="-15.24" x2="-10.16" y2="-15.24" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-15.24" x2="-10.16" y2="17.78" width="0.254" layer="94"/>
<text x="-10.16" y="20.32" size="1.778" layer="95">&gt;NAME</text>
<text x="-10.16" y="-17.78" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GPIO-T.06/EXT_IO2" x="-12.7" y="12.7" length="short"/>
<pin name="GPIO-W.02/USBH_PEN" x="-12.7" y="10.16" length="short"/>
<pin name="GPIO-W.03/USBH_OC" x="-12.7" y="7.62" length="short"/>
<pin name="GPIO-T.05/EXT_IO1" x="-12.7" y="5.08" length="short"/>
<pin name="EXT_IO0/USB_ID" x="-12.7" y="2.54" length="short"/>
<pin name="USBC_DET" x="-12.7" y="0" length="short"/>
<pin name="USBH_P" x="-12.7" y="-2.54" length="short"/>
<pin name="USBH_N" x="-12.7" y="-5.08" length="short"/>
<pin name="USBC_P" x="-12.7" y="-7.62" length="short"/>
<pin name="USBC_N" x="-12.7" y="-10.16" length="short"/>
</symbol>
<symbol name="BATERIE">
<wire x1="0" y1="-2.54" x2="0" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="-1.2065" y1="-0.508" x2="0" y2="-0.508" width="0.8128" layer="94"/>
<wire x1="0" y1="-0.508" x2="1.2065" y2="-0.508" width="0.8128" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="0.8255" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0.635" x2="2.54" y2="0.635" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0.9525" x2="-1.27" y2="1.5875" width="0.254" layer="94"/>
<wire x1="-1.5875" y1="1.27" x2="-0.9525" y2="1.27" width="0.254" layer="94"/>
<text x="-2.54" y="5.08" size="1.778" layer="95" rot="R180">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.778" layer="96" rot="R180">&gt;VALUE</text>
<pin name="+" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="-" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
</symbol>
<symbol name="SDCARD">
<wire x1="-12.7" y1="20.32" x2="-12.7" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-10.16" x2="-10.16" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-12.7" x2="12.7" y2="-12.7" width="0.254" layer="94"/>
<wire x1="12.7" y1="-12.7" x2="12.7" y2="20.32" width="0.254" layer="94"/>
<wire x1="12.7" y1="20.32" x2="-12.7" y2="20.32" width="0.254" layer="94"/>
<pin name="CMD" x="-15.24" y="5.08" length="short" direction="in"/>
<pin name="DAT3" x="-15.24" y="7.62" length="short" direction="in"/>
<pin name="VSS1" x="15.24" y="7.62" length="short" direction="sup" rot="R180"/>
<pin name="VDD" x="15.24" y="17.78" length="short" direction="sup" rot="R180"/>
<pin name="CLK" x="-15.24" y="17.78" length="short" direction="in"/>
<pin name="VSS2" x="15.24" y="5.08" length="short" direction="sup" rot="R180"/>
<pin name="DAT0" x="-15.24" y="15.24" length="short" direction="out"/>
<pin name="DAT1" x="-15.24" y="12.7" length="short"/>
<pin name="DAT2" x="-15.24" y="10.16" length="short"/>
<pin name="WP" x="-15.24" y="0" length="short" direction="pas"/>
<pin name="CD" x="-15.24" y="-2.54" length="short" direction="pas"/>
<pin name="CHASSIS" x="15.24" y="-2.54" visible="pin" length="short" direction="sup" rot="R180"/>
<text x="-5.08" y="-7.62" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="-9.525" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="SOLDER_JUMPER2">
<wire x1="0.381" y1="0.635" x2="0.381" y2="-0.635" width="1.27" layer="94" curve="-180" cap="flat"/>
<wire x1="-0.381" y1="-0.635" x2="-0.381" y2="0.635" width="1.27" layer="94" curve="-180" cap="flat"/>
<wire x1="2.54" y1="0" x2="1.651" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.651" y2="0" width="0.1524" layer="94"/>
<text x="-2.54" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="DIRA_ESD">
<wire x1="-1.27" y1="-1.27" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="1.27" y2="1.27" width="0.254" layer="94"/>
<circle x="0" y="0" radius="1.27" width="0.254" layer="94"/>
<text x="-7.62" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<pin name="P$8" x="5.08" y="0" visible="off" length="short" rot="R180"/>
<wire x1="2.54" y1="0" x2="0" y2="0" width="0.1524" layer="94"/>
</symbol>
<symbol name="XTAL-GND">
<wire x1="-1.27" y1="2.54" x2="1.397" y2="2.54" width="0.4064" layer="94"/>
<wire x1="1.397" y1="2.54" x2="1.397" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.397" y1="-2.54" x2="-1.27" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="-1.27" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="2.3368" y1="2.54" x2="2.3368" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="-2.286" y1="2.54" x2="-2.286" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="1.905" x2="-5.08" y2="3.81" width="0.127" layer="94"/>
<wire x1="-5.08" y1="3.81" x2="-3.175" y2="3.81" width="0.127" layer="94"/>
<wire x1="5.08" y1="-1.905" x2="5.08" y2="-3.81" width="0.127" layer="94"/>
<wire x1="5.08" y1="-3.81" x2="3.175" y2="-3.81" width="0.127" layer="94"/>
<wire x1="-5.08" y1="-3.175" x2="-5.08" y2="-3.81" width="0.127" layer="94"/>
<wire x1="-5.08" y1="-3.81" x2="-3.175" y2="-3.81" width="0.127" layer="94"/>
<wire x1="-0.635" y1="-3.81" x2="1.27" y2="-3.81" width="0.127" layer="94"/>
<wire x1="5.08" y1="3.81" x2="3.175" y2="3.81" width="0.127" layer="94"/>
<wire x1="1.27" y1="3.81" x2="-0.635" y2="3.81" width="0.127" layer="94"/>
<wire x1="5.08" y1="1.27" x2="5.08" y2="-0.635" width="0.127" layer="94"/>
<wire x1="-5.08" y1="0.635" x2="-5.08" y2="-1.27" width="0.127" layer="94"/>
<text x="-3.4925" y="5.08" size="1.778" layer="95">&gt;NAME</text>
<text x="-4.1275" y="-6.6675" size="1.778" layer="95">&gt;VALUE</text>
<pin name="3" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="2" x="7.62" y="-2.54" visible="pad" length="short" direction="pwr" rot="R180"/>
<pin name="4" x="-7.62" y="2.54" visible="pad" length="short" direction="pwr"/>
</symbol>
<symbol name="SOLDER_JUMPER3">
<text x="-7.62" y="9.525" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="6.985" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="6.35" y1="0" x2="8.89" y2="2.54" layer="94"/>
<rectangle x1="11.43" y1="0" x2="13.97" y2="2.54" layer="94"/>
<rectangle x1="1.27" y1="0" x2="3.81" y2="2.54" layer="94"/>
<pin name="1" x="2.54" y="-2.54" length="short" rot="R90"/>
<pin name="2" x="7.62" y="5.08" length="short" rot="R270"/>
<pin name="3" x="12.7" y="-2.54" length="short" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="COLIBRI" prefix="IC">
<description>&lt;B&gt;SODIMM socket&lt;/B&gt; for Toradex Colibri single board with PXA270&lt;P&gt;
200 pins, pitch: 0.6 mm</description>
<gates>
<gate name="AUDIO" symbol="COLIBRI-AUDIO" x="-27.94" y="50.8"/>
<gate name="CORE" symbol="COLIBRI-CORE" x="0" y="-20.32"/>
<gate name="ETH" symbol="COLIBRI-ETH" x="17.78" y="58.42"/>
<gate name="GPIO" symbol="COLIBRI-GPIO" x="139.7" y="22.86"/>
<gate name="LDD" symbol="COLIBRI-LDD" x="88.9" y="45.72"/>
<gate name="PWR" symbol="COLIBRI-PWR" x="60.96" y="-27.94" addlevel="must"/>
<gate name="SERIAL" symbol="COLIBRI-SERIAL" x="180.34" y="38.1"/>
<gate name="TOUCH" symbol="COLIBRI-TOUCH" x="104.14" y="-40.64"/>
<gate name="USB" symbol="COLIBRI-USB" x="177.8" y="-38.1"/>
</gates>
<devices>
<device name="" package="1473005-1">
<connects>
<connect gate="AUDIO" pin="HEADPHONE_GND" pad="13"/>
<connect gate="AUDIO" pin="HEADPHONE_L" pad="15"/>
<connect gate="AUDIO" pin="HEADPHONE_R" pad="17"/>
<connect gate="AUDIO" pin="LINEIN_L" pad="5"/>
<connect gate="AUDIO" pin="LINEIN_R" pad="7"/>
<connect gate="AUDIO" pin="MIC_GND" pad="3"/>
<connect gate="AUDIO" pin="MIC_IN" pad="1"/>
<connect gate="AUDIO" pin="VDDA_AUDIO1" pad="10"/>
<connect gate="AUDIO" pin="VDDA_AUDIO2" pad="12"/>
<connect gate="AUDIO" pin="VSSA_AUDIO1" pad="9"/>
<connect gate="AUDIO" pin="VSSA_AUDIO2" pad="11"/>
<connect gate="CORE" pin="!OE" pad="91"/>
<connect gate="CORE" pin="!RESET_EXT" pad="26"/>
<connect gate="CORE" pin="!WE" pad="89"/>
<connect gate="CORE" pin="AD0" pad="8"/>
<connect gate="CORE" pin="AD1" pad="6"/>
<connect gate="CORE" pin="AD2" pad="4"/>
<connect gate="CORE" pin="AD3" pad="2"/>
<connect gate="CORE" pin="GPIO-B.03/L_PCLK_WR" pad="56"/>
<connect gate="CORE" pin="GPIO-BB.00/MMC_DAT0" pad="192"/>
<connect gate="CORE" pin="GPIO-BB.01/MMC_DAT1" pad="49"/>
<connect gate="CORE" pin="GPIO-BB.02/MMC_DAT2/MMCS0" pad="51"/>
<connect gate="CORE" pin="GPIO-BB.03/MMC_DAT3/MMCS1" pad="53"/>
<connect gate="CORE" pin="GPIO-CC.00/MMC_CLK" pad="47"/>
<connect gate="CORE" pin="GPIO-CC.01/MMC_CMD" pad="190"/>
<connect gate="CORE" pin="GPIO-CC.07/!PCE1" pad="94"/>
<connect gate="CORE" pin="GPIO-I.04/!RESET_OUT" pad="87"/>
<connect gate="CORE" pin="GPIO-I.07/RDY" pad="95"/>
<connect gate="CORE" pin="GPIO-J.01/L_BIAS" pad="44"/>
<connect gate="CORE" pin="GPIO-J.03/L_LCLK_A0" pad="68"/>
<connect gate="CORE" pin="GPIO-J.04/L_FCLK_RD" pad="82"/>
<connect gate="CORE" pin="GPIO-K.02/!EXT_CSO" pad="105"/>
<connect gate="CORE" pin="GPIO-K.03/!EXT_CS1" pad="107"/>
<connect gate="CORE" pin="GPIO-V.01/PRDY" pad="45"/>
<connect gate="CORE" pin="GPIO-X.05/!PXCVREN" pad="100"/>
<connect gate="CORE" pin="GPIO-X.06/!PWAIT" pad="102"/>
<connect gate="CORE" pin="GPIO-X.07/!IOIS16" pad="104"/>
<connect gate="CORE" pin="GPIO-Y.04/!POE" pad="97"/>
<connect gate="CORE" pin="GPIO-Y.06/!PIOR" pad="103"/>
<connect gate="CORE" pin="GPIO-Y.07/!PIOW" pad="101"/>
<connect gate="CORE" pin="GPIO-Z.00/!PCE2" pad="96"/>
<connect gate="CORE" pin="GPIO-Z.01/!PREG" pad="98"/>
<connect gate="CORE" pin="GPIO-Z.03/!PWE" pad="99"/>
<connect gate="CORE" pin="GPIO_K.04/!EXT_CS2" pad="106"/>
<connect gate="CORE" pin="RDWR" pad="93"/>
<connect gate="ETH" pin="!ETH_LINK_ACT" pad="183"/>
<connect gate="ETH" pin="!ETH_SPEED_100" pad="185"/>
<connect gate="ETH" pin="ETH_AGND" pad="191"/>
<connect gate="ETH" pin="ETH_RXI+" pad="195"/>
<connect gate="ETH" pin="ETH_RXI-" pad="193"/>
<connect gate="ETH" pin="ETH_TXO+" pad="189"/>
<connect gate="ETH" pin="ETH_TXO-" pad="187"/>
<connect gate="GPIO" pin="GPIO-A.01" pad="121"/>
<connect gate="GPIO" pin="GPIO-A.02" pad="186"/>
<connect gate="GPIO" pin="GPIO-A.03" pad="184"/>
<connect gate="GPIO" pin="GPIO-A.04" pad="146"/>
<connect gate="GPIO" pin="GPIO-A.05" pad="144"/>
<connect gate="GPIO" pin="GPIO-A.06" pad="30"/>
<connect gate="GPIO" pin="GPIO-A.07" pad="67"/>
<connect gate="GPIO" pin="GPIO-B.02" pad="154"/>
<connect gate="GPIO" pin="GPIO-B.04" pad="59"/>
<connect gate="GPIO" pin="GPIO-B.05" pad="28"/>
<connect gate="GPIO" pin="GPIO-B.06" pad="55"/>
<connect gate="GPIO" pin="GPIO-B.07" pad="63"/>
<connect gate="GPIO" pin="GPIO-BB.04" pad="166"/>
<connect gate="GPIO" pin="GPIO-BB.05" pad="168"/>
<connect gate="GPIO" pin="GPIO-BB.06" pad="170"/>
<connect gate="GPIO" pin="GPIO-BB.07" pad="172"/>
<connect gate="GPIO" pin="GPIO-C.00" pad="119"/>
<connect gate="GPIO" pin="GPIO-C.01" pad="81"/>
<connect gate="GPIO" pin="GPIO-C.07" pad="43"/>
<connect gate="GPIO" pin="GPIO-CC.02" pad="77"/>
<connect gate="GPIO" pin="GPIO-DD.05" pad="69"/>
<connect gate="GPIO" pin="GPIO-DD.06" pad="65"/>
<connect gate="GPIO" pin="GPIO-G.00" pad="149"/>
<connect gate="GPIO" pin="GPIO-G.01" pad="151"/>
<connect gate="GPIO" pin="GPIO-G.02" pad="153"/>
<connect gate="GPIO" pin="GPIO-G.03" pad="155"/>
<connect gate="GPIO" pin="GPIO-G.04" pad="157"/>
<connect gate="GPIO" pin="GPIO-G.05" pad="159"/>
<connect gate="GPIO" pin="GPIO-G.06" pad="161"/>
<connect gate="GPIO" pin="GPIO-G.07" pad="163"/>
<connect gate="GPIO" pin="GPIO-H.00" pad="165"/>
<connect gate="GPIO" pin="GPIO-H.01" pad="167"/>
<connect gate="GPIO" pin="GPIO-H.02" pad="169"/>
<connect gate="GPIO" pin="GPIO-H.03" pad="171"/>
<connect gate="GPIO" pin="GPIO-H.04" pad="173"/>
<connect gate="GPIO" pin="GPIO-H.05" pad="175"/>
<connect gate="GPIO" pin="GPIO-H.06" pad="177"/>
<connect gate="GPIO" pin="GPIO-H.07" pad="179"/>
<connect gate="GPIO" pin="GPIO-I.03" pad="130"/>
<connect gate="GPIO" pin="GPIO-I.06" pad="132"/>
<connect gate="GPIO" pin="GPIO-J.00" pad="126"/>
<connect gate="GPIO" pin="GPIO-J.02" pad="128"/>
<connect gate="GPIO" pin="GPIO-J.05" pad="113"/>
<connect gate="GPIO" pin="GPIO-J.06" pad="111"/>
<connect gate="GPIO" pin="GPIO-K.00" pad="150"/>
<connect gate="GPIO" pin="GPIO-K.01" pad="152"/>
<connect gate="GPIO" pin="GPIO-N.00" pad="174"/>
<connect gate="GPIO" pin="GPIO-N.01" pad="176"/>
<connect gate="GPIO" pin="GPIO-N.02" pad="178"/>
<connect gate="GPIO" pin="GPIO-N.03" pad="180"/>
<connect gate="GPIO" pin="GPIO-N.04" pad="160"/>
<connect gate="GPIO" pin="GPIO-N.05" pad="158"/>
<connect gate="GPIO" pin="GPIO-N.06" pad="162"/>
<connect gate="GPIO" pin="GPIO-P.04" pad="120"/>
<connect gate="GPIO" pin="GPIO-P.05" pad="122"/>
<connect gate="GPIO" pin="GPIO-P.06" pad="124"/>
<connect gate="GPIO" pin="GPIO-P.07" pad="188"/>
<connect gate="GPIO" pin="GPIO-S.00" pad="73"/>
<connect gate="GPIO" pin="GPIO-U.00" pad="123"/>
<connect gate="GPIO" pin="GPIO-U.01" pad="125"/>
<connect gate="GPIO" pin="GPIO-U.02" pad="110"/>
<connect gate="GPIO" pin="GPIO-U.03" pad="112"/>
<connect gate="GPIO" pin="GPIO-U.04" pad="114"/>
<connect gate="GPIO" pin="GPIO-U.05" pad="116"/>
<connect gate="GPIO" pin="GPIO-U.06" pad="118"/>
<connect gate="GPIO" pin="GPIO-V.02" pad="71"/>
<connect gate="GPIO" pin="GPIO-V.03" pad="85"/>
<connect gate="GPIO" pin="GPIO-W.05" pad="75"/>
<connect gate="GPIO" pin="GPIO-W.06" pad="115"/>
<connect gate="GPIO" pin="GPIO-W.07" pad="117"/>
<connect gate="GPIO" pin="GPIO-X.00" pad="142"/>
<connect gate="GPIO" pin="GPIO-X.01" pad="140"/>
<connect gate="GPIO" pin="GPIO-X.02" pad="138"/>
<connect gate="GPIO" pin="GPIO-X.03" pad="136"/>
<connect gate="GPIO" pin="GPIO-X.04" pad="134"/>
<connect gate="GPIO" pin="GPIO-Y.05" pad="79"/>
<connect gate="GPIO" pin="GPIO-Z.02" pad="156"/>
<connect gate="GPIO" pin="GPIO-Z.04" pad="164"/>
<connect gate="LDD" pin="GPIO-E.00/LDD00" pad="76"/>
<connect gate="LDD" pin="GPIO-E.01/LDD01" pad="70"/>
<connect gate="LDD" pin="GPIO-E.02/LDD02" pad="60"/>
<connect gate="LDD" pin="GPIO-E.03/LDD03" pad="58"/>
<connect gate="LDD" pin="GPIO-E.04/LDD04" pad="78"/>
<connect gate="LDD" pin="GPIO-E.05/LDD05" pad="72"/>
<connect gate="LDD" pin="GPIO-E.06/LDD06" pad="80"/>
<connect gate="LDD" pin="GPIO-E.07/LDD07" pad="46"/>
<connect gate="LDD" pin="GPIO-F.00/LDD08" pad="62"/>
<connect gate="LDD" pin="GPIO-F.01/LDD09" pad="48"/>
<connect gate="LDD" pin="GPIO-F.02/LDD10" pad="74"/>
<connect gate="LDD" pin="GPIO-F.03/LDD11" pad="50"/>
<connect gate="LDD" pin="GPIO-F.04/LDD12" pad="52"/>
<connect gate="LDD" pin="GPIO-F.05/LDD13" pad="54"/>
<connect gate="LDD" pin="GPIO-F.06/LDD14" pad="66"/>
<connect gate="LDD" pin="GPIO-F.07/LDD15" pad="64"/>
<connect gate="LDD" pin="GPIO-M.00/LDD16" pad="57"/>
<connect gate="LDD" pin="GPIO-M.01/LDD17" pad="61"/>
<connect gate="PWR" pin="BATT_FAULT" pad="24"/>
<connect gate="PWR" pin="GND" pad="39"/>
<connect gate="PWR" pin="GND@2" pad="109"/>
<connect gate="PWR" pin="GND@3" pad="83"/>
<connect gate="PWR" pin="GND@4" pad="41"/>
<connect gate="PWR" pin="GND@6" pad="147"/>
<connect gate="PWR" pin="GND@7" pad="181"/>
<connect gate="PWR" pin="GND@8" pad="197"/>
<connect gate="PWR" pin="GND@9" pad="199"/>
<connect gate="PWR" pin="VCC_3V3" pad="42"/>
<connect gate="PWR" pin="VCC_3V3@2" pad="84"/>
<connect gate="PWR" pin="VCC_3V3@3" pad="108"/>
<connect gate="PWR" pin="VCC_3V3@4" pad="148"/>
<connect gate="PWR" pin="VCC_3V3@5" pad="182"/>
<connect gate="PWR" pin="VCC_3V3@6" pad="198"/>
<connect gate="PWR" pin="VCC_3V3@7" pad="200"/>
<connect gate="PWR" pin="VCC_3V3_VCCBATT" pad="40"/>
<connect gate="PWR" pin="VDD_FAULT" pad="22"/>
<connect gate="SERIAL" pin="GPIO-B.00/UART_B_RXD" pad="36"/>
<connect gate="SERIAL" pin="GPIO-B.01/UART_B_CTS" pad="32"/>
<connect gate="SERIAL" pin="GPIO-C.02/UART_C_TXD" pad="21"/>
<connect gate="SERIAL" pin="GPIO-C.03/UART_C_RXD" pad="19"/>
<connect gate="SERIAL" pin="GPIO-C.04/I2C_CLK" pad="196"/>
<connect gate="SERIAL" pin="GPIO-C.05/I2C_DATA" pad="194"/>
<connect gate="SERIAL" pin="GPIO-J.07/UART_B_TXD" pad="38"/>
<connect gate="SERIAL" pin="GPIO-K.07/UART_B_RTS" pad="34"/>
<connect gate="SERIAL" pin="GPIO-O.00/UART_A_DTR" pad="23"/>
<connect gate="SERIAL" pin="GPIO-O.01/UART_A_TXD" pad="35"/>
<connect gate="SERIAL" pin="GPIO-O.02/UART_A_RXD" pad="33"/>
<connect gate="SERIAL" pin="GPIO-O.03/UART_A_CTS" pad="25"/>
<connect gate="SERIAL" pin="GPIO-O.04/UART_A_RTS" pad="27"/>
<connect gate="SERIAL" pin="GPIO-O.05/UART_A_RI" pad="37"/>
<connect gate="SERIAL" pin="GPIO-O.06/UART_A_DCD" pad="31"/>
<connect gate="SERIAL" pin="GPIO-O.07/UART_A_DSR" pad="29"/>
<connect gate="SERIAL" pin="GPIO-Y.00/SSPTXD" pad="92"/>
<connect gate="SERIAL" pin="GPIO-Y.01/SSPRXD" pad="90"/>
<connect gate="SERIAL" pin="GPIO-Y.02/SSPCLK" pad="88"/>
<connect gate="SERIAL" pin="GPIO-Y.03/SSPFRM" pad="86"/>
<connect gate="TOUCH" pin="TSMX" pad="16"/>
<connect gate="TOUCH" pin="TSMY" pad="20"/>
<connect gate="TOUCH" pin="TSPX" pad="14"/>
<connect gate="TOUCH" pin="TSPY" pad="18"/>
<connect gate="USB" pin="EXT_IO0/USB_ID" pad="135"/>
<connect gate="USB" pin="GPIO-T.05/EXT_IO1" pad="133"/>
<connect gate="USB" pin="GPIO-T.06/EXT_IO2" pad="127"/>
<connect gate="USB" pin="GPIO-W.02/USBH_PEN" pad="129"/>
<connect gate="USB" pin="GPIO-W.03/USBH_OC" pad="131"/>
<connect gate="USB" pin="USBC_DET" pad="137"/>
<connect gate="USB" pin="USBC_N" pad="145"/>
<connect gate="USB" pin="USBC_P" pad="143"/>
<connect gate="USB" pin="USBH_N" pad="141"/>
<connect gate="USB" pin="USBH_P" pad="139"/>
</connects>
<technologies>
<technology name="">
<attribute name="DODANI" value="VEIT"/>
<attribute name="MELE" value="1276"/>
<attribute name="MPN" value="1473005-1"/>
<attribute name="OSAZENI" value="RACOM"/>
<attribute name="PRISLUSENSTVI" value="VYV 0024"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="BHSD-2032-SM" prefix="BAT">
<description>Držák baterie CR2032; &lt;B&gt;BHSD-2032-SM&lt;/B&gt;&lt;P&gt;

&lt;U&gt;Protikus:&lt;/U&gt;&lt;BR&gt;
MELE1066 - Krytka držáku baterie CR2032;  &lt;B&gt;BHSD-2032-COVER&lt;/B&gt;&lt;P&gt;

&lt;U&gt;Příslušenství:&lt;/U&gt;&lt;BR&gt;
MELE0121 - Baterie knoflíková CR2032 lithiová, Vinnic&lt;B&gt; (542-006)&lt;/B&gt;&lt;P&gt;</description>
<gates>
<gate name="G$1" symbol="BATERIE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="BHSD-2032">
<connects>
<connect gate="G$1" pin="+" pad="PLUS"/>
<connect gate="G$1" pin="-" pad="MINUS"/>
</connects>
<technologies>
<technology name="">
<attribute name="DODANI" value="VEIT"/>
<attribute name="MELE" value="1065"/>
<attribute name="MPN" value="BHSD-2032-SM"/>
<attribute name="OSAZENI" value="RACOM" constant="no"/>
<attribute name="PRISLUSENSTVI" value="MELE1066, MELE0121"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="2041021" prefix="SDCARD">
<description>Držák SD karty; &lt;B&gt;2041021-4&lt;/B&gt;</description>
<gates>
<gate name="G$1" symbol="SDCARD" x="0" y="0"/>
</gates>
<devices>
<device name="" package="2041021">
<connects>
<connect gate="G$1" pin="CD" pad="CD"/>
<connect gate="G$1" pin="CHASSIS" pad="GP GP2 GP3 GP4"/>
<connect gate="G$1" pin="CLK" pad="5"/>
<connect gate="G$1" pin="CMD" pad="2"/>
<connect gate="G$1" pin="DAT0" pad="7"/>
<connect gate="G$1" pin="DAT1" pad="8"/>
<connect gate="G$1" pin="DAT2" pad="9"/>
<connect gate="G$1" pin="DAT3" pad="1"/>
<connect gate="G$1" pin="VDD" pad="4"/>
<connect gate="G$1" pin="VSS1" pad="3"/>
<connect gate="G$1" pin="VSS2" pad="6"/>
<connect gate="G$1" pin="WP" pad="WP"/>
</connects>
<technologies>
<technology name="">
<attribute name="DODANI" value="VEIT"/>
<attribute name="MELE" value="1288"/>
<attribute name="MPN" value="2041021-4"/>
<attribute name="OSAZENI" value="RACOM"/>
<attribute name="PRISLUSENSTVI" value="zaspecifikovat SD kartu" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SOLDER_JUMPER2" prefix="J">
<description>Pájecí propojka</description>
<gates>
<gate name="G$1" symbol="SOLDER_JUMPER2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOLDER_JUMPER2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DODANI" value="none"/>
<attribute name="MELE" value="none"/>
<attribute name="MPN" value="none"/>
<attribute name="OSAZENI" value="none"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ESD_HOLE" prefix="X">
<description>Upevňovací otvor v DPS</description>
<gates>
<gate name="G$1" symbol="DIRA_ESD" x="0" y="0"/>
</gates>
<devices>
<device name="3,2" package="3,2MM_HOLE">
<connects>
<connect gate="G$1" pin="P$8" pad="P$1 P$2 P$3 P$4 P$5 P$6 P$7 P$9 P$10 P$11 P$12 P$13 P$14"/>
</connects>
<technologies>
<technology name="">
<attribute name="DODANI" value="none"/>
<attribute name="MELE" value="none"/>
<attribute name="MPN" value="none"/>
<attribute name="OSAZENI" value="none"/>
<attribute name="PRISLUSENSTVI" value="M3 šroub + distanční sloupek"/>
</technology>
</technologies>
</device>
<device name="3,8" package="3,8MM_HOLE">
<connects>
<connect gate="G$1" pin="P$8" pad="P$1 P$2 P$3 P$4 P$5 P$6 P$7 P$9 P$10 P$11 P$12 P$13 P$14"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="XTAL-SMD" prefix="Q" uservalue="yes">
<description>Krystal 24MHz, 5x3,2mm, SMD</description>
<gates>
<gate name="G$1" symbol="XTAL-GND" x="0" y="0"/>
</gates>
<devices>
<device name="" package="XTALSMD5X3.2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="DODANI" value="VEIT"/>
<attribute name="MELE" value="1308" constant="no"/>
<attribute name="MPN" value="ABM3C-24.000MHZ-D4Y-T"/>
<attribute name="OSAZENI" value="RACOM"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SOLDER_JUMPER3" prefix="J">
<description>Pájecí propojka, 3 kontakty</description>
<gates>
<gate name="G$1" symbol="SOLDER_JUMPER3" x="-7.62" y="-2.54"/>
</gates>
<devices>
<device name="" package="SOLDER_JUMPER3">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="DODANI" value="none"/>
<attribute name="MELE" value="none"/>
<attribute name="MPN" value="none"/>
<attribute name="OSAZENI" value="none"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="_Semiconductors">
<packages>
<package name="SOP-14">
<wire x1="0" y1="22.86" x2="0" y2="25.4" width="0.1524" layer="91"/>
<wire x1="12.7" y1="-15.24" x2="12.7" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="0" y1="-20.32" x2="12.7" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="0" y1="2.54" x2="0" y2="0" width="0.1524" layer="91"/>
<wire x1="0" y1="2.54" x2="7.62" y2="2.54" width="0.1524" layer="91"/>
<wire x1="12.7" y1="-5.08" x2="12.7" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="12.7" y1="10.16" x2="12.7" y2="30.48" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="17.78" x2="-7.62" y2="17.78" width="0.1524" layer="91"/>
<wire x1="0" y1="22.86" x2="0" y2="25.4" width="0.1524" layer="91"/>
<wire x1="12.7" y1="-15.24" x2="12.7" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="0" y1="-20.32" x2="12.7" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="0" y1="2.54" x2="0" y2="0" width="0.1524" layer="91"/>
<wire x1="0" y1="2.54" x2="7.62" y2="2.54" width="0.1524" layer="91"/>
<wire x1="12.7" y1="-5.08" x2="12.7" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="12.7" y1="10.16" x2="12.7" y2="30.48" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="17.78" x2="-7.62" y2="17.78" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="-2.54" x2="5.08" y2="-2.54" width="0.127" layer="21"/>
<wire x1="5.08" y1="-2.54" x2="5.08" y2="2.54" width="0.127" layer="21"/>
<wire x1="5.08" y1="2.54" x2="-5.08" y2="2.54" width="0.127" layer="21"/>
<wire x1="-5.08" y1="2.54" x2="-5.08" y2="-2.54" width="0.127" layer="21"/>
<circle x="-4.1402" y="-1.5367" radius="0.5334" width="0.127" layer="21"/>
<smd name="1" x="-3.81" y="-3.3337" dx="0.635" dy="1.5874" layer="1"/>
<smd name="2" x="-2.54" y="-3.3337" dx="0.635" dy="1.5874" layer="1"/>
<smd name="3" x="-1.27" y="-3.3338" dx="0.635" dy="1.5874" layer="1"/>
<smd name="4" x="0" y="-3.3337" dx="0.635" dy="1.5874" layer="1"/>
<smd name="5" x="1.27" y="-3.3338" dx="0.635" dy="1.5874" layer="1"/>
<smd name="6" x="2.54" y="-3.3338" dx="0.635" dy="1.5874" layer="1"/>
<smd name="7" x="3.81" y="-3.3338" dx="0.635" dy="1.5874" layer="1"/>
<smd name="8" x="3.81" y="3.3337" dx="0.635" dy="1.5874" layer="1"/>
<smd name="9" x="2.54" y="3.3337" dx="0.635" dy="1.5874" layer="1"/>
<smd name="10" x="1.27" y="3.3337" dx="0.635" dy="1.5874" layer="1"/>
<smd name="11" x="0" y="3.3337" dx="0.635" dy="1.5874" layer="1"/>
<smd name="12" x="-1.27" y="3.3337" dx="0.635" dy="1.5874" layer="1"/>
<smd name="13" x="-2.54" y="3.3337" dx="0.635" dy="1.5874" layer="1"/>
<smd name="14" x="-3.81" y="3.3337" dx="0.635" dy="1.5874" layer="1"/>
<text x="-2.794" y="0.254" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.794" y="-1.524" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.9688" y1="2.54" x2="-3.6513" y2="3.81" layer="27"/>
<rectangle x1="-2.6988" y1="2.54" x2="-2.3813" y2="3.81" layer="27"/>
<rectangle x1="-1.4288" y1="2.54" x2="-1.1113" y2="3.81" layer="27"/>
<rectangle x1="-0.1587" y1="2.54" x2="0.1588" y2="3.81" layer="27"/>
<rectangle x1="1.1113" y1="2.54" x2="1.4288" y2="3.81" layer="27"/>
<rectangle x1="2.3813" y1="2.54" x2="2.6988" y2="3.81" layer="27"/>
<rectangle x1="3.6513" y1="2.54" x2="3.9688" y2="3.81" layer="27"/>
<rectangle x1="3.6513" y1="-3.81" x2="3.9688" y2="-2.54" layer="27"/>
<rectangle x1="2.3813" y1="-3.81" x2="2.6988" y2="-2.54" layer="27"/>
<rectangle x1="1.1113" y1="-3.81" x2="1.4288" y2="-2.54" layer="27"/>
<rectangle x1="-0.1587" y1="-3.81" x2="0.1588" y2="-2.54" layer="27"/>
<rectangle x1="-1.4288" y1="-3.81" x2="-1.1113" y2="-2.54" layer="27"/>
<rectangle x1="-2.6988" y1="-3.81" x2="-2.3813" y2="-2.54" layer="27"/>
<rectangle x1="-3.9688" y1="-3.81" x2="-3.6513" y2="-2.54" layer="27"/>
</package>
<package name="SOT-23">
<wire x1="1.4224" y1="-0.508" x2="-1.4732" y2="-0.508" width="0.127" layer="21"/>
<wire x1="-1.4732" y1="-0.508" x2="-1.4732" y2="0.508" width="0.127" layer="21"/>
<wire x1="-1.4732" y1="0.508" x2="1.4224" y2="0.508" width="0.127" layer="21"/>
<wire x1="1.4224" y1="0.508" x2="1.4224" y2="-0.508" width="0.127" layer="21"/>
<smd name="1" x="-0.95" y="-1" dx="0.7" dy="0.8" layer="1" roundness="20" rot="R180"/>
<smd name="2" x="0.95" y="-1" dx="0.7" dy="0.8" layer="1" roundness="20" rot="R180"/>
<smd name="3" x="0" y="1" dx="0.7" dy="0.8" layer="1" roundness="20" rot="R180"/>
<text x="1.556" y="-0.888" size="1.27" layer="27">&gt;VALUE</text>
<text x="1.586" y="0.627" size="1.27" layer="25">&gt;NAME</text>
<rectangle x1="-1.1684" y1="-1.0668" x2="-0.7874" y2="-0.5588" layer="27" rot="R180"/>
<rectangle x1="0.762" y1="-1.0668" x2="1.143" y2="-0.5588" layer="27" rot="R180"/>
<rectangle x1="-0.2032" y1="0.5588" x2="0.1778" y2="1.0668" layer="27" rot="R180"/>
</package>
<package name="DIL08">
<wire x1="-5.715" y1="-0.635" x2="-5.715" y2="-2.794" width="0.127" layer="21"/>
<wire x1="-5.715" y1="-0.635" x2="-5.715" y2="0.635" width="0.127" layer="21" curve="180"/>
<wire x1="5.715" y1="-2.794" x2="5.715" y2="2.794" width="0.127" layer="21"/>
<wire x1="-5.715" y1="2.794" x2="-5.715" y2="0.635" width="0.127" layer="21"/>
<wire x1="-5.715" y1="2.794" x2="5.715" y2="2.794" width="0.127" layer="21"/>
<wire x1="-5.715" y1="-2.794" x2="5.715" y2="-2.794" width="0.127" layer="21"/>
<pad name="1" x="-3.81" y="-3.81" drill="0.8128" diameter="1.6002"/>
<pad name="2" x="-1.27" y="-3.81" drill="0.8128" diameter="1.6002" shape="octagon"/>
<pad name="3" x="1.27" y="-3.81" drill="0.8128" diameter="1.6002" shape="octagon"/>
<pad name="4" x="3.81" y="-3.81" drill="0.8128" diameter="1.6002" shape="octagon"/>
<pad name="5" x="3.81" y="3.81" drill="0.8128" diameter="1.6002" shape="octagon"/>
<pad name="6" x="1.27" y="3.81" drill="0.8128" diameter="1.6002" shape="octagon"/>
<pad name="7" x="-1.27" y="3.81" drill="0.8128" diameter="1.6002" shape="octagon"/>
<pad name="8" x="-3.81" y="3.81" drill="0.8128" diameter="1.6002" shape="octagon"/>
<text x="-2.54" y="0.508" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-1.905" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-4.1402" y="-2.2098" size="1.27" layer="21" ratio="10">1</text>
<text x="-4.445" y="1.27" size="1.27" layer="21" ratio="10">8</text>
</package>
<package name="SO-08">
<wire x1="-2.3622" y1="-1.9304" x2="2.3622" y2="-1.9304" width="0.127" layer="21"/>
<wire x1="2.3622" y1="-1.9304" x2="2.3622" y2="1.9304" width="0.127" layer="21"/>
<wire x1="2.3622" y1="1.9304" x2="-2.3622" y2="1.9304" width="0.127" layer="21"/>
<wire x1="-2.3622" y1="1.9304" x2="-2.3622" y2="-1.9304" width="0.127" layer="21"/>
<circle x="-1.8034" y="-1.3716" radius="0.3556" width="0" layer="21"/>
<smd name="1" x="-1.905" y="-2.7432" dx="0.6096" dy="1.524" layer="1"/>
<smd name="8" x="-1.905" y="2.7432" dx="0.6096" dy="1.524" layer="1"/>
<smd name="2" x="-0.635" y="-2.7432" dx="0.6096" dy="1.524" layer="1"/>
<smd name="3" x="0.635" y="-2.7432" dx="0.6096" dy="1.524" layer="1"/>
<smd name="7" x="-0.635" y="2.7432" dx="0.6096" dy="1.524" layer="1"/>
<smd name="6" x="0.635" y="2.7432" dx="0.6096" dy="1.524" layer="1"/>
<smd name="4" x="1.905" y="-2.7432" dx="0.6096" dy="1.524" layer="1"/>
<smd name="5" x="1.905" y="2.7432" dx="0.6096" dy="1.524" layer="1"/>
<text x="-1.27" y="-1.524" size="1.27" layer="27">&gt;VALUE</text>
<text x="-1.27" y="0.254" size="1.27" layer="25">&gt;NAME</text>
<rectangle x1="-2.0828" y1="-2.9972" x2="-1.7272" y2="-1.9812" layer="27"/>
<rectangle x1="-0.8128" y1="-2.9972" x2="-0.4572" y2="-1.9812" layer="27"/>
<rectangle x1="0.4572" y1="-2.9972" x2="0.8128" y2="-1.9812" layer="27"/>
<rectangle x1="1.7272" y1="-2.9972" x2="2.0828" y2="-1.9812" layer="27"/>
<rectangle x1="-2.0828" y1="1.9812" x2="-1.7272" y2="2.9972" layer="27"/>
<rectangle x1="-0.8128" y1="1.9812" x2="-0.4572" y2="2.9972" layer="27"/>
<rectangle x1="0.4572" y1="1.9812" x2="0.8128" y2="2.9972" layer="27"/>
<rectangle x1="1.7272" y1="1.9812" x2="2.0828" y2="2.9972" layer="27"/>
</package>
<package name="DBV">
<wire x1="-1.5" y1="-0.85" x2="1.5" y2="-0.85" width="0.127" layer="21"/>
<wire x1="1.5" y1="-0.85" x2="1.5" y2="0.85" width="0.127" layer="21"/>
<wire x1="1.5" y1="0.85" x2="-1.5" y2="0.85" width="0.127" layer="21"/>
<wire x1="-1.5" y1="0.85" x2="-1.5" y2="-0.85" width="0.127" layer="21"/>
<circle x="-1.3" y="-0.6" radius="0.05" width="0.127" layer="21"/>
<smd name="1" x="-0.95" y="-1.35" dx="0.55" dy="1" layer="1" roundness="10"/>
<smd name="2" x="0" y="-1.35" dx="0.55" dy="1" layer="1" roundness="10"/>
<smd name="3" x="0.95" y="-1.35" dx="0.55" dy="1" layer="1" roundness="10"/>
<smd name="4" x="0.95" y="1.35" dx="0.55" dy="1" layer="1" roundness="10"/>
<smd name="5" x="0" y="1.35" dx="0.55" dy="1" layer="1" roundness="10"/>
<smd name="6" x="-0.95" y="1.35" dx="0.55" dy="1" layer="1" roundness="10"/>
<text x="-2.29" y="2.45" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.99" y="-3.64" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.1" y1="-1.4" x2="-0.8" y2="-0.9" layer="21"/>
<rectangle x1="-0.15" y1="-1.4" x2="0.15" y2="-0.9" layer="21"/>
<rectangle x1="0.8" y1="-1.4" x2="1.1" y2="-0.9" layer="21"/>
<rectangle x1="0.8" y1="0.9" x2="1.1" y2="1.4" layer="21"/>
<rectangle x1="-0.15" y1="0.9" x2="0.15" y2="1.4" layer="21"/>
<rectangle x1="-1.1" y1="0.9" x2="-0.8" y2="1.4" layer="21"/>
</package>
<package name="USB2514B">
<smd name="PAD" x="0" y="0" dx="3.7" dy="3.7" layer="1"/>
<smd name="14" x="2.85" y="0" dx="0.28" dy="0.9" layer="1" roundness="100" rot="R90"/>
<smd name="5" x="0" y="-2.85" dx="0.28" dy="0.9" layer="1" roundness="100" rot="R180"/>
<smd name="23" x="0" y="2.85" dx="0.28" dy="0.9" layer="1" roundness="100" rot="R180"/>
<smd name="32" x="-2.85" y="0" dx="0.28" dy="0.9" layer="1" roundness="100" rot="R90"/>
<smd name="4" x="-0.5" y="-2.85" dx="0.28" dy="0.9" layer="1" roundness="100" rot="R180"/>
<smd name="3" x="-1" y="-2.85" dx="0.28" dy="0.9" layer="1" roundness="100" rot="R180"/>
<smd name="2" x="-1.5" y="-2.85" dx="0.28" dy="0.9" layer="1" roundness="100" rot="R180"/>
<smd name="15" x="2.85" y="0.5" dx="0.28" dy="0.9" layer="1" roundness="100" rot="R90"/>
<smd name="17" x="2.85" y="1.5" dx="0.28" dy="0.9" layer="1" roundness="100" rot="R90"/>
<smd name="16" x="2.85" y="1" dx="0.28" dy="0.9" layer="1" roundness="100" rot="R90"/>
<smd name="18" x="2.85" y="2" dx="0.28" dy="0.9" layer="1" roundness="100" rot="R90"/>
<smd name="13" x="2.85" y="-0.5" dx="0.28" dy="0.9" layer="1" roundness="100" rot="R90"/>
<smd name="12" x="2.85" y="-1" dx="0.28" dy="0.9" layer="1" roundness="100" rot="R90"/>
<smd name="11" x="2.85" y="-1.5" dx="0.28" dy="0.9" layer="1" roundness="100" rot="R90"/>
<smd name="10" x="2.85" y="-2" dx="0.28" dy="0.9" layer="1" roundness="100" rot="R90"/>
<smd name="1" x="-2" y="-2.85" dx="0.28" dy="0.9" layer="1" roundness="100" rot="R180"/>
<smd name="6" x="0.5" y="-2.85" dx="0.28" dy="0.9" layer="1" roundness="100" rot="R180"/>
<smd name="7" x="1" y="-2.85" dx="0.28" dy="0.9" layer="1" roundness="100" rot="R180"/>
<smd name="8" x="1.5" y="-2.85" dx="0.28" dy="0.9" layer="1" roundness="100" rot="R180"/>
<smd name="9" x="2" y="-2.85" dx="0.28" dy="0.9" layer="1" roundness="100" rot="R180"/>
<smd name="33" x="-2.85" y="-0.5" dx="0.28" dy="0.9" layer="1" roundness="100" rot="R90"/>
<smd name="34" x="-2.85" y="-1" dx="0.28" dy="0.9" layer="1" roundness="100" rot="R90"/>
<smd name="35" x="-2.85" y="-1.5" dx="0.28" dy="0.9" layer="1" roundness="100" rot="R90"/>
<smd name="36" x="-2.85" y="-2" dx="0.28" dy="0.9" layer="1" roundness="100" rot="R90"/>
<smd name="31" x="-2.85" y="0.5" dx="0.28" dy="0.9" layer="1" roundness="100" rot="R90"/>
<smd name="30" x="-2.85" y="1" dx="0.28" dy="0.9" layer="1" roundness="100" rot="R90"/>
<smd name="29" x="-2.85" y="1.5" dx="0.28" dy="0.9" layer="1" roundness="100" rot="R90"/>
<smd name="28" x="-2.85" y="2" dx="0.28" dy="0.9" layer="1" roundness="100" rot="R90"/>
<smd name="24" x="-0.5" y="2.85" dx="0.28" dy="0.9" layer="1" roundness="100" rot="R180"/>
<smd name="25" x="-1" y="2.85" dx="0.28" dy="0.9" layer="1" roundness="100" rot="R180"/>
<smd name="26" x="-1.5" y="2.85" dx="0.28" dy="0.9" layer="1" roundness="100" rot="R180"/>
<smd name="27" x="-2" y="2.85" dx="0.28" dy="0.9" layer="1" roundness="100" rot="R180"/>
<smd name="22" x="0.5" y="2.85" dx="0.28" dy="0.9" layer="1" roundness="100" rot="R180"/>
<smd name="21" x="1" y="2.85" dx="0.28" dy="0.9" layer="1" roundness="100" rot="R180"/>
<smd name="20" x="1.5" y="2.85" dx="0.28" dy="0.9" layer="1" roundness="100" rot="R180"/>
<smd name="19" x="2" y="2.85" dx="0.28" dy="0.9" layer="1" roundness="100" rot="R180"/>
<wire x1="-3.05" y1="3.05" x2="-3.05" y2="-3.05" width="0.127" layer="21"/>
<wire x1="-3.05" y1="-3.05" x2="3.05" y2="-3.05" width="0.127" layer="21"/>
<wire x1="3.05" y1="-3.05" x2="3.05" y2="3.05" width="0.127" layer="21"/>
<wire x1="3.05" y1="3.05" x2="-3.05" y2="3.05" width="0.127" layer="21"/>
<circle x="-2.4" y="-2.4" radius="0.412309375" width="0" layer="21"/>
<text x="-3.5" y="5.7" size="1.27" layer="25">&gt;NAME</text>
<text x="-3" y="4.4" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="TPS51120">
<smd name="PAD" x="0" y="0" dx="3.45" dy="3.45" layer="1"/>
<smd name="13" x="2.475" y="0.25" dx="0.28" dy="0.85" layer="1" roundness="100" rot="R90"/>
<smd name="5" x="0.25" y="-2.475" dx="0.28" dy="0.85" layer="1" roundness="100" rot="R180"/>
<smd name="21" x="-0.25" y="2.475" dx="0.28" dy="0.85" layer="1" roundness="100" rot="R180"/>
<smd name="29" x="-2.475" y="-0.25" dx="0.28" dy="0.85" layer="1" roundness="100" rot="R90"/>
<smd name="4" x="-0.25" y="-2.475" dx="0.28" dy="0.85" layer="1" roundness="100" rot="R180"/>
<smd name="3" x="-0.75" y="-2.475" dx="0.28" dy="0.85" layer="1" roundness="100" rot="R180"/>
<smd name="2" x="-1.25" y="-2.475" dx="0.28" dy="0.85" layer="1" roundness="100" rot="R180"/>
<smd name="14" x="2.475" y="0.75" dx="0.28" dy="0.85" layer="1" roundness="100" rot="R90"/>
<smd name="16" x="2.475" y="1.75" dx="0.28" dy="0.85" layer="1" roundness="100" rot="R90"/>
<smd name="15" x="2.475" y="1.25" dx="0.28" dy="0.85" layer="1" roundness="100" rot="R90"/>
<smd name="12" x="2.475" y="-0.25" dx="0.28" dy="0.85" layer="1" roundness="100" rot="R90"/>
<smd name="11" x="2.475" y="-0.75" dx="0.28" dy="0.85" layer="1" roundness="100" rot="R90"/>
<smd name="10" x="2.475" y="-1.25" dx="0.28" dy="0.85" layer="1" roundness="100" rot="R90"/>
<smd name="9" x="2.475" y="-1.75" dx="0.28" dy="0.85" layer="1" roundness="100" rot="R90"/>
<smd name="1" x="-1.75" y="-2.475" dx="0.28" dy="0.85" layer="1" roundness="100" rot="R180"/>
<smd name="6" x="0.75" y="-2.475" dx="0.28" dy="0.85" layer="1" roundness="100" rot="R180"/>
<smd name="7" x="1.25" y="-2.475" dx="0.28" dy="0.85" layer="1" roundness="100" rot="R180"/>
<smd name="8" x="1.75" y="-2.475" dx="0.28" dy="0.85" layer="1" roundness="100" rot="R180"/>
<smd name="30" x="-2.475" y="-0.75" dx="0.28" dy="0.85" layer="1" roundness="100" rot="R90"/>
<smd name="31" x="-2.475" y="-1.25" dx="0.28" dy="0.85" layer="1" roundness="100" rot="R90"/>
<smd name="32" x="-2.475" y="-1.75" dx="0.28" dy="0.85" layer="1" roundness="100" rot="R90"/>
<smd name="28" x="-2.475" y="0.25" dx="0.28" dy="0.85" layer="1" roundness="100" rot="R90"/>
<smd name="27" x="-2.475" y="0.75" dx="0.28" dy="0.85" layer="1" roundness="100" rot="R90"/>
<smd name="26" x="-2.475" y="1.25" dx="0.28" dy="0.85" layer="1" roundness="100" rot="R90"/>
<smd name="25" x="-2.475" y="1.75" dx="0.28" dy="0.85" layer="1" roundness="100" rot="R90"/>
<smd name="22" x="-0.75" y="2.475" dx="0.28" dy="0.85" layer="1" roundness="100" rot="R180"/>
<smd name="23" x="-1.25" y="2.475" dx="0.28" dy="0.85" layer="1" roundness="100" rot="R180"/>
<smd name="24" x="-1.75" y="2.475" dx="0.28" dy="0.85" layer="1" roundness="100" rot="R180"/>
<smd name="20" x="0.25" y="2.475" dx="0.28" dy="0.85" layer="1" roundness="100" rot="R180"/>
<smd name="19" x="0.75" y="2.475" dx="0.28" dy="0.85" layer="1" roundness="100" rot="R180"/>
<smd name="18" x="1.25" y="2.475" dx="0.28" dy="0.85" layer="1" roundness="100" rot="R180"/>
<smd name="17" x="1.75" y="2.475" dx="0.28" dy="0.85" layer="1" roundness="100" rot="R180"/>
<wire x1="-2.5" y1="2.5" x2="-2.5" y2="-2.5" width="0.127" layer="21"/>
<wire x1="-2.5" y1="-2.5" x2="2.5" y2="-2.5" width="0.127" layer="21"/>
<wire x1="2.5" y1="-2.5" x2="2.5" y2="2.5" width="0.127" layer="21"/>
<wire x1="2.5" y1="2.5" x2="-2.5" y2="2.5" width="0.127" layer="21"/>
<circle x="-1.9" y="-1.9" radius="0.412309375" width="0" layer="21"/>
<text x="-3" y="4.825" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.75" y="3.4" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="1206LED">
<wire x1="-1.1811" y1="0.635" x2="-0.9525" y2="0.635" width="0.127" layer="21"/>
<wire x1="-0.9525" y1="0.635" x2="0.9525" y2="0.635" width="0.127" layer="21"/>
<wire x1="0.9525" y1="0.635" x2="1.1811" y2="0.635" width="0.127" layer="21"/>
<wire x1="-1.1811" y1="-0.635" x2="-0.9525" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-0.9525" y1="-0.635" x2="0.9525" y2="-0.635" width="0.127" layer="21"/>
<wire x1="0.9525" y1="-0.635" x2="1.1811" y2="-0.635" width="0.127" layer="21"/>
<wire x1="1.1811" y1="0.635" x2="1.1811" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-1.1811" y1="0.635" x2="-1.1811" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-0.9525" y1="0.635" x2="-1.27" y2="0.635" width="0.254" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.254" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.9525" y2="-0.635" width="0.254" layer="21"/>
<wire x1="0.9525" y1="0.635" x2="1.27" y2="0.635" width="0.254" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.254" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.9525" y2="-0.635" width="0.254" layer="21"/>
<smd name="2" x="1.4732" y="0" dx="1.143" dy="1.7018" layer="1"/>
<smd name="1" x="-1.4732" y="0" dx="1.143" dy="1.7018" layer="1"/>
<text x="-1.397" y="1.27" size="1.27" layer="27">&gt;VALUE</text>
<text x="-1.397" y="3.175" size="1.27" layer="25">&gt;NAME</text>
<rectangle x1="0.3175" y1="-0.635" x2="0.9525" y2="0.635" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="RX-8025S">
<wire x1="-7.62" y1="-5.08" x2="-7.62" y2="10.16" width="0.254" layer="94"/>
<wire x1="-7.62" y1="10.16" x2="10.16" y2="10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="10.16" x2="10.16" y2="-5.08" width="0.254" layer="94"/>
<wire x1="10.16" y1="-5.08" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<text x="-5.08" y="12.7" size="2.54" layer="95">&gt;NAME</text>
<text x="8.89" y="-6.35" size="2.54" layer="96" rot="R180">&gt;VALUE</text>
<pin name="SCL" x="-10.16" y="7.62" length="short" direction="in"/>
<pin name="FOUT" x="-10.16" y="5.08" length="short" direction="out"/>
<pin name="TEST" x="-10.16" y="2.54" length="short" direction="nc"/>
<pin name="VDD" x="-10.16" y="0" length="short" direction="pwr"/>
<pin name="FOE" x="-10.16" y="-2.54" length="short" direction="in"/>
<pin name="SDA" x="12.7" y="7.62" length="short" rot="R180"/>
<pin name="INTB" x="12.7" y="5.08" length="short" direction="out" rot="R180"/>
<pin name="INTA" x="12.7" y="0" length="short" direction="out" rot="R180"/>
<pin name="GND" x="12.7" y="2.54" length="short" direction="pwr" rot="R180"/>
</symbol>
<symbol name="DIODE-SCHOTTKY_CC">
<wire x1="-1.27" y1="-2.54" x2="-2.54" y2="0" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-3.81" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-3.81" y1="0" x2="-2.54" y2="0" width="0.254" layer="94"/>
<wire x1="-3.81" y1="-2.54" x2="-2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="3.81" y1="-2.54" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="3.81" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="3.81" y2="0" width="0.254" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="2.54" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-3.81" y1="0" x2="-3.81" y2="0.762" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-0.762" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="0.762" width="0.254" layer="94"/>
<wire x1="3.81" y1="-0.762" x2="3.81" y2="0" width="0.254" layer="94"/>
<circle x="0" y="2.54" radius="0.127" width="0.4064" layer="94"/>
<text x="5.8674" y="-2.54" size="1.524" layer="95" rot="R90">&gt;NAME</text>
<text x="-4.6736" y="-2.54" size="1.524" layer="96" rot="R90">&gt;VALUE</text>
<pin name="A1" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="A2" x="2.54" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="K" x="0" y="5.08" visible="pad" length="short" direction="pas" rot="R270"/>
</symbol>
<symbol name="LT1785">
<wire x1="10.16" y1="-10.16" x2="-10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-10.16" y1="10.16" x2="-10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-10.16" y1="10.16" x2="10.16" y2="10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="-10.16" x2="10.16" y2="10.16" width="0.254" layer="94"/>
<text x="-5.08" y="15.24" size="2.54" layer="95">&gt;NAME</text>
<text x="-6.35" y="11.43" size="2.54" layer="96">&gt;VALUE</text>
<pin name="RO" x="-12.7" y="7.62" length="short" direction="out"/>
<pin name="!RE" x="-12.7" y="2.54" length="short"/>
<pin name="DE" x="-12.7" y="-2.54" length="short"/>
<pin name="DI" x="-12.7" y="-7.62" length="short" direction="in"/>
<pin name="GND" x="12.7" y="-7.62" length="short" direction="pwr" rot="R180"/>
<pin name="A" x="12.7" y="-2.54" length="short" rot="R180"/>
<pin name="B" x="12.7" y="2.54" length="short" rot="R180"/>
<pin name="VCC" x="12.7" y="7.62" length="short" direction="pwr" rot="R180"/>
</symbol>
<symbol name="TPD2S017">
<wire x1="12.7" y1="-7.62" x2="-10.16" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-10.16" y1="7.62" x2="-10.16" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-10.16" y1="7.62" x2="12.7" y2="7.62" width="0.254" layer="94"/>
<wire x1="12.7" y1="-7.62" x2="12.7" y2="7.62" width="0.254" layer="94"/>
<circle x="-8.5725" y="6.35" radius="0.7099" width="0.254" layer="94"/>
<text x="-4.445" y="11.43" size="2.54" layer="95">&gt;NAME</text>
<text x="-5.715" y="-14.2875" size="2.54" layer="96">&gt;VALUE</text>
<pin name="GND" x="-12.7" y="0" length="short" direction="pwr"/>
<pin name="CH1IN" x="-12.7" y="-5.08" length="short" direction="in"/>
<pin name="CH2IN" x="15.24" y="-5.08" length="short" direction="in" rot="R180"/>
<pin name="VCC" x="15.24" y="0" length="short" direction="pwr" rot="R180"/>
<pin name="CH1OUT" x="-12.7" y="5.08" length="short" direction="out"/>
<pin name="CH2OUT" x="15.24" y="5.08" length="short" direction="out" rot="R180"/>
</symbol>
<symbol name="USB2514">
<pin name="USBDM_DN1" x="15.24" y="17.78" length="short" rot="R180"/>
<pin name="USBDP_DN1" x="15.24" y="20.32" length="short" rot="R180"/>
<pin name="VDDA33@1" x="-17.78" y="30.48" length="short" direction="pwr"/>
<pin name="TEST" x="-17.78" y="-25.4" length="short"/>
<pin name="PRTPWR1" x="15.24" y="15.24" length="short" rot="R180"/>
<pin name="OCS_N1" x="15.24" y="12.7" length="short" direction="in" rot="R180"/>
<pin name="CRFILT" x="-17.78" y="-20.32" length="short"/>
<pin name="VDD33@2" x="15.24" y="30.48" length="short" direction="pwr" rot="R180"/>
<pin name="PRTPWR2" x="15.24" y="0" length="short" rot="R180"/>
<pin name="OCS_N2" x="15.24" y="-2.54" length="short" direction="in" rot="R180"/>
<pin name="PRTPWR3" x="15.24" y="-15.24" length="short" direction="out" rot="R180"/>
<pin name="OCS_N3" x="15.24" y="-17.78" length="short" direction="in" rot="R180"/>
<pin name="PRTPWR4" x="15.24" y="-30.48" length="short" direction="out" rot="R180"/>
<pin name="OCS_N4" x="15.24" y="-33.02" length="short" direction="in" rot="R180"/>
<pin name="SDA" x="-17.78" y="12.7" length="short"/>
<pin name="VDD33@3" x="15.24" y="33.02" length="short" direction="pwr" rot="R180"/>
<pin name="SCL" x="-17.78" y="10.16" length="short"/>
<pin name="HS_IND" x="-17.78" y="7.62" length="short"/>
<pin name="!RESET" x="-17.78" y="2.54" length="short"/>
<pin name="VBUS_DET" x="-17.78" y="22.86" length="short"/>
<pin name="SUSP_IND" x="-17.78" y="-12.7" length="short"/>
<pin name="VDDA33@2" x="-17.78" y="27.94" length="short" direction="pwr"/>
<pin name="USBDM_UP" x="-17.78" y="17.78" length="short"/>
<pin name="USBDP_UP" x="-17.78" y="20.32" length="short"/>
<pin name="XTALOUT" x="-17.78" y="-10.16" length="short" direction="out"/>
<pin name="XTALIN" x="-17.78" y="0" length="short" direction="out"/>
<pin name="PLLFILT" x="-17.78" y="-22.86" length="short" direction="out"/>
<pin name="RBIAS" x="-17.78" y="-15.24" length="short" direction="out"/>
<pin name="VDDA33@3" x="-17.78" y="25.4" length="short" direction="pwr"/>
<pin name="USBDM_DN2" x="15.24" y="2.54" length="short" rot="R180"/>
<pin name="USBDP_DN2" x="15.24" y="5.08" length="short" rot="R180"/>
<pin name="USBDM_DN3" x="15.24" y="-12.7" length="short" rot="R180"/>
<pin name="USBDP_DN3" x="15.24" y="-10.16" length="short" rot="R180"/>
<pin name="USBDM_DN4" x="15.24" y="-27.94" length="short" rot="R180"/>
<pin name="USBDP_DN4" x="15.24" y="-25.4" length="short" rot="R180"/>
<pin name="VDDA33" x="-17.78" y="33.02" length="short" direction="pwr"/>
<pin name="VSS" x="-17.78" y="-33.02" length="short" direction="pwr"/>
<wire x1="-15.24" y1="35.56" x2="12.7" y2="35.56" width="0.254" layer="94"/>
<wire x1="12.7" y1="35.56" x2="12.7" y2="-35.56" width="0.254" layer="94"/>
<wire x1="12.7" y1="-35.56" x2="-15.24" y2="-35.56" width="0.254" layer="94"/>
<wire x1="-15.24" y1="-35.56" x2="-15.24" y2="35.56" width="0.254" layer="94"/>
<text x="-10.16" y="39.497" size="2.54" layer="95">&gt;NAME</text>
<text x="-7.62" y="36.703" size="2.54" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="TPS51120_DRV">
<pin name="EN3" x="-12.7" y="2.54" length="short" direction="in"/>
<pin name="EN5" x="-12.7" y="7.62" length="short" direction="in"/>
<pin name="VIN" x="-12.7" y="15.24" length="short" direction="pwr"/>
<pin name="TONSEL" x="-12.7" y="-2.54" length="short" direction="in"/>
<pin name="SKIPSEL" x="-12.7" y="-7.62" length="short" direction="in"/>
<pin name="GND" x="-12.7" y="-12.7" length="short" direction="pwr"/>
<pin name="PAD" x="-12.7" y="-15.24" length="short" direction="pwr"/>
<pin name="VREF2" x="12.7" y="-15.24" length="short" direction="out" rot="R180"/>
<pin name="VREG3" x="12.7" y="-5.08" length="short" direction="out" rot="R180"/>
<pin name="V5FILT" x="12.7" y="5.08" length="short" direction="in" rot="R180"/>
<pin name="VREG5" x="12.7" y="15.24" length="short" direction="out" rot="R180"/>
<wire x1="-10.16" y1="17.78" x2="-10.16" y2="-17.78" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-17.78" x2="10.16" y2="-17.78" width="0.254" layer="94"/>
<wire x1="10.16" y1="-17.78" x2="10.16" y2="-15.24" width="0.254" layer="94"/>
<wire x1="10.16" y1="-15.24" x2="10.16" y2="17.78" width="0.254" layer="94"/>
<wire x1="10.16" y1="17.78" x2="-10.16" y2="17.78" width="0.254" layer="94"/>
<text x="-5.08" y="22.86" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="20.32" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="TPS51120_SW">
<wire x1="-10.16" y1="17.78" x2="-10.16" y2="-17.78" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-17.78" x2="10.16" y2="-17.78" width="0.254" layer="94"/>
<wire x1="10.16" y1="-17.78" x2="10.16" y2="-15.24" width="0.254" layer="94"/>
<wire x1="10.16" y1="-15.24" x2="10.16" y2="17.78" width="0.254" layer="94"/>
<wire x1="10.16" y1="17.78" x2="-10.16" y2="17.78" width="0.254" layer="94"/>
<text x="-5.08" y="22.86" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="20.32" size="1.778" layer="96">&gt;VALUE</text>
<pin name="EN" x="-12.7" y="15.24" length="short" direction="in"/>
<pin name="VFB" x="-12.7" y="7.62" length="short" direction="in"/>
<pin name="PGOOD" x="-12.7" y="0" length="short" direction="oc"/>
<pin name="COMP" x="-12.7" y="-7.62" length="short" direction="out"/>
<pin name="VO" x="12.7" y="15.24" length="short" direction="in" rot="R180"/>
<pin name="DRVH" x="12.7" y="10.16" length="short" direction="out" rot="R180"/>
<pin name="VBST" x="12.7" y="5.08" length="short" direction="in" rot="R180"/>
<pin name="LL" x="12.7" y="0" length="short" rot="R180"/>
<pin name="PGND" x="12.7" y="-15.24" length="short" direction="pwr" rot="R180"/>
<pin name="CS" x="12.7" y="-10.16" length="short" direction="in" rot="R180"/>
<pin name="DRVL" x="12.7" y="-5.08" length="short" direction="out" rot="R180"/>
</symbol>
<symbol name="MOSFET_N">
<wire x1="-1.1176" y1="2.413" x2="-1.1176" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-1.1176" y1="-2.54" x2="-2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="1.905" x2="0.5334" y2="1.905" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="1.905" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0.508" y1="-1.905" x2="2.413" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="2.486" y1="0" x2="0.5" y2="0" width="0.1524" layer="94"/>
<wire x1="0.5" y1="0" x2="0.254" y2="0" width="0.1524" layer="94"/>
<wire x1="0.5" y1="0" x2="1.6" y2="0.6" width="0.1524" layer="94"/>
<wire x1="1.6" y1="0.6" x2="1.6" y2="-0.6" width="0.1524" layer="94"/>
<wire x1="1.6" y1="-0.6" x2="0.5" y2="0" width="0.1524" layer="94"/>
<wire x1="3.1" y1="0.6" x2="4.8" y2="0.6" width="0.254" layer="94"/>
<wire x1="4" y1="0.5" x2="3.2" y2="-0.5" width="0.254" layer="94"/>
<wire x1="3.2" y1="-0.5" x2="4" y2="-0.5" width="0.254" layer="94"/>
<wire x1="4" y1="-0.5" x2="4.8" y2="-0.5" width="0.254" layer="94"/>
<wire x1="4.8" y1="-0.5" x2="4" y2="0.5" width="0.254" layer="94"/>
<wire x1="4" y1="0.4" x2="4" y2="0.5" width="0.1524" layer="94"/>
<wire x1="4" y1="0.5" x2="4" y2="1.9" width="0.1524" layer="94"/>
<wire x1="4" y1="1.9" x2="2.5" y2="1.9" width="0.1524" layer="94"/>
<wire x1="4" y1="-0.5" x2="4" y2="-1.9" width="0.1524" layer="94"/>
<wire x1="4" y1="-1.9" x2="2.5" y2="-1.9" width="0.1524" layer="94"/>
<circle x="2.5" y="1.9" radius="0.1" width="0.254" layer="94"/>
<circle x="2.5" y="-1.9" radius="0.1" width="0.254" layer="94"/>
<text x="7.62" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="7.62" y="0" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-2.54" x2="0.508" y2="2.54" layer="94"/>
<pin name="G" x="-5.08" y="-2.54" visible="pad" length="short" direction="pas"/>
<pin name="D" x="2.54" y="5.08" visible="pad" length="short" direction="pas" rot="R270"/>
<pin name="S" x="2.54" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
</symbol>
<symbol name="LT1910">
<wire x1="-7.62" y1="-5.08" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="7.62" y2="10.16" width="0.254" layer="94"/>
<wire x1="7.62" y1="10.16" x2="-7.62" y2="10.16" width="0.254" layer="94"/>
<wire x1="-7.62" y1="10.16" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<text x="-7.62" y="11.557" size="2.54" layer="95">&gt;NAME</text>
<text x="2.54" y="-9.017" size="2.54" layer="96">&gt;VALUE</text>
<pin name="TIM" x="-10.16" y="-2.54" length="short" direction="in"/>
<pin name="G" x="10.16" y="-2.54" length="short" direction="out" rot="R180"/>
<pin name="IN" x="-10.16" y="2.54" length="short" direction="in"/>
<pin name="!FLT" x="-10.16" y="7.62" length="short" direction="out"/>
<pin name="SNS" x="10.16" y="2.54" length="short" direction="in" rot="R180"/>
<pin name="V+" x="10.16" y="7.62" length="short" direction="pwr" rot="R180"/>
<pin name="GND" x="0" y="-7.62" length="short" direction="pwr" rot="R90"/>
</symbol>
<symbol name="LEDSMD">
<wire x1="-2.54" y1="-1.905" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-2.54" y2="1.905" width="0.254" layer="94"/>
<wire x1="-2.54" y1="1.905" x2="-2.54" y2="-1.905" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="1.3716" y2="0" width="0.1524" layer="94"/>
<wire x1="1.397" y1="1.905" x2="1.397" y2="-1.905" width="0.254" layer="94"/>
<wire x1="1.905" y1="0.3175" x2="3.175" y2="1.5875" width="0.127" layer="94"/>
<wire x1="3.175" y1="1.5875" x2="2.54" y2="1.5875" width="0.127" layer="94"/>
<wire x1="3.175" y1="1.5875" x2="3.175" y2="0.9525" width="0.127" layer="94"/>
<wire x1="1.905" y1="1.5875" x2="3.175" y2="2.8575" width="0.127" layer="94"/>
<wire x1="3.175" y1="2.8575" x2="3.175" y2="2.2225" width="0.127" layer="94"/>
<wire x1="3.175" y1="2.8575" x2="2.54" y2="2.8575" width="0.127" layer="94"/>
<text x="-7.3914" y="2.6416" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.5654" y="-4.4958" size="1.778" layer="96">&gt;VALUE</text>
<pin name="A" x="-5.08" y="0" visible="off" length="short" direction="pas"/>
<pin name="K" x="5.08" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="RX-8025SA" prefix="IC">
<description>Real Time Clock 32.768KHz 3.0Volts 5+/-5ppm -40C +85C</description>
<gates>
<gate name="G$1" symbol="RX-8025S" x="0" y="-2.54"/>
</gates>
<devices>
<device name="" package="SOP-14">
<connects>
<connect gate="G$1" pin="FOE" pad="7"/>
<connect gate="G$1" pin="FOUT" pad="3"/>
<connect gate="G$1" pin="GND" pad="11"/>
<connect gate="G$1" pin="INTA" pad="10"/>
<connect gate="G$1" pin="INTB" pad="12"/>
<connect gate="G$1" pin="SCL" pad="2"/>
<connect gate="G$1" pin="SDA" pad="13"/>
<connect gate="G$1" pin="TEST" pad="5"/>
<connect gate="G$1" pin="VDD" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="DODANI" value="VEIT"/>
<attribute name="MELE" value="0239"/>
<attribute name="MPN" value="RX-8025SA"/>
<attribute name="OSAZENI" value="RACOM"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="BAV70LT1G" prefix="D">
<description>Dvojitá dioda BAV70LT1G, pouzdro SOT-23</description>
<gates>
<gate name="G$1" symbol="DIODE-SCHOTTKY_CC" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT-23">
<connects>
<connect gate="G$1" pin="A1" pad="1"/>
<connect gate="G$1" pin="A2" pad="2"/>
<connect gate="G$1" pin="K" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="DODANI" value="VEIT"/>
<attribute name="MELE" value="1078" constant="no"/>
<attribute name="MPN" value="BAV70LT1G"/>
<attribute name="OSAZENI" value="RACOM"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LT1785" prefix="IC">
<description>60V Fault Protected RS485/RS422 Transceiver</description>
<gates>
<gate name="G$1" symbol="LT1785" x="0" y="0"/>
</gates>
<devices>
<device name="CN8" package="DIL08">
<connects>
<connect gate="G$1" pin="!RE" pad="2"/>
<connect gate="G$1" pin="A" pad="6"/>
<connect gate="G$1" pin="B" pad="7"/>
<connect gate="G$1" pin="DE" pad="3"/>
<connect gate="G$1" pin="DI" pad="4"/>
<connect gate="G$1" pin="GND" pad="5"/>
<connect gate="G$1" pin="RO" pad="1"/>
<connect gate="G$1" pin="VCC" pad="8"/>
</connects>
<technologies>
<technology name="">
<attribute name="DODANI" value="VEIT"/>
<attribute name="MELE" value="0656"/>
<attribute name="MPN" value="LT1785CN8"/>
<attribute name="OSAZENI" value="VEIT"/>
</technology>
</technologies>
</device>
<device name="CS8" package="SO-08">
<connects>
<connect gate="G$1" pin="!RE" pad="2"/>
<connect gate="G$1" pin="A" pad="6"/>
<connect gate="G$1" pin="B" pad="7"/>
<connect gate="G$1" pin="DE" pad="3"/>
<connect gate="G$1" pin="DI" pad="4"/>
<connect gate="G$1" pin="GND" pad="5"/>
<connect gate="G$1" pin="RO" pad="1"/>
<connect gate="G$1" pin="VCC" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TPD2S017" prefix="IC">
<description>&lt;B&gt;ESD na sběrnici USB&lt;/B&gt;&lt;P&gt;
2-Channel Ultra-Low Clamp Voltage ESD Solution With Series-Resistor Isolation</description>
<gates>
<gate name="G$1" symbol="TPD2S017" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DBV">
<connects>
<connect gate="G$1" pin="CH1IN" pad="3"/>
<connect gate="G$1" pin="CH1OUT" pad="1"/>
<connect gate="G$1" pin="CH2IN" pad="4"/>
<connect gate="G$1" pin="CH2OUT" pad="6"/>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="VCC" pad="5"/>
</connects>
<technologies>
<technology name="">
<attribute name="DODANI" value="VEIT"/>
<attribute name="MELE" value="0229"/>
<attribute name="MPN" value="TPD2S017"/>
<attribute name="OSAZENI" value="RACOM"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="USB2514B" prefix="IC">
<description>USB 2.0 Hi-Speed Hub Controller</description>
<gates>
<gate name="G$1" symbol="USB2514" x="0" y="0"/>
</gates>
<devices>
<device name="" package="USB2514B">
<connects>
<connect gate="G$1" pin="!RESET" pad="26"/>
<connect gate="G$1" pin="CRFILT" pad="14"/>
<connect gate="G$1" pin="HS_IND" pad="25"/>
<connect gate="G$1" pin="OCS_N1" pad="13"/>
<connect gate="G$1" pin="OCS_N2" pad="17"/>
<connect gate="G$1" pin="OCS_N3" pad="19"/>
<connect gate="G$1" pin="OCS_N4" pad="21"/>
<connect gate="G$1" pin="PLLFILT" pad="34"/>
<connect gate="G$1" pin="PRTPWR1" pad="12"/>
<connect gate="G$1" pin="PRTPWR2" pad="16"/>
<connect gate="G$1" pin="PRTPWR3" pad="18"/>
<connect gate="G$1" pin="PRTPWR4" pad="20"/>
<connect gate="G$1" pin="RBIAS" pad="35"/>
<connect gate="G$1" pin="SCL" pad="24"/>
<connect gate="G$1" pin="SDA" pad="22"/>
<connect gate="G$1" pin="SUSP_IND" pad="28"/>
<connect gate="G$1" pin="TEST" pad="11"/>
<connect gate="G$1" pin="USBDM_DN1" pad="1"/>
<connect gate="G$1" pin="USBDM_DN2" pad="3"/>
<connect gate="G$1" pin="USBDM_DN3" pad="6"/>
<connect gate="G$1" pin="USBDM_DN4" pad="8"/>
<connect gate="G$1" pin="USBDM_UP" pad="30"/>
<connect gate="G$1" pin="USBDP_DN1" pad="2"/>
<connect gate="G$1" pin="USBDP_DN2" pad="4"/>
<connect gate="G$1" pin="USBDP_DN3" pad="7"/>
<connect gate="G$1" pin="USBDP_DN4" pad="9"/>
<connect gate="G$1" pin="USBDP_UP" pad="31"/>
<connect gate="G$1" pin="VBUS_DET" pad="27"/>
<connect gate="G$1" pin="VDD33@2" pad="15"/>
<connect gate="G$1" pin="VDD33@3" pad="23"/>
<connect gate="G$1" pin="VDDA33" pad="5"/>
<connect gate="G$1" pin="VDDA33@1" pad="10"/>
<connect gate="G$1" pin="VDDA33@2" pad="29"/>
<connect gate="G$1" pin="VDDA33@3" pad="36"/>
<connect gate="G$1" pin="VSS" pad="PAD"/>
<connect gate="G$1" pin="XTALIN" pad="33"/>
<connect gate="G$1" pin="XTALOUT" pad="32"/>
</connects>
<technologies>
<technology name="">
<attribute name="DODANI" value="VEIT"/>
<attribute name="MELE" value="1309"/>
<attribute name="MF" value="Microchip"/>
<attribute name="MPN" value="USB2514B/M2"/>
<attribute name="OSAZENI" value="RACOM"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TPS51120" prefix="IC">
<description>Dual Synchronous Step-Down Controller With 100-mA Standby Regulators</description>
<gates>
<gate name="_LDO" symbol="TPS51120_DRV" x="-35.56" y="0"/>
<gate name="_5V" symbol="TPS51120_SW" x="35.56" y="0"/>
<gate name="_3,3V" symbol="TPS51120_SW" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TPS51120">
<connects>
<connect gate="_3,3V" pin="COMP" pad="7"/>
<connect gate="_3,3V" pin="CS" pad="18"/>
<connect gate="_3,3V" pin="DRVH" pad="14"/>
<connect gate="_3,3V" pin="DRVL" pad="16"/>
<connect gate="_3,3V" pin="EN" pad="12"/>
<connect gate="_3,3V" pin="LL" pad="15"/>
<connect gate="_3,3V" pin="PGND" pad="17"/>
<connect gate="_3,3V" pin="PGOOD" pad="11"/>
<connect gate="_3,3V" pin="VBST" pad="13"/>
<connect gate="_3,3V" pin="VFB" pad="6"/>
<connect gate="_3,3V" pin="VO" pad="8"/>
<connect gate="_5V" pin="COMP" pad="2"/>
<connect gate="_5V" pin="CS" pad="23"/>
<connect gate="_5V" pin="DRVH" pad="27"/>
<connect gate="_5V" pin="DRVL" pad="25"/>
<connect gate="_5V" pin="EN" pad="29"/>
<connect gate="_5V" pin="LL" pad="26"/>
<connect gate="_5V" pin="PGND" pad="24"/>
<connect gate="_5V" pin="PGOOD" pad="30"/>
<connect gate="_5V" pin="VBST" pad="28"/>
<connect gate="_5V" pin="VFB" pad="3"/>
<connect gate="_5V" pin="VO" pad="1"/>
<connect gate="_LDO" pin="EN3" pad="10"/>
<connect gate="_LDO" pin="EN5" pad="9"/>
<connect gate="_LDO" pin="GND" pad="5"/>
<connect gate="_LDO" pin="PAD" pad="PAD"/>
<connect gate="_LDO" pin="SKIPSEL" pad="32"/>
<connect gate="_LDO" pin="TONSEL" pad="31"/>
<connect gate="_LDO" pin="V5FILT" pad="20"/>
<connect gate="_LDO" pin="VIN" pad="22"/>
<connect gate="_LDO" pin="VREF2" pad="4"/>
<connect gate="_LDO" pin="VREG3" pad="19"/>
<connect gate="_LDO" pin="VREG5" pad="21"/>
</connects>
<technologies>
<technology name="">
<attribute name="DODANI" value="VEIT"/>
<attribute name="MELE" value="1310"/>
<attribute name="MF" value="TI"/>
<attribute name="MPN" value="TPS51120RHBR"/>
<attribute name="OSAZENI" value="RACOM"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FDS6982" prefix="T">
<description>Dual Power Supply N-Channel PowerTrench® SyncFET™&lt;P&gt;

Features
• Q2: Optimized to minimize conduction losses
Includes SyncFET Schottky body diode
8.6A, 30V RDS(on) max= 13.5mΩ @ VGS = 10V
RDS(on) max= 16.5mΩ @ VGS = 4.5V
• Low gate charge (21nC typical)
• Q1: Optimized for low switching losses
6.3A, 30V RDS(on) max= 28.0mΩ @ VGS = 10V
RDS(on) max= 35.0mΩ @ VGS = 4.5V
• Low gate charge (11nC typical)</description>
<gates>
<gate name="_1" symbol="MOSFET_N" x="-2.54" y="10.16"/>
<gate name="_2" symbol="MOSFET_N" x="-2.54" y="-10.16"/>
</gates>
<devices>
<device name="" package="SO-08">
<connects>
<connect gate="_1" pin="D" pad="5 6"/>
<connect gate="_1" pin="G" pad="4"/>
<connect gate="_1" pin="S" pad="3"/>
<connect gate="_2" pin="D" pad="7 8"/>
<connect gate="_2" pin="G" pad="2"/>
<connect gate="_2" pin="S" pad="1"/>
</connects>
<technologies>
<technology name="">
<attribute name="DODANI" value="VEIT"/>
<attribute name="MELE" value="1313"/>
<attribute name="MPN" value="FDS6982AS" constant="no"/>
<attribute name="OSAZENI" value="RACOM"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LT1910" prefix="IC">
<description>Protected high side Mosfet driver&lt;br&gt;
Vsup=8..48V&lt;br&gt;</description>
<gates>
<gate name="G$1" symbol="LT1910" x="0" y="-2.54"/>
</gates>
<devices>
<device name="IS8#PBF" package="SO-08">
<connects>
<connect gate="G$1" pin="!FLT" pad="3"/>
<connect gate="G$1" pin="G" pad="5"/>
<connect gate="G$1" pin="GND" pad="1"/>
<connect gate="G$1" pin="IN" pad="4"/>
<connect gate="G$1" pin="SNS" pad="6"/>
<connect gate="G$1" pin="TIM" pad="2"/>
<connect gate="G$1" pin="V+" pad="8"/>
</connects>
<technologies>
<technology name="">
<attribute name="DODANI" value="VEIT"/>
<attribute name="MELE" value="0796"/>
<attribute name="MPN" value="LT1910IS8#PBF"/>
<attribute name="OSAZENI" value="RACOM"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LEDSMD" prefix="LED" uservalue="yes">
<description>LED dioda Zelená nízkopříkonová, SMD 1206</description>
<gates>
<gate name="G$1" symbol="LEDSMD" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1206LED">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="K" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DODANI" value="VEIT"/>
<attribute name="MELE" value="1246"/>
<attribute name="MPN" value="APT3216LZGCK"/>
<attribute name="OSAZENI" value="RACOM"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="_RCL">
<packages>
<package name="0603">
<wire x1="-0.3556" y1="0.254" x2="0.3556" y2="0.254" width="0.127" layer="21"/>
<wire x1="0.3556" y1="-0.254" x2="-0.3556" y2="-0.254" width="0.127" layer="21"/>
<wire x1="0.3556" y1="0.254" x2="0.3556" y2="-0.254" width="0.127" layer="21"/>
<wire x1="-0.3556" y1="0.254" x2="-0.3556" y2="-0.254" width="0.127" layer="21"/>
<smd name="2" x="0.7874" y="0" dx="0.762" dy="0.9652" layer="1"/>
<smd name="1" x="-0.7874" y="0" dx="0.762" dy="0.9652" layer="1"/>
<text x="-0.762" y="0.762" size="1.27" layer="27">&gt;VALUE</text>
<text x="-0.762" y="2.667" size="1.27" layer="25">&gt;NAME</text>
<rectangle x1="-0.762" y1="-0.4064" x2="-0.4064" y2="0.4064" layer="27"/>
<rectangle x1="0.4064" y1="-0.4064" x2="0.762" y2="0.4064" layer="27"/>
</package>
<package name="1206">
<wire x1="-0.8636" y1="0.635" x2="0.8636" y2="0.635" width="0.127" layer="21"/>
<wire x1="-0.8636" y1="-0.635" x2="0.8636" y2="-0.635" width="0.127" layer="21"/>
<wire x1="0.8636" y1="0.635" x2="0.8636" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-0.8636" y1="0.635" x2="-0.8636" y2="-0.635" width="0.127" layer="21"/>
<smd name="2" x="1.4732" y="0" dx="1.143" dy="1.7018" layer="1"/>
<smd name="1" x="-1.4732" y="0" dx="1.143" dy="1.7018" layer="1"/>
<text x="-1.397" y="1.27" size="1.27" layer="27">&gt;VALUE</text>
<text x="-1.397" y="3.175" size="1.27" layer="25">&gt;NAME</text>
<rectangle x1="-1.4224" y1="-0.7874" x2="-0.9144" y2="0.7874" layer="27"/>
<rectangle x1="0.9144" y1="-0.7874" x2="1.4224" y2="0.7874" layer="27"/>
</package>
<package name="0805">
<wire x1="-0.3556" y1="0.4572" x2="0.3556" y2="0.4572" width="0.127" layer="21"/>
<wire x1="0.3556" y1="-0.4572" x2="-0.3556" y2="-0.4572" width="0.127" layer="21"/>
<wire x1="0.3556" y1="0.4572" x2="0.3556" y2="-0.4572" width="0.127" layer="21"/>
<wire x1="-0.3556" y1="0.4572" x2="-0.3556" y2="-0.4572" width="0.127" layer="21"/>
<smd name="2" x="0.9398" y="0" dx="1.0922" dy="1.397" layer="1"/>
<smd name="1" x="-0.9398" y="0" dx="1.0922" dy="1.397" layer="1"/>
<text x="-0.889" y="1.016" size="1.27" layer="27">&gt;VALUE</text>
<text x="-0.889" y="2.921" size="1.27" layer="25">&gt;NAME</text>
<rectangle x1="0.4064" y1="-0.6096" x2="0.9144" y2="0.6096" layer="27"/>
<rectangle x1="-0.9144" y1="-0.6096" x2="-0.4064" y2="0.6096" layer="27"/>
</package>
<package name="2512">
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
</package>
<package name="1210">
<description>&lt;b&gt;SMD CHIP CAP&lt;/b&gt;&lt;p&gt;
1210, grid 0.0125 inch</description>
<wire x1="-0.9652" y1="1.2446" x2="0.9652" y2="1.2446" width="0.1016" layer="51"/>
<wire x1="-0.9652" y1="-1.2446" x2="0.9652" y2="-1.2446" width="0.1016" layer="51"/>
<smd name="P$1" x="-1.5875" y="0" dx="1.524" dy="2.54" layer="1"/>
<smd name="P$2" x="1.5875" y="0" dx="1.524" dy="2.54" layer="1"/>
<text x="-1.5875" y="1.5875" size="1.016" layer="25" ratio="14">&gt;NAME</text>
<text x="-1.5875" y="-2.54" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.7012" y1="-1.2954" x2="-0.9511" y2="1.3045" layer="51"/>
<rectangle x1="0.9517" y1="-1.3039" x2="1.7018" y2="1.296" layer="51"/>
<rectangle x1="-0.3175" y1="-0.7" x2="0.3175" y2="0.7" layer="35"/>
</package>
<package name="L_7447797470">
<smd name="1" x="-4.1" y="0" dx="2.4" dy="3.4" layer="1"/>
<smd name="2" x="4.1" y="0" dx="2.4" dy="3.4" layer="1"/>
<wire x1="-5.1" y1="4" x2="-5.1" y2="-4" width="0.127" layer="21"/>
<wire x1="-4" y1="-5.1" x2="4" y2="-5.1" width="0.127" layer="21"/>
<wire x1="5.1" y1="-4" x2="5.1" y2="3.5" width="0.127" layer="21"/>
<text x="0" y="1.27" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.27" size="1.27" layer="27" align="bottom-center">&gt;VALUE</text>
<wire x1="3.5" y1="5.1" x2="-4" y2="5.1" width="0.127" layer="21"/>
<wire x1="-5.1" y1="-4" x2="-4" y2="-5.1" width="0.127" layer="21" curve="53.130102"/>
<wire x1="-4" y1="5.1" x2="-5.1" y2="4" width="0.127" layer="21" curve="53.130102"/>
<wire x1="4" y1="-5.1" x2="5.1" y2="-4" width="0.127" layer="21" curve="53.130102"/>
<circle x="0" y="0" radius="5" width="0.127" layer="21"/>
<wire x1="3" y1="-4" x2="4" y2="-3" width="0.127" layer="21" curve="180"/>
<wire x1="-4" y1="-3" x2="-3" y2="-4" width="0.127" layer="21" curve="180"/>
<wire x1="4" y1="3" x2="3" y2="4" width="0.127" layer="21" curve="180"/>
<wire x1="-3" y1="4" x2="-4" y2="3" width="0.127" layer="21" curve="180"/>
<wire x1="3.5" y1="5.1" x2="5.1" y2="3.5" width="0.127" layer="21"/>
</package>
<package name="L_744771008">
<smd name="1" x="-4.95" y="0" dx="2.9" dy="5.4" layer="1"/>
<smd name="2" x="4.95" y="0" dx="2.9" dy="5.4" layer="1"/>
<wire x1="-6" y1="5" x2="-6" y2="-5" width="0.127" layer="21"/>
<wire x1="-5" y1="-6" x2="5" y2="-6" width="0.127" layer="21"/>
<wire x1="6" y1="-5" x2="6" y2="5" width="0.127" layer="21"/>
<text x="0" y="1.27" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.54" size="1.27" layer="27" align="bottom-center">&gt;VALUE</text>
<wire x1="5" y1="6" x2="-5" y2="6" width="0.127" layer="21"/>
<wire x1="-6" y1="-5" x2="-5" y2="-6" width="0.127" layer="21" curve="53.130102"/>
<wire x1="-5" y1="6" x2="-6" y2="5" width="0.127" layer="21" curve="53.130102"/>
<wire x1="6" y1="5" x2="5" y2="6" width="0.127" layer="21" curve="53.130102"/>
<wire x1="5" y1="-6" x2="6" y2="-5" width="0.127" layer="21" curve="53.130102"/>
<circle x="0" y="0" radius="5" width="0.127" layer="21"/>
<wire x1="3" y1="-4" x2="4" y2="-3" width="0.127" layer="21" curve="180"/>
<wire x1="-4" y1="-3" x2="-3" y2="-4" width="0.127" layer="21" curve="180"/>
<wire x1="4" y1="3" x2="3" y2="4" width="0.127" layer="21" curve="180"/>
<wire x1="-3" y1="4" x2="-4" y2="3" width="0.127" layer="21" curve="180"/>
</package>
<package name="CAP_TANT_C">
<smd name="A" x="-3" y="0" dx="3" dy="2.4" layer="1"/>
<smd name="C" x="3" y="0" dx="3" dy="2.4" layer="1"/>
<wire x1="-3" y1="1.6" x2="-3" y2="-1.6" width="0.127" layer="21"/>
<wire x1="-3" y1="-1.6" x2="3" y2="-1.6" width="0.127" layer="21"/>
<wire x1="3" y1="-1.6" x2="3" y2="1.6" width="0.127" layer="21"/>
<wire x1="3" y1="1.6" x2="-3" y2="1.6" width="0.127" layer="21"/>
<polygon width="0.127" layer="21">
<vertex x="-1.8" y="-1.6"/>
<vertex x="-1.6" y="-1.6"/>
<vertex x="-1.6" y="1.6"/>
<vertex x="-3" y="1.6"/>
<vertex x="-3" y="-1.6"/>
</polygon>
<text x="-4.77" y="3.16" size="1.27" layer="25">&gt;NAME</text>
<text x="-4.27" y="1.86" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="CAP_ELYT_6,3X7,7">
<wire x1="-0.635" y1="-1.0477" x2="-0.2857" y2="-1.0477" width="0.127" layer="21"/>
<wire x1="-0.2857" y1="-1.0477" x2="-0.2857" y2="1.0478" width="0.127" layer="21"/>
<wire x1="-0.2857" y1="1.0478" x2="-0.635" y2="1.0478" width="0.127" layer="21"/>
<wire x1="-1.7201" y1="2.2072" x2="-1.7201" y2="1.2602" width="0.127" layer="21"/>
<wire x1="-2.194" y1="1.7337" x2="-1.247" y2="1.7337" width="0.127" layer="21"/>
<wire x1="-0.635" y1="1.0478" x2="-0.6351" y2="0" width="0.127" layer="21"/>
<wire x1="-0.6351" y1="0" x2="-0.635" y2="-1.0477" width="0.127" layer="21"/>
<wire x1="-0.6351" y1="0" x2="-4.0401" y2="0" width="0.127" layer="21"/>
<wire x1="0.7144" y1="0" x2="3.99" y2="0" width="0.127" layer="21"/>
<wire x1="-2.2225" y1="3.4925" x2="3.4925" y2="3.4925" width="0.127" layer="21"/>
<wire x1="3.4925" y1="3.4925" x2="3.4925" y2="-3.4925" width="0.127" layer="21"/>
<wire x1="3.4925" y1="-3.4925" x2="-2.2225" y2="-3.4925" width="0.127" layer="21"/>
<wire x1="-2.2225" y1="-3.4925" x2="-3.4925" y2="-2.2225" width="0.127" layer="21"/>
<wire x1="-3.4925" y1="-2.2225" x2="-3.4925" y2="2.2225" width="0.127" layer="21"/>
<wire x1="-3.4925" y1="2.2225" x2="-2.2225" y2="3.4925" width="0.127" layer="21"/>
<circle x="0" y="0" radius="3.3147" width="0.127" layer="21"/>
<smd name="C" x="3.025" y="0" dx="3.81" dy="1.9304" layer="1"/>
<smd name="A" x="-3.025" y="0" dx="3.81" dy="1.9304" layer="1"/>
<text x="-2.6877" y="-5.1073" size="1.27" layer="27">&gt;VALUE</text>
<text x="-2.8577" y="3.9076" size="1.27" layer="25">&gt;NAME</text>
<rectangle x1="0.3175" y1="-1.1113" x2="0.7143" y2="1.1113" layer="21"/>
<rectangle x1="1.3825" y1="-0.5" x2="4.2825" y2="0.5" layer="27"/>
<rectangle x1="-4.2825" y1="-0.5" x2="-1.3825" y2="0.5" layer="27"/>
</package>
<package name="CAP_ELYT_RM5_D13">
<wire x1="0" y1="1.143" x2="0" y2="0.889" width="0.1524" layer="21"/>
<wire x1="0" y1="0.889" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.889" x2="-1.27" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.254" x2="1.27" y2="0.254" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.254" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.889" x2="0" y2="0.889" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.635" x2="0" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.016" x2="0" y2="-1.524" width="0.1524" layer="51"/>
<wire x1="0" y1="1.651" x2="0" y2="1.143" width="0.1524" layer="51"/>
<circle x="0" y="0" radius="6.985" width="0.1524" layer="21"/>
<pad name="+" x="0" y="2.54" drill="1.016" diameter="2.54" rot="R270"/>
<pad name="-" x="0" y="-2.54" drill="1.016" diameter="2.54" shape="octagon" rot="R270"/>
<text x="4.1148" y="-6.3754" size="1.778" layer="25" ratio="10" rot="R270">&gt;NAME</text>
<text x="-3.937" y="4.572" size="1.778" layer="27" ratio="10" rot="R270">&gt;VALUE</text>
<rectangle x1="-0.3175" y1="-1.8415" x2="0.3175" y2="0.6985" layer="21" rot="R270"/>
<wire x1="-1" y1="5.4" x2="1" y2="5.4" width="0.6096" layer="21"/>
<wire x1="0" y1="4.5" x2="0" y2="6.4" width="0.6096" layer="21"/>
<wire x1="-0.9" y1="-5.5" x2="1.1" y2="-5.5" width="0.6096" layer="21"/>
</package>
<package name="DLW21S">
<wire x1="0" y1="-0.6" x2="1" y2="-0.6" width="0.254" layer="21"/>
<wire x1="1" y1="-0.6" x2="1" y2="0.6" width="0.254" layer="21"/>
<wire x1="1" y1="0.6" x2="-1" y2="0.6" width="0.254" layer="21"/>
<wire x1="-1" y1="0.6" x2="-1" y2="-0.6" width="0.254" layer="21"/>
<wire x1="-1" y1="-0.6" x2="0" y2="-0.6" width="0.254" layer="21"/>
<smd name="P$1" x="-0.8" y="0.4" dx="0.5" dy="0.6" layer="1" rot="R90"/>
<smd name="P$2" x="0.8" y="0.4" dx="0.5" dy="0.6" layer="1" rot="R90"/>
<smd name="P$4" x="-0.8" y="-0.4" dx="0.5" dy="0.6" layer="1" rot="R90"/>
<smd name="P$3" x="0.8" y="-0.4" dx="0.5" dy="0.6" layer="1" rot="R90"/>
<wire x1="-0.2" y1="0.4" x2="-0.3" y2="-0.4" width="0.05" layer="21"/>
<wire x1="-0.1" y1="0.4" x2="-0.2" y2="-0.4" width="0.05" layer="21"/>
<wire x1="0" y1="0.4" x2="-0.1" y2="-0.4" width="0.05" layer="21"/>
<wire x1="0.1" y1="0.4" x2="0" y2="-0.4" width="0.05" layer="21"/>
<wire x1="0.2" y1="0.4" x2="0.1" y2="-0.4" width="0.05" layer="21"/>
<wire x1="0.3" y1="0.4" x2="0.2" y2="-0.4" width="0.05" layer="21"/>
<wire x1="0.3" y1="-0.4" x2="0.4" y2="0.4" width="0.05" layer="21"/>
<wire x1="-0.3" y1="0.4" x2="-0.4" y2="-0.4" width="0.05" layer="21"/>
<wire x1="-0.4" y1="0.3" x2="-0.7" y2="0.3" width="0.05" layer="21"/>
<wire x1="-0.4" y1="-0.3" x2="-0.7" y2="-0.3" width="0.05" layer="21"/>
<wire x1="0.4" y1="-0.3" x2="0.7" y2="-0.3" width="0.05" layer="21"/>
<wire x1="0.4" y1="0.3" x2="0.7" y2="0.3" width="0.05" layer="21"/>
<text x="-1.397" y="0.97" size="1.27" layer="27">&gt;VALUE</text>
<text x="-1.397" y="2.875" size="1.27" layer="25">&gt;NAME</text>
</package>
</packages>
<symbols>
<symbol name="R">
<wire x1="-2.54" y1="1.27" x2="-2.54" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-1.27" x2="2.54" y2="-1.27" width="0.254" layer="94"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="1.27" width="0.254" layer="94"/>
<wire x1="2.54" y1="1.27" x2="-2.54" y2="1.27" width="0.254" layer="94"/>
<text x="-2.54" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-4.445" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="C">
<wire x1="-2.54" y1="0" x2="-0.508" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.508" y1="1.524" x2="-0.508" y2="-1.524" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="0.508" y2="0" width="0.1524" layer="94"/>
<wire x1="0.508" y1="1.524" x2="0.508" y2="-1.524" width="0.254" layer="94"/>
<text x="-2.54" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-4.445" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
<pin name="2" x="2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="INDUKTOR">
<wire x1="0" y1="5.08" x2="1.27" y2="3.81" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="0" y1="2.54" x2="1.27" y2="3.81" width="0.254" layer="94" curve="90" cap="flat"/>
<wire x1="0" y1="2.54" x2="1.27" y2="1.27" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="0" y1="0" x2="1.27" y2="1.27" width="0.254" layer="94" curve="90" cap="flat"/>
<wire x1="0" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.54" x2="1.27" y2="-1.27" width="0.254" layer="94" curve="90" cap="flat"/>
<wire x1="0" y1="-2.54" x2="1.27" y2="-3.81" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="0" y1="-5.08" x2="1.27" y2="-3.81" width="0.254" layer="94" curve="90" cap="flat"/>
<wire x1="1.905" y1="5.08" x2="1.905" y2="-5.08" width="0.254" layer="94"/>
<text x="-8.89" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="-10.16" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="0" y="7.62" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-7.62" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
<symbol name="L">
<wire x1="-2.54" y1="0" x2="0" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="0" y1="0" x2="2.54" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="2.54" y1="0" x2="5.08" y2="0" width="0.254" layer="94" curve="-180"/>
<text x="-2.9464" y="2.6416" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.2004" y="-3.2258" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="7.62" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="C_POL">
<wire x1="-1.524" y1="-0.508" x2="1.524" y2="-0.508" width="0.254" layer="94"/>
<wire x1="-1.524" y1="0.508" x2="1.524" y2="0.508" width="0.1524" layer="94"/>
<wire x1="1.524" y1="0.508" x2="1.524" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="1.016" x2="-1.524" y2="0.508" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="1.016" x2="1.524" y2="1.016" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="0.9525" width="0.1524" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="4.445" y="-2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<text x="2.54" y="1.27" size="1.778" layer="94" rot="R90">+</text>
<pin name="C" x="0" y="-2.54" visible="off" length="point" direction="pas" rot="R90"/>
<pin name="A" x="0" y="2.54" visible="off" length="point" direction="pas" rot="R270"/>
</symbol>
<symbol name="L_CM">
<wire x1="-2.54" y1="2.54" x2="0" y2="2.54" width="0.254" layer="94" curve="180"/>
<wire x1="0" y1="2.54" x2="2.54" y2="2.54" width="0.254" layer="94" curve="180"/>
<wire x1="2.54" y1="2.54" x2="5.08" y2="2.54" width="0.254" layer="94" curve="180"/>
<text x="-3.048" y="5.334" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.048" y="3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-5.08" y="2.54" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="7.62" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<wire x1="5.08" y1="-2.54" x2="2.54" y2="-2.54" width="0.254" layer="94" curve="180"/>
<wire x1="2.54" y1="-2.54" x2="0" y2="-2.54" width="0.254" layer="94" curve="180"/>
<wire x1="0" y1="-2.54" x2="-2.54" y2="-2.54" width="0.254" layer="94" curve="180"/>
<pin name="3" x="7.62" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="-5.08" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1"/>
<wire x1="-2.54" y1="0.508" x2="5.08" y2="0.508" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.508" x2="5.08" y2="-0.508" width="0.254" layer="94"/>
<circle x="-3.302" y="1.524" radius="0.508" width="0" layer="94"/>
<circle x="-3.302" y="-1.524" radius="0.508" width="0" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="R" prefix="R">
<description>Rezistor</description>
<gates>
<gate name="G$1" symbol="R" x="0" y="0"/>
</gates>
<devices>
<device name="_22K/0603/AEC" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DODANI" value="VEIT"/>
<attribute name="MELE" value="1240"/>
<attribute name="MPN" value="ERJ-3GEYJ223V"/>
<attribute name="OSAZENI" value="RACOM"/>
</technology>
</technologies>
</device>
<device name="_10K/0603/AEC" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DODANI" value="VEIT"/>
<attribute name="MELE" value="1241"/>
<attribute name="MPN" value="ERJ-3GEYJ223V"/>
<attribute name="OSAZENI" value="RACOM"/>
</technology>
</technologies>
</device>
<device name="_100R/1206/AEC" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DODANI" value="VEIT"/>
<attribute name="MELE" value="1191"/>
<attribute name="MPN" value="ERJ-P08J101V"/>
<attribute name="OSAZENI" value="RACOM"/>
</technology>
</technologies>
</device>
<device name="_150R/0603/AEC" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DODANI" value="VEIT"/>
<attribute name="MELE" value="založit s. k. ABRA"/>
<attribute name="MPN" value="????"/>
<attribute name="OSAZENI" value="RACOM"/>
</technology>
</technologies>
</device>
<device name="_2K2/0603/AEC" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_33K/0603/AEC" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DODANI" value="VEIT"/>
<attribute name="MELE" value="1247"/>
<attribute name="MPN" value="ERJ-3GEYJ333V"/>
<attribute name="OSAZENI" value="RACOM"/>
</technology>
</technologies>
</device>
<device name="_470K/0603/AEC" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DODANI" value="VEIT"/>
<attribute name="MELE" value="1238"/>
<attribute name="MPN" value="ERJ-3GEYJ474V"/>
<attribute name="OSAZENI" value="RACOM"/>
</technology>
</technologies>
</device>
<device name="_47K/0603/AEC" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DODANI" value="VEIT"/>
<attribute name="MELE" value="1239" constant="no"/>
<attribute name="MPN" value="ERJ-3GEYJ473V"/>
<attribute name="OSAZENI" value="RACOM"/>
</technology>
</technologies>
</device>
<device name="_4K7/0603/AEC" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DODANI" value="VEIT"/>
<attribute name="MELE" value="založit s. k. ABRA"/>
<attribute name="MPN" value="????"/>
<attribute name="OSAZENI" value="RACOM"/>
</technology>
</technologies>
</device>
<device name="_470R/0603/AEC" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2512" package="2512">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_100K/0603" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DODANI" value="VEIT"/>
<attribute name="MELE" value="1056"/>
<attribute name="MPN" value="RC0603FR-13100KL"/>
<attribute name="OC_FARNELL" value="2309107"/>
<attribute name="OSAZENI" value="RACOM"/>
</technology>
</technologies>
</device>
<device name="_22R/0603" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DODANI" value="VEIT"/>
<attribute name="MELE" value="1356"/>
<attribute name="MPN" value="RC0603FR-0722RL"/>
<attribute name="OSAZENI" value="RACOM"/>
</technology>
</technologies>
</device>
<device name="_68K/0603" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DODANI" value="VEIT"/>
<attribute name="MELE" value="1355"/>
<attribute name="MPN" value="RC0603FR-0768KL"/>
<attribute name="OSAZENI" value="RACOM"/>
</technology>
</technologies>
</device>
<device name="_R015/1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DODANI" value="VEIT"/>
<attribute name="MELE" value="1317"/>
<attribute name="MPN" value="ERJ-8CWFR015V"/>
<attribute name="OSAZENI" value="RACOM"/>
</technology>
</technologies>
</device>
<device name="_120R/0805" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DODANI" value="VEIT"/>
<attribute name="MELE" value="1351"/>
<attribute name="MPN" value="RC0805FR-07120RL"/>
<attribute name="OSAZENI" value="RACOM"/>
</technology>
</technologies>
</device>
<device name="_12K/0603" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DODANI" value="VEIT"/>
<attribute name="MELE" value="1316"/>
<attribute name="MPN" value="RC0603FR-0712KL"/>
<attribute name="OSAZENI" value="RACOM"/>
</technology>
</technologies>
</device>
<device name="_6K2/0603" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DODANI" value="VEIT"/>
<attribute name="MELE" value="1336"/>
<attribute name="MF" value="PANASONIC"/>
<attribute name="MPN" value="ERJ3GEYJ622V"/>
<attribute name="OC_FARNELL" value="2059622"/>
<attribute name="OSAZENI" value="RACOM"/>
</technology>
</technologies>
</device>
<device name="_4R7/0603" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DODANI" value="VEIT"/>
<attribute name="MELE" value="1353"/>
<attribute name="MPN" value="RC0603FR-074R7L"/>
<attribute name="OSAZENI" value="RACOM"/>
</technology>
</technologies>
</device>
<device name="_0R0/0603" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DODANI" value="VEIT"/>
<attribute name="MELE" value="založit s. k. ABRA"/>
<attribute name="MPN" value="????"/>
<attribute name="OSAZENI" value="RACOM"/>
</technology>
</technologies>
</device>
<device name="_4K7/0603" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DODANI" value="VEIT"/>
<attribute name="MELE" value="1354"/>
<attribute name="MPN" value="RC0603FR-074K7L"/>
<attribute name="OSAZENI" value="RACOM"/>
</technology>
</technologies>
</device>
<device name="_150R/0603" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DODANI" value="VEIT"/>
<attribute name="MELE" value="1352"/>
<attribute name="MPN" value="RC0603FR-07150RL"/>
<attribute name="OSAZENI" value="RACOM"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="C" prefix="C">
<description>Kondenzátor</description>
<gates>
<gate name="G$1" symbol="C" x="0" y="0"/>
</gates>
<devices>
<device name="_10UF/16V/1210/X8R/AEC" package="1210">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DODANI" value="VEIT"/>
<attribute name="MELE" value="1236"/>
<attribute name="MPN" value="CGA6P3X8R1C106K250AB"/>
<attribute name="OSAZENI" value="RACOM"/>
</technology>
</technologies>
</device>
<device name="_680NF/100V/1210/X8R/AEC" package="1210">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DODANI" value="VEIT"/>
<attribute name="MELE" value="1237"/>
<attribute name="MPN" value="CGA6P3X8R2A684K250AB"/>
<attribute name="OSAZENI" value="RACOM"/>
</technology>
</technologies>
</device>
<device name="_100NF/25V/0603/X8R/AEC" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DODANI" value="VEIT"/>
<attribute name="MELE" value="1193"/>
<attribute name="MPN" value="CGA3E2X8R1E104K080AA"/>
<attribute name="OSAZENI" value="RACOM"/>
</technology>
</technologies>
</device>
<device name="_2,2UF/25V/1206/X8R/AEC" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DODANI" value="VEIT"/>
<attribute name="MELE" value="1250"/>
<attribute name="MPN" value="CGA5L3X8R1E225K160AB"/>
<attribute name="OSAZENI" value="RACOM"/>
</technology>
</technologies>
</device>
<device name="0603" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1210" package="1210">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2512" package="2512">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_1NF/50V/0603/X7R" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DODANI" value="VEIT"/>
<attribute name="MELE" value="1298"/>
<attribute name="MPN" value="06035C102KAT2A"/>
<attribute name="OSAZENI" value="RACOM"/>
</technology>
</technologies>
</device>
<device name="_47UF/6,3V/1210/X7R" package="1210">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DODANI" value="VEIT"/>
<attribute name="MELE" value="1294"/>
<attribute name="MPN" value="GRM32ER70J476KE20L"/>
<attribute name="OSAZENI" value="RACOM"/>
</technology>
</technologies>
</device>
<device name="_10UF/10V/0805/X5R" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DODANI" value="VEIT"/>
<attribute name="MELE" value="1295"/>
<attribute name="MPN" value="CL21A106KPFNNNG"/>
<attribute name="OSAZENI" value="RACOM"/>
</technology>
</technologies>
</device>
<device name="_1UF/16V/0603/X7R" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DODANI" value="VEIT"/>
<attribute name="MELE" value="1296"/>
<attribute name="MPN" value="CL10B105MO8NNWC"/>
<attribute name="OSAZENI" value="RACOM"/>
</technology>
</technologies>
</device>
<device name="_100NF/50V/0603/X7R" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DODANI" value="VEIT"/>
<attribute name="MELE" value="1297"/>
<attribute name="MPN" value="C0603C104M5RACTU"/>
<attribute name="OSAZENI" value="RACOM"/>
</technology>
</technologies>
</device>
<device name="_2N7/50V/0603/X7R" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DODANI" value="VEIT"/>
<attribute name="MELE" value="1315"/>
<attribute name="MPN" value="GRM155R71H272KA01J"/>
<attribute name="OSAZENI" value="RACOM"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="BLM31PG121SH1L" prefix="L">
<description>Tlumivka 120R @ 100MHz, pouzdro 1206, SMD; BLM31PG121SH1L</description>
<gates>
<gate name="G$1" symbol="INDUKTOR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DODANI" value="VEIT"/>
<attribute name="MELE" value="1235"/>
<attribute name="MPN" value="BLM31PG121SH1L"/>
<attribute name="OSAZENI" value="RACOM"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="4,7UH_7447797470" prefix="L">
<description>Tlumivka 4,7uH, 10,2x10,2mm, SMD</description>
<gates>
<gate name="G$1" symbol="L" x="0" y="0"/>
</gates>
<devices>
<device name="" package="L_7447797470">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DODANI" value="VEIT"/>
<attribute name="MELE" value="1291"/>
<attribute name="MF" value="Wurth"/>
<attribute name="MPN" value="7447797470"/>
<attribute name="OSAZENI" value="RACOM"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="8,2UH_744771008" prefix="L">
<description>Tlumivka 8,2uH, 12x12mm, SMD</description>
<gates>
<gate name="G$1" symbol="L" x="0" y="0"/>
</gates>
<devices>
<device name="" package="L_744771008">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DODANI" value="VEIT"/>
<attribute name="MELE" value="1290"/>
<attribute name="MF" value="Wurth"/>
<attribute name="MPN" value="744771008"/>
<attribute name="OSAZENI" value="RACOM"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="C_POL" prefix="C" uservalue="yes">
<description>Polarizovaný kondenzátor</description>
<gates>
<gate name="G$1" symbol="C_POL" x="0" y="0"/>
</gates>
<devices>
<device name="C" package="CAP_TANT_C">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="D=6,3_H=7,7" package="CAP_ELYT_6,3X7,7">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DODANI" value="VEIT"/>
<attribute name="MELE" value="1293"/>
<attribute name="MPN" value="875105245015"/>
<attribute name="OSAZENI" value="RACOM"/>
</technology>
</technologies>
</device>
<device name="RM5,D13" package="CAP_ELYT_RM5_D13">
<connects>
<connect gate="G$1" pin="A" pad="+"/>
<connect gate="G$1" pin="C" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="DODANI" value="VEIT"/>
<attribute name="MELE" value="1292"/>
<attribute name="MPN" value="ESY108M035AL4AA"/>
<attribute name="OSAZENI" value="VEIT"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DLW21SN900SQ2" prefix="L">
<description>Common mode choke coil&lt;br&gt;90R@100MHz, 330mA, 50Vdc</description>
<gates>
<gate name="G$1" symbol="L_CM" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DLW21S">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
<connect gate="G$1" pin="3" pad="P$3"/>
<connect gate="G$1" pin="4" pad="P$4"/>
</connects>
<technologies>
<technology name="">
<attribute name="DODANI" value="VEIT"/>
<attribute name="MELE" value="1314"/>
<attribute name="MPN" value="DLW21SN900SQ2L"/>
<attribute name="OSAZENI" value="RACOM"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SupplyVeit">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="POWER_3V3">
<circle x="0" y="0.4762" radius="0.635" width="0.3048" layer="94"/>
<text x="-2.54" y="2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="POWER_3V3" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="+5V">
<wire x1="1.27" y1="-0.635" x2="0" y2="1.27" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="-1.27" y2="-0.635" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+5V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
<wire x1="0" y1="0" x2="0" y2="1.27" width="0.1524" layer="94"/>
</symbol>
<symbol name="+24V">
<wire x1="1.27" y1="-0.635" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-0.635" width="0.254" layer="94"/>
<wire x1="1.27" y1="-0.635" x2="0" y2="1.27" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="-1.27" y2="-0.635" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+24V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="CHASSIS">
<wire x1="-1.905" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="-0.3175" y2="0" width="0.254" layer="94"/>
<wire x1="-0.3175" y1="0" x2="0.635" y2="0" width="0.254" layer="94"/>
<wire x1="0.635" y1="0" x2="1.5875" y2="0" width="0.254" layer="94"/>
<wire x1="1.5875" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.905" y2="-0.9525" width="0.254" layer="94"/>
<wire x1="-0.3175" y1="0" x2="-0.9525" y2="-0.9525" width="0.254" layer="94"/>
<wire x1="0.635" y1="0" x2="0" y2="-0.9525" width="0.254" layer="94"/>
<wire x1="1.5875" y1="0" x2="0.9525" y2="-0.9525" width="0.254" layer="94"/>
<pin name="CHASSIS" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="+3V3" prefix="+3V3">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$3" symbol="POWER_3V3" x="2.54" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+5V" prefix="P+">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="+5V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+24V" prefix="P+">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="+24V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CHASSIS" prefix="CHASSIS">
<gates>
<gate name="G$1" symbol="CHASSIS" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="_Connectors">
<packages>
<package name="ARJC02-111009D">
<wire x1="-8" y1="-10.8" x2="8" y2="-10.8" width="0.127" layer="21"/>
<wire x1="-8" y1="10.3" x2="8" y2="10.3" width="0.127" layer="21"/>
<wire x1="-8" y1="-10.8" x2="-8" y2="10.3" width="0.127" layer="21"/>
<wire x1="8" y1="-10.8" x2="8" y2="10.3" width="0.127" layer="21"/>
<pad name="SH2" x="7.875" y="3.05" drill="1.6"/>
<pad name="SH1" x="-7.875" y="3.05" drill="1.6"/>
<pad name="12" x="6.325" y="-3.38" drill="1.05"/>
<pad name="11" x="3.785" y="-4.9" drill="1.05"/>
<pad name="10" x="-3.785" y="-3.38" drill="1.05"/>
<pad name="9" x="-6.325" y="-4.9" drill="1.05"/>
<pad name="5" x="0.635" y="6.35" drill="0.9" rot="R180"/>
<pad name="4" x="-0.635" y="8.89" drill="0.9" rot="R180"/>
<pad name="3" x="-1.905" y="6.35" drill="0.9" rot="R180"/>
<pad name="6" x="1.905" y="8.89" drill="0.9" rot="R180"/>
<pad name="7" x="3.175" y="6.35" drill="0.9" rot="R180"/>
<pad name="2" x="-3.175" y="8.89" drill="0.9" rot="R180"/>
<pad name="8" x="4.445" y="8.89" drill="0.9" rot="R180"/>
<pad name="1" x="-4.445" y="6.35" drill="0.9" rot="R180"/>
<text x="9.8125" y="3.445" size="1.778" layer="25" rot="R270">&gt;NAME</text>
<text x="-11.7725" y="3.1375" size="1.778" layer="27" rot="R270">&gt;VALUE</text>
<hole x="-5.715" y="0" drill="3.25"/>
<hole x="5.715" y="0" drill="3.25"/>
</package>
<package name="MKDSN1,5/2-5,08">
<description>&lt;b&gt;MKDSN 1,5/ 2-5,08&lt;/b&gt; Printklemme&lt;p&gt;
Nennstrom: 13,5 A&lt;br&gt;
Nennspannung: 250 V&lt;br&gt;
Rastermaß: 5,08 mm&lt;br&gt;
Polzahl: 2&lt;br&gt;
Anschlussart: Schraubanschluss&lt;br&gt;
Montage: Löten&lt;br&gt;
Anschlussrichtung Leiter/Platine: 0 °&lt;br&gt;
Artikelnummer: 1729128&lt;br&gt;
Source: http://eshop.phoenixcontact.com .. 1729128.pdf</description>
<wire x1="-5.1011" y1="-4.05" x2="5.0589" y2="-4.05" width="0.2032" layer="21"/>
<wire x1="-5.1011" y1="2.5243" x2="5.0589" y2="2.5243" width="0.2032" layer="21"/>
<wire x1="-2.7211" y1="-0.63" x2="-3.6341" y2="-1.543" width="0.2032" layer="51"/>
<wire x1="-5.1011" y1="-3.327" x2="-5.2298" y2="-3.327" width="0.2032" layer="21"/>
<wire x1="-5.2298" y1="-2.913" x2="-5.1011" y2="-2.913" width="0.2032" layer="21"/>
<wire x1="-5.1011" y1="-3.1279" x2="-5.1011" y2="-2.913" width="0.2032" layer="21"/>
<wire x1="-5.1011" y1="-3.327" x2="-5.1011" y2="-3.1279" width="0.2032" layer="21"/>
<wire x1="-5.6711" y1="-2.763" x2="-5.4081" y2="-2.763" width="0.2032" layer="21"/>
<wire x1="-5.6711" y1="-3.477" x2="-5.4081" y2="-3.477" width="0.2032" layer="21"/>
<wire x1="-5.1011" y1="-2.913" x2="-5.1011" y2="-2.4479" width="0.2032" layer="21"/>
<wire x1="-5.2298" y1="-3.327" x2="-5.4081" y2="-3.477" width="0.2032" layer="21"/>
<wire x1="-5.6711" y1="-2.763" x2="-5.6711" y2="-3.477" width="0.2032" layer="21"/>
<wire x1="-5.1011" y1="-2.4479" x2="-5.1011" y2="2.5243" width="0.2032" layer="21"/>
<wire x1="-5.4081" y1="-2.763" x2="-5.2298" y2="-2.913" width="0.2032" layer="21"/>
<wire x1="5.0589" y1="-2.4479" x2="-5.1011" y2="-2.4479" width="0.2032" layer="21"/>
<wire x1="-3.2001" y1="-0.119" x2="-2.4421" y2="0.639" width="0.2032" layer="51"/>
<wire x1="-4.1291" y1="-1.048" x2="-3.2001" y2="-0.119" width="0.2032" layer="51"/>
<wire x1="0.9509" y1="-1.048" x2="1.8799" y2="-0.119" width="0.2032" layer="51"/>
<wire x1="2.3589" y1="-0.63" x2="1.4459" y2="-1.543" width="0.2032" layer="51"/>
<wire x1="1.8799" y1="-0.119" x2="2.6379" y2="0.639" width="0.2032" layer="51"/>
<wire x1="4.0869" y1="1.098" x2="3.1489" y2="0.16" width="0.2032" layer="51"/>
<wire x1="2.6379" y1="0.639" x2="3.5919" y2="1.593" width="0.2032" layer="51"/>
<wire x1="3.1489" y1="0.16" x2="2.3589" y2="-0.63" width="0.2032" layer="51"/>
<wire x1="-1.9311" y1="0.16" x2="-2.7211" y2="-0.63" width="0.2032" layer="51"/>
<wire x1="-2.4421" y1="0.639" x2="-1.4881" y2="1.593" width="0.2032" layer="51"/>
<wire x1="-0.9931" y1="1.098" x2="-1.9311" y2="0.16" width="0.2032" layer="51"/>
<wire x1="-5.1011" y1="-3.1279" x2="5.0589" y2="-3.1279" width="0.2032" layer="21"/>
<wire x1="5.0589" y1="-3.1279" x2="5.0589" y2="-2.4479" width="0.2032" layer="21"/>
<wire x1="5.0589" y1="-2.4479" x2="5.0589" y2="2.5243" width="0.2032" layer="21"/>
<wire x1="5.0589" y1="2.5243" x2="5.0589" y2="4.05" width="0.2032" layer="21"/>
<wire x1="-5.1011" y1="-3.75" x2="-5.1011" y2="-3.327" width="0.2032" layer="21"/>
<wire x1="5.0589" y1="-3.75" x2="5.0589" y2="-3.1279" width="0.2032" layer="21"/>
<wire x1="-5.1011" y1="4.05" x2="5.0589" y2="4.05" width="0.2032" layer="21"/>
<wire x1="-5.1011" y1="2.5243" x2="-5.1011" y2="4.05" width="0.2032" layer="21"/>
<wire x1="5.0589" y1="-3.75" x2="-5.1011" y2="-3.75" width="0.2032" layer="21"/>
<wire x1="-5.1011" y1="-3.75" x2="-5.1011" y2="-4.05" width="0.2032" layer="21"/>
<wire x1="5.0589" y1="-3.75" x2="5.0589" y2="-4.05" width="0.2032" layer="21"/>
<wire x1="-3.2001" y1="-0.119" x2="-2.7211" y2="-0.63" width="0.2032" layer="51" curve="65.201851"/>
<wire x1="1.8799" y1="-0.119" x2="2.3589" y2="-0.63" width="0.2032" layer="51" curve="65.201851"/>
<wire x1="3.1489" y1="0.16" x2="2.6379" y2="0.639" width="0.2032" layer="51" curve="65.201851"/>
<wire x1="-1.9311" y1="0.16" x2="-2.4421" y2="0.639" width="0.2032" layer="51" curve="65.201851"/>
<circle x="-2.5611" y="0.025" radius="1.915" width="0.2032" layer="21"/>
<circle x="2.5189" y="0.025" radius="1.915" width="0.2032" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="1.5" diameter="3.429" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.5" diameter="3.429" rot="R90"/>
<text x="-4.7511" y="4.445" size="1.27" layer="25">&gt;NAME</text>
<text x="6.985" y="-3.81" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<text x="-3.175" y="2.54" size="1.27" layer="21">1</text>
<text x="1.905" y="2.54" size="1.27" layer="21">2</text>
</package>
<package name="USB1X90">
<wire x1="10.27" y1="-6.643" x2="10.27" y2="6.673" width="0.127" layer="21"/>
<wire x1="-4.04" y1="-6.653" x2="-4.04" y2="6.653" width="0.127" layer="21"/>
<wire x1="-4.015" y1="-6.665" x2="10.255" y2="-6.665" width="0.127" layer="21"/>
<wire x1="-4.005" y1="6.665" x2="10.245" y2="6.665" width="0.127" layer="21"/>
<pad name="4" x="-2.72" y="3.5" drill="1.016"/>
<pad name="3" x="-2.71" y="1" drill="1.016"/>
<pad name="1" x="-2.71" y="-3.5" drill="1.016"/>
<pad name="2" x="-2.71" y="-1" drill="1.016"/>
<pad name="5" x="0" y="6.57" drill="2.54" diameter="4" thermals="no"/>
<pad name="6" x="0.01" y="-6.56" drill="2.54" diameter="4" thermals="no"/>
<text x="4.175" y="7.2725" size="1.778" layer="25">&gt;NAME</text>
<text x="3.2125" y="-9.2325" size="1.778" layer="27">&gt;VALUE</text>
<text x="-1" y="-3.5" size="1.016" layer="21" align="center-left">1</text>
<text x="-1" y="-1" size="1.016" layer="22" rot="MR180" align="center-left">2</text>
<text x="-1" y="1" size="1.016" layer="22" rot="MR180" align="center-left">3</text>
<text x="-1" y="3.5" size="1.016" layer="22" rot="MR180" align="center-left">4</text>
</package>
<package name="163-0179-EX">
<wire x1="-9.82" y1="4.445" x2="2.54" y2="4.445" width="0.127" layer="21"/>
<wire x1="2.54" y1="4.445" x2="4.966" y2="4.445" width="0.127" layer="21"/>
<wire x1="4.966" y1="-4.445" x2="4.966" y2="4.445" width="0.127" layer="21"/>
<wire x1="4.966" y1="-4.445" x2="2.54" y2="-4.445" width="0.127" layer="21"/>
<wire x1="2.54" y1="-4.445" x2="-9.02" y2="-4.445" width="0.127" layer="21"/>
<wire x1="-9.82" y1="4.445" x2="-9.82" y2="2" width="0.127" layer="21"/>
<wire x1="-9.82" y1="2" x2="-9.7" y2="2" width="0.127" layer="21"/>
<wire x1="-9" y1="4.4" x2="-9" y2="-3.2" width="0.127" layer="21"/>
<wire x1="-9" y1="-3.2" x2="-9" y2="-4.4" width="0.127" layer="21"/>
<wire x1="-9.1" y1="3.2" x2="-9.6" y2="3.2" width="0.127" layer="21"/>
<wire x1="-9.6" y1="3.2" x2="-9.6" y2="1.5" width="0.127" layer="21"/>
<wire x1="-9.6" y1="1.5" x2="-9.6" y2="-1.5" width="0.127" layer="21"/>
<wire x1="-9.6" y1="-1.5" x2="-9.6" y2="-3.2" width="0.127" layer="21"/>
<wire x1="-9.6" y1="-3.2" x2="-9" y2="-3.2" width="0.127" layer="21"/>
<wire x1="-9.6" y1="1.5" x2="-9.8" y2="1.5" width="0.127" layer="21"/>
<wire x1="-9.8" y1="1.5" x2="-9.8" y2="-1.5" width="0.127" layer="21"/>
<wire x1="-9.8" y1="-1.5" x2="-9.6" y2="-1.5" width="0.127" layer="21"/>
<wire x1="-9.1" y1="-3.3" x2="-9.1" y2="-4.6" width="0.254" layer="21"/>
<wire x1="-9.1" y1="-4.6" x2="1.7" y2="-4.6" width="0.254" layer="21"/>
<wire x1="2.54" y1="4.445" x2="2.54" y2="3.175" width="0.127" layer="21"/>
<wire x1="2.54" y1="3.175" x2="2.54" y2="-3.175" width="0.127" layer="21"/>
<wire x1="2.54" y1="-3.175" x2="2.54" y2="-4.445" width="0.127" layer="21"/>
<wire x1="-3.81" y1="3.175" x2="-3.81" y2="-3.175" width="0.127" layer="21" curve="180"/>
<wire x1="-3.81" y1="3.175" x2="2.54" y2="3.175" width="0.127" layer="21"/>
<wire x1="-3.81" y1="-3.175" x2="2.54" y2="-3.175" width="0.127" layer="21"/>
<pad name="P$1" x="-4.5" y="0" drill="1.778" diameter="1.9304"/>
<pad name="P$2" x="0" y="0" drill="1.5" diameter="1.6764"/>
<smd name="1" x="0" y="5.3975" dx="2.7" dy="2.2" layer="1"/>
<smd name="2" x="-6.15" y="5.3975" dx="2.7" dy="2.2" layer="1"/>
<smd name="3" x="0" y="-5.3975" dx="2.7" dy="2.2" layer="1"/>
<smd name="4" x="-6.15" y="-5.3975" dx="2.7" dy="2.2" layer="1"/>
<text x="0.635" y="8.6995" size="1.778" layer="25" ratio="10" rot="R180">&gt;NAME</text>
<text x="2.54" y="-7.3025" size="1.778" layer="27" ratio="10" rot="R180">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="ARJC02-111009D">
<pin name="CHASSIS@1" x="-20.32" y="-25.4" visible="off" length="short" direction="pas" swaplevel="2"/>
<pin name="CHASSIS@2" x="-20.32" y="-22.86" visible="off" length="short" direction="pas" swaplevel="2"/>
<pin name="R+" x="-20.32" y="27.94" length="short" direction="pas" swaplevel="1"/>
<pin name="R-" x="-20.32" y="12.7" length="short" direction="pas" swaplevel="1"/>
<pin name="T+" x="-20.32" y="7.62" length="short" direction="pas" swaplevel="1"/>
<pin name="CT_R" x="-20.32" y="20.32" length="short" direction="pas" swaplevel="1"/>
<pin name="CT_T" x="-20.32" y="0" length="short" direction="pas" swaplevel="1"/>
<pin name="T-" x="-20.32" y="-7.62" length="short" direction="pas" swaplevel="1"/>
<pin name="8" x="-20.32" y="-17.78" visible="pad" length="short" direction="pas" swaplevel="1"/>
<pin name="L2K" x="-20.32" y="-35.56" visible="pad" length="short" direction="pas"/>
<pin name="L2A" x="-20.32" y="-30.48" visible="pad" length="short" direction="pas"/>
<pin name="L1K" x="-20.32" y="38.1" visible="pad" length="short" direction="pas"/>
<pin name="L1A" x="-20.32" y="33.02" visible="pad" length="short" direction="pas"/>
<wire x1="-17.78" y1="38.1" x2="10.16" y2="38.1" width="0.1524" layer="94"/>
<wire x1="-17.78" y1="33.02" x2="10.16" y2="33.02" width="0.1524" layer="94"/>
<wire x1="16.51" y1="-31.75" x2="15.24" y2="-34.29" width="0.254" layer="94"/>
<wire x1="15.24" y1="-34.29" x2="13.97" y2="-31.75" width="0.254" layer="94"/>
<wire x1="16.51" y1="-34.29" x2="15.24" y2="-34.29" width="0.254" layer="94"/>
<wire x1="15.24" y1="-34.29" x2="13.97" y2="-34.29" width="0.254" layer="94"/>
<wire x1="16.51" y1="-31.75" x2="15.24" y2="-31.75" width="0.254" layer="94"/>
<wire x1="15.24" y1="-31.75" x2="13.97" y2="-31.75" width="0.254" layer="94"/>
<wire x1="15.24" y1="-31.75" x2="15.24" y2="-34.29" width="0.1524" layer="94"/>
<wire x1="13.208" y1="-32.512" x2="11.811" y2="-33.909" width="0.1524" layer="94"/>
<wire x1="13.335" y1="-33.655" x2="11.938" y2="-35.052" width="0.1524" layer="94"/>
<wire x1="15.24" y1="-35.56" x2="15.24" y2="-34.29" width="0.1524" layer="94"/>
<wire x1="-17.78" y1="-30.48" x2="15.24" y2="-30.48" width="0.1524" layer="94"/>
<wire x1="15.24" y1="-30.48" x2="15.24" y2="-31.75" width="0.1524" layer="94"/>
<wire x1="-17.78" y1="-35.56" x2="15.24" y2="-35.56" width="0.1524" layer="94"/>
<wire x1="-10.16" y1="12.7" x2="-8.255" y2="12.7" width="0.1524" layer="94"/>
<wire x1="-10.16" y1="27.94" x2="-8.255" y2="27.94" width="0.1524" layer="94"/>
<wire x1="-8.255" y1="20.32" x2="-8.255" y2="22.86" width="0.1524" layer="94" curve="180"/>
<wire x1="-8.255" y1="22.86" x2="-8.255" y2="25.4" width="0.1524" layer="94" curve="180"/>
<wire x1="-8.255" y1="17.78" x2="-8.255" y2="20.32" width="0.1524" layer="94" curve="180"/>
<wire x1="-8.255" y1="15.24" x2="-8.255" y2="17.78" width="0.1524" layer="94" curve="180"/>
<wire x1="-5.715" y1="26.67" x2="-5.715" y2="13.97" width="0.635" layer="94"/>
<wire x1="-4.445" y1="26.67" x2="-4.445" y2="13.97" width="0.635" layer="94"/>
<wire x1="-1.905" y1="20.32" x2="-1.905" y2="22.86" width="0.1524" layer="94" curve="-180"/>
<wire x1="-1.905" y1="22.86" x2="-1.905" y2="25.4" width="0.1524" layer="94" curve="-180"/>
<wire x1="-1.905" y1="17.78" x2="-1.905" y2="20.32" width="0.1524" layer="94" curve="-180"/>
<wire x1="-1.905" y1="15.24" x2="-1.905" y2="17.78" width="0.1524" layer="94" curve="-180"/>
<wire x1="8.89" y1="17.145" x2="6.35" y2="17.145" width="0.1524" layer="94" curve="180"/>
<wire x1="6.35" y1="17.145" x2="3.81" y2="17.145" width="0.1524" layer="94" curve="180"/>
<wire x1="3.81" y1="17.145" x2="1.27" y2="17.145" width="0.1524" layer="94" curve="180"/>
<wire x1="8.89" y1="23.495" x2="6.35" y2="23.495" width="0.1524" layer="94" curve="-180"/>
<wire x1="6.35" y1="23.495" x2="3.81" y2="23.495" width="0.1524" layer="94" curve="-180"/>
<wire x1="3.81" y1="23.495" x2="1.27" y2="23.495" width="0.1524" layer="94" curve="-180"/>
<wire x1="-1.905" y1="27.94" x2="1.27" y2="27.94" width="0.1524" layer="94"/>
<wire x1="1.27" y1="27.94" x2="1.27" y2="23.495" width="0.1524" layer="94"/>
<wire x1="1.27" y1="12.7" x2="-1.905" y2="12.7" width="0.1524" layer="94"/>
<wire x1="1.27" y1="12.7" x2="1.27" y2="17.145" width="0.1524" layer="94"/>
<wire x1="8.89" y1="23.495" x2="8.89" y2="27.94" width="0.1524" layer="94"/>
<wire x1="8.89" y1="27.94" x2="16.51" y2="27.94" width="0.1524" layer="94"/>
<wire x1="8.89" y1="17.145" x2="8.89" y2="12.7" width="0.1524" layer="94"/>
<wire x1="8.89" y1="12.7" x2="16.51" y2="12.7" width="0.1524" layer="94"/>
<wire x1="1.27" y1="19.685" x2="8.89" y2="19.685" width="0.635" layer="94"/>
<wire x1="1.27" y1="20.955" x2="8.89" y2="20.955" width="0.635" layer="94"/>
<wire x1="-10.16" y1="-7.62" x2="-8.255" y2="-7.62" width="0.1524" layer="94"/>
<wire x1="-10.16" y1="7.62" x2="-8.255" y2="7.62" width="0.1524" layer="94"/>
<wire x1="-8.255" y1="0" x2="-8.255" y2="2.54" width="0.1524" layer="94" curve="180"/>
<wire x1="-8.255" y1="2.54" x2="-8.255" y2="5.08" width="0.1524" layer="94" curve="180"/>
<wire x1="-8.255" y1="-2.54" x2="-8.255" y2="0" width="0.1524" layer="94" curve="180"/>
<wire x1="-8.255" y1="-5.08" x2="-8.255" y2="-2.54" width="0.1524" layer="94" curve="180"/>
<wire x1="-5.715" y1="6.35" x2="-5.715" y2="-6.35" width="0.635" layer="94"/>
<wire x1="-4.445" y1="6.35" x2="-4.445" y2="-6.35" width="0.635" layer="94"/>
<wire x1="-1.905" y1="0" x2="-1.905" y2="2.54" width="0.1524" layer="94" curve="-180"/>
<wire x1="-1.905" y1="2.54" x2="-1.905" y2="5.08" width="0.1524" layer="94" curve="-180"/>
<wire x1="-1.905" y1="-2.54" x2="-1.905" y2="0" width="0.1524" layer="94" curve="-180"/>
<wire x1="-1.905" y1="-5.08" x2="-1.905" y2="-2.54" width="0.1524" layer="94" curve="-180"/>
<wire x1="8.89" y1="-3.175" x2="6.35" y2="-3.175" width="0.1524" layer="94" curve="180"/>
<wire x1="6.35" y1="-3.175" x2="3.81" y2="-3.175" width="0.1524" layer="94" curve="180"/>
<wire x1="3.81" y1="-3.175" x2="1.27" y2="-3.175" width="0.1524" layer="94" curve="180"/>
<wire x1="8.89" y1="3.175" x2="6.35" y2="3.175" width="0.1524" layer="94" curve="-180"/>
<wire x1="6.35" y1="3.175" x2="3.81" y2="3.175" width="0.1524" layer="94" curve="-180"/>
<wire x1="3.81" y1="3.175" x2="1.27" y2="3.175" width="0.1524" layer="94" curve="-180"/>
<wire x1="-1.905" y1="7.62" x2="1.27" y2="7.62" width="0.1524" layer="94"/>
<wire x1="1.27" y1="7.62" x2="1.27" y2="3.175" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-7.62" x2="-1.905" y2="-7.62" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-7.62" x2="1.27" y2="-3.175" width="0.1524" layer="94"/>
<wire x1="8.89" y1="3.175" x2="8.89" y2="7.62" width="0.1524" layer="94"/>
<wire x1="8.89" y1="7.62" x2="16.51" y2="7.62" width="0.1524" layer="94"/>
<wire x1="8.89" y1="-3.175" x2="8.89" y2="-7.62" width="0.1524" layer="94"/>
<wire x1="8.89" y1="-7.62" x2="16.51" y2="-7.62" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-0.635" x2="8.89" y2="-0.635" width="0.635" layer="94"/>
<wire x1="1.27" y1="0.635" x2="8.89" y2="0.635" width="0.635" layer="94"/>
<wire x1="16.51" y1="-15.24" x2="15.24" y2="-15.24" width="0.1524" layer="94"/>
<wire x1="15.24" y1="-15.24" x2="15.24" y2="-17.78" width="0.1524" layer="94"/>
<wire x1="15.24" y1="-17.78" x2="16.51" y2="-17.78" width="0.1524" layer="94"/>
<wire x1="16.51" y1="-22.86" x2="15.24" y2="-22.86" width="0.1524" layer="94"/>
<wire x1="15.24" y1="-22.86" x2="15.24" y2="-25.4" width="0.1524" layer="94"/>
<wire x1="15.24" y1="-25.4" x2="16.51" y2="-25.4" width="0.1524" layer="94"/>
<wire x1="-17.78" y1="40.64" x2="-17.78" y2="-17.78" width="0.254" layer="94"/>
<wire x1="-17.78" y1="-17.78" x2="-17.78" y2="-22.86" width="0.254" layer="94"/>
<wire x1="-17.78" y1="-22.86" x2="-17.78" y2="-25.4" width="0.254" layer="94"/>
<wire x1="-17.78" y1="-25.4" x2="-17.78" y2="-38.1" width="0.254" layer="94"/>
<wire x1="-17.78" y1="-38.1" x2="17.78" y2="-38.1" width="0.254" layer="94"/>
<wire x1="-17.78" y1="40.64" x2="17.78" y2="40.64" width="0.254" layer="94"/>
<wire x1="17.78" y1="40.64" x2="17.78" y2="29.21" width="0.254" layer="94"/>
<wire x1="17.78" y1="-38.1" x2="17.78" y2="-26.67" width="0.254" layer="94"/>
<wire x1="17.78" y1="-21.59" x2="17.78" y2="-19.05" width="0.254" layer="94"/>
<wire x1="17.78" y1="-13.97" x2="17.78" y2="-8.89" width="0.254" layer="94"/>
<wire x1="17.78" y1="-6.35" x2="17.78" y2="6.35" width="0.254" layer="94"/>
<wire x1="17.78" y1="8.89" x2="17.78" y2="11.43" width="0.254" layer="94"/>
<wire x1="17.78" y1="13.97" x2="17.78" y2="26.67" width="0.254" layer="94"/>
<wire x1="5.08" y1="-20.32" x2="7.62" y2="-20.32" width="0.1524" layer="94"/>
<wire x1="-8.255" y1="20.32" x2="-11.43" y2="20.32" width="0.1524" layer="94"/>
<wire x1="-11.43" y1="0" x2="-8.255" y2="0" width="0.1524" layer="94"/>
<wire x1="-8.255" y1="25.4" x2="-8.255" y2="27.94" width="0.1524" layer="94"/>
<wire x1="-8.255" y1="12.7" x2="-8.255" y2="15.24" width="0.1524" layer="94"/>
<wire x1="-8.255" y1="5.08" x2="-8.255" y2="7.62" width="0.1524" layer="94"/>
<wire x1="-8.255" y1="-7.62" x2="-8.255" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="5.08" x2="-1.905" y2="7.62" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="12.7" x2="-1.905" y2="15.24" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-7.62" x2="-1.905" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="25.4" x2="-1.905" y2="27.94" width="0.1524" layer="94"/>
<wire x1="11.557" y1="0" x2="11.557" y2="2.54" width="0.1524" layer="94" curve="-180"/>
<wire x1="11.557" y1="2.54" x2="11.557" y2="5.08" width="0.1524" layer="94" curve="-180"/>
<wire x1="11.557" y1="-2.54" x2="11.557" y2="0" width="0.1524" layer="94" curve="-180"/>
<wire x1="11.557" y1="-5.08" x2="11.557" y2="-2.54" width="0.1524" layer="94" curve="-180"/>
<wire x1="11.557" y1="5.08" x2="11.557" y2="7.62" width="0.1524" layer="94"/>
<wire x1="11.557" y1="-7.62" x2="11.557" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="11.557" y1="20.32" x2="11.557" y2="22.86" width="0.1524" layer="94" curve="-180"/>
<wire x1="11.557" y1="22.86" x2="11.557" y2="25.4" width="0.1524" layer="94" curve="-180"/>
<wire x1="11.557" y1="17.78" x2="11.557" y2="20.32" width="0.1524" layer="94" curve="-180"/>
<wire x1="11.557" y1="15.24" x2="11.557" y2="17.78" width="0.1524" layer="94" curve="-180"/>
<wire x1="11.557" y1="25.4" x2="11.557" y2="27.94" width="0.1524" layer="94"/>
<wire x1="11.557" y1="12.7" x2="11.557" y2="15.24" width="0.1524" layer="94"/>
<wire x1="12.7" y1="0" x2="11.557" y2="0" width="0.1524" layer="94"/>
<wire x1="12.7" y1="0" x2="12.7" y2="-15.24" width="0.1524" layer="94"/>
<wire x1="11.557" y1="20.32" x2="15.24" y2="20.32" width="0.1524" layer="94"/>
<wire x1="15.24" y1="20.32" x2="15.24" y2="-12.7" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-17.78" x2="7.62" y2="-17.78" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-15.24" x2="7.62" y2="-15.24" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-12.7" x2="7.62" y2="-12.7" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-19.812" x2="5.08" y2="-20.32" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-20.32" x2="5.08" y2="-20.828" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-19.812" x2="2.54" y2="-20.32" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-20.32" x2="2.54" y2="-20.828" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-20.828" x2="5.08" y2="-20.828" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-19.812" x2="5.08" y2="-19.812" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-20.32" x2="0" y2="-20.32" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-17.272" x2="5.08" y2="-17.78" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-17.78" x2="5.08" y2="-18.288" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-17.272" x2="2.54" y2="-17.78" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-17.78" x2="2.54" y2="-18.288" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-18.288" x2="5.08" y2="-18.288" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-17.272" x2="5.08" y2="-17.272" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-17.78" x2="0" y2="-17.78" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-14.732" x2="5.08" y2="-15.24" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-15.24" x2="5.08" y2="-15.748" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-14.732" x2="2.54" y2="-15.24" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-15.24" x2="2.54" y2="-15.748" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-15.748" x2="5.08" y2="-15.748" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-14.732" x2="5.08" y2="-14.732" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-15.24" x2="0" y2="-15.24" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-12.192" x2="5.08" y2="-12.7" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-12.7" x2="5.08" y2="-13.208" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-12.192" x2="2.54" y2="-12.7" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-12.7" x2="2.54" y2="-13.208" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-13.208" x2="5.08" y2="-13.208" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-12.192" x2="5.08" y2="-12.192" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-12.7" x2="0" y2="-12.7" width="0.1524" layer="94"/>
<wire x1="0" y1="-12.7" x2="0" y2="-15.24" width="0.1524" layer="94"/>
<wire x1="0" y1="-17.78" x2="0" y2="-15.24" width="0.1524" layer="94"/>
<wire x1="0" y1="-20.32" x2="0" y2="-17.78" width="0.1524" layer="94"/>
<wire x1="-6.604" y1="-17.78" x2="0" y2="-17.78" width="0.1524" layer="94"/>
<wire x1="-17.78" y1="-17.78" x2="-7.62" y2="-17.78" width="0.1524" layer="94"/>
<wire x1="12.192" y1="34.29" x2="13.462" y2="36.83" width="0.254" layer="94"/>
<wire x1="13.462" y1="36.83" x2="14.732" y2="34.29" width="0.254" layer="94"/>
<wire x1="12.192" y1="36.83" x2="13.462" y2="36.83" width="0.254" layer="94"/>
<wire x1="13.462" y1="36.83" x2="14.732" y2="36.83" width="0.254" layer="94"/>
<wire x1="12.192" y1="34.29" x2="13.462" y2="34.29" width="0.254" layer="94"/>
<wire x1="13.462" y1="34.29" x2="14.732" y2="34.29" width="0.254" layer="94"/>
<wire x1="13.462" y1="34.29" x2="13.462" y2="36.83" width="0.1524" layer="94"/>
<wire x1="15.494" y1="35.052" x2="16.891" y2="36.449" width="0.1524" layer="94"/>
<wire x1="15.367" y1="36.195" x2="16.764" y2="37.592" width="0.1524" layer="94"/>
<wire x1="13.462" y1="38.1" x2="13.462" y2="36.83" width="0.1524" layer="94"/>
<wire x1="13.462" y1="33.02" x2="13.462" y2="34.29" width="0.1524" layer="94"/>
<wire x1="10.16" y1="38.1" x2="13.462" y2="38.1" width="0.1524" layer="94"/>
<wire x1="13.462" y1="33.02" x2="10.16" y2="33.02" width="0.1524" layer="94"/>
<circle x="-8.255" y="24.13" radius="0.381" width="0" layer="94"/>
<circle x="-1.905" y="24.13" radius="0.381" width="0" layer="94"/>
<circle x="2.54" y="17.145" radius="0.381" width="0" layer="94"/>
<circle x="2.54" y="23.495" radius="0.381" width="0" layer="94"/>
<circle x="-8.255" y="3.81" radius="0.381" width="0" layer="94"/>
<circle x="-1.905" y="3.81" radius="0.381" width="0" layer="94"/>
<circle x="2.54" y="-3.175" radius="0.381" width="0" layer="94"/>
<circle x="2.54" y="3.175" radius="0.381" width="0" layer="94"/>
<circle x="17.145" y="27.94" radius="0.635" width="0.1524" layer="94"/>
<circle x="17.145" y="12.7" radius="0.635" width="0.1524" layer="94"/>
<circle x="17.145" y="7.62" radius="0.635" width="0.1524" layer="94"/>
<circle x="17.145" y="-7.62" radius="0.635" width="0.1524" layer="94"/>
<circle x="17.145" y="-15.24" radius="0.635" width="0.1524" layer="94"/>
<circle x="17.145" y="-17.78" radius="0.635" width="0.1524" layer="94"/>
<circle x="17.145" y="-22.86" radius="0.635" width="0.1524" layer="94"/>
<circle x="17.145" y="-25.4" radius="0.635" width="0.1524" layer="94"/>
<circle x="15.24" y="-22.86" radius="0.381" width="0" layer="94"/>
<circle x="15.24" y="-17.78" radius="0.381" width="0" layer="94"/>
<circle x="11.557" y="3.81" radius="0.381" width="0" layer="94"/>
<circle x="11.557" y="24.13" radius="0.381" width="0" layer="94"/>
<circle x="11.557" y="27.94" radius="0.381" width="0" layer="94"/>
<circle x="11.557" y="12.7" radius="0.381" width="0" layer="94"/>
<circle x="11.557" y="7.62" radius="0.381" width="0" layer="94"/>
<circle x="11.557" y="-7.62" radius="0.381" width="0" layer="94"/>
<circle x="0" y="-17.78" radius="0.381" width="0" layer="94"/>
<circle x="0" y="-17.78" radius="0.381" width="0" layer="94"/>
<circle x="0" y="-15.24" radius="0.381" width="0" layer="94"/>
<circle x="-17.78" y="-25.4" radius="0.381" width="0" layer="94"/>
<circle x="-17.78" y="-22.86" radius="0.381" width="0" layer="94"/>
<text x="-2.54" y="35.56" size="1.778" layer="94">LED1</text>
<text x="-5.08" y="-33.02" size="1.778" layer="94">LED2</text>
<text x="19.05" y="27.178" size="1.524" layer="94">1 TX+</text>
<text x="19.05" y="11.938" size="1.524" layer="94">2 TX-</text>
<text x="19.05" y="6.858" size="1.524" layer="94">3 RX+</text>
<text x="19.05" y="-8.382" size="1.524" layer="94">6 RX-</text>
<text x="19.05" y="-16.002" size="1.524" layer="94">4</text>
<text x="19.05" y="-18.542" size="1.524" layer="94">5</text>
<text x="19.05" y="-23.622" size="1.524" layer="94">7</text>
<text x="19.05" y="-26.162" size="1.524" layer="94">8</text>
<text x="-17.78" y="41.91" size="1.778" layer="95">&gt;NAME</text>
<text x="-17.78" y="-40.64" size="1.778" layer="96">&gt;VALUE</text>
<text x="2.54" y="-19.685" size="1.016" layer="94">75R</text>
<text x="2.54" y="-17.145" size="1.016" layer="94">75R</text>
<text x="2.54" y="-14.605" size="1.016" layer="94">75R</text>
<text x="2.54" y="-12.065" size="1.016" layer="94">75R</text>
<text x="-8.255" y="-15.875" size="1.016" layer="94">1 nF</text>
<text x="15.24" y="38.862" size="1.27" layer="94">G</text>
<rectangle x1="-7.62" y1="-19.05" x2="-7.366" y2="-16.51" layer="94"/>
<rectangle x1="-6.858" y1="-19.05" x2="-6.604" y2="-16.51" layer="94"/>
<polygon width="0.1524" layer="94">
<vertex x="11.811" y="-33.909"/>
<vertex x="12.192" y="-33.02"/>
<vertex x="12.7" y="-33.528"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="11.938" y="-35.052"/>
<vertex x="12.319" y="-34.163"/>
<vertex x="12.827" y="-34.671"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="16.891" y="36.449"/>
<vertex x="16.51" y="35.56"/>
<vertex x="16.002" y="36.068"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="16.764" y="37.592"/>
<vertex x="16.383" y="36.703"/>
<vertex x="15.875" y="37.211"/>
</polygon>
<wire x1="15.24" y1="-12.7" x2="7.62" y2="-12.7" width="0.1524" layer="94"/>
<wire x1="12.7" y1="-15.24" x2="7.62" y2="-15.24" width="0.1524" layer="94"/>
<wire x1="15.24" y1="-17.78" x2="7.62" y2="-17.78" width="0.1524" layer="94"/>
<wire x1="7.62" y1="-20.32" x2="15.24" y2="-20.32" width="0.1524" layer="94"/>
<wire x1="15.24" y1="-20.32" x2="15.24" y2="-22.86" width="0.1524" layer="94"/>
</symbol>
<symbol name="PINHD2">
<wire x1="-6.35" y1="-2.54" x2="1.27" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.27" y2="5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="5.08" x2="-6.35" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="5.08" x2="-6.35" y2="-2.54" width="0.4064" layer="94"/>
<text x="-6.35" y="5.715" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
<symbol name="USB-1X90">
<wire x1="-6.35" y1="-3.175" x2="-6.35" y2="-0.3175" width="0.254" layer="94"/>
<wire x1="-6.35" y1="-0.3175" x2="-6.35" y2="0.3175" width="0.254" layer="94"/>
<wire x1="-6.35" y1="0.3175" x2="-6.35" y2="3.175" width="0.254" layer="94"/>
<wire x1="-6.35" y1="3.175" x2="8.89" y2="3.175" width="0.254" layer="94"/>
<wire x1="8.89" y1="3.175" x2="8.89" y2="0.3175" width="0.254" layer="94"/>
<wire x1="8.89" y1="0.3175" x2="8.89" y2="-0.3175" width="0.254" layer="94"/>
<wire x1="8.89" y1="-0.3175" x2="8.89" y2="-3.175" width="0.254" layer="94"/>
<wire x1="8.89" y1="-3.175" x2="-6.35" y2="-3.175" width="0.254" layer="94"/>
<wire x1="-2.2225" y1="0" x2="-2.8575" y2="0" width="0.6096" layer="94"/>
<wire x1="0.3175" y1="0" x2="-0.3175" y2="0" width="0.6096" layer="94"/>
<wire x1="2.8575" y1="0" x2="2.2225" y2="0" width="0.6096" layer="94"/>
<wire x1="5.3975" y1="0" x2="4.7625" y2="0" width="0.6096" layer="94"/>
<wire x1="-4.445" y1="0.3175" x2="6.985" y2="0.3175" width="0.254" layer="94"/>
<wire x1="6.985" y1="0.3175" x2="6.985" y2="1.905" width="0.254" layer="94"/>
<wire x1="6.985" y1="1.905" x2="-4.445" y2="1.905" width="0.254" layer="94"/>
<wire x1="-4.445" y1="1.905" x2="-4.445" y2="0.3175" width="0.254" layer="94"/>
<wire x1="-5.715" y1="-2.54" x2="8.255" y2="-2.54" width="0.254" layer="94"/>
<wire x1="8.255" y1="-2.54" x2="8.255" y2="2.54" width="0.254" layer="94"/>
<wire x1="8.255" y1="2.54" x2="-5.715" y2="2.54" width="0.254" layer="94"/>
<wire x1="-5.715" y1="2.54" x2="-5.715" y2="-2.54" width="0.254" layer="94"/>
<wire x1="7.9375" y1="-0.635" x2="7.9375" y2="0.4763" width="0.6096" layer="94"/>
<wire x1="-5.3975" y1="-0.6351" x2="-5.3975" y2="0.4763" width="0.6096" layer="94"/>
<wire x1="-6.35" y1="-0.3175" x2="-6.35" y2="0.3175" width="0.3048" layer="94" curve="-180"/>
<wire x1="8.89" y1="0.3175" x2="8.89" y2="-0.3175" width="0.3048" layer="94" curve="-180"/>
<text x="6.6675" y="-6.0325" size="1.778" layer="96" rot="R180">&gt;VALUE</text>
<text x="5.3975" y="-3.6195" size="1.778" layer="95" rot="R180">&gt;NAME</text>
<text x="-2.54" y="4.7625" size="1.27" layer="94" rot="R180">1</text>
<text x="0" y="4.7625" size="1.27" layer="94" rot="R180">2</text>
<text x="2.54" y="4.7625" size="1.27" layer="94" rot="R180">3</text>
<text x="3.81" y="3.4925" size="1.27" layer="94">4</text>
<text x="10.795" y="-0.635" size="1.27" layer="94" rot="R180">5</text>
<text x="-6.985" y="-0.635" size="1.27" layer="94" rot="R180">6</text>
<text x="-6.985" y="3.81" size="1.27" layer="96">VCC</text>
<text x="-1.27" y="-1.905" size="1.27" layer="96">D-</text>
<text x="1.5875" y="-1.905" size="1.27" layer="96">D+</text>
<text x="5.715" y="3.81" size="1.27" layer="96">GND</text>
<pin name="1" x="-2.54" y="5.08" visible="off" length="middle" direction="pwr" rot="R270"/>
<pin name="2" x="0" y="5.08" visible="off" length="middle" rot="R270"/>
<pin name="3" x="2.54" y="5.08" visible="off" length="middle" rot="R270"/>
<pin name="4" x="5.08" y="5.08" visible="off" length="middle" direction="pwr" rot="R270"/>
<pin name="5" x="10.16" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="6" x="-7.62" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="163-0179-EX">
<wire x1="-12.065" y1="7.62" x2="-12.065" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-12.065" y1="-7.62" x2="6.985" y2="-7.62" width="0.254" layer="94"/>
<wire x1="6.985" y1="-7.62" x2="6.985" y2="7.62" width="0.254" layer="94"/>
<wire x1="6.985" y1="7.62" x2="-12.065" y2="7.62" width="0.254" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="5.715" width="0.254" layer="94"/>
<wire x1="-2.54" y1="5.715" x2="-9.525" y2="5.715" width="0.254" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="4.445" width="0.254" layer="94"/>
<wire x1="-2.54" y1="4.445" x2="-9.525" y2="4.445" width="0.254" layer="94"/>
<wire x1="-9.525" y1="4.445" x2="-9.525" y2="5.715" width="0.254" layer="94" curve="-180"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-5.715" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-5.715" y1="-5.08" x2="-7.62" y2="0" width="0.254" layer="94"/>
<wire x1="-7.62" y1="0" x2="-9.525" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-3.175" y2="-3.81" width="0.254" layer="94"/>
<wire x1="-3.175" y1="-3.81" x2="-1.905" y2="-3.81" width="0.254" layer="94"/>
<wire x1="-1.905" y1="-3.81" x2="-2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="0" y1="5.08" x2="-2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="-2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="-5.08" x2="-2.54" y2="-5.08" width="0.254" layer="94"/>
<text x="1.905" y="-9.525" size="1.778" layer="96" rot="R180">&gt;VALUE</text>
<text x="1.27" y="10.668" size="1.778" layer="95" rot="R180">&gt;NAME</text>
<pin name="1" x="5.08" y="-5.08" visible="pad" length="short" direction="pwr" rot="R180"/>
<pin name="2" x="5.08" y="0" visible="pad" length="short" direction="pwr" rot="R180"/>
<pin name="3" x="5.08" y="5.08" visible="pad" length="short" direction="pwr" swaplevel="1" rot="R180"/>
<pin name="4" x="2.54" y="5.08" visible="pad" length="short" direction="pwr" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ARJC02-111009D" prefix="X">
<description>ETHernetový konektor RJ45</description>
<gates>
<gate name="G$1" symbol="ARJC02-111009D" x="0" y="0"/>
</gates>
<devices>
<device name="" package="ARJC02-111009D">
<connects>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="CHASSIS@1" pad="SH1"/>
<connect gate="G$1" pin="CHASSIS@2" pad="SH2"/>
<connect gate="G$1" pin="CT_R" pad="4"/>
<connect gate="G$1" pin="CT_T" pad="5"/>
<connect gate="G$1" pin="L1A" pad="9"/>
<connect gate="G$1" pin="L1K" pad="10"/>
<connect gate="G$1" pin="L2A" pad="12"/>
<connect gate="G$1" pin="L2K" pad="11"/>
<connect gate="G$1" pin="R+" pad="1"/>
<connect gate="G$1" pin="R-" pad="2"/>
<connect gate="G$1" pin="T+" pad="3"/>
<connect gate="G$1" pin="T-" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="DODANI" value="VEIT"/>
<attribute name="MELE" value="1289"/>
<attribute name="MPN" value="ARJC02-111009D"/>
<attribute name="OSAZENI" value="VEIT"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MKDSN1,5/2-5,08" prefix="X">
<description>Násuvná vidlice, 2pin, THT do DPS</description>
<gates>
<gate name="G$1" symbol="PINHD2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MKDSN1,5/2-5,08">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DODANI" value="VEIT"/>
<attribute name="MELE" value="1301"/>
<attribute name="MPN" value="STLZ950/2-5.08-V-GREEN"/>
<attribute name="OC_GM" value="821-087"/>
<attribute name="OSAZENI" value="VEIT" constant="no"/>
<attribute name="PRISLUSENSTVI" value="MELE1299"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="USB-1X90" prefix="CON">
<description>Konektor USB A do DPS, zásuvka 90°</description>
<gates>
<gate name="G$1" symbol="USB-1X90" x="0" y="0"/>
</gates>
<devices>
<device name="" package="USB1X90">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="DODANI" value="VEIT"/>
<attribute name="MELE" value="1349"/>
<attribute name="MPN" value="USB1X90"/>
<attribute name="OC_GM" value="832-111"/>
<attribute name="OSAZENI" value="VEIT"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="JACK2.1SMD" prefix="K">
<description>Napájecí konektor na DPS 5A/24V; SMD;</description>
<gates>
<gate name="G$1" symbol="163-0179-EX" x="2.54" y="0" swaplevel="1"/>
</gates>
<devices>
<device name="" package="163-0179-EX">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="DODANI" value="VEIT"/>
<attribute name="MELE" value="0300"/>
<attribute name="MPN" value="PJ-002AH-SMT-TR"/>
<attribute name="OSAZENI" value="RACOM"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="frames">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="DINA4_L">
<frame x1="0" y1="0" x2="264.16" y2="180.34" columns="4" rows="4" layer="94" border-left="no" border-top="no" border-right="no" border-bottom="no"/>
</symbol>
<symbol name="DOCFIELD">
<wire x1="0" y1="0" x2="71.12" y2="0" width="0.1016" layer="94"/>
<wire x1="101.6" y1="15.24" x2="87.63" y2="15.24" width="0.1016" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.1016" layer="94"/>
<wire x1="0" y1="5.08" x2="71.12" y2="5.08" width="0.1016" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="15.24" width="0.1016" layer="94"/>
<wire x1="101.6" y1="15.24" x2="101.6" y2="5.08" width="0.1016" layer="94"/>
<wire x1="71.12" y1="5.08" x2="71.12" y2="0" width="0.1016" layer="94"/>
<wire x1="71.12" y1="5.08" x2="87.63" y2="5.08" width="0.1016" layer="94"/>
<wire x1="71.12" y1="0" x2="101.6" y2="0" width="0.1016" layer="94"/>
<wire x1="87.63" y1="15.24" x2="87.63" y2="5.08" width="0.1016" layer="94"/>
<wire x1="87.63" y1="15.24" x2="0" y2="15.24" width="0.1016" layer="94"/>
<wire x1="87.63" y1="5.08" x2="101.6" y2="5.08" width="0.1016" layer="94"/>
<wire x1="101.6" y1="5.08" x2="101.6" y2="0" width="0.1016" layer="94"/>
<wire x1="0" y1="15.24" x2="0" y2="22.86" width="0.1016" layer="94"/>
<wire x1="101.6" y1="35.56" x2="0" y2="35.56" width="0.1016" layer="94"/>
<wire x1="101.6" y1="35.56" x2="101.6" y2="22.86" width="0.1016" layer="94"/>
<wire x1="0" y1="22.86" x2="101.6" y2="22.86" width="0.1016" layer="94"/>
<wire x1="0" y1="22.86" x2="0" y2="35.56" width="0.1016" layer="94"/>
<wire x1="101.6" y1="22.86" x2="101.6" y2="15.24" width="0.1016" layer="94"/>
<text x="1.27" y="1.27" size="2.54" layer="94">Date:</text>
<text x="12.7" y="1.27" size="2.54" layer="94">&gt;LAST_DATE_TIME</text>
<text x="72.39" y="1.27" size="2.54" layer="94">Sheet:</text>
<text x="86.36" y="1.27" size="2.54" layer="94">&gt;SHEET</text>
<text x="88.9" y="11.43" size="2.54" layer="94">REV:</text>
<text x="1.27" y="19.05" size="2.54" layer="94">TITLE:</text>
<text x="1.27" y="11.43" size="2.54" layer="94">Document Number:</text>
<text x="17.78" y="19.05" size="2.54" layer="94">&gt;DRAWING_NAME</text>
</symbol>
<symbol name="DINA3_L">
<frame x1="0" y1="0" x2="388.62" y2="264.16" columns="4" rows="4" layer="94" border-left="no" border-top="no" border-right="no" border-bottom="no"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="DINA4_L" prefix="FRAME" uservalue="yes">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A4, landscape with extra doc field</description>
<gates>
<gate name="G$1" symbol="DINA4_L" x="0" y="0"/>
<gate name="G$2" symbol="DOCFIELD" x="162.56" y="0" addlevel="must"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DINA3_L" prefix="FRAME" uservalue="yes">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A3, landscape with extra doc field</description>
<gates>
<gate name="G$1" symbol="DINA3_L" x="0" y="0"/>
<gate name="G$2" symbol="DOCFIELD" x="287.02" y="0" addlevel="must"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="CMI">
<description>Project library CMI</description>
<packages>
<package name="SO-16">
<wire x1="4.699" y1="1.9558" x2="-4.699" y2="1.9558" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-1.9558" x2="5.08" y2="-1.5748" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.5748" x2="-4.699" y2="1.9558" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="1.9558" x2="5.08" y2="1.5748" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="-1.5748" x2="-4.699" y2="-1.9558" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.699" y1="-1.9558" x2="4.699" y2="-1.9558" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.5748" x2="5.08" y2="1.5748" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.5748" x2="-5.08" y2="0.508" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.508" x2="-5.08" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-0.508" x2="-5.08" y2="-1.5748" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.508" x2="-5.08" y2="-0.508" width="0.1524" layer="21" curve="-180"/>
<wire x1="-5.08" y1="-1.6002" x2="5.08" y2="-1.6002" width="0.0508" layer="21"/>
<smd name="1" x="-4.445" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="16" x="-4.445" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="2" x="-3.175" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="3" x="-1.905" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="15" x="-3.175" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="14" x="-1.905" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="4" x="-0.635" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="13" x="-0.635" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="5" x="0.635" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="12" x="0.635" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="6" x="1.905" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="7" x="3.175" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="11" x="1.905" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="10" x="3.175" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="8" x="4.445" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="9" x="4.445" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<text x="-4.064" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-5.461" y="-1.778" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<rectangle x1="-0.889" y1="1.9558" x2="-0.381" y2="3.0988" layer="51"/>
<rectangle x1="-4.699" y1="-3.0988" x2="-4.191" y2="-1.9558" layer="51"/>
<rectangle x1="-3.429" y1="-3.0988" x2="-2.921" y2="-1.9558" layer="51"/>
<rectangle x1="-2.159" y1="-3.0734" x2="-1.651" y2="-1.9304" layer="51"/>
<rectangle x1="-0.889" y1="-3.0988" x2="-0.381" y2="-1.9558" layer="51"/>
<rectangle x1="-2.159" y1="1.9558" x2="-1.651" y2="3.0988" layer="51"/>
<rectangle x1="-3.429" y1="1.9558" x2="-2.921" y2="3.0988" layer="51"/>
<rectangle x1="-4.699" y1="1.9558" x2="-4.191" y2="3.0988" layer="51"/>
<rectangle x1="0.381" y1="-3.0988" x2="0.889" y2="-1.9558" layer="51"/>
<rectangle x1="1.651" y1="-3.0988" x2="2.159" y2="-1.9558" layer="51"/>
<rectangle x1="2.921" y1="-3.0988" x2="3.429" y2="-1.9558" layer="51"/>
<rectangle x1="4.191" y1="-3.0988" x2="4.699" y2="-1.9558" layer="51"/>
<rectangle x1="0.381" y1="1.9558" x2="0.889" y2="3.0988" layer="51"/>
<rectangle x1="1.651" y1="1.9558" x2="2.159" y2="3.0988" layer="51"/>
<rectangle x1="2.921" y1="1.9558" x2="3.429" y2="3.0988" layer="51"/>
<rectangle x1="4.191" y1="1.9558" x2="4.699" y2="3.0988" layer="51"/>
</package>
<package name="SOT23-6L">
<description>&lt;b&gt;Small Outline Transistor&lt;/b&gt;</description>
<wire x1="1.422" y1="0.81" x2="1.422" y2="-0.81" width="0.1524" layer="21"/>
<wire x1="1.422" y1="-0.81" x2="-1.422" y2="-0.81" width="0.1524" layer="51"/>
<wire x1="-1.422" y1="-0.81" x2="-1.422" y2="0.81" width="0.1524" layer="21"/>
<wire x1="-1.422" y1="0.81" x2="1.422" y2="0.81" width="0.1524" layer="51"/>
<wire x1="-0.428" y1="-0.81" x2="-0.522" y2="-0.81" width="0.1524" layer="21"/>
<wire x1="0.522" y1="-0.81" x2="0.428" y2="-0.81" width="0.1524" layer="21"/>
<wire x1="-1.328" y1="-0.81" x2="-1.422" y2="-0.81" width="0.1524" layer="21"/>
<wire x1="1.422" y1="-0.81" x2="1.328" y2="-0.81" width="0.1524" layer="21"/>
<wire x1="1.328" y1="0.81" x2="1.422" y2="0.81" width="0.1524" layer="21"/>
<wire x1="-1.422" y1="0.81" x2="-1.328" y2="0.81" width="0.1524" layer="21"/>
<wire x1="0.428" y1="0.81" x2="0.522" y2="0.81" width="0.1524" layer="21"/>
<wire x1="-0.522" y1="0.81" x2="-0.428" y2="0.81" width="0.1524" layer="21"/>
<circle x="-1" y="-0.45" radius="0.1" width="0" layer="21"/>
<smd name="1" x="-0.95" y="-1.3" dx="0.6" dy="1.2" layer="1" roundness="20"/>
<smd name="2" x="0" y="-1.3" dx="0.6" dy="1.2" layer="1" roundness="20"/>
<smd name="3" x="0.95" y="-1.3" dx="0.6" dy="1.2" layer="1" roundness="20"/>
<smd name="4" x="0.95" y="1.3" dx="0.6" dy="1.2" layer="1" roundness="20"/>
<smd name="5" x="0" y="1.3" dx="0.6" dy="1.2" layer="1" roundness="20"/>
<smd name="6" x="-0.95" y="1.3" dx="0.6" dy="1.2" layer="1" roundness="20"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.2" y1="-1.5" x2="-0.7" y2="-0.85" layer="51"/>
<rectangle x1="-0.25" y1="-1.5" x2="0.25" y2="-0.85" layer="51"/>
<rectangle x1="0.7" y1="-1.5" x2="1.2" y2="-0.85" layer="51"/>
<rectangle x1="0.7" y1="0.85" x2="1.2" y2="1.5" layer="51"/>
<rectangle x1="-0.25" y1="0.85" x2="0.25" y2="1.5" layer="51"/>
<rectangle x1="-1.2" y1="0.85" x2="-0.7" y2="1.5" layer="51"/>
</package>
<package name="SOT23-5L">
<description>&lt;b&gt;Small Outline Transistor&lt;/b&gt;&lt;p&gt;
package type OT</description>
<wire x1="1.422" y1="0.81" x2="1.422" y2="-0.81" width="0.1524" layer="21"/>
<wire x1="1.422" y1="-0.81" x2="-1.422" y2="-0.81" width="0.1524" layer="51"/>
<wire x1="-1.422" y1="-0.81" x2="-1.422" y2="0.81" width="0.1524" layer="21"/>
<wire x1="-1.422" y1="0.81" x2="1.422" y2="0.81" width="0.1524" layer="51"/>
<wire x1="-0.522" y1="0.81" x2="0.522" y2="0.81" width="0.1524" layer="21"/>
<wire x1="-0.428" y1="-0.81" x2="-0.522" y2="-0.81" width="0.1524" layer="21"/>
<wire x1="0.522" y1="-0.81" x2="0.428" y2="-0.81" width="0.1524" layer="21"/>
<wire x1="-1.328" y1="-0.81" x2="-1.422" y2="-0.81" width="0.1524" layer="21"/>
<wire x1="1.422" y1="-0.81" x2="1.328" y2="-0.81" width="0.1524" layer="21"/>
<wire x1="1.328" y1="0.81" x2="1.422" y2="0.81" width="0.1524" layer="21"/>
<wire x1="-1.422" y1="0.81" x2="-1.328" y2="0.81" width="0.1524" layer="21"/>
<smd name="1" x="-0.95" y="-1.3" dx="0.6" dy="1.2" layer="1" roundness="20"/>
<smd name="2" x="0" y="-1.3" dx="0.6" dy="1.2" layer="1" roundness="20"/>
<smd name="3" x="0.95" y="-1.3" dx="0.6" dy="1.2" layer="1" roundness="20"/>
<smd name="4" x="0.95" y="1.3" dx="0.6" dy="1.2" layer="1" roundness="20"/>
<smd name="5" x="-0.95" y="1.3" dx="0.6" dy="1.2" layer="1" roundness="20"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.429" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.2" y1="-1.5" x2="-0.7" y2="-0.85" layer="51"/>
<rectangle x1="-0.25" y1="-1.5" x2="0.25" y2="-0.85" layer="51"/>
<rectangle x1="0.7" y1="-1.5" x2="1.2" y2="-0.85" layer="51"/>
<rectangle x1="0.7" y1="0.85" x2="1.2" y2="1.5" layer="51"/>
<rectangle x1="-1.2" y1="0.85" x2="-0.7" y2="1.5" layer="51"/>
<circle x="-1" y="-0.45" radius="0.1" width="0" layer="21"/>
</package>
<package name="SMC">
<wire x1="-3.575" y1="3.125" x2="3.575" y2="3.125" width="0.127" layer="21"/>
<wire x1="3.575" y1="-3.125" x2="-3.575" y2="-3.125" width="0.127" layer="21"/>
<wire x1="-3.59" y1="-0.7777" x2="-3.5778" y2="0.9444" width="0.127" layer="21" curve="225.582798" cap="flat"/>
<wire x1="-3.575" y1="3.125" x2="-3.575" y2="0.9443" width="0.127" layer="21"/>
<wire x1="-3.575" y1="-3.125" x2="-3.575" y2="-0.7829" width="0.127" layer="21"/>
<wire x1="3.575" y1="3.125" x2="3.575" y2="-3.125" width="0.127" layer="21"/>
<wire x1="-3.1795" y1="2.7223" x2="3.2639" y2="2.7223" width="0.0508" layer="21"/>
<wire x1="3.2639" y1="2.7223" x2="3.2639" y2="-2.7559" width="0.0508" layer="21"/>
<wire x1="-3.1795" y1="-2.7559" x2="3.2639" y2="-2.7559" width="0.0508" layer="21"/>
<wire x1="-3.1795" y1="-2.7559" x2="-3.1795" y2="-0.8591" width="0.0508" layer="21"/>
<wire x1="-3.1795" y1="0.9951" x2="-3.1795" y2="2.7223" width="0.0508" layer="21"/>
<smd name="C" x="-3.325" y="0" dx="1.54" dy="3.14" layer="1"/>
<smd name="A" x="3.325" y="0" dx="1.54" dy="3.14" layer="1"/>
<text x="-3.5687" y="3.7735" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.5687" y="-5.0165" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="2.475" y1="-1.6" x2="4.075" y2="1.6" layer="51"/>
<rectangle x1="-4.075" y1="-1.6" x2="-2.475" y2="1.6" layer="51"/>
</package>
<package name="CTB9302/5">
<pad name="1" x="-10" y="0" drill="1.5" diameter="2.54" shape="long" rot="R90"/>
<pad name="2" x="-5" y="0" drill="1.5" diameter="2.54" shape="long" rot="R90"/>
<pad name="3" x="0" y="0" drill="1.5" diameter="2.54" shape="long" rot="R90"/>
<pad name="4" x="5" y="0" drill="1.5" diameter="2.54" shape="long" rot="R90"/>
<text x="-13" y="-4.5" size="1.778" layer="25" align="top-left">&gt;NAME</text>
<text x="13" y="-4.5" size="1.778" layer="27" align="top-right">&gt;VALUE</text>
<pad name="5" x="10" y="0" drill="1.5" diameter="2.54" shape="long" rot="R90"/>
<wire x1="13.1" y1="-3.7" x2="-13.1" y2="-3.7" width="0.127" layer="21"/>
<wire x1="13.1" y1="4.6" x2="-13.1" y2="4.6" width="0.127" layer="21"/>
<wire x1="-13.1" y1="-2.2" x2="-13.1" y2="2.9" width="0.127" layer="21"/>
<wire x1="-13.1" y1="2.9" x2="-13.1" y2="4.6" width="0.127" layer="21"/>
<wire x1="-13.1" y1="-2.2" x2="-13.1" y2="-3.7" width="0.127" layer="21"/>
<wire x1="13.1" y1="-2.2" x2="13.1" y2="2.9" width="0.127" layer="21"/>
<text x="0" y="-3.515" size="0.254" layer="49" align="bottom-center">Touto stranou k hraně DPS</text>
<wire x1="13.1" y1="2.9" x2="13.1" y2="4.6" width="0.127" layer="21"/>
<wire x1="13.1" y1="-2.2" x2="13.1" y2="-3.7" width="0.127" layer="21"/>
<wire x1="-13.1" y1="-2.2" x2="-12.5" y2="-2.2" width="0.127" layer="21"/>
<wire x1="-12.5" y1="-2.2" x2="-12.1" y2="-2.2" width="0.127" layer="21"/>
<wire x1="-7.9" y1="-2.2" x2="-7.1" y2="-2.2" width="0.127" layer="21"/>
<wire x1="-2.9" y1="-2.2" x2="-2.1" y2="-2.2" width="0.127" layer="21"/>
<wire x1="2.1" y1="-2.2" x2="2.9" y2="-2.2" width="0.127" layer="21"/>
<wire x1="7.1" y1="-2.2" x2="7.9" y2="-2.2" width="0.127" layer="21"/>
<wire x1="12.1" y1="-2.2" x2="12.5" y2="-2.2" width="0.127" layer="21"/>
<wire x1="12.5" y1="-2.2" x2="13.1" y2="-2.2" width="0.127" layer="21"/>
<wire x1="-13.1" y1="2.9" x2="-12.5" y2="2.9" width="0.127" layer="21"/>
<wire x1="-12.5" y1="2.9" x2="12.5" y2="2.9" width="0.127" layer="21"/>
<wire x1="12.5" y1="2.9" x2="13.1" y2="2.9" width="0.127" layer="21"/>
<wire x1="-12.5" y1="2.9" x2="-12.5" y2="-2.2" width="0.127" layer="21"/>
<wire x1="12.5" y1="2.9" x2="12.5" y2="-2.2" width="0.127" layer="21"/>
<wire x1="-2.1" y1="-2.2" x2="2.1" y2="-2.2" width="0.127" layer="21" curve="87.33556"/>
<wire x1="-7.1" y1="-2.2" x2="-2.9" y2="-2.2" width="0.127" layer="21" curve="87.33556"/>
<wire x1="-12.1" y1="-2.2" x2="-7.9" y2="-2.2" width="0.127" layer="21" curve="87.33556"/>
<wire x1="2.9" y1="-2.2" x2="7.1" y2="-2.2" width="0.127" layer="21" curve="87.33556"/>
<wire x1="7.9" y1="-2.2" x2="12.1" y2="-2.2" width="0.127" layer="21" curve="87.33556"/>
<wire x1="-1" y1="3.2" x2="1" y2="3.2" width="0.127" layer="51"/>
<wire x1="-1" y1="4.2" x2="1" y2="4.2" width="0.127" layer="51"/>
<wire x1="-1" y1="4.2" x2="-1" y2="4" width="0.127" layer="51"/>
<wire x1="-1" y1="4" x2="-1" y2="3.2" width="0.127" layer="51"/>
<wire x1="-1" y1="3.2" x2="-1" y2="2.9" width="0.127" layer="51"/>
<wire x1="1" y1="4.2" x2="1" y2="4" width="0.127" layer="51"/>
<wire x1="1" y1="4" x2="1" y2="3.2" width="0.127" layer="51"/>
<wire x1="1" y1="3.2" x2="1" y2="2.9" width="0.127" layer="51"/>
<wire x1="-6" y1="2.9" x2="-4" y2="2.9" width="0.127" layer="51"/>
<wire x1="-6" y1="3.2" x2="-4" y2="3.2" width="0.127" layer="51"/>
<wire x1="-6" y1="4.2" x2="-4" y2="4.2" width="0.127" layer="51"/>
<wire x1="-6" y1="4.2" x2="-6" y2="4" width="0.127" layer="51"/>
<wire x1="-6" y1="4" x2="-6" y2="3.2" width="0.127" layer="51"/>
<wire x1="-6" y1="3.2" x2="-6" y2="2.9" width="0.127" layer="51"/>
<wire x1="-4" y1="4.2" x2="-4" y2="4" width="0.127" layer="51"/>
<wire x1="-4" y1="4" x2="-4" y2="3.2" width="0.127" layer="51"/>
<wire x1="-4" y1="3.2" x2="-4" y2="2.9" width="0.127" layer="51"/>
<wire x1="-11" y1="2.9" x2="-9" y2="2.9" width="0.127" layer="51"/>
<wire x1="-11" y1="3.2" x2="-9" y2="3.2" width="0.127" layer="51"/>
<wire x1="-11" y1="4.2" x2="-9" y2="4.2" width="0.127" layer="51"/>
<wire x1="-11" y1="4.2" x2="-11" y2="4" width="0.127" layer="51"/>
<wire x1="-11" y1="4" x2="-11" y2="3.2" width="0.127" layer="51"/>
<wire x1="-11" y1="3.2" x2="-11" y2="2.9" width="0.127" layer="51"/>
<wire x1="-9" y1="4.2" x2="-9" y2="4" width="0.127" layer="51"/>
<wire x1="-9" y1="4" x2="-9" y2="3.2" width="0.127" layer="51"/>
<wire x1="-9" y1="3.2" x2="-9" y2="2.9" width="0.127" layer="51"/>
<wire x1="4" y1="2.9" x2="6" y2="2.9" width="0.127" layer="51"/>
<wire x1="4" y1="3.2" x2="6" y2="3.2" width="0.127" layer="51"/>
<wire x1="4" y1="4.2" x2="6" y2="4.2" width="0.127" layer="51"/>
<wire x1="4" y1="4.2" x2="4" y2="4" width="0.127" layer="51"/>
<wire x1="4" y1="4" x2="4" y2="3.2" width="0.127" layer="51"/>
<wire x1="4" y1="3.2" x2="4" y2="2.9" width="0.127" layer="51"/>
<wire x1="6" y1="4.2" x2="6" y2="4" width="0.127" layer="51"/>
<wire x1="6" y1="4" x2="6" y2="3.2" width="0.127" layer="51"/>
<wire x1="6" y1="3.2" x2="6" y2="2.9" width="0.127" layer="51"/>
<wire x1="9" y1="2.9" x2="11" y2="2.9" width="0.127" layer="51"/>
<wire x1="9" y1="3.2" x2="11" y2="3.2" width="0.127" layer="51"/>
<wire x1="9" y1="4.2" x2="11" y2="4.2" width="0.127" layer="51"/>
<wire x1="9" y1="4.2" x2="9" y2="4" width="0.127" layer="51"/>
<wire x1="9" y1="4" x2="9" y2="3.2" width="0.127" layer="51"/>
<wire x1="9" y1="3.2" x2="9" y2="2.9" width="0.127" layer="51"/>
<wire x1="11" y1="4.2" x2="11" y2="4" width="0.127" layer="51"/>
<wire x1="11" y1="4" x2="11" y2="3.2" width="0.127" layer="51"/>
<wire x1="11" y1="3.2" x2="11" y2="2.9" width="0.127" layer="51"/>
<wire x1="-1" y1="4" x2="-4" y2="4" width="0.127" layer="51"/>
<wire x1="-6" y1="4" x2="-9" y2="4" width="0.127" layer="51"/>
<wire x1="-11" y1="4" x2="-13.1" y2="4" width="0.127" layer="51"/>
<wire x1="1" y1="4" x2="4" y2="4" width="0.127" layer="51"/>
<wire x1="6" y1="4" x2="9" y2="4" width="0.127" layer="51"/>
<wire x1="11" y1="4" x2="13.1" y2="4" width="0.127" layer="51"/>
<rectangle x1="-0.5" y1="-0.5" x2="0.5" y2="0.5" layer="51"/>
<rectangle x1="4.5" y1="-0.5" x2="5.5" y2="0.5" layer="51"/>
<rectangle x1="9.5" y1="-0.5" x2="10.5" y2="0.5" layer="51"/>
<rectangle x1="-5.5" y1="-0.5" x2="-4.5" y2="0.5" layer="51"/>
<rectangle x1="-10.5" y1="-0.5" x2="-9.5" y2="0.5" layer="51"/>
<circle x="0" y="0" radius="1.23693125" width="0.127" layer="51"/>
<wire x1="-0.3" y1="1.2" x2="-0.3" y2="2.9" width="0.127" layer="51"/>
<wire x1="0.3" y1="1.2" x2="0.3" y2="2.9" width="0.127" layer="51"/>
<wire x1="0.3" y1="-1.2" x2="0.3" y2="-3" width="0.127" layer="51"/>
<wire x1="-0.3" y1="-1.2" x2="-0.3" y2="-3" width="0.127" layer="51"/>
<circle x="-5" y="0" radius="1.23693125" width="0.127" layer="51"/>
<wire x1="-5.3" y1="1.2" x2="-5.3" y2="2.9" width="0.127" layer="51"/>
<wire x1="-4.7" y1="1.2" x2="-4.7" y2="2.9" width="0.127" layer="51"/>
<wire x1="-4.7" y1="-1.2" x2="-4.7" y2="-3" width="0.127" layer="51"/>
<wire x1="-5.3" y1="-1.2" x2="-5.3" y2="-3" width="0.127" layer="51"/>
<circle x="-10" y="0" radius="1.23693125" width="0.127" layer="51"/>
<wire x1="-10.3" y1="1.2" x2="-10.3" y2="2.9" width="0.127" layer="51"/>
<wire x1="-9.7" y1="1.2" x2="-9.7" y2="2.9" width="0.127" layer="51"/>
<wire x1="-9.7" y1="-1.2" x2="-9.7" y2="-3" width="0.127" layer="51"/>
<wire x1="-10.3" y1="-1.2" x2="-10.3" y2="-3" width="0.127" layer="51"/>
<circle x="5" y="0" radius="1.23693125" width="0.127" layer="51"/>
<wire x1="4.7" y1="1.2" x2="4.7" y2="2.9" width="0.127" layer="51"/>
<wire x1="5.3" y1="1.2" x2="5.3" y2="2.9" width="0.127" layer="51"/>
<wire x1="5.3" y1="-1.2" x2="5.3" y2="-3" width="0.127" layer="51"/>
<wire x1="4.7" y1="-1.2" x2="4.7" y2="-3" width="0.127" layer="51"/>
<circle x="10" y="0" radius="1.23693125" width="0.127" layer="51"/>
<wire x1="9.7" y1="1.2" x2="9.7" y2="2.9" width="0.127" layer="51"/>
<wire x1="10.3" y1="1.2" x2="10.3" y2="2.9" width="0.127" layer="51"/>
<wire x1="10.3" y1="-1.2" x2="10.3" y2="-3" width="0.127" layer="51"/>
<wire x1="9.7" y1="-1.2" x2="9.7" y2="-3" width="0.127" layer="51"/>
<wire x1="-1" y1="2.9" x2="1" y2="2.9" width="0.127" layer="51"/>
<text x="-10" y="3.7" size="0.6096" layer="51" align="center">1</text>
<text x="-5" y="3.7" size="0.6096" layer="51" align="center">2</text>
<text x="0" y="3.7" size="0.6096" layer="51" align="center">3</text>
<text x="5" y="3.7" size="0.6096" layer="51" align="center">4</text>
<text x="10" y="3.7" size="0.6096" layer="51" align="center">5</text>
</package>
<package name="SMA">
<wire x1="-2.3" y1="1.45" x2="2.3" y2="1.45" width="0.254" layer="51"/>
<wire x1="2.3" y1="1.45" x2="2.3" y2="-1.45" width="0.254" layer="51"/>
<wire x1="2.3" y1="-1.45" x2="-2.3" y2="-1.45" width="0.254" layer="51"/>
<wire x1="-2.3" y1="-1.45" x2="-2.3" y2="1.45" width="0.254" layer="51"/>
<wire x1="-0.5" y1="0" x2="0.55" y2="0.75" width="0.254" layer="21"/>
<wire x1="0.55" y1="0.75" x2="0.55" y2="-0.7" width="0.254" layer="21"/>
<wire x1="0.55" y1="-0.7" x2="-0.5" y2="0" width="0.254" layer="21"/>
<smd name="C" x="-2.015" y="0" dx="1.4" dy="1.64" layer="1"/>
<smd name="A" x="2.015" y="0" dx="1.4" dy="1.64" layer="1"/>
<text x="0" y="1.8" size="1.27" layer="25" ratio="10" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.8" size="1.27" layer="27" ratio="10" align="top-center">&gt;VALUE</text>
<rectangle x1="-2.675" y1="-0.825" x2="-1.5" y2="0.825" layer="51"/>
<rectangle x1="1.5" y1="-0.825" x2="2.675" y2="0.825" layer="51"/>
<rectangle x1="-0.7" y1="-1.05" x2="-0.4" y2="1.05" layer="21"/>
</package>
<package name="B3F-10XX">
<description>&lt;b&gt;OMRON SWITCH&lt;/b&gt;</description>
<wire x1="3.302" y1="-0.762" x2="3.048" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-0.762" x2="3.302" y2="0.762" width="0.1524" layer="21"/>
<wire x1="3.048" y1="0.762" x2="3.302" y2="0.762" width="0.1524" layer="21"/>
<wire x1="3.048" y1="1.016" x2="3.048" y2="2.54" width="0.1524" layer="51"/>
<wire x1="-3.302" y1="0.762" x2="-3.048" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="0.762" x2="-3.302" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-3.048" y1="-0.762" x2="-3.302" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="3.048" y1="2.54" x2="2.54" y2="3.048" width="0.1524" layer="51"/>
<wire x1="2.54" y1="-3.048" x2="3.048" y2="-2.54" width="0.1524" layer="51"/>
<wire x1="3.048" y1="-2.54" x2="3.048" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="3.048" x2="-3.048" y2="2.54" width="0.1524" layer="51"/>
<wire x1="-3.048" y1="2.54" x2="-3.048" y2="1.016" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="-3.048" x2="-3.048" y2="-2.54" width="0.1524" layer="51"/>
<wire x1="-3.048" y1="-2.54" x2="-3.048" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.0508" layer="51"/>
<wire x1="1.27" y1="-1.27" x2="-1.27" y2="-1.27" width="0.0508" layer="51"/>
<wire x1="1.27" y1="-1.27" x2="1.27" y2="1.27" width="0.0508" layer="51"/>
<wire x1="-1.27" y1="1.27" x2="1.27" y2="1.27" width="0.0508" layer="51"/>
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="2.794" width="0.0508" layer="21"/>
<wire x1="1.27" y1="2.794" x2="-1.27" y2="2.794" width="0.0508" layer="21"/>
<wire x1="1.27" y1="2.794" x2="1.27" y2="3.048" width="0.0508" layer="21"/>
<wire x1="1.143" y1="-2.794" x2="-1.27" y2="-2.794" width="0.0508" layer="21"/>
<wire x1="1.143" y1="-2.794" x2="1.143" y2="-3.048" width="0.0508" layer="21"/>
<wire x1="-1.27" y1="-2.794" x2="-1.27" y2="-3.048" width="0.0508" layer="21"/>
<wire x1="2.54" y1="-3.048" x2="2.159" y2="-3.048" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="-3.048" x2="-2.159" y2="-3.048" width="0.1524" layer="51"/>
<wire x1="-2.159" y1="-3.048" x2="-1.27" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="3.048" x2="-2.159" y2="3.048" width="0.1524" layer="51"/>
<wire x1="2.54" y1="3.048" x2="2.159" y2="3.048" width="0.1524" layer="51"/>
<wire x1="2.159" y1="3.048" x2="1.27" y2="3.048" width="0.1524" layer="21"/>
<wire x1="1.27" y1="3.048" x2="-1.27" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="3.048" x2="-2.159" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-3.048" x2="1.143" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-3.048" x2="2.159" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="3.048" y1="-0.762" x2="3.048" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="3.048" y1="0.762" x2="3.048" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-3.048" y1="-0.762" x2="-3.048" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-3.048" y1="0.762" x2="-3.048" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-2.159" x2="1.27" y2="-2.159" width="0.1524" layer="51"/>
<wire x1="1.27" y1="2.286" x2="-1.27" y2="2.286" width="0.1524" layer="51"/>
<wire x1="-2.413" y1="1.27" x2="-2.413" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-2.413" y1="-0.508" x2="-2.413" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-2.413" y1="0.508" x2="-2.159" y2="-0.381" width="0.1524" layer="51"/>
<circle x="0" y="0" radius="1.778" width="0.1524" layer="21"/>
<circle x="-2.159" y="-2.159" radius="0.508" width="0.1524" layer="51"/>
<circle x="2.159" y="-2.032" radius="0.508" width="0.1524" layer="51"/>
<circle x="2.159" y="2.159" radius="0.508" width="0.1524" layer="51"/>
<circle x="-2.159" y="2.159" radius="0.508" width="0.1524" layer="51"/>
<circle x="0" y="0" radius="0.635" width="0.0508" layer="51"/>
<circle x="0" y="0" radius="0.254" width="0.1524" layer="21"/>
<pad name="1" x="-3.2512" y="2.2606" drill="1" shape="long"/>
<pad name="3" x="-3.2512" y="-2.2606" drill="1" shape="long"/>
<pad name="2" x="3.2512" y="2.2606" drill="1" shape="long"/>
<pad name="4" x="3.2512" y="-2.2606" drill="1" shape="long"/>
<text x="-3.048" y="3.683" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.048" y="-5.08" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-4.318" y="1.651" size="1.27" layer="51" ratio="10">1</text>
<text x="3.556" y="1.524" size="1.27" layer="51" ratio="10">2</text>
<text x="-4.572" y="-2.794" size="1.27" layer="51" ratio="10">3</text>
<text x="3.556" y="-2.794" size="1.27" layer="51" ratio="10">4</text>
</package>
<package name="1X06">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="0.635" y1="1.27" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="4.445" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0.635" x2="5.08" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-0.635" x2="4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.27" x2="-2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="1.27" x2="-5.715" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="1.27" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.635" x2="-5.08" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-0.635" x2="-5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.635" x2="-4.445" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="1.27" x2="-3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-0.635" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-1.27" x2="-4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="-1.27" x2="-5.08" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0.635" x2="-7.62" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="1.27" x2="-7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="-0.635" x2="-6.985" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-1.27" x2="-6.985" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.985" y2="1.27" width="0.1524" layer="21"/>
<wire x1="6.985" y1="1.27" x2="7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="7.62" y1="0.635" x2="7.62" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-0.635" x2="6.985" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="5.715" y1="1.27" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-0.635" x2="5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-1.27" x2="5.715" y2="-1.27" width="0.1524" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="-3.81" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="-1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="5" x="3.81" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="6" x="6.35" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-7.6962" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-7.62" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="3.556" y1="-0.254" x2="4.064" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="-4.064" y1="-0.254" x2="-3.556" y2="0.254" layer="51"/>
<rectangle x1="-6.604" y1="-0.254" x2="-6.096" y2="0.254" layer="51"/>
<rectangle x1="6.096" y1="-0.254" x2="6.604" y2="0.254" layer="51"/>
</package>
<package name="1X06/90">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-7.62" y1="-1.905" x2="-5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.635" x2="-7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0.635" x2="-7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="6.985" x2="-6.35" y2="1.27" width="0.762" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="6.985" x2="-3.81" y2="1.27" width="0.762" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="6.985" x2="-1.27" y2="1.27" width="0.762" layer="21"/>
<wire x1="0" y1="-1.905" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="6.985" x2="1.27" y2="1.27" width="0.762" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0.635" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="6.985" x2="3.81" y2="1.27" width="0.762" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-1.905" x2="7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="7.62" y1="0.635" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="6.35" y1="6.985" x2="6.35" y2="1.27" width="0.762" layer="21"/>
<pad name="1" x="-6.35" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="-3.81" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="-1.27" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="1.27" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="5" x="3.81" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="6" x="6.35" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<text x="-8.255" y="-3.81" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="9.525" y="-3.81" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-6.731" y1="0.635" x2="-5.969" y2="1.143" layer="21"/>
<rectangle x1="-4.191" y1="0.635" x2="-3.429" y2="1.143" layer="21"/>
<rectangle x1="-1.651" y1="0.635" x2="-0.889" y2="1.143" layer="21"/>
<rectangle x1="0.889" y1="0.635" x2="1.651" y2="1.143" layer="21"/>
<rectangle x1="3.429" y1="0.635" x2="4.191" y2="1.143" layer="21"/>
<rectangle x1="5.969" y1="0.635" x2="6.731" y2="1.143" layer="21"/>
<rectangle x1="-6.731" y1="-2.921" x2="-5.969" y2="-1.905" layer="21"/>
<rectangle x1="-4.191" y1="-2.921" x2="-3.429" y2="-1.905" layer="21"/>
<rectangle x1="-1.651" y1="-2.921" x2="-0.889" y2="-1.905" layer="21"/>
<rectangle x1="0.889" y1="-2.921" x2="1.651" y2="-1.905" layer="21"/>
<rectangle x1="3.429" y1="-2.921" x2="4.191" y2="-1.905" layer="21"/>
<rectangle x1="5.969" y1="-2.921" x2="6.731" y2="-1.905" layer="21"/>
</package>
<package name="06/90SMD">
<wire x1="-7.62" y1="-1.905" x2="-5.08" y2="-1.905" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-5.08" y2="0.635" width="0.127" layer="21"/>
<wire x1="-5.08" y1="0.635" x2="-7.62" y2="0.635" width="0.127" layer="21"/>
<wire x1="-7.62" y1="0.635" x2="-7.62" y2="-1.905" width="0.127" layer="21"/>
<wire x1="-6.35" y1="8.255" x2="-6.35" y2="1.27" width="0.762" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-2.54" y2="-1.905" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="-2.54" y2="0.635" width="0.127" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-5.08" y2="0.635" width="0.127" layer="21"/>
<wire x1="-3.81" y1="8.255" x2="-3.81" y2="1.27" width="0.762" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="0" y2="-1.905" width="0.127" layer="21"/>
<wire x1="0" y1="-1.905" x2="0" y2="0.635" width="0.127" layer="21"/>
<wire x1="0" y1="0.635" x2="-2.54" y2="0.635" width="0.127" layer="21"/>
<wire x1="-1.27" y1="8.255" x2="-1.27" y2="1.27" width="0.762" layer="21"/>
<wire x1="0" y1="-1.905" x2="2.54" y2="-1.905" width="0.127" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="0.635" width="0.127" layer="21"/>
<wire x1="2.54" y1="0.635" x2="0" y2="0.635" width="0.127" layer="21"/>
<wire x1="1.27" y1="8.255" x2="1.27" y2="1.27" width="0.762" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="5.08" y2="-1.905" width="0.127" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="5.08" y2="0.635" width="0.127" layer="21"/>
<wire x1="5.08" y1="0.635" x2="2.54" y2="0.635" width="0.127" layer="21"/>
<wire x1="3.81" y1="8.255" x2="3.81" y2="1.27" width="0.762" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="7.62" y2="-1.905" width="0.127" layer="21"/>
<wire x1="7.62" y1="-1.905" x2="7.62" y2="0.635" width="0.127" layer="21"/>
<wire x1="7.62" y1="0.635" x2="5.08" y2="0.635" width="0.127" layer="21"/>
<wire x1="6.35" y1="8.255" x2="6.35" y2="1.27" width="0.762" layer="21"/>
<smd name="1" x="-6.35" y="-3.81" dx="1.6764" dy="5.08" layer="1"/>
<smd name="2" x="-3.81" y="-3.81" dx="1.6764" dy="5.08" layer="1"/>
<smd name="3" x="-1.27" y="-3.81" dx="1.6764" dy="5.08" layer="1"/>
<smd name="4" x="1.27" y="-3.81" dx="1.6764" dy="5.08" layer="1"/>
<smd name="5" x="3.81" y="-3.81" dx="1.6764" dy="5.08" layer="1"/>
<smd name="6" x="6.35" y="-3.81" dx="1.6764" dy="5.08" layer="1"/>
<text x="-8.255" y="-3.81" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="9.525" y="-3.81" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-6.731" y1="0.635" x2="-5.969" y2="1.143" layer="21"/>
<rectangle x1="-4.191" y1="0.635" x2="-3.429" y2="1.143" layer="21"/>
<rectangle x1="-1.651" y1="0.635" x2="-0.889" y2="1.143" layer="21"/>
<rectangle x1="0.889" y1="0.635" x2="1.651" y2="1.143" layer="21"/>
<rectangle x1="3.429" y1="0.635" x2="4.191" y2="1.143" layer="21"/>
<rectangle x1="5.969" y1="0.635" x2="6.731" y2="1.143" layer="21"/>
<rectangle x1="-6.731" y1="-2.921" x2="-5.969" y2="-1.905" layer="21"/>
<rectangle x1="-4.191" y1="-2.921" x2="-3.429" y2="-1.905" layer="21"/>
<rectangle x1="-1.651" y1="-2.921" x2="-0.889" y2="-1.905" layer="21"/>
<rectangle x1="0.889" y1="-2.921" x2="1.651" y2="-1.905" layer="21"/>
<rectangle x1="3.429" y1="-2.921" x2="4.191" y2="-1.905" layer="21"/>
<rectangle x1="5.969" y1="-2.921" x2="6.731" y2="-1.905" layer="21"/>
</package>
<package name="06/SMD">
<smd name="1" x="-6.35" y="0" dx="1.6764" dy="6.4516" layer="1"/>
<smd name="2" x="-3.81" y="0" dx="1.6764" dy="6.4516" layer="1"/>
<smd name="3" x="-1.27" y="0" dx="1.6764" dy="6.4516" layer="1"/>
<smd name="4" x="1.27" y="0" dx="1.6764" dy="6.4516" layer="1"/>
<smd name="5" x="3.81" y="0" dx="1.6764" dy="6.4516" layer="1"/>
<smd name="6" x="6.35" y="0" dx="1.6764" dy="6.4516" layer="1"/>
<text x="-7.62" y="1.905" size="1.27" layer="25" ratio="10" rot="R180">&gt;NAME</text>
<text x="-3.81" y="4.445" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="PSH-06">
<wire x1="-7.62" y1="2.54" x2="-6.6675" y2="2.54" width="0.127" layer="21"/>
<wire x1="-6.6675" y1="2.54" x2="6.6675" y2="2.54" width="0.127" layer="21"/>
<wire x1="6.6675" y1="2.54" x2="7.62" y2="2.54" width="0.127" layer="21"/>
<wire x1="7.62" y1="-3.4925" x2="-7.62" y2="-3.4925" width="0.127" layer="21"/>
<wire x1="-6.6675" y1="2.54" x2="-6.6675" y2="1.905" width="0.127" layer="21"/>
<wire x1="-6.6675" y1="1.905" x2="6.6675" y2="1.905" width="0.127" layer="21"/>
<wire x1="6.6675" y1="1.905" x2="6.6675" y2="2.54" width="0.127" layer="21"/>
<wire x1="-7.62" y1="-3.4925" x2="-7.62" y2="2.54" width="0.127" layer="21"/>
<wire x1="7.62" y1="2.54" x2="7.62" y2="-3.4925" width="0.127" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="-3.81" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="-1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="5" x="3.81" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="6" x="6.35" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-7.3787" y="3.0988" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-7.3025" y="-5.3975" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="3.556" y1="-0.254" x2="4.064" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="-4.064" y1="-0.254" x2="-3.556" y2="0.254" layer="51"/>
<rectangle x1="-6.604" y1="-0.254" x2="-6.096" y2="0.254" layer="51"/>
<rectangle x1="6.096" y1="-0.254" x2="6.604" y2="0.254" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="TPS2054">
<wire x1="12.7" y1="-15.24" x2="-10.16" y2="-15.24" width="0.254" layer="94"/>
<wire x1="-10.16" y1="15.24" x2="-10.16" y2="-15.24" width="0.254" layer="94"/>
<wire x1="-10.16" y1="15.24" x2="12.7" y2="15.24" width="0.254" layer="94"/>
<wire x1="12.7" y1="-15.24" x2="12.7" y2="15.24" width="0.254" layer="94"/>
<text x="3.175" y="16.51" size="2.54" layer="95">&gt;NAME</text>
<text x="4.445" y="-19.3675" size="2.54" layer="96">&gt;VALUE</text>
<pin name="GND@1" x="0" y="-17.78" length="short" direction="pwr" rot="R90"/>
<pin name="IN@1" x="0" y="17.78" length="short" direction="pwr" rot="R270"/>
<pin name="EN1" x="-12.7" y="12.7" length="short"/>
<pin name="EN2" x="-12.7" y="5.08" length="short"/>
<pin name="GND@2" x="2.54" y="-17.78" length="short" direction="pwr" rot="R90"/>
<pin name="IN@2" x="2.54" y="17.78" length="short" direction="pwr" rot="R270"/>
<pin name="EN3" x="-12.7" y="-2.54" length="short"/>
<pin name="EN4" x="-12.7" y="-10.16" length="short"/>
<pin name="!OC4" x="-12.7" y="-12.7" length="short" direction="oc"/>
<pin name="OUT4" x="15.24" y="-10.16" length="short" direction="sup" rot="R180"/>
<pin name="OUT3" x="15.24" y="-2.54" length="short" direction="sup" rot="R180"/>
<pin name="!OC3" x="-12.7" y="-5.08" length="short" direction="oc"/>
<pin name="!OC2" x="-12.7" y="2.54" length="short" direction="oc"/>
<pin name="OUT2" x="15.24" y="5.08" length="short" direction="sup" rot="R180"/>
<pin name="OUT1" x="15.24" y="12.7" length="short" direction="sup" rot="R180"/>
<pin name="!OC1" x="-12.7" y="10.16" length="short" direction="oc"/>
</symbol>
<symbol name="74LVC1T45">
<wire x1="6.35" y1="-5.08" x2="-6.35" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="5.08" x2="-6.35" y2="2.54" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="2.54" x2="-6.35" y2="0" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="0" x2="-6.35" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="-2.54" x2="-6.35" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="5.08" x2="6.35" y2="5.08" width="0.4064" layer="94"/>
<wire x1="6.35" y1="-5.08" x2="6.35" y2="-2.54" width="0.4064" layer="94"/>
<text x="0" y="5.842" size="1.778" layer="95" align="bottom-center">&gt;NAME</text>
<text x="0" y="-7.62" size="1.778" layer="96" align="bottom-center">&gt;VALUE</text>
<pin name="VCCA" x="-7.62" y="2.54" length="point" direction="pwr"/>
<pin name="VCCB" x="7.62" y="2.54" length="point" direction="pwr" rot="R180"/>
<pin name="GND" x="-7.62" y="0" length="point" direction="pwr"/>
<pin name="DIR" x="7.62" y="0" length="point" direction="in" rot="R180"/>
<pin name="A" x="-7.62" y="-2.54" length="point"/>
<pin name="B" x="7.62" y="-2.54" length="point" rot="R180"/>
<wire x1="6.35" y1="-2.54" x2="6.35" y2="0" width="0.4064" layer="94"/>
<wire x1="6.35" y1="0" x2="6.35" y2="2.54" width="0.4064" layer="94"/>
<wire x1="6.35" y1="2.54" x2="6.35" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-7.62" y1="2.54" x2="-6.35" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="0" x2="-6.35" y2="0" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="-2.54" x2="-6.35" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="7.62" y1="-2.54" x2="6.35" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="7.62" y1="0" x2="6.35" y2="0" width="0.1524" layer="94"/>
<wire x1="7.62" y1="2.54" x2="6.35" y2="2.54" width="0.1524" layer="94"/>
<text x="0" y="-2.54" size="2.54" layer="94" align="center">LS</text>
</symbol>
<symbol name="74LVC1G04">
<description>Single Inverter Gate</description>
<wire x1="1.27" y1="0" x2="-3.81" y2="2.54" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="7.62" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="-6.35" y1="6.35" x2="6.35" y2="6.35" width="0.4064" layer="94"/>
<wire x1="6.35" y1="6.35" x2="6.35" y2="5.08" width="0.4064" layer="94"/>
<wire x1="6.35" y1="5.08" x2="6.35" y2="-6.35" width="0.4064" layer="94"/>
<wire x1="6.35" y1="-6.35" x2="-6.35" y2="-6.35" width="0.4064" layer="94"/>
<text x="0" y="6.985" size="1.778" layer="95" align="bottom-center">&gt;NAME</text>
<text x="0" y="-8.89" size="1.778" layer="96" align="bottom-center">&gt;VALUE</text>
<pin name="NC" x="-7.62" y="5.08" length="point" direction="nc"/>
<pin name="A" x="-7.62" y="0" visible="pad" length="point" direction="in"/>
<pin name="GND" x="-7.62" y="-5.08" length="point" direction="pwr"/>
<pin name="Y" x="7.62" y="-5.08" visible="pad" length="point" direction="out" rot="R180"/>
<pin name="VCC" x="7.62" y="5.08" length="point" direction="pwr" rot="R180"/>
<wire x1="-6.35" y1="-6.35" x2="-6.35" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="-5.08" x2="-6.35" y2="0" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="0" x2="-6.35" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="5.08" x2="-6.35" y2="6.35" width="0.4064" layer="94"/>
<wire x1="1.27" y1="0" x2="-3.81" y2="-2.54" width="0.4064" layer="94"/>
<circle x="2.032" y="0" radius="0.508" width="0.254" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="3.81" y2="0" width="0.1524" layer="94"/>
<wire x1="3.81" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-6.35" y1="0" x2="-3.81" y2="0" width="0.1524" layer="94"/>
<wire x1="-3.81" y1="2.54" x2="-3.81" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="-7.62" y1="5.08" x2="-6.35" y2="5.08" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="0" x2="-6.35" y2="0" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="-6.35" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="6.35" y1="5.08" x2="7.62" y2="5.08" width="0.1524" layer="94"/>
</symbol>
<symbol name="D_SCHOTTKY">
<wire x1="-2.54" y1="-1.905" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-2.54" y2="1.905" width="0.254" layer="94"/>
<wire x1="-2.54" y1="1.905" x2="-2.54" y2="-1.905" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="1.3716" y2="0" width="0.1524" layer="94"/>
<wire x1="1.397" y1="1.905" x2="1.397" y2="-1.905" width="0.254" layer="94"/>
<wire x1="1.397" y1="1.905" x2="2.413" y2="1.905" width="0.254" layer="94"/>
<wire x1="0.381" y1="-1.905" x2="1.397" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.3114" y="2.6416" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.5654" y="-4.4958" size="1.778" layer="96">&gt;VALUE</text>
<pin name="A" x="-5.08" y="0" visible="off" length="short" direction="pas"/>
<pin name="K" x="5.08" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="PLUG5">
<wire x1="2.54" y1="8.89" x2="1.27" y2="10.16" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="1.27" y1="10.16" x2="2.54" y2="11.43" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="-1.27" y1="10.16" x2="1.27" y2="10.16" width="0.254" layer="94"/>
<wire x1="2.54" y1="8.89" x2="2.54" y2="11.43" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="2.54" y1="10.16" x2="5.08" y2="10.16" width="0.6096" layer="94"/>
<wire x1="2.54" y1="3.81" x2="1.27" y2="5.08" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="1.27" y1="5.08" x2="2.54" y2="6.35" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="-1.27" y1="5.08" x2="1.27" y2="5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="3.81" x2="2.54" y2="6.35" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="2.54" y1="5.08" x2="5.08" y2="5.08" width="0.6096" layer="94"/>
<wire x1="-7.62" y1="12.7" x2="-7.62" y2="-12.7" width="0.4064" layer="94"/>
<wire x1="-7.62" y1="-12.7" x2="10.16" y2="-12.7" width="0.4064" layer="94"/>
<wire x1="10.16" y1="-12.7" x2="10.16" y2="12.7" width="0.4064" layer="94"/>
<wire x1="10.16" y1="12.7" x2="-7.62" y2="12.7" width="0.4064" layer="94"/>
<wire x1="2.54" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="1.27" y1="0" x2="2.54" y2="1.27" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="1.27" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="2.54" y1="0" x2="5.08" y2="0" width="0.6096" layer="94"/>
<wire x1="2.54" y1="-6.35" x2="1.27" y2="-5.08" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="1.27" y1="-5.08" x2="2.54" y2="-3.81" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="-1.27" y1="-5.08" x2="1.27" y2="-5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="-6.35" x2="2.54" y2="-3.81" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="2.54" y1="-5.08" x2="5.08" y2="-5.08" width="0.6096" layer="94"/>
<circle x="-2.54" y="10.16" radius="1.27" width="0.254" layer="94"/>
<circle x="-2.54" y="10.16" radius="1.27" width="0.254" layer="94"/>
<circle x="-2.54" y="5.08" radius="1.27" width="0.254" layer="94"/>
<circle x="-2.54" y="5.08" radius="1.27" width="0.254" layer="94"/>
<circle x="-2.54" y="0" radius="1.27" width="0.254" layer="94"/>
<circle x="-2.54" y="0" radius="1.27" width="0.254" layer="94"/>
<circle x="-2.54" y="-5.08" radius="1.27" width="0.254" layer="94"/>
<circle x="-2.54" y="-5.08" radius="1.27" width="0.254" layer="94"/>
<text x="-4.064" y="11.049" size="1.778" layer="95" rot="R180">1</text>
<text x="-4.064" y="5.969" size="1.778" layer="95" rot="R180">2</text>
<text x="-7.62" y="15.24" size="1.778" layer="95" align="top-left">&gt;NAME</text>
<text x="-7.62" y="-15.24" size="1.778" layer="95">&gt;VALUE</text>
<text x="-4.064" y="0.889" size="1.778" layer="95" rot="R180">3</text>
<text x="-4.064" y="-4.191" size="1.778" layer="95" rot="R180">4</text>
<pin name="1" x="7.62" y="10.16" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="2" x="7.62" y="5.08" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="3" x="7.62" y="0" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="4" x="7.62" y="-5.08" visible="pad" length="short" direction="pas" rot="R180"/>
<wire x1="2.54" y1="-11.43" x2="1.27" y2="-10.16" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="1.27" y1="-10.16" x2="2.54" y2="-8.89" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="-1.27" y1="-10.16" x2="1.27" y2="-10.16" width="0.254" layer="94"/>
<wire x1="2.54" y1="-11.43" x2="2.54" y2="-8.89" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="2.54" y1="-10.16" x2="5.08" y2="-10.16" width="0.6096" layer="94"/>
<circle x="-2.54" y="-10.16" radius="1.27" width="0.254" layer="94"/>
<circle x="-2.54" y="-10.16" radius="1.27" width="0.254" layer="94"/>
<text x="-4.064" y="-9.271" size="1.778" layer="95" rot="R180">5</text>
<pin name="5" x="7.62" y="-10.16" visible="pad" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="D_TVS_BIDIR">
<wire x1="1.27" y1="-2.54" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-1.397" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="0" size="1.778" layer="95" rot="R90" align="bottom-center">&gt;NAME</text>
<text x="2.54" y="0" size="1.778" layer="96" rot="R90" align="top-center">&gt;VALUE</text>
<pin name="A" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="B" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<wire x1="0" y1="0" x2="1.397" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="2.54" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="-1.397" y1="0" x2="-1.905" y2="-0.508" width="0.254" layer="94"/>
<wire x1="1.397" y1="0" x2="1.905" y2="0.508" width="0.254" layer="94"/>
</symbol>
<symbol name="TS2">
<wire x1="0" y1="1.905" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="-4.445" y1="1.905" x2="-3.175" y2="1.905" width="0.254" layer="94"/>
<wire x1="-4.445" y1="-1.905" x2="-3.175" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-4.445" y1="1.905" x2="-4.445" y2="0" width="0.254" layer="94"/>
<wire x1="-4.445" y1="0" x2="-4.445" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.905" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0" x2="-0.635" y2="0" width="0.1524" layer="94"/>
<wire x1="-4.445" y1="0" x2="-3.175" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.54" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="1.905" width="0.254" layer="94"/>
<circle x="0" y="-2.54" radius="0.127" width="0.4064" layer="94"/>
<circle x="0" y="2.54" radius="0.127" width="0.4064" layer="94"/>
<text x="-6.35" y="-2.54" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="-3.81" y="3.175" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="P" x="0" y="-5.08" visible="pad" length="short" direction="pas" swaplevel="2" rot="R90"/>
<pin name="S" x="0" y="5.08" visible="pad" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="S1" x="2.54" y="5.08" visible="pad" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="P1" x="2.54" y="-5.08" visible="pad" length="short" direction="pas" swaplevel="2" rot="R90"/>
</symbol>
<symbol name="PINHD6">
<wire x1="-6.35" y1="-7.62" x2="1.27" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-7.62" x2="1.27" y2="10.16" width="0.4064" layer="94"/>
<wire x1="1.27" y1="10.16" x2="-6.35" y2="10.16" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="10.16" x2="-6.35" y2="-7.62" width="0.4064" layer="94"/>
<text x="-6.35" y="10.795" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="7.62" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="3" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="4" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="5" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="6" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="TPS2054" prefix="IC">
<description>&lt;B&gt;Current-Limited, Power-Distribution Switches&lt;/B&gt;&lt;P&gt;
• 70-mΩ High-Side MOSFET&lt;BR&gt;
• 500-mA Continuous Current&lt;BR&gt;
• Thermal and Short-Circuit Protection&lt;BR&gt;
• Accurate Current Limit
(0.75 A Minimum, 1.25 A Maximum)&lt;BR&gt;
• Operating Range: 2.7 V to 5.5 V&lt;BR&gt;</description>
<gates>
<gate name="G$1" symbol="TPS2054" x="0" y="0"/>
</gates>
<devices>
<device name="BDR" package="SO-16">
<connects>
<connect gate="G$1" pin="!OC1" pad="16"/>
<connect gate="G$1" pin="!OC2" pad="13"/>
<connect gate="G$1" pin="!OC3" pad="12"/>
<connect gate="G$1" pin="!OC4" pad="9"/>
<connect gate="G$1" pin="EN1" pad="3"/>
<connect gate="G$1" pin="EN2" pad="4"/>
<connect gate="G$1" pin="EN3" pad="7"/>
<connect gate="G$1" pin="EN4" pad="8"/>
<connect gate="G$1" pin="GND@1" pad="1"/>
<connect gate="G$1" pin="GND@2" pad="5"/>
<connect gate="G$1" pin="IN@1" pad="2"/>
<connect gate="G$1" pin="IN@2" pad="6"/>
<connect gate="G$1" pin="OUT1" pad="15"/>
<connect gate="G$1" pin="OUT2" pad="14"/>
<connect gate="G$1" pin="OUT3" pad="11"/>
<connect gate="G$1" pin="OUT4" pad="10"/>
</connects>
<technologies>
<technology name="">
<attribute name="DODANI" value="VEIT"/>
<attribute name="MELE" value="1358"/>
<attribute name="MPN" value="TPS2054BDR"/>
<attribute name="OSAZENI" value="RACOM"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SN74LVC1T45" prefix="IC">
<description>&lt;B&gt;Single Level Shifter&lt;/B&gt;&lt;P&gt;
Single-Bit Dual-Supply Bus Transceiver With Configurable VoltageTranslation and 3-State Outputs</description>
<gates>
<gate name="G$1" symbol="74LVC1T45" x="0" y="0"/>
</gates>
<devices>
<device name="DBV" package="SOT23-6L">
<connects>
<connect gate="G$1" pin="A" pad="3"/>
<connect gate="G$1" pin="B" pad="4"/>
<connect gate="G$1" pin="DIR" pad="5"/>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="VCCA" pad="1"/>
<connect gate="G$1" pin="VCCB" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="DODANI" value="VEIT"/>
<attribute name="MELE" value="0409"/>
<attribute name="MPN" value="SN74LVC1T45DBVT"/>
<attribute name="OSAZENI" value="RACOM"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SN74LVC1G04" prefix="IC">
<description>&lt;B&gt;Single Inverter Gate&lt;/B&gt;&lt;P&gt;
• VCC = 1.65V to 5.5V&lt;BR&gt;
• ±24mA Output Drive at 3.3V&lt;BR&gt;
• Max tpd of 3.3 ns at 3.3V&lt;BR&gt;</description>
<gates>
<gate name="G$1" symbol="74LVC1G04" x="0" y="0"/>
</gates>
<devices>
<device name="DBV" package="SOT23-5L">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="GND" pad="3"/>
<connect gate="G$1" pin="NC" pad="1"/>
<connect gate="G$1" pin="VCC" pad="5"/>
<connect gate="G$1" pin="Y" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="DODANI" value="VEIT"/>
<attribute name="MELE" value="1357"/>
<attribute name="MPN" value="SN74LVC1G04DBV"/>
<attribute name="OSAZENI" value="RACOM"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="NRVBS3100T3G" prefix="D">
<description>Diode schottky 60V/3A &lt;br&gt;Ifsm = 100A</description>
<gates>
<gate name="G$1" symbol="D_SCHOTTKY" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SMC">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DODANI" value="VEIT"/>
<attribute name="MELE" value="1255"/>
<attribute name="MPN" value="NRVBS3100T3G"/>
<attribute name="OSAZENI" value="RACOM"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CTB9302/5" prefix="X">
<description>&lt;B&gt;Zasouvací svorkovnice 5 pólů&lt;/B&gt;&lt;P&gt;
Header svorkovnice, 5 pólů, THT do DPS; &lt;B&gt;CTB9302/5&lt;/B&gt;&lt;BR&gt;&lt;I&gt;(Farnell: 2493668)&lt;/I&gt;&lt;P&gt;

&lt;U&gt;Protikus:&lt;/U&gt;&lt;BR&gt;
Zasouvací svorkovnice na kabel, 5 pólů, šroubovací; &lt;B&gt;CTB922VG/5S&lt;/B&gt;&lt;BR&gt;&lt;I&gt;(Farnell: 2493664)&lt;/I&gt;</description>
<gates>
<gate name="G$1" symbol="PLUG5" x="0" y="0"/>
</gates>
<devices>
<device name="" package="CTB9302/5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
</connects>
<technologies>
<technology name="">
<attribute name="DODANI" value="VEIT"/>
<attribute name="MELE" value="1302"/>
<attribute name="MF" value="CAMDENBOSS "/>
<attribute name="MPN" value="CTB9302/5"/>
<attribute name="OC_FARNELL" value="2493668"/>
<attribute name="OC_GM" value="none"/>
<attribute name="OC_MOUSER" value="none"/>
<attribute name="OSAZENI" value="VEIT" constant="no"/>
<attribute name="PRISLUSENSTVI" value="MELE1300"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SMAJ*CA" prefix="TSV">
<description>Obousměrný transil 400W</description>
<gates>
<gate name="G$1" symbol="D_TVS_BIDIR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SMA">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="B" pad="C"/>
</connects>
<technologies>
<technology name="30">
<attribute name="DODANI" value="VEIT"/>
<attribute name="MARKING" value="CL"/>
<attribute name="MELE" value="1312"/>
<attribute name="MPN" value="SMAJ30CA-TR"/>
<attribute name="OSAZENI" value="RACOM"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="B3F-1000" prefix="SW">
<description>Mikrotlačítko, THT; &lt;b&gt;B3F-1000&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="TS2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="B3F-10XX">
<connects>
<connect gate="1" pin="P" pad="3"/>
<connect gate="1" pin="P1" pad="4"/>
<connect gate="1" pin="S" pad="1"/>
<connect gate="1" pin="S1" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DODANI" value="VEIT"/>
<attribute name="MELE" value="1350"/>
<attribute name="MF" value="OMRON"/>
<attribute name="MPN" value="B3F-1000" constant="no"/>
<attribute name="OC_FARNELL" value="176432" constant="no"/>
<attribute name="OC_MOUSER" value="653-B3F-1000"/>
<attribute name="OSAZENI" value="VEIT"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PINHD-1X6" prefix="JP" uservalue="yes">
<description>&lt;b&gt;Kolíková lišta&lt;/b&gt; 2,54 mm</description>
<gates>
<gate name="A" symbol="PINHD6" x="0" y="-2.54"/>
</gates>
<devices>
<device name="0" package="1X06">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="DODANI" value="VEIT"/>
<attribute name="MELE" value="1100"/>
<attribute name="MPN" value="S1G40"/>
<attribute name="OSAZENI" value="VEIT"/>
</technology>
</technologies>
</device>
<device name="90" package="1X06/90">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="06/90SMD">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="06/SMD" package="06/SMD">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="A" package="PSH-06">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="IC1" library="_Special" deviceset="COLIBRI" device="" value="COLIBRI_T30"/>
<part name="CON7" library="_Connectors" deviceset="USB-1X90" device=""/>
<part name="IC7" library="_Semiconductors" deviceset="RX-8025SA" device="" value="RX-8025SA"/>
<part name="R1" library="_RCL" deviceset="R" device="_4K7/0603" value="4k7"/>
<part name="R2" library="_RCL" deviceset="R" device="_4K7/0603" value="4k7"/>
<part name="+3V1" library="SupplyVeit" deviceset="+3V3" device=""/>
<part name="P+1" library="SupplyVeit" deviceset="+5V" device=""/>
<part name="P+2" library="SupplyVeit" deviceset="+24V" device=""/>
<part name="CHASSIS1" library="SupplyVeit" deviceset="CHASSIS" device=""/>
<part name="GND1" library="SupplyVeit" deviceset="GND" device=""/>
<part name="+3V2" library="SupplyVeit" deviceset="+3V3" device=""/>
<part name="+3V3" library="SupplyVeit" deviceset="+3V3" device=""/>
<part name="BAT1" library="_Special" deviceset="BHSD-2032-SM" device="" value="BHSD-2032-SM"/>
<part name="GND2" library="SupplyVeit" deviceset="GND" device=""/>
<part name="C1" library="_RCL" deviceset="C" device="_100NF/50V/0603/X7R" value="100nF"/>
<part name="GND3" library="SupplyVeit" deviceset="GND" device=""/>
<part name="D1" library="_Semiconductors" deviceset="BAV70LT1G" device=""/>
<part name="+3V4" library="SupplyVeit" deviceset="+3V3" device=""/>
<part name="+3V5" library="SupplyVeit" deviceset="+3V3" device=""/>
<part name="GND4" library="SupplyVeit" deviceset="GND" device=""/>
<part name="IC10" library="_Semiconductors" deviceset="LT1785" device="CN8"/>
<part name="C5" library="_RCL" deviceset="C" device="_100NF/50V/0603/X7R" value="100nF"/>
<part name="P+3" library="SupplyVeit" deviceset="+5V" device=""/>
<part name="GND5" library="SupplyVeit" deviceset="GND" device=""/>
<part name="GND6" library="SupplyVeit" deviceset="GND" device=""/>
<part name="GND7" library="SupplyVeit" deviceset="GND" device=""/>
<part name="CHASSIS2" library="SupplyVeit" deviceset="CHASSIS" device=""/>
<part name="CHASSIS3" library="SupplyVeit" deviceset="CHASSIS" device=""/>
<part name="R10" library="_RCL" deviceset="R" device="_22K/0603/AEC" value="22k"/>
<part name="R11" library="_RCL" deviceset="R" device="_22K/0603/AEC" value="22k"/>
<part name="P+4" library="SupplyVeit" deviceset="+5V" device=""/>
<part name="GND8" library="SupplyVeit" deviceset="GND" device=""/>
<part name="TVS3" library="CMI" deviceset="SMAJ*CA" device="" technology="30" value="SMAJ30CA"/>
<part name="CHASSIS5" library="SupplyVeit" deviceset="CHASSIS" device=""/>
<part name="GND9" library="SupplyVeit" deviceset="GND" device=""/>
<part name="L1" library="_RCL" deviceset="BLM31PG121SH1L" device=""/>
<part name="L2" library="_RCL" deviceset="BLM31PG121SH1L" device=""/>
<part name="L3" library="_RCL" deviceset="BLM31PG121SH1L" device=""/>
<part name="GND10" library="SupplyVeit" deviceset="GND" device=""/>
<part name="IC11" library="_Semiconductors" deviceset="LT1785" device="CN8"/>
<part name="C6" library="_RCL" deviceset="C" device="_100NF/50V/0603/X7R" value="100nF"/>
<part name="P+6" library="SupplyVeit" deviceset="+5V" device=""/>
<part name="GND11" library="SupplyVeit" deviceset="GND" device=""/>
<part name="GND12" library="SupplyVeit" deviceset="GND" device=""/>
<part name="GND13" library="SupplyVeit" deviceset="GND" device=""/>
<part name="CHASSIS4" library="SupplyVeit" deviceset="CHASSIS" device=""/>
<part name="CHASSIS6" library="SupplyVeit" deviceset="CHASSIS" device=""/>
<part name="R14" library="_RCL" deviceset="R" device="_22K/0603/AEC" value="22k"/>
<part name="R15" library="_RCL" deviceset="R" device="_22K/0603/AEC" value="22k"/>
<part name="P+7" library="SupplyVeit" deviceset="+5V" device=""/>
<part name="GND14" library="SupplyVeit" deviceset="GND" device=""/>
<part name="TVS6" library="CMI" deviceset="SMAJ*CA" device="" technology="30" value="SMAJ30CA"/>
<part name="CHASSIS7" library="SupplyVeit" deviceset="CHASSIS" device=""/>
<part name="GND15" library="SupplyVeit" deviceset="GND" device=""/>
<part name="L4" library="_RCL" deviceset="BLM31PG121SH1L" device=""/>
<part name="L5" library="_RCL" deviceset="BLM31PG121SH1L" device=""/>
<part name="L6" library="_RCL" deviceset="BLM31PG121SH1L" device=""/>
<part name="GND16" library="SupplyVeit" deviceset="GND" device=""/>
<part name="+3V6" library="SupplyVeit" deviceset="+3V3" device=""/>
<part name="GND17" library="SupplyVeit" deviceset="GND" device=""/>
<part name="CON9" library="CMI" deviceset="PINHD-1X6" device="0" value="DEBUG - TTL-232R-3V3"/>
<part name="GND18" library="SupplyVeit" deviceset="GND" device=""/>
<part name="C2" library="_RCL" deviceset="C" device="_10UF/10V/0805/X5R" value="10uF/10V"/>
<part name="C3" library="_RCL" deviceset="C" device="_10UF/10V/0805/X5R" value="10uF/10V"/>
<part name="C4" library="_RCL" deviceset="C" device="_10UF/10V/0805/X5R" value="10uF/10V"/>
<part name="C7" library="_RCL" deviceset="C" device="_10UF/10V/0805/X5R" value="10uF/10V"/>
<part name="GND19" library="SupplyVeit" deviceset="GND" device=""/>
<part name="+3V7" library="SupplyVeit" deviceset="+3V3" device=""/>
<part name="GND20" library="SupplyVeit" deviceset="GND" device=""/>
<part name="GND21" library="SupplyVeit" deviceset="GND" device=""/>
<part name="GND22" library="SupplyVeit" deviceset="GND" device=""/>
<part name="+3V8" library="SupplyVeit" deviceset="+3V3" device=""/>
<part name="+3V9" library="SupplyVeit" deviceset="+3V3" device=""/>
<part name="+3V10" library="SupplyVeit" deviceset="+3V3" device=""/>
<part name="GND23" library="SupplyVeit" deviceset="GND" device=""/>
<part name="C8" library="_RCL" deviceset="C" device="_10UF/10V/0805/X5R" value="10uF/10V"/>
<part name="C9" library="_RCL" deviceset="C" device="_10UF/10V/0805/X5R" value="10uF/10V"/>
<part name="C10" library="_RCL" deviceset="C" device="_10UF/10V/0805/X5R" value="10uF/10V"/>
<part name="C11" library="_RCL" deviceset="C" device="_10UF/10V/0805/X5R" value="10uF/10V"/>
<part name="GND24" library="SupplyVeit" deviceset="GND" device=""/>
<part name="+3V11" library="SupplyVeit" deviceset="+3V3" device=""/>
<part name="GND25" library="SupplyVeit" deviceset="GND" device=""/>
<part name="GND26" library="SupplyVeit" deviceset="GND" device=""/>
<part name="GND27" library="SupplyVeit" deviceset="GND" device=""/>
<part name="+3V12" library="SupplyVeit" deviceset="+3V3" device=""/>
<part name="+3V13" library="SupplyVeit" deviceset="+3V3" device=""/>
<part name="+3V14" library="SupplyVeit" deviceset="+3V3" device=""/>
<part name="IC5" library="_Semiconductors" deviceset="TPD2S017" device=""/>
<part name="SDCARD1" library="_Special" deviceset="2041021" device=""/>
<part name="GND28" library="SupplyVeit" deviceset="GND" device=""/>
<part name="C12" library="_RCL" deviceset="C" device="_10UF/10V/0805/X5R" value="10uF/10V"/>
<part name="+3V15" library="SupplyVeit" deviceset="+3V3" device=""/>
<part name="C13" library="_RCL" deviceset="C" device="_100NF/50V/0603/X7R" value="100nF"/>
<part name="L7" library="_RCL" deviceset="BLM31PG121SH1L" device=""/>
<part name="R3" library="_RCL" deviceset="R" device="_150R/0603" value="150R"/>
<part name="R4" library="_RCL" deviceset="R" device="_150R/0603" value="150R"/>
<part name="L8" library="_RCL" deviceset="BLM31PG121SH1L" device=""/>
<part name="+3V17" library="SupplyVeit" deviceset="+3V3" device=""/>
<part name="C14" library="_RCL" deviceset="C" device="_47UF/6,3V/1210/X7R" value="47uF/6,3V"/>
<part name="CON8" library="_Connectors" deviceset="ARJC02-111009D" device=""/>
<part name="CHASSIS8" library="SupplyVeit" deviceset="CHASSIS" device=""/>
<part name="R5" library="_RCL" deviceset="R" device="_22R/0603" value="22R"/>
<part name="R6" library="_RCL" deviceset="R" device="_22R/0603" value="22R"/>
<part name="R7" library="_RCL" deviceset="R" device="_22R/0603" value="22R"/>
<part name="R8" library="_RCL" deviceset="R" device="_22R/0603" value="22R"/>
<part name="R9" library="_RCL" deviceset="R" device="_22R/0603" value="22R"/>
<part name="R18" library="_RCL" deviceset="R" device="_22R/0603" value="22R"/>
<part name="R19" library="_RCL" deviceset="R" device="_22R/0603" value="22R"/>
<part name="R20" library="_RCL" deviceset="R" device="_33K/0603/AEC" value="33k"/>
<part name="R21" library="_RCL" deviceset="R" device="_68K/0603" value="68k"/>
<part name="R22" library="_RCL" deviceset="R" device="_68K/0603" value="68k"/>
<part name="R23" library="_RCL" deviceset="R" device="_68K/0603" value="68k"/>
<part name="R24" library="_RCL" deviceset="R" device="_68K/0603" value="68k"/>
<part name="+3V16" library="SupplyVeit" deviceset="+3V3" device=""/>
<part name="C16" library="_RCL" deviceset="C" device="_1NF/50V/0603/X7R" value="1nF"/>
<part name="R25" library="_RCL" deviceset="R" device="_10K/0603/AEC" value="10k"/>
<part name="GND30" library="SupplyVeit" deviceset="GND" device=""/>
<part name="IC6" library="CMI" deviceset="TPS2054" device="BDR"/>
<part name="P+9" library="SupplyVeit" deviceset="+5V" device=""/>
<part name="GND31" library="SupplyVeit" deviceset="GND" device=""/>
<part name="GND32" library="SupplyVeit" deviceset="GND" device=""/>
<part name="L12" library="_RCL" deviceset="BLM31PG121SH1L" device=""/>
<part name="CHASSIS12" library="SupplyVeit" deviceset="CHASSIS" device=""/>
<part name="GND33" library="SupplyVeit" deviceset="GND" device=""/>
<part name="CHASSIS13" library="SupplyVeit" deviceset="CHASSIS" device=""/>
<part name="CHASSIS14" library="SupplyVeit" deviceset="CHASSIS" device=""/>
<part name="L10" library="_RCL" deviceset="BLM31PG121SH1L" device=""/>
<part name="CON1" library="_Connectors" deviceset="USB-1X90" device=""/>
<part name="IC2" library="_Semiconductors" deviceset="TPD2S017" device=""/>
<part name="L11" library="_RCL" deviceset="BLM31PG121SH1L" device=""/>
<part name="CHASSIS9" library="SupplyVeit" deviceset="CHASSIS" device=""/>
<part name="GND34" library="SupplyVeit" deviceset="GND" device=""/>
<part name="CHASSIS10" library="SupplyVeit" deviceset="CHASSIS" device=""/>
<part name="CHASSIS11" library="SupplyVeit" deviceset="CHASSIS" device=""/>
<part name="L13" library="_RCL" deviceset="BLM31PG121SH1L" device=""/>
<part name="CON3" library="_Connectors" deviceset="USB-1X90" device=""/>
<part name="IC3" library="_Semiconductors" deviceset="TPD2S017" device=""/>
<part name="L14" library="_RCL" deviceset="BLM31PG121SH1L" device=""/>
<part name="CHASSIS15" library="SupplyVeit" deviceset="CHASSIS" device=""/>
<part name="GND35" library="SupplyVeit" deviceset="GND" device=""/>
<part name="CHASSIS16" library="SupplyVeit" deviceset="CHASSIS" device=""/>
<part name="CHASSIS17" library="SupplyVeit" deviceset="CHASSIS" device=""/>
<part name="L15" library="_RCL" deviceset="BLM31PG121SH1L" device=""/>
<part name="CON5" library="_Connectors" deviceset="USB-1X90" device=""/>
<part name="IC4" library="_Semiconductors" deviceset="TPD2S017" device=""/>
<part name="L16" library="_RCL" deviceset="BLM31PG121SH1L" device=""/>
<part name="CHASSIS18" library="SupplyVeit" deviceset="CHASSIS" device=""/>
<part name="GND36" library="SupplyVeit" deviceset="GND" device=""/>
<part name="CHASSIS19" library="SupplyVeit" deviceset="CHASSIS" device=""/>
<part name="CHASSIS20" library="SupplyVeit" deviceset="CHASSIS" device=""/>
<part name="L17" library="_RCL" deviceset="BLM31PG121SH1L" device=""/>
<part name="IC8" library="_Semiconductors" deviceset="USB2514B" device=""/>
<part name="GND37" library="SupplyVeit" deviceset="GND" device=""/>
<part name="+3V18" library="SupplyVeit" deviceset="+3V3" device=""/>
<part name="+3V19" library="SupplyVeit" deviceset="+3V3" device=""/>
<part name="R26" library="_RCL" deviceset="R" device="_12K/0603" value="12k"/>
<part name="R27" library="_RCL" deviceset="R" device="_100K/0603" value="100k"/>
<part name="C17" library="_RCL" deviceset="C" device="_100NF/50V/0603/X7R" value="100nF"/>
<part name="C18" library="_RCL" deviceset="C" device="_1UF/16V/0603/X7R" value="1uF/16V"/>
<part name="C19" library="_RCL" deviceset="C" device="_100NF/50V/0603/X7R" value="100nF"/>
<part name="C20" library="_RCL" deviceset="C" device="_1UF/16V/0603/X7R" value="1uF/16V"/>
<part name="C21" library="_RCL" deviceset="C" device="_100NF/50V/0603/X7R" value="100nF"/>
<part name="C22" library="_RCL" deviceset="C" device="_100NF/50V/0603/X7R" value="100nF"/>
<part name="C23" library="_RCL" deviceset="C" device="_100NF/50V/0603/X7R" value="100nF"/>
<part name="C24" library="_RCL" deviceset="C" device="_100NF/50V/0603/X7R" value="100nF"/>
<part name="C25" library="_RCL" deviceset="C" device="_1UF/16V/0603/X7R" value="1uF/16V"/>
<part name="GND38" library="SupplyVeit" deviceset="GND" device=""/>
<part name="C26" library="_RCL" deviceset="C" device="_10UF/10V/0805/X5R" value="10uF/10V"/>
<part name="C28" library="_RCL" deviceset="C" device="_100NF/50V/0603/X7R" value="100nF"/>
<part name="C27" library="_RCL" deviceset="C" device="_100NF/50V/0603/X7R" value="100nF"/>
<part name="GND39" library="SupplyVeit" deviceset="GND" device=""/>
<part name="R28" library="_RCL" deviceset="R" device="_0R0/0603" value="*_DNP_0R0"/>
<part name="R29" library="_RCL" deviceset="R" device="_100K/0603" value="100k"/>
<part name="+3V20" library="SupplyVeit" deviceset="+3V3" device=""/>
<part name="C31" library="_RCL" deviceset="C" device="_100NF/50V/0603/X7R" value="100nF"/>
<part name="C32" library="_RCL" deviceset="C" device="_100NF/50V/0603/X7R" value="100nF"/>
<part name="C33" library="_RCL" deviceset="C" device="_100NF/50V/0603/X7R" value="100nF"/>
<part name="C34" library="_RCL" deviceset="C" device="_100NF/50V/0603/X7R" value="100nF"/>
<part name="GND40" library="SupplyVeit" deviceset="GND" device=""/>
<part name="GND41" library="SupplyVeit" deviceset="GND" device=""/>
<part name="GND42" library="SupplyVeit" deviceset="GND" device=""/>
<part name="GND43" library="SupplyVeit" deviceset="GND" device=""/>
<part name="GND44" library="SupplyVeit" deviceset="GND" device=""/>
<part name="GND45" library="SupplyVeit" deviceset="GND" device=""/>
<part name="GND46" library="SupplyVeit" deviceset="GND" device=""/>
<part name="GND47" library="SupplyVeit" deviceset="GND" device=""/>
<part name="R30" library="_RCL" deviceset="R" device="_22R/0603" value="22R"/>
<part name="CON6" library="_Connectors" deviceset="JACK2.1SMD" device=""/>
<part name="TVS7" library="CMI" deviceset="SMAJ*CA" device="" technology="30" value="SMAJ30CA"/>
<part name="D2" library="CMI" deviceset="NRVBS3100T3G" device=""/>
<part name="L18" library="_RCL" deviceset="BLM31PG121SH1L" device=""/>
<part name="L19" library="_RCL" deviceset="BLM31PG121SH1L" device=""/>
<part name="IC9" library="_Semiconductors" deviceset="TPS51120" device=""/>
<part name="C43" library="_RCL" deviceset="C" device="_1UF/16V/0603/X7R" value="1uF/16V"/>
<part name="C44" library="_RCL" deviceset="C" device="_10UF/10V/0805/X5R" value="10uF/10V"/>
<part name="C45" library="_RCL" deviceset="C" device="_10UF/10V/0805/X5R" value="10uF/10V"/>
<part name="GND48" library="SupplyVeit" deviceset="GND" device=""/>
<part name="GND49" library="SupplyVeit" deviceset="GND" device=""/>
<part name="GND50" library="SupplyVeit" deviceset="GND" device=""/>
<part name="R31" library="_RCL" deviceset="R" device="_4R7/0603" value="4R7"/>
<part name="C46" library="_RCL" deviceset="C" device="_1NF/50V/0603/X7R" value="1nF"/>
<part name="GND51" library="SupplyVeit" deviceset="GND" device=""/>
<part name="GND52" library="SupplyVeit" deviceset="GND" device=""/>
<part name="R32" library="_RCL" deviceset="R" device="_6K2/0603" value="6k2"/>
<part name="C47" library="_RCL" deviceset="C" device="_2N7/50V/0603/X7R" value="2,7nF"/>
<part name="C48" library="_RCL" deviceset="C" device="_1NF/50V/0603/X7R" value="1nF"/>
<part name="R33" library="_RCL" deviceset="R" device="_10K/0603/AEC" value="10k"/>
<part name="GND53" library="SupplyVeit" deviceset="GND" device=""/>
<part name="GND54" library="SupplyVeit" deviceset="GND" device=""/>
<part name="C49" library="_RCL" deviceset="C" device="_100NF/50V/0603/X7R" value="100nF"/>
<part name="C50" library="_RCL" deviceset="C" device="_100NF/50V/0603/X7R" value="100nF"/>
<part name="R34" library="_RCL" deviceset="R" device="_R015/1206" value="15mR"/>
<part name="R35" library="_RCL" deviceset="R" device="_R015/1206" value="15mR"/>
<part name="GND55" library="SupplyVeit" deviceset="GND" device=""/>
<part name="GND56" library="SupplyVeit" deviceset="GND" device=""/>
<part name="R36" library="_RCL" deviceset="R" device="_4R7/0603" value="4R7"/>
<part name="R37" library="_RCL" deviceset="R" device="_4R7/0603" value="4R7"/>
<part name="T1" library="_Semiconductors" deviceset="FDS6982" device=""/>
<part name="T2" library="_Semiconductors" deviceset="FDS6982" device=""/>
<part name="L20" library="_RCL" deviceset="4,7UH_7447797470" device="" value="4,7uH"/>
<part name="L21" library="_RCL" deviceset="8,2UH_744771008" device="" value="8,2uH"/>
<part name="C35" library="_RCL" deviceset="C" device="_47UF/6,3V/1210/X7R" value="47uF/6,3V"/>
<part name="C36" library="_RCL" deviceset="C" device="_47UF/6,3V/1210/X7R" value="47uF/6,3V"/>
<part name="C37" library="_RCL" deviceset="C" device="_47UF/6,3V/1210/X7R" value="47uF/6,3V"/>
<part name="C38" library="_RCL" deviceset="C" device="_47UF/6,3V/1210/X7R" value="47uF/6,3V"/>
<part name="C51" library="_RCL" deviceset="C_POL" device="D=6,3_H=7,7" value="330uF/10V"/>
<part name="C52" library="_RCL" deviceset="C_POL" device="D=6,3_H=7,7" value="330uF/10V"/>
<part name="+3V21" library="SupplyVeit" deviceset="+3V3" device=""/>
<part name="P+10" library="SupplyVeit" deviceset="+5V" device=""/>
<part name="C53" library="_RCL" deviceset="C" device="_100NF/50V/0603/X7R" value="100nF"/>
<part name="GND57" library="SupplyVeit" deviceset="GND" device=""/>
<part name="FRAME1" library="frames" deviceset="DINA4_L" device=""/>
<part name="FRAME2" library="frames" deviceset="DINA3_L" device=""/>
<part name="FRAME3" library="frames" deviceset="DINA4_L" device=""/>
<part name="FRAME4" library="frames" deviceset="DINA4_L" device=""/>
<part name="GND58" library="SupplyVeit" deviceset="GND" device=""/>
<part name="FRAME5" library="frames" deviceset="DINA4_L" device=""/>
<part name="CHASSIS21" library="SupplyVeit" deviceset="CHASSIS" device=""/>
<part name="FRAME6" library="frames" deviceset="DINA4_L" device=""/>
<part name="FRAME7" library="frames" deviceset="DINA4_L" device=""/>
<part name="GND59" library="SupplyVeit" deviceset="GND" device=""/>
<part name="P+11" library="SupplyVeit" deviceset="+24V" device=""/>
<part name="P+12" library="SupplyVeit" deviceset="+24V" device=""/>
<part name="P+13" library="SupplyVeit" deviceset="+24V" device=""/>
<part name="P+14" library="SupplyVeit" deviceset="+24V" device=""/>
<part name="C54" library="_RCL" deviceset="C" device="_100NF/50V/0603/X7R" value="100nF"/>
<part name="IC13" library="_Semiconductors" deviceset="LT1910" device="IS8#PBF"/>
<part name="T3" library="_Semiconductors" deviceset="FDS6982" device=""/>
<part name="R38" library="_RCL" deviceset="R" device="_R015/1206" value="15mR"/>
<part name="P+8" library="SupplyVeit" deviceset="+24V" device=""/>
<part name="GND60" library="SupplyVeit" deviceset="GND" device=""/>
<part name="C55" library="_RCL" deviceset="C" device="_100NF/50V/0603/X7R" value="100nF"/>
<part name="GND61" library="SupplyVeit" deviceset="GND" device=""/>
<part name="C56" library="_RCL" deviceset="C_POL" device="RM5,D13" value="1mF/35V"/>
<part name="R40" library="_RCL" deviceset="R" device="_10K/0603/AEC" value="10k"/>
<part name="+3V22" library="SupplyVeit" deviceset="+3V3" device=""/>
<part name="C57" library="_RCL" deviceset="C_POL" device="RM5,D13" value="1mF/35V"/>
<part name="C60" library="_RCL" deviceset="C" device="_100NF/50V/0603/X7R" value="100nF"/>
<part name="C61" library="_RCL" deviceset="C" device="_100NF/50V/0603/X7R" value="100nF"/>
<part name="C62" library="_RCL" deviceset="C" device="_100NF/50V/0603/X7R" value="100nF"/>
<part name="GND62" library="SupplyVeit" deviceset="GND" device=""/>
<part name="GND63" library="SupplyVeit" deviceset="GND" device=""/>
<part name="GND64" library="SupplyVeit" deviceset="GND" device=""/>
<part name="J1" library="_Special" deviceset="SOLDER_JUMPER2" device="" value="*_DNP_solder"/>
<part name="LED1" library="_Semiconductors" deviceset="LEDSMD" device="" value="APT3216LZGCK"/>
<part name="LED2" library="_Semiconductors" deviceset="LEDSMD" device="" value="APT3216LZGCK"/>
<part name="LED3" library="_Semiconductors" deviceset="LEDSMD" device="" value="APT3216LZGCK"/>
<part name="LED4" library="_Semiconductors" deviceset="LEDSMD" device="" value="APT3216LZGCK"/>
<part name="R39" library="_RCL" deviceset="R" device="_4K7/0603" value="4k7"/>
<part name="R41" library="_RCL" deviceset="R" device="_4K7/0603" value="4k7"/>
<part name="R42" library="_RCL" deviceset="R" device="_4K7/0603" value="4k7"/>
<part name="R43" library="_RCL" deviceset="R" device="_4K7/0603" value="4k7"/>
<part name="LED5" library="_Semiconductors" deviceset="LEDSMD" device="" value="APT3216LZGCK"/>
<part name="R44" library="_RCL" deviceset="R" device="_10K/0603/AEC" value="10k"/>
<part name="LED6" library="_Semiconductors" deviceset="LEDSMD" device="" value="APT3216LZGCK"/>
<part name="R45" library="_RCL" deviceset="R" device="_10K/0603/AEC" value="10k"/>
<part name="P+5" library="SupplyVeit" deviceset="+24V" device=""/>
<part name="LED7" library="_Semiconductors" deviceset="LEDSMD" device="" value="APT3216LZGCK"/>
<part name="R46" library="_RCL" deviceset="R" device="_4K7/0603" value="4k7"/>
<part name="LED8" library="_Semiconductors" deviceset="LEDSMD" device="" value="APT3216LZGCK"/>
<part name="R47" library="_RCL" deviceset="R" device="_4K7/0603" value="4k7"/>
<part name="GND65" library="SupplyVeit" deviceset="GND" device=""/>
<part name="GND66" library="SupplyVeit" deviceset="GND" device=""/>
<part name="GND67" library="SupplyVeit" deviceset="GND" device=""/>
<part name="GND68" library="SupplyVeit" deviceset="GND" device=""/>
<part name="+3V23" library="SupplyVeit" deviceset="+3V3" device=""/>
<part name="P+15" library="SupplyVeit" deviceset="+5V" device=""/>
<part name="C63" library="_RCL" deviceset="C" device="_100NF/50V/0603/X7R" value="100nF"/>
<part name="GND69" library="SupplyVeit" deviceset="GND" device=""/>
<part name="GND71" library="SupplyVeit" deviceset="GND" device=""/>
<part name="CHASSIS22" library="SupplyVeit" deviceset="CHASSIS" device=""/>
<part name="GND29" library="SupplyVeit" deviceset="GND" device=""/>
<part name="X1" library="_Special" deviceset="ESD_HOLE" device="3,2" value="*_DNP_ESD_HOLE3,2"/>
<part name="X2" library="_Special" deviceset="ESD_HOLE" device="3,2" value="*_DNP_ESD_HOLE3,2"/>
<part name="X3" library="_Special" deviceset="ESD_HOLE" device="3,2" value="*_DNP_ESD_HOLE3,2"/>
<part name="X4" library="_Special" deviceset="ESD_HOLE" device="3,2" value="*_DNP_ESD_HOLE3,2"/>
<part name="X5" library="_Special" deviceset="ESD_HOLE" device="3,2" value="*_DNP_ESD_HOLE3,2"/>
<part name="CHASSIS23" library="SupplyVeit" deviceset="CHASSIS" device=""/>
<part name="XTAL1" library="_Special" deviceset="XTAL-SMD" device="" value="ABM3C-24.000MHZ-D4Y-T"/>
<part name="GND72" library="SupplyVeit" deviceset="GND" device=""/>
<part name="X6" library="_Connectors" deviceset="MKDSN1,5/2-5,08" device="" value="*_DNP_MKDSN1,5/2-5,08"/>
<part name="L9" library="_RCL" deviceset="BLM31PG121SH1L" device=""/>
<part name="L22" library="_RCL" deviceset="BLM31PG121SH1L" device=""/>
<part name="L23" library="_RCL" deviceset="BLM31PG121SH1L" device=""/>
<part name="L24" library="_RCL" deviceset="BLM31PG121SH1L" device=""/>
<part name="L25" library="_RCL" deviceset="BLM31PG121SH1L" device=""/>
<part name="GND70" library="SupplyVeit" deviceset="GND" device=""/>
<part name="CHASSIS24" library="SupplyVeit" deviceset="CHASSIS" device=""/>
<part name="D3" library="CMI" deviceset="NRVBS3100T3G" device="" value="*_DNP_NRVBS3100T3G"/>
<part name="C15" library="_RCL" deviceset="C" device="_100NF/50V/0603/X7R" value="100nF"/>
<part name="GND73" library="SupplyVeit" deviceset="GND" device=""/>
<part name="C29" library="_RCL" deviceset="C" device="_100NF/50V/0603/X7R" value="100nF"/>
<part name="FRAME8" library="frames" deviceset="DINA4_L" device=""/>
<part name="+3V24" library="SupplyVeit" deviceset="+3V3" device=""/>
<part name="P+16" library="SupplyVeit" deviceset="+5V" device=""/>
<part name="P+17" library="SupplyVeit" deviceset="+24V" device=""/>
<part name="CHASSIS25" library="SupplyVeit" deviceset="CHASSIS" device=""/>
<part name="GND74" library="SupplyVeit" deviceset="GND" device=""/>
<part name="+3V25" library="SupplyVeit" deviceset="+3V3" device=""/>
<part name="P+18" library="SupplyVeit" deviceset="+5V" device=""/>
<part name="P+19" library="SupplyVeit" deviceset="+24V" device=""/>
<part name="CHASSIS26" library="SupplyVeit" deviceset="CHASSIS" device=""/>
<part name="GND75" library="SupplyVeit" deviceset="GND" device=""/>
<part name="+3V26" library="SupplyVeit" deviceset="+3V3" device=""/>
<part name="P+20" library="SupplyVeit" deviceset="+5V" device=""/>
<part name="P+21" library="SupplyVeit" deviceset="+24V" device=""/>
<part name="CHASSIS27" library="SupplyVeit" deviceset="CHASSIS" device=""/>
<part name="GND76" library="SupplyVeit" deviceset="GND" device=""/>
<part name="+3V27" library="SupplyVeit" deviceset="+3V3" device=""/>
<part name="P+22" library="SupplyVeit" deviceset="+5V" device=""/>
<part name="P+23" library="SupplyVeit" deviceset="+24V" device=""/>
<part name="CHASSIS28" library="SupplyVeit" deviceset="CHASSIS" device=""/>
<part name="GND77" library="SupplyVeit" deviceset="GND" device=""/>
<part name="+3V28" library="SupplyVeit" deviceset="+3V3" device=""/>
<part name="P+24" library="SupplyVeit" deviceset="+5V" device=""/>
<part name="P+25" library="SupplyVeit" deviceset="+24V" device=""/>
<part name="CHASSIS29" library="SupplyVeit" deviceset="CHASSIS" device=""/>
<part name="GND78" library="SupplyVeit" deviceset="GND" device=""/>
<part name="+3V29" library="SupplyVeit" deviceset="+3V3" device=""/>
<part name="P+26" library="SupplyVeit" deviceset="+5V" device=""/>
<part name="P+27" library="SupplyVeit" deviceset="+24V" device=""/>
<part name="CHASSIS30" library="SupplyVeit" deviceset="CHASSIS" device=""/>
<part name="GND79" library="SupplyVeit" deviceset="GND" device=""/>
<part name="+3V30" library="SupplyVeit" deviceset="+3V3" device=""/>
<part name="P+28" library="SupplyVeit" deviceset="+5V" device=""/>
<part name="P+29" library="SupplyVeit" deviceset="+24V" device=""/>
<part name="CHASSIS31" library="SupplyVeit" deviceset="CHASSIS" device=""/>
<part name="GND80" library="SupplyVeit" deviceset="GND" device=""/>
<part name="L26" library="_RCL" deviceset="DLW21SN900SQ2" device=""/>
<part name="L27" library="_RCL" deviceset="DLW21SN900SQ2" device=""/>
<part name="L28" library="_RCL" deviceset="DLW21SN900SQ2" device=""/>
<part name="L29" library="_RCL" deviceset="DLW21SN900SQ2" device=""/>
<part name="C30" library="_RCL" deviceset="C" device="_100NF/50V/0603/X7R" value="100nF"/>
<part name="C39" library="_RCL" deviceset="C" device="_100NF/50V/0603/X7R" value="100nF"/>
<part name="C40" library="_RCL" deviceset="C" device="_100NF/50V/0603/X7R" value="100nF"/>
<part name="C41" library="_RCL" deviceset="C" device="_100NF/50V/0603/X7R" value="100nF"/>
<part name="J2" library="_Special" deviceset="SOLDER_JUMPER3" device="" value="*_DNP_solder"/>
<part name="R12" library="_RCL" deviceset="R" device="_100K/0603" value="100k"/>
<part name="R13" library="_RCL" deviceset="R" device="_100K/0603" value="100k"/>
<part name="R16" library="_RCL" deviceset="R" device="_100K/0603" value="100k"/>
<part name="GND82" library="SupplyVeit" deviceset="GND" device=""/>
<part name="GND81" library="SupplyVeit" deviceset="GND" device=""/>
<part name="GND83" library="SupplyVeit" deviceset="GND" device=""/>
<part name="R17" library="_RCL" deviceset="R" device="_100K/0603" value="100k"/>
<part name="R48" library="_RCL" deviceset="R" device="_100K/0603" value="100k"/>
<part name="R49" library="_RCL" deviceset="R" device="_100K/0603" value="100k"/>
<part name="R50" library="_RCL" deviceset="R" device="_100K/0603" value="100k"/>
<part name="+3V32" library="SupplyVeit" deviceset="+3V3" device=""/>
<part name="R51" library="_RCL" deviceset="R" device="_10K/0603/AEC" value="10k"/>
<part name="J3" library="_Special" deviceset="SOLDER_JUMPER2" device="" value="*_DNP_solder"/>
<part name="J4" library="_Special" deviceset="SOLDER_JUMPER2" device="" value="*_DNP_solder"/>
<part name="C42" library="_RCL" deviceset="C" device="_100NF/50V/0603/X7R" value="100nF"/>
<part name="IC12" library="CMI" deviceset="SN74LVC1T45" device="DBV"/>
<part name="IC14" library="CMI" deviceset="SN74LVC1T45" device="DBV"/>
<part name="IC15" library="CMI" deviceset="SN74LVC1G04" device="DBV"/>
<part name="IC16" library="CMI" deviceset="SN74LVC1G04" device="DBV"/>
<part name="GND84" library="SupplyVeit" deviceset="GND" device=""/>
<part name="C58" library="_RCL" deviceset="C" device="_100NF/50V/0603/X7R" value="100nF"/>
<part name="GND85" library="SupplyVeit" deviceset="GND" device=""/>
<part name="R52" library="_RCL" deviceset="R" device="_100K/0603" value="100k"/>
<part name="R53" library="_RCL" deviceset="R" device="_100K/0603" value="100k"/>
<part name="J5" library="_Special" deviceset="SOLDER_JUMPER2" device="" value="*_DNP_solder"/>
<part name="J6" library="_Special" deviceset="SOLDER_JUMPER2" device="" value="*_DNP_solder"/>
<part name="+3V33" library="SupplyVeit" deviceset="+3V3" device=""/>
<part name="C59" library="_RCL" deviceset="C" device="_100NF/50V/0603/X7R" value="100nF"/>
<part name="GND86" library="SupplyVeit" deviceset="GND" device=""/>
<part name="P+32" library="SupplyVeit" deviceset="+5V" device=""/>
<part name="GND87" library="SupplyVeit" deviceset="GND" device=""/>
<part name="GND88" library="SupplyVeit" deviceset="GND" device=""/>
<part name="GND89" library="SupplyVeit" deviceset="GND" device=""/>
<part name="GND90" library="SupplyVeit" deviceset="GND" device=""/>
<part name="GND91" library="SupplyVeit" deviceset="GND" device=""/>
<part name="+3V31" library="SupplyVeit" deviceset="+3V3" device=""/>
<part name="R54" library="_RCL" deviceset="R" device="_120R/0805" value="120R"/>
<part name="R55" library="_RCL" deviceset="R" device="_120R/0805" value="120R"/>
<part name="SW1" library="CMI" deviceset="B3F-1000" device=""/>
<part name="R56" library="_RCL" deviceset="R" device="_100K/0603" value="100k"/>
<part name="R57" library="_RCL" deviceset="R" device="_100K/0603" value="100k"/>
<part name="R58" library="_RCL" deviceset="R" device="_100K/0603" value="100k"/>
<part name="R59" library="_RCL" deviceset="R" device="_100K/0603" value="100k"/>
<part name="GND92" library="SupplyVeit" deviceset="GND" device=""/>
<part name="GND93" library="SupplyVeit" deviceset="GND" device=""/>
<part name="GND94" library="SupplyVeit" deviceset="GND" device=""/>
<part name="GND95" library="SupplyVeit" deviceset="GND" device=""/>
<part name="CON2" library="CMI" deviceset="CTB9302/5" device=""/>
<part name="CON4" library="CMI" deviceset="CTB9302/5" device=""/>
<part name="TVS2" library="CMI" deviceset="SMAJ*CA" device="" technology="30" value="SMAJ30CA"/>
<part name="TVS1" library="CMI" deviceset="SMAJ*CA" device="" technology="30" value="SMAJ30CA"/>
<part name="TVS4" library="CMI" deviceset="SMAJ*CA" device="" technology="30" value="SMAJ30CA"/>
<part name="TVS5" library="CMI" deviceset="SMAJ*CA" device="" technology="30" value="SMAJ30CA"/>
<part name="P+30" library="SupplyVeit" deviceset="+5V" device=""/>
<part name="GND96" library="SupplyVeit" deviceset="GND" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<text x="60.96" y="170.18" size="1.778" layer="91" align="top-left"> V0.2 changes log:
============

- dotažení zemního ringu ve vrstvě BOT
- odpojení vstupů SDA, SCL, HS_IND obvodu USB2514B (IC8) a ošetřeno pull-down rezistory
- záměna obvodu TPS2044BDR (IC6) za TPS2054BDR (má opačnou polaritu zapínacích vstupů ENx)
- open-drain výstupy (OCx) TPS2054BDR ošetřeny pull-up rezistory
- SMD resetovací tlačítko nahrazeno THT tlačítkem
- označena polarita diod D2 a D3 přímo v Cu, upraven obrys součástky pro snadnější rozlišení polarity
- držák baterie (přidány skladová čísla součástí do schématu)
- přidány invertory pro řízení budičů RS-485 + možnost zapnutí echa (kontrola vysílaných dat)
- přidány level-shiftery na výstupy RO budičů RS-485 (RO je 5V výstup, Colibri je 3,3V)
- přidány terminační rezistory 120R mezi linkové vodiče RS-485
- přidány pull-down rezistory na vstupy ENx (IC6)
- změna konektorů pro RS-485 na CTB9302/5
- vyplnění atributů pro budoucí automatické generování součástkového stromu do ABRY
- po kontrole smazán seznam součástek na listu č. 1 (nahrzují ho atributy MPN a MELE)
- vstup FOE IC7 (RX-8025SA) preventivně uzemněn</text>
<text x="10.16" y="109.22" size="1.778" layer="91" align="top-left">POPIS DůLEŽITÝCH ATRIBUTů:
===================
MPN: Manufacturer Product Number - jednoznačný primární identifikátor součástky (v Abře by ho měla obsahovat každá skladová karta - za středníkem)
MELE: naše skladové číslo z ABRY, uvedeno pro kontrolu a rychlejší hledání (skripty by měly umět zkontolovat a upozornit na inkonzistenci mezi MPN a MELE)
OSAZENI: kde se bude daná součástka osazovat (RACOM/VEIT)
DODANI: kdo součástku dodá na osazení (RACOM/VEIT)
PRISLUSENSTVI: atribut určuje další díly potřebné pro kompletaci desky nebo její montáž do zařízení,
    jedná se pouze o prvky přímo související se součástkou, u které je atribut uveden 
    (typicky např. protikusy konektorů, zásuvné moduly, baterie do držáků, paměťové moduly/karty), pozn. PRISLUSENSTVI není nutné dodávat na osazování
    formát zápisu: MELExxxx, MELEyyyy, MELEzzzz, ...</text>
<text x="10.16" y="71.12" size="1.778" layer="91" align="top-left">POPIS OSTATNÍCH (NEPOVINNÝCH) ATRIBUTů:
==============================
MARKING: zkrácené kódové označení typu na SMD součástce (usnadňuje identifikaci součástky při pracech na desce)
MF: ManuFacturer - název výrobce součástky (pro rychlou orientaci)
OC_GM: OrderCode GM electronic (xxx-yyy)
OC_FARNELL: OrderCode Farnell (xxxxxxx)
OC_MOUSER: OrderCode Mouser
OC_DIGI-KEY: OrderCode Digi-Key</text>
<text x="10.16" y="43.18" size="1.778" layer="91" align="top-left">Poznámka k osazování:
===============
Součástky jejichž VALUE začíná řetězcem

*_DNP_

se neosazují a zároveň není potřeba je přidávat do stromu součástek v ABŘE</text>
</plain>
<instances>
<instance part="+3V1" gate="G$3" x="25.4" y="167.64"/>
<instance part="P+1" gate="1" x="25.4" y="157.48"/>
<instance part="P+2" gate="1" x="25.4" y="147.32"/>
<instance part="CHASSIS1" gate="G$1" x="25.4" y="119.38"/>
<instance part="GND31" gate="1" x="25.4" y="132.08"/>
<instance part="FRAME8" gate="G$1" x="0" y="0"/>
<instance part="FRAME8" gate="G$2" x="162.56" y="0"/>
</instances>
<busses>
</busses>
<nets>
<net name="POWER_3V3" class="0">
<segment>
<pinref part="+3V1" gate="G$3" pin="POWER_3V3"/>
<wire x1="25.4" y1="165.1" x2="25.4" y2="162.56" width="0.1524" layer="91"/>
<wire x1="25.4" y1="162.56" x2="33.02" y2="162.56" width="0.1524" layer="91"/>
<label x="33.02" y="162.56" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="POWER_+5V" class="0">
<segment>
<pinref part="P+1" gate="1" pin="+5V"/>
<wire x1="25.4" y1="154.94" x2="25.4" y2="152.4" width="0.1524" layer="91"/>
<wire x1="25.4" y1="152.4" x2="33.02" y2="152.4" width="0.1524" layer="91"/>
<label x="33.02" y="152.4" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="POWER_24V" class="0">
<segment>
<pinref part="P+2" gate="1" pin="+24V"/>
<wire x1="25.4" y1="144.78" x2="25.4" y2="142.24" width="0.1524" layer="91"/>
<wire x1="25.4" y1="142.24" x2="33.02" y2="142.24" width="0.1524" layer="91"/>
<label x="33.02" y="142.24" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="POWER_GND" class="0">
<segment>
<pinref part="GND31" gate="1" pin="GND"/>
<wire x1="25.4" y1="134.62" x2="25.4" y2="137.16" width="0.1524" layer="91"/>
<wire x1="25.4" y1="137.16" x2="33.02" y2="137.16" width="0.1524" layer="91"/>
<label x="33.02" y="137.16" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="CHASSIS" class="0">
<segment>
<pinref part="CHASSIS1" gate="G$1" pin="CHASSIS"/>
<wire x1="25.4" y1="121.92" x2="25.4" y2="124.46" width="0.1524" layer="91"/>
<wire x1="25.4" y1="124.46" x2="33.02" y2="124.46" width="0.1524" layer="91"/>
<label x="33.02" y="124.46" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<text x="37.338" y="73.152" size="1.27" layer="91">J2 - kdyz se propaji, tak tim lze zapnout vnitrni kompenzaci</text>
</plain>
<instances>
<instance part="CON6" gate="G$1" x="15.24" y="154.94" smashed="yes">
<attribute name="VALUE" x="17.145" y="142.875" size="1.778" layer="96" rot="R180"/>
<attribute name="NAME" x="3.81" y="144.272" size="1.778" layer="95"/>
</instance>
<instance part="TVS7" gate="G$1" x="43.18" y="157.48"/>
<instance part="D2" gate="G$1" x="76.2" y="167.64" smashed="yes">
<attribute name="NAME" x="73.8886" y="170.2816" size="1.778" layer="95"/>
<attribute name="VALUE" x="69.8246" y="163.1442" size="1.778" layer="96"/>
</instance>
<instance part="L18" gate="G$1" x="55.88" y="167.64" smashed="yes" rot="R90">
<attribute name="NAME" x="54.864" y="171.958" size="1.778" layer="95" rot="MR180"/>
<attribute name="VALUE" x="48.26" y="166.624" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="L19" gate="G$1" x="55.88" y="149.86" smashed="yes" rot="R90">
<attribute name="NAME" x="54.864" y="154.178" size="1.778" layer="95" rot="MR180"/>
<attribute name="VALUE" x="48.26" y="148.844" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="IC9" gate="_LDO" x="134.62" y="50.8"/>
<instance part="IC9" gate="_5V" x="27.94" y="109.22"/>
<instance part="IC9" gate="_3,3V" x="27.94" y="38.1"/>
<instance part="C43" gate="G$1" x="152.4" y="53.34" smashed="yes" rot="R90">
<attribute name="NAME" x="154.178" y="54.102" size="1.778" layer="95"/>
<attribute name="VALUE" x="154.178" y="51.943" size="1.778" layer="96"/>
</instance>
<instance part="C44" gate="G$1" x="152.4" y="63.5" smashed="yes" rot="R90">
<attribute name="NAME" x="154.178" y="64.262" size="1.778" layer="95"/>
<attribute name="VALUE" x="154.178" y="62.103" size="1.778" layer="96"/>
</instance>
<instance part="C45" gate="G$1" x="152.4" y="43.18" smashed="yes" rot="R90">
<attribute name="NAME" x="154.178" y="43.942" size="1.778" layer="95"/>
<attribute name="VALUE" x="154.178" y="41.783" size="1.778" layer="96"/>
</instance>
<instance part="GND48" gate="1" x="152.4" y="58.42"/>
<instance part="GND49" gate="1" x="152.4" y="48.26"/>
<instance part="GND50" gate="1" x="152.4" y="38.1"/>
<instance part="R31" gate="G$1" x="167.64" y="60.96" smashed="yes" rot="R270">
<attribute name="NAME" x="172.212" y="58.674" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="168.529" y="58.42" size="1.6764" layer="96" rot="R90"/>
</instance>
<instance part="C46" gate="G$1" x="152.4" y="33.02" rot="R90"/>
<instance part="GND51" gate="1" x="152.4" y="27.94"/>
<instance part="GND52" gate="1" x="119.38" y="30.48"/>
<instance part="R32" gate="G$1" x="12.7" y="93.98" smashed="yes" rot="R270">
<attribute name="NAME" x="17.272" y="91.694" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="13.589" y="91.44" size="1.6764" layer="96" rot="R90"/>
</instance>
<instance part="C47" gate="G$1" x="12.7" y="83.82" rot="R90"/>
<instance part="C48" gate="G$1" x="12.7" y="12.7" rot="R90"/>
<instance part="R33" gate="G$1" x="12.7" y="22.86" smashed="yes" rot="R270">
<attribute name="NAME" x="17.272" y="20.574" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="13.589" y="20.32" size="1.6764" layer="96" rot="R90"/>
</instance>
<instance part="GND53" gate="1" x="12.7" y="76.2"/>
<instance part="GND54" gate="1" x="12.7" y="5.08"/>
<instance part="C49" gate="G$1" x="45.72" y="111.76" rot="R90"/>
<instance part="C50" gate="G$1" x="45.72" y="40.64" rot="R90"/>
<instance part="R34" gate="G$1" x="63.5" y="22.86" rot="R90"/>
<instance part="R35" gate="G$1" x="63.5" y="93.98" rot="R90"/>
<instance part="GND55" gate="1" x="63.5" y="10.16"/>
<instance part="GND56" gate="1" x="63.5" y="81.28"/>
<instance part="R36" gate="G$1" x="48.26" y="119.38" smashed="yes">
<attribute name="NAME" x="50.546" y="123.952" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="50.8" y="120.269" size="1.6764" layer="96" rot="R180"/>
</instance>
<instance part="R37" gate="G$1" x="48.26" y="48.26" smashed="yes">
<attribute name="NAME" x="50.546" y="52.832" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="50.8" y="49.149" size="1.6764" layer="96" rot="R180"/>
</instance>
<instance part="T1" gate="_1" x="60.96" y="121.92"/>
<instance part="T1" gate="_2" x="60.96" y="106.68"/>
<instance part="T2" gate="_1" x="60.96" y="50.8"/>
<instance part="T2" gate="_2" x="60.96" y="35.56"/>
<instance part="L20" gate="G$1" x="83.82" y="43.18"/>
<instance part="L21" gate="G$1" x="83.82" y="114.3"/>
<instance part="C51" gate="G$1" x="93.98" y="38.1" smashed="yes">
<attribute name="NAME" x="96.52" y="38.1" size="1.778" layer="95"/>
<attribute name="VALUE" x="96.52" y="35.56" size="1.778" layer="96"/>
</instance>
<instance part="C52" gate="G$1" x="93.98" y="109.22" smashed="yes">
<attribute name="NAME" x="96.52" y="109.22" size="1.778" layer="95"/>
<attribute name="VALUE" x="96.52" y="106.68" size="1.778" layer="96"/>
</instance>
<instance part="+3V21" gate="G$3" x="104.14" y="43.18" rot="R270"/>
<instance part="P+10" gate="1" x="106.68" y="114.3" rot="R270"/>
<instance part="FRAME4" gate="G$1" x="0" y="0"/>
<instance part="FRAME4" gate="G$2" x="162.56" y="0"/>
<instance part="GND58" gate="1" x="99.06" y="144.78"/>
<instance part="P+11" gate="1" x="63.5" y="142.24"/>
<instance part="P+12" gate="1" x="63.5" y="71.12"/>
<instance part="P+13" gate="1" x="119.38" y="76.2"/>
<instance part="P+14" gate="1" x="99.06" y="172.72" rot="MR0"/>
<instance part="C54" gate="G$1" x="33.02" y="157.48" rot="R90"/>
<instance part="IC13" gate="G$1" x="170.18" y="142.24" smashed="yes">
<attribute name="NAME" x="164.084" y="156.591" size="2.54" layer="95"/>
<attribute name="VALUE" x="162.814" y="153.035" size="2.54" layer="96"/>
</instance>
<instance part="T3" gate="_1" x="190.5" y="139.7" smashed="yes">
<attribute name="NAME" x="196.342" y="137.922" size="1.778" layer="95"/>
<attribute name="VALUE" x="196.342" y="133.604" size="1.778" layer="96"/>
</instance>
<instance part="T3" gate="_2" x="190.5" y="127" smashed="yes" rot="MR180">
<attribute name="NAME" x="196.596" y="129.032" size="1.778" layer="95"/>
</instance>
<instance part="R38" gate="G$1" x="203.2" y="154.94" rot="R90"/>
<instance part="P+8" gate="1" x="203.2" y="167.64" rot="MR0"/>
<instance part="GND60" gate="1" x="170.18" y="127"/>
<instance part="C55" gate="G$1" x="157.48" y="134.62" rot="R90"/>
<instance part="GND61" gate="1" x="157.48" y="127"/>
<instance part="C56" gate="G$1" x="88.9" y="157.48"/>
<instance part="R40" gate="G$1" x="147.32" y="157.48" smashed="yes" rot="R90">
<attribute name="NAME" x="145.796" y="155.194" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="148.209" y="155.448" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="+3V22" gate="G$3" x="147.32" y="167.64"/>
<instance part="C57" gate="G$1" x="99.06" y="157.48"/>
<instance part="C60" gate="G$1" x="68.58" y="137.16" rot="R180"/>
<instance part="C61" gate="G$1" x="68.58" y="66.04" rot="R180"/>
<instance part="C62" gate="G$1" x="114.3" y="66.04" rot="R180"/>
<instance part="GND62" gate="1" x="109.22" y="60.96"/>
<instance part="GND63" gate="1" x="73.66" y="60.96"/>
<instance part="GND64" gate="1" x="73.66" y="132.08"/>
<instance part="J1" gate="G$1" x="213.36" y="139.7" rot="R90"/>
<instance part="LED5" gate="G$1" x="218.44" y="71.12" rot="R270"/>
<instance part="R44" gate="G$1" x="218.44" y="83.82" rot="R90"/>
<instance part="LED6" gate="G$1" x="228.6" y="71.12" rot="R270"/>
<instance part="R45" gate="G$1" x="228.6" y="83.82" rot="R90"/>
<instance part="P+5" gate="1" x="218.44" y="93.98"/>
<instance part="LED7" gate="G$1" x="238.76" y="71.12" rot="R270"/>
<instance part="R46" gate="G$1" x="238.76" y="83.82" rot="R90"/>
<instance part="LED8" gate="G$1" x="248.92" y="71.12" rot="R270"/>
<instance part="R47" gate="G$1" x="248.92" y="83.82" rot="R90"/>
<instance part="GND65" gate="1" x="218.44" y="60.96"/>
<instance part="GND66" gate="1" x="228.6" y="60.96"/>
<instance part="GND67" gate="1" x="238.76" y="60.96"/>
<instance part="GND68" gate="1" x="248.92" y="60.96"/>
<instance part="+3V23" gate="G$3" x="248.92" y="93.98"/>
<instance part="P+15" gate="1" x="238.76" y="93.98"/>
<instance part="CHASSIS22" gate="G$1" x="25.4" y="144.78"/>
<instance part="X1" gate="G$1" x="238.76" y="170.18"/>
<instance part="X2" gate="G$1" x="238.76" y="165.1"/>
<instance part="X3" gate="G$1" x="238.76" y="160.02"/>
<instance part="X4" gate="G$1" x="238.76" y="154.94"/>
<instance part="X5" gate="G$1" x="238.76" y="149.86"/>
<instance part="CHASSIS23" gate="G$1" x="246.38" y="144.78"/>
<instance part="X6" gate="G$1" x="15.24" y="167.64" smashed="yes" rot="MR0">
<attribute name="NAME" x="11.43" y="168.275" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="3.81" y="175.26" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="D3" gate="G$1" x="129.54" y="119.38"/>
<instance part="C15" gate="G$1" x="213.36" y="114.3" rot="R90"/>
<instance part="GND73" gate="1" x="213.36" y="106.68"/>
<instance part="C29" gate="G$1" x="68.58" y="157.48" rot="R90"/>
<instance part="+3V24" gate="G$3" x="187.96" y="88.9"/>
<instance part="P+16" gate="1" x="187.96" y="78.74"/>
<instance part="P+17" gate="1" x="187.96" y="68.58"/>
<instance part="CHASSIS25" gate="G$1" x="187.96" y="40.64"/>
<instance part="GND74" gate="1" x="187.96" y="53.34"/>
<instance part="J2" gate="G$1" x="33.02" y="83.82" smashed="yes" rot="R270">
<attribute name="NAME" x="38.1" y="83.82" size="1.778" layer="95"/>
<attribute name="VALUE" x="37.846" y="81.153" size="1.778" layer="96"/>
</instance>
<instance part="R51" gate="G$1" x="157.48" y="157.48" smashed="yes" rot="R90">
<attribute name="NAME" x="155.956" y="155.194" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="158.369" y="155.448" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="J3" gate="G$1" x="139.7" y="149.86"/>
<instance part="J4" gate="G$1" x="139.7" y="139.7"/>
<instance part="P+30" gate="1" x="157.48" y="167.64" smashed="yes">
<attribute name="VALUE" x="154.94" y="170.18" size="1.778" layer="96"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="POWER_3V3" class="0">
<segment>
<pinref part="IC9" gate="_3,3V" pin="VO"/>
<wire x1="40.64" y1="53.34" x2="53.34" y2="53.34" width="0.1524" layer="91"/>
<wire x1="53.34" y1="53.34" x2="58.42" y2="58.42" width="0.1524" layer="91"/>
<wire x1="58.42" y1="58.42" x2="76.2" y2="58.42" width="0.1524" layer="91"/>
<wire x1="76.2" y1="58.42" x2="78.74" y2="58.42" width="0.1524" layer="91"/>
<wire x1="78.74" y1="58.42" x2="93.98" y2="43.18" width="0.1524" layer="91"/>
<pinref part="L20" gate="G$1" pin="2"/>
<wire x1="93.98" y1="43.18" x2="91.44" y2="43.18" width="0.1524" layer="91"/>
<wire x1="93.98" y1="43.18" x2="101.6" y2="43.18" width="0.1524" layer="91"/>
<junction x="93.98" y="43.18"/>
<pinref part="C51" gate="G$1" pin="A"/>
<wire x1="93.98" y1="40.64" x2="93.98" y2="43.18" width="0.1524" layer="91"/>
<pinref part="+3V21" gate="G$3" pin="POWER_3V3"/>
</segment>
<segment>
<pinref part="R40" gate="G$1" pin="2"/>
<pinref part="+3V22" gate="G$3" pin="POWER_3V3"/>
<wire x1="147.32" y1="162.56" x2="147.32" y2="165.1" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R47" gate="G$1" pin="2"/>
<wire x1="248.92" y1="91.44" x2="248.92" y2="88.9" width="0.1524" layer="91"/>
<pinref part="+3V23" gate="G$3" pin="POWER_3V3"/>
</segment>
<segment>
<pinref part="+3V24" gate="G$3" pin="POWER_3V3"/>
<wire x1="187.96" y1="86.36" x2="187.96" y2="83.82" width="0.1524" layer="91"/>
<wire x1="187.96" y1="83.82" x2="195.58" y2="83.82" width="0.1524" layer="91"/>
<label x="195.58" y="83.82" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="POWER_GND" class="0">
<segment>
<pinref part="C44" gate="G$1" pin="1"/>
<pinref part="GND48" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C43" gate="G$1" pin="1"/>
<pinref part="GND49" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C45" gate="G$1" pin="1"/>
<pinref part="GND50" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C46" gate="G$1" pin="1"/>
<pinref part="GND51" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="IC9" gate="_LDO" pin="GND"/>
<wire x1="121.92" y1="38.1" x2="119.38" y2="38.1" width="0.1524" layer="91"/>
<wire x1="119.38" y1="38.1" x2="119.38" y2="35.56" width="0.1524" layer="91"/>
<pinref part="IC9" gate="_LDO" pin="PAD"/>
<wire x1="119.38" y1="35.56" x2="121.92" y2="35.56" width="0.1524" layer="91"/>
<wire x1="119.38" y1="35.56" x2="119.38" y2="33.02" width="0.1524" layer="91"/>
<junction x="119.38" y="35.56"/>
<pinref part="GND52" gate="1" pin="GND"/>
<pinref part="IC9" gate="_LDO" pin="SKIPSEL"/>
<wire x1="121.92" y1="43.18" x2="119.38" y2="43.18" width="0.1524" layer="91"/>
<wire x1="119.38" y1="43.18" x2="119.38" y2="38.1" width="0.1524" layer="91"/>
<junction x="119.38" y="38.1"/>
<pinref part="IC9" gate="_LDO" pin="TONSEL"/>
<wire x1="121.92" y1="48.26" x2="119.38" y2="48.26" width="0.1524" layer="91"/>
<wire x1="119.38" y1="48.26" x2="119.38" y2="43.18" width="0.1524" layer="91"/>
<junction x="119.38" y="43.18"/>
</segment>
<segment>
<pinref part="C47" gate="G$1" pin="1"/>
<pinref part="GND53" gate="1" pin="GND"/>
<wire x1="12.7" y1="78.74" x2="12.7" y2="81.28" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C48" gate="G$1" pin="1"/>
<pinref part="GND54" gate="1" pin="GND"/>
<wire x1="12.7" y1="7.62" x2="12.7" y2="10.16" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R34" gate="G$1" pin="1"/>
<pinref part="GND55" gate="1" pin="GND"/>
<wire x1="63.5" y1="17.78" x2="63.5" y2="15.24" width="0.1524" layer="91"/>
<pinref part="IC9" gate="_3,3V" pin="PGND"/>
<wire x1="63.5" y1="15.24" x2="63.5" y2="12.7" width="0.1524" layer="91"/>
<wire x1="63.5" y1="15.24" x2="55.88" y2="22.86" width="0.1524" layer="91"/>
<wire x1="55.88" y1="22.86" x2="40.64" y2="22.86" width="0.1524" layer="91"/>
<junction x="63.5" y="15.24"/>
<pinref part="C51" gate="G$1" pin="C"/>
<wire x1="93.98" y1="35.56" x2="93.98" y2="33.02" width="0.1524" layer="91"/>
<wire x1="93.98" y1="33.02" x2="76.2" y2="15.24" width="0.1524" layer="91"/>
<wire x1="76.2" y1="15.24" x2="63.5" y2="15.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R35" gate="G$1" pin="1"/>
<pinref part="GND56" gate="1" pin="GND"/>
<wire x1="63.5" y1="88.9" x2="63.5" y2="86.36" width="0.1524" layer="91"/>
<pinref part="IC9" gate="_5V" pin="PGND"/>
<wire x1="63.5" y1="86.36" x2="63.5" y2="83.82" width="0.1524" layer="91"/>
<wire x1="63.5" y1="86.36" x2="55.88" y2="93.98" width="0.1524" layer="91"/>
<wire x1="55.88" y1="93.98" x2="40.64" y2="93.98" width="0.1524" layer="91"/>
<junction x="63.5" y="86.36"/>
<pinref part="C52" gate="G$1" pin="C"/>
<wire x1="93.98" y1="106.68" x2="93.98" y2="104.14" width="0.1524" layer="91"/>
<wire x1="93.98" y1="104.14" x2="76.2" y2="86.36" width="0.1524" layer="91"/>
<wire x1="76.2" y1="86.36" x2="63.5" y2="86.36" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC13" gate="G$1" pin="GND"/>
<pinref part="GND60" gate="1" pin="GND"/>
<wire x1="170.18" y1="129.54" x2="170.18" y2="134.62" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C55" gate="G$1" pin="1"/>
<pinref part="GND61" gate="1" pin="GND"/>
<wire x1="157.48" y1="129.54" x2="157.48" y2="132.08" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C62" gate="G$1" pin="2"/>
<wire x1="111.76" y1="66.04" x2="109.22" y2="66.04" width="0.1524" layer="91"/>
<pinref part="GND62" gate="1" pin="GND"/>
<wire x1="109.22" y1="66.04" x2="109.22" y2="63.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C61" gate="G$1" pin="1"/>
<pinref part="GND63" gate="1" pin="GND"/>
<wire x1="71.12" y1="66.04" x2="73.66" y2="66.04" width="0.1524" layer="91"/>
<wire x1="73.66" y1="66.04" x2="73.66" y2="63.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C60" gate="G$1" pin="1"/>
<pinref part="GND64" gate="1" pin="GND"/>
<wire x1="71.12" y1="137.16" x2="73.66" y2="137.16" width="0.1524" layer="91"/>
<wire x1="73.66" y1="137.16" x2="73.66" y2="134.62" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="L19" gate="G$1" pin="2"/>
<wire x1="63.5" y1="149.86" x2="68.58" y2="149.86" width="0.1524" layer="91"/>
<pinref part="C56" gate="G$1" pin="C"/>
<wire x1="68.58" y1="149.86" x2="88.9" y2="149.86" width="0.1524" layer="91"/>
<wire x1="88.9" y1="154.94" x2="88.9" y2="149.86" width="0.1524" layer="91"/>
<junction x="88.9" y="149.86"/>
<pinref part="C57" gate="G$1" pin="C"/>
<wire x1="88.9" y1="149.86" x2="99.06" y2="149.86" width="0.1524" layer="91"/>
<wire x1="99.06" y1="149.86" x2="99.06" y2="154.94" width="0.1524" layer="91"/>
<junction x="99.06" y="149.86"/>
<pinref part="GND58" gate="1" pin="GND"/>
<wire x1="99.06" y1="149.86" x2="99.06" y2="147.32" width="0.1524" layer="91"/>
<pinref part="C29" gate="G$1" pin="1"/>
<wire x1="68.58" y1="154.94" x2="68.58" y2="149.86" width="0.1524" layer="91"/>
<junction x="68.58" y="149.86"/>
</segment>
<segment>
<pinref part="LED5" gate="G$1" pin="K"/>
<wire x1="218.44" y1="66.04" x2="218.44" y2="63.5" width="0.1524" layer="91"/>
<pinref part="GND65" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="LED6" gate="G$1" pin="K"/>
<wire x1="228.6" y1="66.04" x2="228.6" y2="63.5" width="0.1524" layer="91"/>
<pinref part="GND66" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="LED7" gate="G$1" pin="K"/>
<wire x1="238.76" y1="66.04" x2="238.76" y2="63.5" width="0.1524" layer="91"/>
<pinref part="GND67" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="LED8" gate="G$1" pin="K"/>
<wire x1="248.92" y1="66.04" x2="248.92" y2="63.5" width="0.1524" layer="91"/>
<pinref part="GND68" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C15" gate="G$1" pin="1"/>
<pinref part="GND73" gate="1" pin="GND"/>
<wire x1="213.36" y1="109.22" x2="213.36" y2="111.76" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND74" gate="1" pin="GND"/>
<wire x1="187.96" y1="55.88" x2="187.96" y2="58.42" width="0.1524" layer="91"/>
<wire x1="187.96" y1="58.42" x2="195.58" y2="58.42" width="0.1524" layer="91"/>
<label x="195.58" y="58.42" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="POWER_24V" class="0">
<segment>
<pinref part="IC9" gate="_LDO" pin="EN5"/>
<wire x1="121.92" y1="58.42" x2="119.38" y2="58.42" width="0.1524" layer="91"/>
<wire x1="119.38" y1="58.42" x2="119.38" y2="66.04" width="0.1524" layer="91"/>
<pinref part="IC9" gate="_LDO" pin="VIN"/>
<wire x1="121.92" y1="66.04" x2="119.38" y2="66.04" width="0.1524" layer="91"/>
<pinref part="P+13" gate="1" pin="+24V"/>
<wire x1="119.38" y1="73.66" x2="119.38" y2="66.04" width="0.1524" layer="91"/>
<junction x="119.38" y="66.04"/>
<pinref part="C62" gate="G$1" pin="1"/>
<wire x1="116.84" y1="66.04" x2="119.38" y2="66.04" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="T2" gate="_1" pin="D"/>
<pinref part="P+12" gate="1" pin="+24V"/>
<wire x1="63.5" y1="68.58" x2="63.5" y2="66.04" width="0.1524" layer="91"/>
<pinref part="C61" gate="G$1" pin="2"/>
<wire x1="63.5" y1="66.04" x2="63.5" y2="55.88" width="0.1524" layer="91"/>
<wire x1="66.04" y1="66.04" x2="63.5" y2="66.04" width="0.1524" layer="91"/>
<junction x="63.5" y="66.04"/>
</segment>
<segment>
<pinref part="T1" gate="_1" pin="D"/>
<pinref part="P+11" gate="1" pin="+24V"/>
<wire x1="63.5" y1="139.7" x2="63.5" y2="137.16" width="0.1524" layer="91"/>
<pinref part="C60" gate="G$1" pin="2"/>
<wire x1="63.5" y1="137.16" x2="63.5" y2="127" width="0.1524" layer="91"/>
<wire x1="66.04" y1="137.16" x2="63.5" y2="137.16" width="0.1524" layer="91"/>
<junction x="63.5" y="137.16"/>
</segment>
<segment>
<pinref part="R38" gate="G$1" pin="2"/>
<pinref part="P+8" gate="1" pin="+24V"/>
<wire x1="203.2" y1="165.1" x2="203.2" y2="162.56" width="0.1524" layer="91"/>
<pinref part="IC13" gate="G$1" pin="V+"/>
<wire x1="203.2" y1="162.56" x2="203.2" y2="160.02" width="0.1524" layer="91"/>
<wire x1="180.34" y1="149.86" x2="198.12" y2="149.86" width="0.1524" layer="91"/>
<wire x1="198.12" y1="149.86" x2="198.12" y2="162.56" width="0.1524" layer="91"/>
<wire x1="198.12" y1="162.56" x2="203.2" y2="162.56" width="0.1524" layer="91"/>
<junction x="203.2" y="162.56"/>
<pinref part="J1" gate="G$1" pin="2"/>
<wire x1="203.2" y1="162.56" x2="213.36" y2="162.56" width="0.1524" layer="91"/>
<wire x1="213.36" y1="162.56" x2="213.36" y2="149.86" width="0.1524" layer="91"/>
<wire x1="213.36" y1="149.86" x2="213.36" y2="144.78" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C56" gate="G$1" pin="A"/>
<wire x1="88.9" y1="160.02" x2="88.9" y2="167.64" width="0.1524" layer="91"/>
<pinref part="C57" gate="G$1" pin="A"/>
<wire x1="88.9" y1="167.64" x2="99.06" y2="167.64" width="0.1524" layer="91"/>
<wire x1="99.06" y1="167.64" x2="99.06" y2="160.02" width="0.1524" layer="91"/>
<junction x="99.06" y="167.64"/>
<pinref part="P+14" gate="1" pin="+24V"/>
<wire x1="99.06" y1="167.64" x2="99.06" y2="170.18" width="0.1524" layer="91"/>
<pinref part="D2" gate="G$1" pin="K"/>
<wire x1="81.28" y1="167.64" x2="88.9" y2="167.64" width="0.1524" layer="91"/>
<junction x="88.9" y="167.64"/>
</segment>
<segment>
<pinref part="R44" gate="G$1" pin="2"/>
<wire x1="218.44" y1="91.44" x2="218.44" y2="88.9" width="0.1524" layer="91"/>
<pinref part="P+5" gate="1" pin="+24V"/>
</segment>
<segment>
<pinref part="P+17" gate="1" pin="+24V"/>
<wire x1="187.96" y1="66.04" x2="187.96" y2="63.5" width="0.1524" layer="91"/>
<wire x1="187.96" y1="63.5" x2="195.58" y2="63.5" width="0.1524" layer="91"/>
<label x="195.58" y="63.5" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="POWER_+5V" class="0">
<segment>
<pinref part="IC9" gate="_5V" pin="VO"/>
<wire x1="40.64" y1="124.46" x2="53.34" y2="124.46" width="0.1524" layer="91"/>
<wire x1="53.34" y1="124.46" x2="58.42" y2="129.54" width="0.1524" layer="91"/>
<wire x1="58.42" y1="129.54" x2="78.74" y2="129.54" width="0.1524" layer="91"/>
<wire x1="78.74" y1="129.54" x2="93.98" y2="114.3" width="0.1524" layer="91"/>
<pinref part="L21" gate="G$1" pin="2"/>
<wire x1="93.98" y1="114.3" x2="91.44" y2="114.3" width="0.1524" layer="91"/>
<wire x1="93.98" y1="114.3" x2="104.14" y2="114.3" width="0.1524" layer="91"/>
<junction x="93.98" y="114.3"/>
<pinref part="C52" gate="G$1" pin="A"/>
<wire x1="93.98" y1="111.76" x2="93.98" y2="114.3" width="0.1524" layer="91"/>
<pinref part="P+10" gate="1" pin="+5V"/>
</segment>
<segment>
<pinref part="R46" gate="G$1" pin="2"/>
<wire x1="238.76" y1="91.44" x2="238.76" y2="88.9" width="0.1524" layer="91"/>
<pinref part="P+15" gate="1" pin="+5V"/>
</segment>
<segment>
<pinref part="P+16" gate="1" pin="+5V"/>
<wire x1="187.96" y1="76.2" x2="187.96" y2="73.66" width="0.1524" layer="91"/>
<wire x1="187.96" y1="73.66" x2="195.58" y2="73.66" width="0.1524" layer="91"/>
<label x="195.58" y="73.66" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="P+30" gate="1" pin="+5V"/>
<pinref part="R51" gate="G$1" pin="2"/>
<wire x1="157.48" y1="165.1" x2="157.48" y2="162.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CHASSIS" class="0">
<segment>
<pinref part="CON6" gate="G$1" pin="2"/>
<pinref part="CON6" gate="G$1" pin="1"/>
<pinref part="TVS7" gate="G$1" pin="A"/>
<wire x1="43.18" y1="152.4" x2="43.18" y2="149.86" width="0.1524" layer="91"/>
<wire x1="43.18" y1="149.86" x2="33.02" y2="149.86" width="0.1524" layer="91"/>
<wire x1="33.02" y1="149.86" x2="25.4" y2="149.86" width="0.1524" layer="91"/>
<wire x1="25.4" y1="149.86" x2="25.4" y2="154.94" width="0.1524" layer="91"/>
<pinref part="L19" gate="G$1" pin="1"/>
<wire x1="43.18" y1="149.86" x2="48.26" y2="149.86" width="0.1524" layer="91"/>
<junction x="43.18" y="149.86"/>
<pinref part="C54" gate="G$1" pin="1"/>
<wire x1="33.02" y1="154.94" x2="33.02" y2="149.86" width="0.1524" layer="91"/>
<junction x="33.02" y="149.86"/>
<pinref part="CHASSIS22" gate="G$1" pin="CHASSIS"/>
<wire x1="25.4" y1="147.32" x2="25.4" y2="149.86" width="0.1524" layer="91"/>
<junction x="25.4" y="149.86"/>
<wire x1="17.78" y1="167.64" x2="25.4" y2="167.64" width="0.1524" layer="91"/>
<wire x1="25.4" y1="167.64" x2="25.4" y2="154.94" width="0.1524" layer="91"/>
<junction x="25.4" y="154.94"/>
<pinref part="X6" gate="G$1" pin="2"/>
<wire x1="20.32" y1="154.94" x2="25.4" y2="154.94" width="0.1524" layer="91"/>
<wire x1="20.32" y1="149.86" x2="25.4" y2="149.86" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X1" gate="G$1" pin="P$8"/>
<wire x1="243.84" y1="170.18" x2="246.38" y2="170.18" width="0.1524" layer="91"/>
<wire x1="246.38" y1="170.18" x2="246.38" y2="165.1" width="0.1524" layer="91"/>
<pinref part="X5" gate="G$1" pin="P$8"/>
<wire x1="246.38" y1="165.1" x2="246.38" y2="160.02" width="0.1524" layer="91"/>
<wire x1="246.38" y1="160.02" x2="246.38" y2="154.94" width="0.1524" layer="91"/>
<wire x1="246.38" y1="154.94" x2="246.38" y2="149.86" width="0.1524" layer="91"/>
<wire x1="246.38" y1="149.86" x2="246.38" y2="147.32" width="0.1524" layer="91"/>
<wire x1="243.84" y1="149.86" x2="246.38" y2="149.86" width="0.1524" layer="91"/>
<junction x="246.38" y="149.86"/>
<pinref part="X4" gate="G$1" pin="P$8"/>
<wire x1="243.84" y1="154.94" x2="246.38" y2="154.94" width="0.1524" layer="91"/>
<junction x="246.38" y="154.94"/>
<pinref part="X3" gate="G$1" pin="P$8"/>
<wire x1="243.84" y1="160.02" x2="246.38" y2="160.02" width="0.1524" layer="91"/>
<junction x="246.38" y="160.02"/>
<pinref part="X2" gate="G$1" pin="P$8"/>
<wire x1="243.84" y1="165.1" x2="246.38" y2="165.1" width="0.1524" layer="91"/>
<junction x="246.38" y="165.1"/>
<pinref part="CHASSIS23" gate="G$1" pin="CHASSIS"/>
</segment>
<segment>
<pinref part="CHASSIS25" gate="G$1" pin="CHASSIS"/>
<wire x1="187.96" y1="43.18" x2="187.96" y2="45.72" width="0.1524" layer="91"/>
<wire x1="187.96" y1="45.72" x2="195.58" y2="45.72" width="0.1524" layer="91"/>
<label x="195.58" y="45.72" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$86" class="0">
<segment>
<pinref part="CON6" gate="G$1" pin="3"/>
<wire x1="20.32" y1="160.02" x2="27.94" y2="160.02" width="0.1524" layer="91"/>
<wire x1="27.94" y1="160.02" x2="27.94" y2="167.64" width="0.1524" layer="91"/>
<wire x1="27.94" y1="167.64" x2="33.02" y2="167.64" width="0.1524" layer="91"/>
<pinref part="TVS7" gate="G$1" pin="B"/>
<wire x1="33.02" y1="167.64" x2="43.18" y2="167.64" width="0.1524" layer="91"/>
<wire x1="43.18" y1="167.64" x2="43.18" y2="162.56" width="0.1524" layer="91"/>
<pinref part="C54" gate="G$1" pin="2"/>
<wire x1="33.02" y1="160.02" x2="33.02" y2="167.64" width="0.1524" layer="91"/>
<junction x="33.02" y="167.64"/>
<wire x1="17.78" y1="170.18" x2="27.94" y2="170.18" width="0.1524" layer="91"/>
<wire x1="27.94" y1="170.18" x2="27.94" y2="167.64" width="0.1524" layer="91"/>
<junction x="27.94" y="167.64"/>
<pinref part="L18" gate="G$1" pin="1"/>
<wire x1="43.18" y1="167.64" x2="48.26" y2="167.64" width="0.1524" layer="91"/>
<junction x="43.18" y="167.64"/>
<pinref part="X6" gate="G$1" pin="1"/>
<pinref part="CON6" gate="G$1" pin="4"/>
<wire x1="17.78" y1="160.02" x2="20.32" y2="160.02" width="0.1524" layer="91"/>
<junction x="20.32" y="160.02"/>
</segment>
</net>
<net name="V5FILT" class="0">
<segment>
<pinref part="IC9" gate="_LDO" pin="V5FILT"/>
<pinref part="C43" gate="G$1" pin="2"/>
<wire x1="147.32" y1="55.88" x2="152.4" y2="55.88" width="0.1524" layer="91"/>
<wire x1="152.4" y1="55.88" x2="167.64" y2="55.88" width="0.1524" layer="91"/>
<junction x="152.4" y="55.88"/>
<pinref part="R31" gate="G$1" pin="2"/>
<wire x1="167.64" y1="55.88" x2="172.72" y2="55.88" width="0.1524" layer="91"/>
<junction x="167.64" y="55.88"/>
<label x="172.72" y="55.88" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="IC9" gate="_5V" pin="VFB"/>
<wire x1="15.24" y1="116.84" x2="12.7" y2="116.84" width="0.1524" layer="91"/>
<label x="12.7" y="116.84" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="IC9" gate="_3,3V" pin="VFB"/>
<wire x1="15.24" y1="45.72" x2="12.7" y2="45.72" width="0.1524" layer="91"/>
<label x="12.7" y="45.72" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="2"/>
<wire x1="38.1" y1="76.2" x2="43.18" y2="76.2" width="0.1524" layer="91"/>
<label x="43.18" y="76.2" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="VREG5" class="0">
<segment>
<pinref part="IC9" gate="_LDO" pin="VREG5"/>
<pinref part="C44" gate="G$1" pin="2"/>
<wire x1="147.32" y1="66.04" x2="152.4" y2="66.04" width="0.1524" layer="91"/>
<wire x1="152.4" y1="66.04" x2="167.64" y2="66.04" width="0.1524" layer="91"/>
<junction x="152.4" y="66.04"/>
<pinref part="R31" gate="G$1" pin="1"/>
<wire x1="167.64" y1="66.04" x2="172.72" y2="66.04" width="0.1524" layer="91"/>
<junction x="167.64" y="66.04"/>
<label x="172.72" y="66.04" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="IC9" gate="_LDO" pin="EN3"/>
<wire x1="121.92" y1="53.34" x2="119.38" y2="53.34" width="0.1524" layer="91"/>
<label x="119.38" y="53.34" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="IC9" gate="_5V" pin="EN"/>
<wire x1="15.24" y1="124.46" x2="12.7" y2="124.46" width="0.1524" layer="91"/>
<label x="12.7" y="124.46" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="IC9" gate="_3,3V" pin="EN"/>
<wire x1="15.24" y1="53.34" x2="12.7" y2="53.34" width="0.1524" layer="91"/>
<label x="12.7" y="53.34" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="VREG3" class="0">
<segment>
<pinref part="IC9" gate="_LDO" pin="VREG3"/>
<pinref part="C45" gate="G$1" pin="2"/>
<wire x1="147.32" y1="45.72" x2="152.4" y2="45.72" width="0.1524" layer="91"/>
<wire x1="152.4" y1="45.72" x2="172.72" y2="45.72" width="0.1524" layer="91"/>
<junction x="152.4" y="45.72"/>
<label x="172.72" y="45.72" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="VREF2" class="0">
<segment>
<pinref part="IC9" gate="_LDO" pin="VREF2"/>
<pinref part="C46" gate="G$1" pin="2"/>
<wire x1="147.32" y1="35.56" x2="152.4" y2="35.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$92" class="0">
<segment>
<pinref part="R32" gate="G$1" pin="2"/>
<pinref part="C47" gate="G$1" pin="2"/>
<wire x1="12.7" y1="86.36" x2="12.7" y2="88.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="COMP" class="0">
<segment>
<pinref part="IC9" gate="_3,3V" pin="COMP"/>
<pinref part="R33" gate="G$1" pin="1"/>
<wire x1="15.24" y1="30.48" x2="12.7" y2="30.48" width="0.1524" layer="91"/>
<wire x1="12.7" y1="30.48" x2="12.7" y2="27.94" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="3"/>
<wire x1="30.48" y1="71.12" x2="22.86" y2="71.12" width="0.1524" layer="91"/>
<wire x1="22.86" y1="71.12" x2="22.86" y2="66.04" width="0.1524" layer="91"/>
<wire x1="22.86" y1="66.04" x2="2.54" y2="66.04" width="0.1524" layer="91"/>
<wire x1="2.54" y1="66.04" x2="2.54" y2="30.48" width="0.1524" layer="91"/>
<wire x1="2.54" y1="30.48" x2="12.7" y2="30.48" width="0.1524" layer="91"/>
<junction x="12.7" y="30.48"/>
</segment>
</net>
<net name="N$94" class="0">
<segment>
<pinref part="R33" gate="G$1" pin="2"/>
<pinref part="C48" gate="G$1" pin="2"/>
<wire x1="12.7" y1="17.78" x2="12.7" y2="15.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="LL_3" class="0">
<segment>
<pinref part="IC9" gate="_3,3V" pin="LL"/>
<pinref part="C50" gate="G$1" pin="1"/>
<wire x1="45.72" y1="38.1" x2="40.64" y2="38.1" width="0.1524" layer="91"/>
<pinref part="T2" gate="_1" pin="S"/>
<pinref part="T2" gate="_2" pin="D"/>
<wire x1="63.5" y1="40.64" x2="63.5" y2="43.18" width="0.1524" layer="91"/>
<wire x1="63.5" y1="43.18" x2="63.5" y2="45.72" width="0.1524" layer="91"/>
<wire x1="63.5" y1="43.18" x2="58.42" y2="43.18" width="0.1524" layer="91"/>
<wire x1="58.42" y1="43.18" x2="53.34" y2="38.1" width="0.1524" layer="91"/>
<junction x="63.5" y="43.18"/>
<wire x1="53.34" y1="38.1" x2="48.26" y2="38.1" width="0.1524" layer="91"/>
<wire x1="48.26" y1="38.1" x2="45.72" y2="38.1" width="0.1524" layer="91"/>
<junction x="45.72" y="38.1"/>
<pinref part="L20" gate="G$1" pin="1"/>
<wire x1="78.74" y1="43.18" x2="63.5" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VBST_3" class="0">
<segment>
<pinref part="IC9" gate="_3,3V" pin="VBST"/>
<pinref part="C50" gate="G$1" pin="2"/>
<wire x1="45.72" y1="43.18" x2="40.64" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CS_3" class="0">
<segment>
<pinref part="IC9" gate="_3,3V" pin="CS"/>
<pinref part="R34" gate="G$1" pin="2"/>
<wire x1="40.64" y1="27.94" x2="63.5" y2="27.94" width="0.1524" layer="91"/>
<pinref part="T2" gate="_2" pin="S"/>
<wire x1="63.5" y1="27.94" x2="63.5" y2="30.48" width="0.1524" layer="91"/>
<junction x="63.5" y="27.94"/>
</segment>
</net>
<net name="DRVH_3" class="0">
<segment>
<pinref part="IC9" gate="_3,3V" pin="DRVH"/>
<pinref part="R37" gate="G$1" pin="1"/>
<wire x1="43.18" y1="48.26" x2="40.64" y2="48.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$95" class="0">
<segment>
<pinref part="R37" gate="G$1" pin="2"/>
<pinref part="T2" gate="_1" pin="G"/>
<wire x1="55.88" y1="48.26" x2="53.34" y2="48.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DRVL_3" class="0">
<segment>
<pinref part="IC9" gate="_3,3V" pin="DRVL"/>
<pinref part="T2" gate="_2" pin="G"/>
<wire x1="55.88" y1="33.02" x2="40.64" y2="33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$96" class="0">
<segment>
<pinref part="R36" gate="G$1" pin="2"/>
<pinref part="T1" gate="_1" pin="G"/>
<wire x1="55.88" y1="119.38" x2="53.34" y2="119.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DRVH_5" class="0">
<segment>
<pinref part="IC9" gate="_5V" pin="DRVH"/>
<pinref part="R36" gate="G$1" pin="1"/>
<wire x1="43.18" y1="119.38" x2="40.64" y2="119.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VBST_5" class="0">
<segment>
<pinref part="IC9" gate="_5V" pin="VBST"/>
<pinref part="C49" gate="G$1" pin="2"/>
<wire x1="45.72" y1="114.3" x2="40.64" y2="114.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="LL_5" class="0">
<segment>
<pinref part="IC9" gate="_5V" pin="LL"/>
<pinref part="C49" gate="G$1" pin="1"/>
<wire x1="45.72" y1="109.22" x2="40.64" y2="109.22" width="0.1524" layer="91"/>
<pinref part="T1" gate="_2" pin="D"/>
<pinref part="T1" gate="_1" pin="S"/>
<wire x1="63.5" y1="116.84" x2="63.5" y2="114.3" width="0.1524" layer="91"/>
<wire x1="63.5" y1="114.3" x2="63.5" y2="111.76" width="0.1524" layer="91"/>
<wire x1="63.5" y1="114.3" x2="58.42" y2="114.3" width="0.1524" layer="91"/>
<wire x1="58.42" y1="114.3" x2="53.34" y2="109.22" width="0.1524" layer="91"/>
<junction x="63.5" y="114.3"/>
<wire x1="53.34" y1="109.22" x2="45.72" y2="109.22" width="0.1524" layer="91"/>
<junction x="45.72" y="109.22"/>
<pinref part="L21" gate="G$1" pin="1"/>
<wire x1="78.74" y1="114.3" x2="63.5" y2="114.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DRVL_5" class="0">
<segment>
<pinref part="IC9" gate="_5V" pin="DRVL"/>
<pinref part="T1" gate="_2" pin="G"/>
<wire x1="55.88" y1="104.14" x2="40.64" y2="104.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CS_5" class="0">
<segment>
<pinref part="IC9" gate="_5V" pin="CS"/>
<pinref part="R35" gate="G$1" pin="2"/>
<wire x1="40.64" y1="99.06" x2="63.5" y2="99.06" width="0.1524" layer="91"/>
<pinref part="T1" gate="_2" pin="S"/>
<wire x1="63.5" y1="99.06" x2="63.5" y2="101.6" width="0.1524" layer="91"/>
<junction x="63.5" y="99.06"/>
</segment>
</net>
<net name="COMP_5" class="0">
<segment>
<pinref part="IC9" gate="_5V" pin="COMP"/>
<wire x1="15.24" y1="101.6" x2="12.7" y2="101.6" width="0.1524" layer="91"/>
<pinref part="R32" gate="G$1" pin="1"/>
<wire x1="12.7" y1="101.6" x2="12.7" y2="99.06" width="0.1524" layer="91"/>
<wire x1="12.7" y1="101.6" x2="2.54" y2="101.6" width="0.1524" layer="91"/>
<wire x1="2.54" y1="101.6" x2="2.54" y2="68.58" width="0.1524" layer="91"/>
<junction x="12.7" y="101.6"/>
<wire x1="2.54" y1="68.58" x2="20.32" y2="68.58" width="0.1524" layer="91"/>
<wire x1="20.32" y1="68.58" x2="20.32" y2="81.28" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="1"/>
<wire x1="20.32" y1="81.28" x2="30.48" y2="81.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="RS485_PWR" class="0">
<segment>
<wire x1="193.04" y1="119.38" x2="213.36" y2="119.38" width="0.1524" layer="91"/>
<label x="223.52" y="119.38" size="1.778" layer="95" xref="yes"/>
<wire x1="213.36" y1="119.38" x2="223.52" y2="119.38" width="0.1524" layer="91"/>
<pinref part="J1" gate="G$1" pin="1"/>
<wire x1="213.36" y1="134.62" x2="213.36" y2="129.54" width="0.1524" layer="91"/>
<junction x="213.36" y="119.38"/>
<pinref part="T3" gate="_2" pin="D"/>
<wire x1="213.36" y1="129.54" x2="213.36" y2="119.38" width="0.1524" layer="91"/>
<wire x1="193.04" y1="119.38" x2="193.04" y2="121.92" width="0.1524" layer="91"/>
<pinref part="D3" gate="G$1" pin="K"/>
<wire x1="134.62" y1="119.38" x2="193.04" y2="119.38" width="0.1524" layer="91"/>
<junction x="193.04" y="119.38"/>
<pinref part="C15" gate="G$1" pin="2"/>
<wire x1="213.36" y1="116.84" x2="213.36" y2="119.38" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R45" gate="G$1" pin="2"/>
<wire x1="228.6" y1="91.44" x2="228.6" y2="88.9" width="0.1524" layer="91"/>
<label x="228.6" y="91.44" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="N$37" class="0">
<segment>
<pinref part="IC13" gate="G$1" pin="G"/>
<pinref part="T3" gate="_1" pin="G"/>
<wire x1="182.88" y1="139.7" x2="180.34" y2="139.7" width="0.1524" layer="91"/>
<wire x1="185.42" y1="137.16" x2="182.88" y2="137.16" width="0.1524" layer="91"/>
<wire x1="182.88" y1="137.16" x2="182.88" y2="139.7" width="0.1524" layer="91"/>
<pinref part="T3" gate="_2" pin="G"/>
<wire x1="185.42" y1="129.54" x2="182.88" y2="129.54" width="0.1524" layer="91"/>
<wire x1="182.88" y1="129.54" x2="182.88" y2="137.16" width="0.1524" layer="91"/>
<junction x="182.88" y="137.16"/>
</segment>
</net>
<net name="N$98" class="0">
<segment>
<pinref part="T3" gate="_1" pin="D"/>
<wire x1="193.04" y1="144.78" x2="193.04" y2="147.32" width="0.1524" layer="91"/>
<wire x1="193.04" y1="147.32" x2="203.2" y2="147.32" width="0.1524" layer="91"/>
<pinref part="R38" gate="G$1" pin="1"/>
<wire x1="203.2" y1="149.86" x2="203.2" y2="147.32" width="0.1524" layer="91"/>
<wire x1="193.04" y1="147.32" x2="182.88" y2="147.32" width="0.1524" layer="91"/>
<wire x1="182.88" y1="147.32" x2="182.88" y2="144.78" width="0.1524" layer="91"/>
<junction x="193.04" y="147.32"/>
<pinref part="IC13" gate="G$1" pin="SNS"/>
<wire x1="180.34" y1="144.78" x2="182.88" y2="144.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$93" class="0">
<segment>
<pinref part="IC13" gate="G$1" pin="TIM"/>
<wire x1="160.02" y1="139.7" x2="157.48" y2="139.7" width="0.1524" layer="91"/>
<pinref part="C55" gate="G$1" pin="2"/>
<wire x1="157.48" y1="137.16" x2="157.48" y2="139.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="RS485_PWR_EN" class="0">
<segment>
<label x="132.08" y="139.7" size="1.27" layer="95" rot="R180" xref="yes"/>
<wire x1="134.62" y1="139.7" x2="132.08" y2="139.7" width="0.1524" layer="91"/>
<pinref part="J4" gate="G$1" pin="1"/>
</segment>
</net>
<net name="!RS485_PWR_FLT" class="0">
<segment>
<label x="132.08" y="149.86" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="J3" gate="G$1" pin="1"/>
<wire x1="134.62" y1="149.86" x2="132.08" y2="149.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$97" class="0">
<segment>
<pinref part="T3" gate="_1" pin="S"/>
<pinref part="T3" gate="_2" pin="S"/>
<wire x1="193.04" y1="132.08" x2="193.04" y2="134.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$104" class="0">
<segment>
<pinref part="R44" gate="G$1" pin="1"/>
<pinref part="LED5" gate="G$1" pin="A"/>
<wire x1="218.44" y1="78.74" x2="218.44" y2="76.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$106" class="0">
<segment>
<pinref part="R45" gate="G$1" pin="1"/>
<pinref part="LED6" gate="G$1" pin="A"/>
<wire x1="228.6" y1="78.74" x2="228.6" y2="76.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$108" class="0">
<segment>
<pinref part="R46" gate="G$1" pin="1"/>
<pinref part="LED7" gate="G$1" pin="A"/>
<wire x1="238.76" y1="78.74" x2="238.76" y2="76.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$110" class="0">
<segment>
<pinref part="R47" gate="G$1" pin="1"/>
<pinref part="LED8" gate="G$1" pin="A"/>
<wire x1="248.92" y1="78.74" x2="248.92" y2="76.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$91" class="0">
<segment>
<pinref part="L18" gate="G$1" pin="2"/>
<pinref part="D2" gate="G$1" pin="A"/>
<wire x1="71.12" y1="167.64" x2="68.58" y2="167.64" width="0.1524" layer="91"/>
<wire x1="68.58" y1="167.64" x2="63.5" y2="167.64" width="0.1524" layer="91"/>
<wire x1="68.58" y1="167.64" x2="68.58" y2="175.26" width="0.1524" layer="91"/>
<junction x="68.58" y="167.64"/>
<wire x1="68.58" y1="175.26" x2="109.22" y2="175.26" width="0.1524" layer="91"/>
<wire x1="109.22" y1="175.26" x2="109.22" y2="119.38" width="0.1524" layer="91"/>
<pinref part="D3" gate="G$1" pin="A"/>
<wire x1="109.22" y1="119.38" x2="124.46" y2="119.38" width="0.1524" layer="91"/>
<pinref part="C29" gate="G$1" pin="2"/>
<wire x1="68.58" y1="160.02" x2="68.58" y2="167.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$118" class="0">
<segment>
<pinref part="J3" gate="G$1" pin="2"/>
<pinref part="IC13" gate="G$1" pin="!FLT"/>
<wire x1="144.78" y1="149.86" x2="147.32" y2="149.86" width="0.1524" layer="91"/>
<pinref part="R40" gate="G$1" pin="1"/>
<wire x1="147.32" y1="149.86" x2="160.02" y2="149.86" width="0.1524" layer="91"/>
<wire x1="147.32" y1="152.4" x2="147.32" y2="149.86" width="0.1524" layer="91"/>
<junction x="147.32" y="149.86"/>
</segment>
</net>
<net name="N$119" class="0">
<segment>
<pinref part="J4" gate="G$1" pin="2"/>
<wire x1="144.78" y1="139.7" x2="152.4" y2="139.7" width="0.1524" layer="91"/>
<wire x1="152.4" y1="139.7" x2="152.4" y2="144.78" width="0.1524" layer="91"/>
<pinref part="IC13" gate="G$1" pin="IN"/>
<wire x1="152.4" y1="144.78" x2="157.48" y2="144.78" width="0.1524" layer="91"/>
<pinref part="R51" gate="G$1" pin="1"/>
<wire x1="157.48" y1="144.78" x2="160.02" y2="144.78" width="0.1524" layer="91"/>
<wire x1="157.48" y1="152.4" x2="157.48" y2="144.78" width="0.1524" layer="91"/>
<junction x="157.48" y="144.78"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<text x="175.26" y="25.4" size="6.4516" layer="94">T30 GPIO + PWR</text>
</plain>
<instances>
<instance part="IC1" gate="AUDIO" x="132.08" y="109.22" smashed="yes">
<attribute name="NAME" x="134.62" y="129.54" size="1.778" layer="95"/>
<attribute name="VALUE" x="132.08" y="132.08" size="1.778" layer="96"/>
</instance>
<instance part="IC1" gate="PWR" x="200.66" y="109.22" smashed="yes">
<attribute name="VALUE" x="193.04" y="104.14" size="1.778" layer="96"/>
<attribute name="NAME" x="195.58" y="106.68" size="1.778" layer="95"/>
</instance>
<instance part="IC1" gate="GPIO" x="78.74" y="88.9" smashed="yes">
<attribute name="NAME" x="71.12" y="165.1" size="1.778" layer="95"/>
<attribute name="VALUE" x="68.58" y="167.64" size="1.778" layer="96"/>
</instance>
<instance part="+3V5" gate="G$3" x="205.74" y="134.62"/>
<instance part="GND4" gate="1" x="205.74" y="83.82"/>
<instance part="+3V6" gate="G$3" x="154.94" y="139.7"/>
<instance part="GND17" gate="1" x="154.94" y="78.74"/>
<instance part="C2" gate="G$1" x="226.06" y="96.52" rot="R90"/>
<instance part="C3" gate="G$1" x="236.22" y="96.52" rot="R90"/>
<instance part="C4" gate="G$1" x="246.38" y="96.52" rot="R90"/>
<instance part="C7" gate="G$1" x="256.54" y="96.52" rot="R90"/>
<instance part="GND19" gate="1" x="226.06" y="88.9"/>
<instance part="+3V7" gate="G$3" x="226.06" y="104.14"/>
<instance part="GND20" gate="1" x="236.22" y="88.9"/>
<instance part="GND21" gate="1" x="246.38" y="88.9"/>
<instance part="GND22" gate="1" x="256.54" y="88.9"/>
<instance part="+3V8" gate="G$3" x="236.22" y="104.14"/>
<instance part="+3V9" gate="G$3" x="246.38" y="104.14"/>
<instance part="+3V10" gate="G$3" x="256.54" y="104.14"/>
<instance part="GND23" gate="1" x="121.92" y="96.52"/>
<instance part="C8" gate="G$1" x="226.06" y="121.92" rot="R90"/>
<instance part="C9" gate="G$1" x="236.22" y="121.92" rot="R90"/>
<instance part="C10" gate="G$1" x="246.38" y="121.92" rot="R90"/>
<instance part="C11" gate="G$1" x="256.54" y="121.92" rot="R90"/>
<instance part="GND24" gate="1" x="226.06" y="114.3"/>
<instance part="+3V11" gate="G$3" x="226.06" y="129.54"/>
<instance part="GND25" gate="1" x="236.22" y="114.3"/>
<instance part="GND26" gate="1" x="246.38" y="114.3"/>
<instance part="GND27" gate="1" x="256.54" y="114.3"/>
<instance part="+3V12" gate="G$3" x="236.22" y="129.54"/>
<instance part="+3V13" gate="G$3" x="246.38" y="129.54"/>
<instance part="+3V14" gate="G$3" x="256.54" y="129.54"/>
<instance part="FRAME7" gate="G$1" x="0" y="0"/>
<instance part="FRAME7" gate="G$2" x="162.56" y="0"/>
<instance part="+3V25" gate="G$3" x="7.62" y="172.72"/>
<instance part="P+18" gate="1" x="7.62" y="162.56"/>
<instance part="P+19" gate="1" x="7.62" y="152.4"/>
<instance part="CHASSIS26" gate="G$1" x="7.62" y="124.46"/>
<instance part="GND75" gate="1" x="7.62" y="137.16"/>
</instances>
<busses>
</busses>
<nets>
<net name="POWER_3V3" class="0">
<segment>
<pinref part="IC1" gate="PWR" pin="VCC_3V3@7"/>
<wire x1="205.74" y1="127" x2="205.74" y2="129.54" width="0.1524" layer="91"/>
<wire x1="205.74" y1="129.54" x2="203.2" y2="129.54" width="0.1524" layer="91"/>
<pinref part="IC1" gate="PWR" pin="VCC_3V3@6"/>
<wire x1="203.2" y1="129.54" x2="203.2" y2="127" width="0.1524" layer="91"/>
<pinref part="IC1" gate="PWR" pin="VCC_3V3@5"/>
<wire x1="203.2" y1="129.54" x2="200.66" y2="129.54" width="0.1524" layer="91"/>
<wire x1="200.66" y1="129.54" x2="200.66" y2="127" width="0.1524" layer="91"/>
<junction x="203.2" y="129.54"/>
<pinref part="IC1" gate="PWR" pin="VCC_3V3@4"/>
<wire x1="200.66" y1="129.54" x2="198.12" y2="129.54" width="0.1524" layer="91"/>
<wire x1="198.12" y1="129.54" x2="198.12" y2="127" width="0.1524" layer="91"/>
<junction x="200.66" y="129.54"/>
<pinref part="IC1" gate="PWR" pin="VCC_3V3@3"/>
<wire x1="198.12" y1="129.54" x2="195.58" y2="129.54" width="0.1524" layer="91"/>
<wire x1="195.58" y1="129.54" x2="195.58" y2="127" width="0.1524" layer="91"/>
<junction x="198.12" y="129.54"/>
<pinref part="IC1" gate="PWR" pin="VCC_3V3@2"/>
<wire x1="195.58" y1="129.54" x2="193.04" y2="129.54" width="0.1524" layer="91"/>
<wire x1="193.04" y1="129.54" x2="193.04" y2="127" width="0.1524" layer="91"/>
<junction x="195.58" y="129.54"/>
<pinref part="IC1" gate="PWR" pin="VCC_3V3"/>
<wire x1="193.04" y1="129.54" x2="190.5" y2="129.54" width="0.1524" layer="91"/>
<wire x1="190.5" y1="129.54" x2="190.5" y2="127" width="0.1524" layer="91"/>
<junction x="193.04" y="129.54"/>
<wire x1="205.74" y1="129.54" x2="205.74" y2="132.08" width="0.1524" layer="91"/>
<junction x="205.74" y="129.54"/>
<pinref part="+3V5" gate="G$3" pin="POWER_3V3"/>
<wire x1="190.5" y1="129.54" x2="187.96" y2="129.54" width="0.1524" layer="91"/>
<junction x="190.5" y="129.54"/>
<pinref part="IC1" gate="PWR" pin="VCC_3V3_VCCBATT"/>
<wire x1="187.96" y1="129.54" x2="187.96" y2="127" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="+3V6" gate="G$3" pin="POWER_3V3"/>
<pinref part="IC1" gate="AUDIO" pin="VDDA_AUDIO2"/>
<wire x1="154.94" y1="137.16" x2="154.94" y2="134.62" width="0.1524" layer="91"/>
<wire x1="154.94" y1="134.62" x2="154.94" y2="132.08" width="0.1524" layer="91"/>
<wire x1="154.94" y1="134.62" x2="152.4" y2="134.62" width="0.1524" layer="91"/>
<junction x="154.94" y="134.62"/>
<pinref part="IC1" gate="AUDIO" pin="VDDA_AUDIO1"/>
<wire x1="152.4" y1="134.62" x2="152.4" y2="132.08" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C2" gate="G$1" pin="2"/>
<pinref part="+3V7" gate="G$3" pin="POWER_3V3"/>
<wire x1="226.06" y1="101.6" x2="226.06" y2="99.06" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C3" gate="G$1" pin="2"/>
<pinref part="+3V8" gate="G$3" pin="POWER_3V3"/>
<wire x1="236.22" y1="101.6" x2="236.22" y2="99.06" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C4" gate="G$1" pin="2"/>
<pinref part="+3V9" gate="G$3" pin="POWER_3V3"/>
<wire x1="246.38" y1="101.6" x2="246.38" y2="99.06" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C7" gate="G$1" pin="2"/>
<pinref part="+3V10" gate="G$3" pin="POWER_3V3"/>
<wire x1="256.54" y1="101.6" x2="256.54" y2="99.06" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C8" gate="G$1" pin="2"/>
<pinref part="+3V11" gate="G$3" pin="POWER_3V3"/>
<wire x1="226.06" y1="127" x2="226.06" y2="124.46" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C9" gate="G$1" pin="2"/>
<pinref part="+3V12" gate="G$3" pin="POWER_3V3"/>
<wire x1="236.22" y1="127" x2="236.22" y2="124.46" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C10" gate="G$1" pin="2"/>
<pinref part="+3V13" gate="G$3" pin="POWER_3V3"/>
<wire x1="246.38" y1="127" x2="246.38" y2="124.46" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C11" gate="G$1" pin="2"/>
<pinref part="+3V14" gate="G$3" pin="POWER_3V3"/>
<wire x1="256.54" y1="127" x2="256.54" y2="124.46" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="+3V25" gate="G$3" pin="POWER_3V3"/>
<wire x1="7.62" y1="170.18" x2="7.62" y2="167.64" width="0.1524" layer="91"/>
<wire x1="7.62" y1="167.64" x2="15.24" y2="167.64" width="0.1524" layer="91"/>
<label x="15.24" y="167.64" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="POWER_GND" class="0">
<segment>
<pinref part="IC1" gate="PWR" pin="GND"/>
<wire x1="187.96" y1="91.44" x2="187.96" y2="88.9" width="0.1524" layer="91"/>
<pinref part="IC1" gate="PWR" pin="GND@4"/>
<wire x1="187.96" y1="88.9" x2="190.5" y2="88.9" width="0.1524" layer="91"/>
<wire x1="190.5" y1="88.9" x2="190.5" y2="91.44" width="0.1524" layer="91"/>
<pinref part="IC1" gate="PWR" pin="GND@3"/>
<wire x1="190.5" y1="88.9" x2="193.04" y2="88.9" width="0.1524" layer="91"/>
<wire x1="193.04" y1="88.9" x2="193.04" y2="91.44" width="0.1524" layer="91"/>
<junction x="190.5" y="88.9"/>
<pinref part="IC1" gate="PWR" pin="GND@2"/>
<wire x1="193.04" y1="88.9" x2="195.58" y2="88.9" width="0.1524" layer="91"/>
<wire x1="195.58" y1="88.9" x2="195.58" y2="91.44" width="0.1524" layer="91"/>
<junction x="193.04" y="88.9"/>
<pinref part="IC1" gate="PWR" pin="GND@6"/>
<wire x1="195.58" y1="88.9" x2="198.12" y2="88.9" width="0.1524" layer="91"/>
<wire x1="198.12" y1="88.9" x2="198.12" y2="91.44" width="0.1524" layer="91"/>
<junction x="195.58" y="88.9"/>
<pinref part="IC1" gate="PWR" pin="GND@7"/>
<wire x1="198.12" y1="88.9" x2="200.66" y2="88.9" width="0.1524" layer="91"/>
<wire x1="200.66" y1="88.9" x2="200.66" y2="91.44" width="0.1524" layer="91"/>
<junction x="198.12" y="88.9"/>
<pinref part="IC1" gate="PWR" pin="GND@8"/>
<wire x1="200.66" y1="88.9" x2="203.2" y2="88.9" width="0.1524" layer="91"/>
<wire x1="203.2" y1="88.9" x2="203.2" y2="91.44" width="0.1524" layer="91"/>
<junction x="200.66" y="88.9"/>
<pinref part="IC1" gate="PWR" pin="GND@9"/>
<wire x1="203.2" y1="88.9" x2="205.74" y2="88.9" width="0.1524" layer="91"/>
<wire x1="205.74" y1="88.9" x2="205.74" y2="91.44" width="0.1524" layer="91"/>
<junction x="203.2" y="88.9"/>
<pinref part="GND4" gate="1" pin="GND"/>
<wire x1="205.74" y1="88.9" x2="205.74" y2="86.36" width="0.1524" layer="91"/>
<junction x="205.74" y="88.9"/>
</segment>
<segment>
<pinref part="IC1" gate="AUDIO" pin="VSSA_AUDIO1"/>
<wire x1="152.4" y1="86.36" x2="152.4" y2="83.82" width="0.1524" layer="91"/>
<wire x1="152.4" y1="83.82" x2="154.94" y2="83.82" width="0.1524" layer="91"/>
<pinref part="GND17" gate="1" pin="GND"/>
<wire x1="154.94" y1="83.82" x2="154.94" y2="81.28" width="0.1524" layer="91"/>
<pinref part="IC1" gate="AUDIO" pin="VSSA_AUDIO2"/>
<wire x1="154.94" y1="86.36" x2="154.94" y2="83.82" width="0.1524" layer="91"/>
<junction x="154.94" y="83.82"/>
</segment>
<segment>
<pinref part="C2" gate="G$1" pin="1"/>
<pinref part="GND19" gate="1" pin="GND"/>
<wire x1="226.06" y1="91.44" x2="226.06" y2="93.98" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C3" gate="G$1" pin="1"/>
<pinref part="GND20" gate="1" pin="GND"/>
<wire x1="236.22" y1="91.44" x2="236.22" y2="93.98" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C4" gate="G$1" pin="1"/>
<pinref part="GND21" gate="1" pin="GND"/>
<wire x1="246.38" y1="91.44" x2="246.38" y2="93.98" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C7" gate="G$1" pin="1"/>
<pinref part="GND22" gate="1" pin="GND"/>
<wire x1="256.54" y1="91.44" x2="256.54" y2="93.98" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC1" gate="AUDIO" pin="HEADPHONE_GND"/>
<pinref part="GND23" gate="1" pin="GND"/>
<wire x1="124.46" y1="111.76" x2="121.92" y2="111.76" width="0.1524" layer="91"/>
<wire x1="121.92" y1="111.76" x2="121.92" y2="101.6" width="0.1524" layer="91"/>
<pinref part="IC1" gate="AUDIO" pin="MIC_GND"/>
<wire x1="121.92" y1="101.6" x2="121.92" y2="99.06" width="0.1524" layer="91"/>
<wire x1="124.46" y1="101.6" x2="121.92" y2="101.6" width="0.1524" layer="91"/>
<junction x="121.92" y="101.6"/>
</segment>
<segment>
<pinref part="C8" gate="G$1" pin="1"/>
<pinref part="GND24" gate="1" pin="GND"/>
<wire x1="226.06" y1="116.84" x2="226.06" y2="119.38" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C9" gate="G$1" pin="1"/>
<pinref part="GND25" gate="1" pin="GND"/>
<wire x1="236.22" y1="116.84" x2="236.22" y2="119.38" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C10" gate="G$1" pin="1"/>
<pinref part="GND26" gate="1" pin="GND"/>
<wire x1="246.38" y1="116.84" x2="246.38" y2="119.38" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C11" gate="G$1" pin="1"/>
<pinref part="GND27" gate="1" pin="GND"/>
<wire x1="256.54" y1="116.84" x2="256.54" y2="119.38" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND75" gate="1" pin="GND"/>
<wire x1="7.62" y1="139.7" x2="7.62" y2="142.24" width="0.1524" layer="91"/>
<wire x1="7.62" y1="142.24" x2="15.24" y2="142.24" width="0.1524" layer="91"/>
<label x="15.24" y="142.24" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="SD_CARD_CD" class="0">
<segment>
<pinref part="IC1" gate="GPIO" pin="GPIO-C.07"/>
<wire x1="60.96" y1="160.02" x2="58.42" y2="160.02" width="0.1524" layer="91"/>
<label x="58.42" y="160.02" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="USB_HS_IND" class="0">
<segment>
<wire x1="60.96" y1="157.48" x2="58.42" y2="157.48" width="0.1524" layer="91"/>
<label x="58.42" y="157.48" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="IC1" gate="GPIO" pin="GPIO-B.06"/>
</segment>
</net>
<net name="RS485_PWR_EN" class="0">
<segment>
<label x="96.52" y="157.48" size="1.27" layer="95" xref="yes"/>
<pinref part="IC1" gate="GPIO" pin="GPIO-A.06"/>
<wire x1="96.52" y1="157.48" x2="93.98" y2="157.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="!RS485_PWR_FLT" class="0">
<segment>
<label x="96.52" y="160.02" size="1.27" layer="95" xref="yes"/>
<pinref part="IC1" gate="GPIO" pin="GPIO-B.05"/>
<wire x1="96.52" y1="160.02" x2="93.98" y2="160.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="POWER_+5V" class="0">
<segment>
<pinref part="P+18" gate="1" pin="+5V"/>
<wire x1="7.62" y1="160.02" x2="7.62" y2="157.48" width="0.1524" layer="91"/>
<wire x1="7.62" y1="157.48" x2="15.24" y2="157.48" width="0.1524" layer="91"/>
<label x="15.24" y="157.48" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="POWER_24V" class="0">
<segment>
<pinref part="P+19" gate="1" pin="+24V"/>
<wire x1="7.62" y1="149.86" x2="7.62" y2="147.32" width="0.1524" layer="91"/>
<wire x1="7.62" y1="147.32" x2="15.24" y2="147.32" width="0.1524" layer="91"/>
<label x="15.24" y="147.32" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="CHASSIS" class="0">
<segment>
<pinref part="CHASSIS26" gate="G$1" pin="CHASSIS"/>
<wire x1="7.62" y1="127" x2="7.62" y2="129.54" width="0.1524" layer="91"/>
<wire x1="7.62" y1="129.54" x2="15.24" y2="129.54" width="0.1524" layer="91"/>
<label x="15.24" y="129.54" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<text x="187.96" y="22.86" size="6.4516" layer="94">SD card</text>
</plain>
<instances>
<instance part="IC1" gate="CORE" x="88.9" y="91.44" smashed="yes" rot="MR0">
<attribute name="NAME" x="96.52" y="124.46" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="99.06" y="127" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="SDCARD1" gate="G$1" x="193.04" y="81.28"/>
<instance part="GND28" gate="1" x="220.98" y="81.28"/>
<instance part="C12" gate="G$1" x="220.98" y="91.44" rot="MR90"/>
<instance part="+3V15" gate="G$3" x="220.98" y="124.46" rot="MR0"/>
<instance part="C13" gate="G$1" x="210.82" y="93.98" rot="MR90"/>
<instance part="L7" gate="G$1" x="220.98" y="109.22" smashed="yes" rot="MR180">
<attribute name="NAME" x="225.298" y="108.204" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="219.964" y="101.6" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R5" gate="G$1" x="142.24" y="99.06" smashed="yes" rot="MR0">
<attribute name="NAME" x="147.828" y="99.314" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="144.272" y="98.171" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="R6" gate="G$1" x="132.08" y="96.52" smashed="yes" rot="MR0">
<attribute name="NAME" x="137.414" y="96.774" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="134.112" y="95.631" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="R7" gate="G$1" x="142.24" y="93.98" smashed="yes" rot="MR0">
<attribute name="NAME" x="147.828" y="94.234" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="144.272" y="93.091" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="R8" gate="G$1" x="132.08" y="91.44" smashed="yes" rot="MR0">
<attribute name="NAME" x="137.668" y="91.694" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="134.112" y="90.551" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="R9" gate="G$1" x="142.24" y="88.9" smashed="yes" rot="MR0">
<attribute name="NAME" x="147.828" y="89.154" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="144.272" y="88.011" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="R18" gate="G$1" x="132.08" y="86.36" smashed="yes" rot="MR0">
<attribute name="NAME" x="138.938" y="86.614" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="134.112" y="85.471" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="R19" gate="G$1" x="167.64" y="78.74" smashed="yes" rot="MR0">
<attribute name="NAME" x="170.18" y="81.28" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="170.18" y="74.295" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="R20" gate="G$1" x="170.18" y="114.3" smashed="yes" rot="MR90">
<attribute name="NAME" x="171.704" y="112.014" size="1.778" layer="95" rot="MR90"/>
<attribute name="VALUE" x="169.291" y="112.268" size="1.778" layer="96" rot="MR90"/>
</instance>
<instance part="R21" gate="G$1" x="165.1" y="114.3" smashed="yes" rot="MR90">
<attribute name="NAME" x="166.624" y="112.014" size="1.778" layer="95" rot="MR90"/>
<attribute name="VALUE" x="164.211" y="112.268" size="1.778" layer="96" rot="MR90"/>
</instance>
<instance part="R22" gate="G$1" x="160.02" y="114.3" smashed="yes" rot="MR90">
<attribute name="NAME" x="161.544" y="112.014" size="1.778" layer="95" rot="MR90"/>
<attribute name="VALUE" x="159.131" y="112.268" size="1.778" layer="96" rot="MR90"/>
</instance>
<instance part="R23" gate="G$1" x="154.94" y="114.3" smashed="yes" rot="MR90">
<attribute name="NAME" x="156.464" y="112.014" size="1.778" layer="95" rot="MR90"/>
<attribute name="VALUE" x="154.051" y="112.268" size="1.778" layer="96" rot="MR90"/>
</instance>
<instance part="R24" gate="G$1" x="149.86" y="114.3" smashed="yes" rot="MR90">
<attribute name="NAME" x="151.384" y="112.014" size="1.778" layer="95" rot="MR90"/>
<attribute name="VALUE" x="148.971" y="112.268" size="1.778" layer="96" rot="MR90"/>
</instance>
<instance part="+3V16" gate="G$3" x="160.02" y="127" rot="MR0"/>
<instance part="C16" gate="G$1" x="175.26" y="68.58" rot="MR90"/>
<instance part="R25" gate="G$1" x="175.26" y="114.3" smashed="yes" rot="MR90">
<attribute name="NAME" x="176.784" y="112.014" size="1.778" layer="95" rot="MR90"/>
<attribute name="VALUE" x="174.371" y="112.268" size="1.778" layer="96" rot="MR90"/>
</instance>
<instance part="GND30" gate="1" x="175.26" y="60.96"/>
<instance part="R30" gate="G$1" x="38.1" y="116.84" smashed="yes" rot="MR0">
<attribute name="NAME" x="40.132" y="118.618" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="40.132" y="115.951" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="FRAME5" gate="G$1" x="0" y="0"/>
<instance part="FRAME5" gate="G$2" x="162.56" y="0"/>
<instance part="CHASSIS21" gate="G$1" x="210.82" y="73.66"/>
<instance part="GND29" gate="1" x="22.86" y="127"/>
<instance part="L25" gate="G$1" x="218.44" y="63.5" smashed="yes" rot="MR90">
<attribute name="NAME" x="216.154" y="67.818" size="1.778" layer="95"/>
<attribute name="VALUE" x="208.28" y="65.786" size="1.778" layer="96"/>
</instance>
<instance part="GND70" gate="1" x="228.6" y="58.42"/>
<instance part="CHASSIS24" gate="G$1" x="208.28" y="58.42"/>
<instance part="+3V26" gate="G$3" x="20.32" y="66.04"/>
<instance part="P+20" gate="1" x="20.32" y="55.88"/>
<instance part="P+21" gate="1" x="20.32" y="45.72"/>
<instance part="CHASSIS27" gate="G$1" x="20.32" y="17.78"/>
<instance part="GND76" gate="1" x="20.32" y="30.48"/>
<instance part="SW1" gate="1" x="38.1" y="132.08" rot="R270"/>
</instances>
<busses>
</busses>
<nets>
<net name="POWER_3V3" class="0">
<segment>
<pinref part="+3V15" gate="G$3" pin="POWER_3V3"/>
<pinref part="L7" gate="G$1" pin="2"/>
<wire x1="220.98" y1="121.92" x2="220.98" y2="116.84" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R20" gate="G$1" pin="2"/>
<wire x1="170.18" y1="119.38" x2="170.18" y2="121.92" width="0.1524" layer="91"/>
<wire x1="170.18" y1="121.92" x2="165.1" y2="121.92" width="0.1524" layer="91"/>
<pinref part="R21" gate="G$1" pin="2"/>
<wire x1="165.1" y1="121.92" x2="165.1" y2="119.38" width="0.1524" layer="91"/>
<pinref part="R22" gate="G$1" pin="2"/>
<wire x1="165.1" y1="121.92" x2="160.02" y2="121.92" width="0.1524" layer="91"/>
<wire x1="160.02" y1="121.92" x2="160.02" y2="119.38" width="0.1524" layer="91"/>
<junction x="165.1" y="121.92"/>
<pinref part="R23" gate="G$1" pin="2"/>
<wire x1="160.02" y1="121.92" x2="154.94" y2="121.92" width="0.1524" layer="91"/>
<wire x1="154.94" y1="121.92" x2="154.94" y2="119.38" width="0.1524" layer="91"/>
<junction x="160.02" y="121.92"/>
<pinref part="R24" gate="G$1" pin="2"/>
<wire x1="154.94" y1="121.92" x2="149.86" y2="121.92" width="0.1524" layer="91"/>
<wire x1="149.86" y1="121.92" x2="149.86" y2="119.38" width="0.1524" layer="91"/>
<junction x="154.94" y="121.92"/>
<wire x1="160.02" y1="121.92" x2="160.02" y2="124.46" width="0.1524" layer="91"/>
<pinref part="+3V16" gate="G$3" pin="POWER_3V3"/>
<wire x1="170.18" y1="121.92" x2="175.26" y2="121.92" width="0.1524" layer="91"/>
<junction x="170.18" y="121.92"/>
<pinref part="R25" gate="G$1" pin="2"/>
<wire x1="175.26" y1="121.92" x2="175.26" y2="119.38" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="+3V26" gate="G$3" pin="POWER_3V3"/>
<wire x1="20.32" y1="63.5" x2="20.32" y2="60.96" width="0.1524" layer="91"/>
<wire x1="20.32" y1="60.96" x2="27.94" y2="60.96" width="0.1524" layer="91"/>
<label x="27.94" y="60.96" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="POWER_GND" class="0">
<segment>
<pinref part="C16" gate="G$1" pin="1"/>
<pinref part="GND30" gate="1" pin="GND"/>
<wire x1="175.26" y1="63.5" x2="175.26" y2="66.04" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SDCARD1" gate="G$1" pin="VSS1"/>
<wire x1="208.28" y1="88.9" x2="210.82" y2="88.9" width="0.1524" layer="91"/>
<pinref part="C13" gate="G$1" pin="1"/>
<wire x1="210.82" y1="91.44" x2="210.82" y2="88.9" width="0.1524" layer="91"/>
<pinref part="C12" gate="G$1" pin="1"/>
<wire x1="220.98" y1="88.9" x2="220.98" y2="86.36" width="0.1524" layer="91"/>
<pinref part="SDCARD1" gate="G$1" pin="VSS2"/>
<wire x1="208.28" y1="86.36" x2="210.82" y2="86.36" width="0.1524" layer="91"/>
<wire x1="210.82" y1="86.36" x2="220.98" y2="86.36" width="0.1524" layer="91"/>
<wire x1="210.82" y1="88.9" x2="210.82" y2="86.36" width="0.1524" layer="91"/>
<junction x="210.82" y="88.9"/>
<junction x="210.82" y="86.36"/>
<pinref part="GND28" gate="1" pin="GND"/>
<wire x1="220.98" y1="86.36" x2="220.98" y2="83.82" width="0.1524" layer="91"/>
<junction x="220.98" y="86.36"/>
</segment>
<segment>
<pinref part="GND29" gate="1" pin="GND"/>
<pinref part="SW1" gate="1" pin="P"/>
<wire x1="33.02" y1="132.08" x2="30.48" y2="132.08" width="0.1524" layer="91"/>
<wire x1="30.48" y1="132.08" x2="22.86" y2="132.08" width="0.1524" layer="91"/>
<wire x1="22.86" y1="132.08" x2="22.86" y2="129.54" width="0.1524" layer="91"/>
<pinref part="SW1" gate="1" pin="P1"/>
<wire x1="33.02" y1="129.54" x2="30.48" y2="129.54" width="0.1524" layer="91"/>
<wire x1="30.48" y1="129.54" x2="30.48" y2="132.08" width="0.1524" layer="91"/>
<junction x="30.48" y="132.08"/>
</segment>
<segment>
<pinref part="L25" gate="G$1" pin="1"/>
<pinref part="GND70" gate="1" pin="GND"/>
<wire x1="226.06" y1="63.5" x2="228.6" y2="63.5" width="0.1524" layer="91"/>
<wire x1="228.6" y1="63.5" x2="228.6" y2="60.96" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND76" gate="1" pin="GND"/>
<wire x1="20.32" y1="33.02" x2="20.32" y2="35.56" width="0.1524" layer="91"/>
<wire x1="20.32" y1="35.56" x2="27.94" y2="35.56" width="0.1524" layer="91"/>
<label x="27.94" y="35.56" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="CHASSIS" class="0">
<segment>
<pinref part="SDCARD1" gate="G$1" pin="CHASSIS"/>
<pinref part="CHASSIS21" gate="G$1" pin="CHASSIS"/>
<wire x1="208.28" y1="78.74" x2="210.82" y2="78.74" width="0.1524" layer="91"/>
<wire x1="210.82" y1="78.74" x2="210.82" y2="76.2" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="L25" gate="G$1" pin="2"/>
<pinref part="CHASSIS24" gate="G$1" pin="CHASSIS"/>
<wire x1="210.82" y1="63.5" x2="208.28" y2="63.5" width="0.1524" layer="91"/>
<wire x1="208.28" y1="63.5" x2="208.28" y2="60.96" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="CHASSIS27" gate="G$1" pin="CHASSIS"/>
<wire x1="20.32" y1="20.32" x2="20.32" y2="22.86" width="0.1524" layer="91"/>
<wire x1="20.32" y1="22.86" x2="27.94" y2="22.86" width="0.1524" layer="91"/>
<label x="27.94" y="22.86" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="IC1" gate="CORE" pin="GPIO-CC.01/MMC_CMD"/>
<pinref part="R18" gate="G$1" pin="2"/>
<wire x1="127" y1="86.36" x2="124.46" y2="86.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<pinref part="IC1" gate="CORE" pin="GPIO-BB.03/MMC_DAT3/MMCS1"/>
<pinref part="R9" gate="G$1" pin="2"/>
<wire x1="137.16" y1="88.9" x2="124.46" y2="88.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<pinref part="IC1" gate="CORE" pin="GPIO-CC.00/MMC_CLK"/>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="137.16" y1="99.06" x2="124.46" y2="99.06" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<pinref part="IC1" gate="CORE" pin="GPIO-BB.00/MMC_DAT0"/>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="127" y1="96.52" x2="124.46" y2="96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<pinref part="IC1" gate="CORE" pin="GPIO-BB.01/MMC_DAT1"/>
<pinref part="R7" gate="G$1" pin="2"/>
<wire x1="137.16" y1="93.98" x2="124.46" y2="93.98" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<pinref part="IC1" gate="CORE" pin="GPIO-BB.02/MMC_DAT2/MMCS0"/>
<pinref part="R8" gate="G$1" pin="2"/>
<wire x1="127" y1="91.44" x2="124.46" y2="91.44" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SD_CARD_CD" class="0">
<segment>
<label x="160.02" y="78.74" size="1.27" layer="95" rot="MR0" xref="yes"/>
<wire x1="160.02" y1="78.74" x2="162.56" y2="78.74" width="0.1524" layer="91"/>
<pinref part="R19" gate="G$1" pin="2"/>
</segment>
</net>
<net name="VDD" class="0">
<segment>
<pinref part="SDCARD1" gate="G$1" pin="VDD"/>
<wire x1="208.28" y1="99.06" x2="210.82" y2="99.06" width="0.1524" layer="91"/>
<pinref part="C13" gate="G$1" pin="2"/>
<wire x1="210.82" y1="99.06" x2="210.82" y2="96.52" width="0.1524" layer="91"/>
<wire x1="210.82" y1="99.06" x2="220.98" y2="99.06" width="0.1524" layer="91"/>
<junction x="210.82" y="99.06"/>
<pinref part="C12" gate="G$1" pin="2"/>
<wire x1="220.98" y1="99.06" x2="220.98" y2="93.98" width="0.1524" layer="91"/>
<pinref part="L7" gate="G$1" pin="1"/>
<wire x1="220.98" y1="99.06" x2="220.98" y2="101.6" width="0.1524" layer="91"/>
<junction x="220.98" y="99.06"/>
</segment>
</net>
<net name="N$42" class="0">
<segment>
<pinref part="SDCARD1" gate="G$1" pin="CMD"/>
<wire x1="177.8" y1="86.36" x2="170.18" y2="86.36" width="0.1524" layer="91"/>
<pinref part="R18" gate="G$1" pin="1"/>
<pinref part="R20" gate="G$1" pin="1"/>
<wire x1="170.18" y1="86.36" x2="137.16" y2="86.36" width="0.1524" layer="91"/>
<wire x1="170.18" y1="109.22" x2="170.18" y2="86.36" width="0.1524" layer="91"/>
<junction x="170.18" y="86.36"/>
</segment>
</net>
<net name="N$43" class="0">
<segment>
<pinref part="R9" gate="G$1" pin="1"/>
<pinref part="SDCARD1" gate="G$1" pin="DAT3"/>
<wire x1="177.8" y1="88.9" x2="165.1" y2="88.9" width="0.1524" layer="91"/>
<pinref part="R21" gate="G$1" pin="1"/>
<wire x1="165.1" y1="88.9" x2="147.32" y2="88.9" width="0.1524" layer="91"/>
<wire x1="165.1" y1="109.22" x2="165.1" y2="88.9" width="0.1524" layer="91"/>
<junction x="165.1" y="88.9"/>
</segment>
</net>
<net name="N$44" class="0">
<segment>
<pinref part="R8" gate="G$1" pin="1"/>
<pinref part="SDCARD1" gate="G$1" pin="DAT2"/>
<wire x1="177.8" y1="91.44" x2="160.02" y2="91.44" width="0.1524" layer="91"/>
<pinref part="R22" gate="G$1" pin="1"/>
<wire x1="160.02" y1="91.44" x2="137.16" y2="91.44" width="0.1524" layer="91"/>
<wire x1="160.02" y1="109.22" x2="160.02" y2="91.44" width="0.1524" layer="91"/>
<junction x="160.02" y="91.44"/>
</segment>
</net>
<net name="N$45" class="0">
<segment>
<pinref part="R7" gate="G$1" pin="1"/>
<pinref part="SDCARD1" gate="G$1" pin="DAT1"/>
<wire x1="177.8" y1="93.98" x2="154.94" y2="93.98" width="0.1524" layer="91"/>
<pinref part="R23" gate="G$1" pin="1"/>
<wire x1="154.94" y1="93.98" x2="147.32" y2="93.98" width="0.1524" layer="91"/>
<wire x1="154.94" y1="109.22" x2="154.94" y2="93.98" width="0.1524" layer="91"/>
<junction x="154.94" y="93.98"/>
</segment>
</net>
<net name="N$46" class="0">
<segment>
<pinref part="R6" gate="G$1" pin="1"/>
<pinref part="SDCARD1" gate="G$1" pin="DAT0"/>
<wire x1="177.8" y1="96.52" x2="149.86" y2="96.52" width="0.1524" layer="91"/>
<pinref part="R24" gate="G$1" pin="1"/>
<wire x1="149.86" y1="96.52" x2="137.16" y2="96.52" width="0.1524" layer="91"/>
<wire x1="149.86" y1="109.22" x2="149.86" y2="96.52" width="0.1524" layer="91"/>
<junction x="149.86" y="96.52"/>
</segment>
</net>
<net name="N$47" class="0">
<segment>
<pinref part="R5" gate="G$1" pin="1"/>
<pinref part="SDCARD1" gate="G$1" pin="CLK"/>
<wire x1="177.8" y1="99.06" x2="147.32" y2="99.06" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$48" class="0">
<segment>
<pinref part="SDCARD1" gate="G$1" pin="CD"/>
<pinref part="R19" gate="G$1" pin="1"/>
<wire x1="172.72" y1="78.74" x2="175.26" y2="78.74" width="0.1524" layer="91"/>
<pinref part="R25" gate="G$1" pin="1"/>
<wire x1="175.26" y1="78.74" x2="177.8" y2="78.74" width="0.1524" layer="91"/>
<wire x1="175.26" y1="109.22" x2="175.26" y2="78.74" width="0.1524" layer="91"/>
<junction x="175.26" y="78.74"/>
<pinref part="C16" gate="G$1" pin="2"/>
<wire x1="175.26" y1="71.12" x2="175.26" y2="78.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="!RESET_OUT" class="0">
<segment>
<pinref part="R30" gate="G$1" pin="2"/>
<wire x1="33.02" y1="116.84" x2="22.86" y2="116.84" width="0.1524" layer="91"/>
<label x="22.86" y="116.84" size="1.27" layer="95" rot="MR0" xref="yes"/>
</segment>
</net>
<net name="N$89" class="0">
<segment>
<pinref part="IC1" gate="CORE" pin="GPIO-I.04/!RESET_OUT"/>
<pinref part="R30" gate="G$1" pin="1"/>
<wire x1="43.18" y1="116.84" x2="55.88" y2="116.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$88" class="0">
<segment>
<pinref part="IC1" gate="CORE" pin="!RESET_EXT"/>
<wire x1="55.88" y1="119.38" x2="53.34" y2="119.38" width="0.1524" layer="91"/>
<wire x1="53.34" y1="119.38" x2="53.34" y2="132.08" width="0.1524" layer="91"/>
<pinref part="SW1" gate="1" pin="S"/>
<wire x1="53.34" y1="132.08" x2="45.72" y2="132.08" width="0.1524" layer="91"/>
<pinref part="SW1" gate="1" pin="S1"/>
<wire x1="45.72" y1="132.08" x2="43.18" y2="132.08" width="0.1524" layer="91"/>
<wire x1="43.18" y1="129.54" x2="45.72" y2="129.54" width="0.1524" layer="91"/>
<wire x1="45.72" y1="129.54" x2="45.72" y2="132.08" width="0.1524" layer="91"/>
<junction x="45.72" y="132.08"/>
</segment>
</net>
<net name="POWER_+5V" class="0">
<segment>
<pinref part="P+20" gate="1" pin="+5V"/>
<wire x1="20.32" y1="53.34" x2="20.32" y2="50.8" width="0.1524" layer="91"/>
<wire x1="20.32" y1="50.8" x2="27.94" y2="50.8" width="0.1524" layer="91"/>
<label x="27.94" y="50.8" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="POWER_24V" class="0">
<segment>
<pinref part="P+21" gate="1" pin="+24V"/>
<wire x1="20.32" y1="43.18" x2="20.32" y2="40.64" width="0.1524" layer="91"/>
<wire x1="20.32" y1="40.64" x2="27.94" y2="40.64" width="0.1524" layer="91"/>
<label x="27.94" y="40.64" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<text x="193.04" y="25.4" size="6.4516" layer="94">ETHernet</text>
</plain>
<instances>
<instance part="IC1" gate="ETH" x="60.96" y="104.14" smashed="yes" rot="MR0">
<attribute name="NAME" x="45.72" y="88.9" size="1.778" layer="95"/>
<attribute name="VALUE" x="48.26" y="86.36" size="1.778" layer="96"/>
</instance>
<instance part="R3" gate="G$1" x="124.46" y="81.28" rot="R270"/>
<instance part="R4" gate="G$1" x="124.46" y="124.46" rot="R90"/>
<instance part="L8" gate="G$1" x="93.98" y="147.32" smashed="yes" rot="R180">
<attribute name="NAME" x="89.662" y="146.304" size="1.778" layer="95" rot="MR90"/>
<attribute name="VALUE" x="94.996" y="139.7" size="1.778" layer="96" rot="MR90"/>
</instance>
<instance part="+3V17" gate="G$3" x="93.98" y="162.56"/>
<instance part="C14" gate="G$1" x="93.98" y="129.54" rot="R90"/>
<instance part="CON8" gate="G$1" x="160.02" y="101.6"/>
<instance part="CHASSIS8" gate="G$1" x="134.62" y="60.96"/>
<instance part="FRAME3" gate="G$1" x="0" y="0"/>
<instance part="FRAME3" gate="G$2" x="162.56" y="0"/>
<instance part="GND59" gate="1" x="73.66" y="86.36"/>
<instance part="C63" gate="G$1" x="104.14" y="129.54" rot="MR270"/>
<instance part="GND69" gate="1" x="104.14" y="121.92"/>
<instance part="GND71" gate="1" x="93.98" y="121.92"/>
<instance part="+3V27" gate="G$3" x="10.16" y="170.18"/>
<instance part="P+22" gate="1" x="10.16" y="160.02"/>
<instance part="P+23" gate="1" x="10.16" y="149.86"/>
<instance part="CHASSIS28" gate="G$1" x="10.16" y="121.92"/>
<instance part="GND77" gate="1" x="10.16" y="134.62"/>
</instances>
<busses>
</busses>
<nets>
<net name="POWER_3V3" class="0">
<segment>
<pinref part="L8" gate="G$1" pin="2"/>
<pinref part="+3V17" gate="G$3" pin="POWER_3V3"/>
<wire x1="93.98" y1="154.94" x2="93.98" y2="160.02" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="+3V27" gate="G$3" pin="POWER_3V3"/>
<wire x1="10.16" y1="167.64" x2="10.16" y2="165.1" width="0.1524" layer="91"/>
<wire x1="10.16" y1="165.1" x2="17.78" y2="165.1" width="0.1524" layer="91"/>
<label x="17.78" y="165.1" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="POWER_GND" class="0">
<segment>
<pinref part="IC1" gate="ETH" pin="ETH_AGND"/>
<wire x1="71.12" y1="101.6" x2="73.66" y2="101.6" width="0.1524" layer="91"/>
<pinref part="GND59" gate="1" pin="GND"/>
<wire x1="73.66" y1="88.9" x2="73.66" y2="101.6" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C14" gate="G$1" pin="1"/>
<wire x1="93.98" y1="124.46" x2="93.98" y2="127" width="0.1524" layer="91"/>
<pinref part="GND71" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C63" gate="G$1" pin="2"/>
<pinref part="GND69" gate="1" pin="GND"/>
<wire x1="104.14" y1="124.46" x2="104.14" y2="127" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND77" gate="1" pin="GND"/>
<wire x1="10.16" y1="137.16" x2="10.16" y2="139.7" width="0.1524" layer="91"/>
<wire x1="10.16" y1="139.7" x2="17.78" y2="139.7" width="0.1524" layer="91"/>
<label x="17.78" y="139.7" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="CHASSIS" class="0">
<segment>
<pinref part="CON8" gate="G$1" pin="8"/>
<wire x1="139.7" y1="83.82" x2="134.62" y2="83.82" width="0.1524" layer="91"/>
<wire x1="134.62" y1="83.82" x2="134.62" y2="78.74" width="0.1524" layer="91"/>
<pinref part="CON8" gate="G$1" pin="CHASSIS@2"/>
<wire x1="134.62" y1="78.74" x2="139.7" y2="78.74" width="0.1524" layer="91"/>
<pinref part="CON8" gate="G$1" pin="CHASSIS@1"/>
<wire x1="139.7" y1="76.2" x2="134.62" y2="76.2" width="0.1524" layer="91"/>
<wire x1="134.62" y1="76.2" x2="134.62" y2="78.74" width="0.1524" layer="91"/>
<junction x="134.62" y="78.74"/>
<wire x1="134.62" y1="76.2" x2="134.62" y2="63.5" width="0.1524" layer="91"/>
<junction x="134.62" y="76.2"/>
<pinref part="CHASSIS8" gate="G$1" pin="CHASSIS"/>
</segment>
<segment>
<pinref part="CHASSIS28" gate="G$1" pin="CHASSIS"/>
<wire x1="10.16" y1="124.46" x2="10.16" y2="127" width="0.1524" layer="91"/>
<wire x1="10.16" y1="127" x2="17.78" y2="127" width="0.1524" layer="91"/>
<label x="17.78" y="127" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$35" class="0">
<segment>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="124.46" y1="119.38" x2="124.46" y2="111.76" width="0.1524" layer="91"/>
<pinref part="IC1" gate="ETH" pin="!ETH_LINK_ACT"/>
<wire x1="124.46" y1="111.76" x2="71.12" y2="111.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$36" class="0">
<segment>
<pinref part="IC1" gate="ETH" pin="!ETH_SPEED_100"/>
<wire x1="71.12" y1="109.22" x2="124.46" y2="109.22" width="0.1524" layer="91"/>
<wire x1="124.46" y1="109.22" x2="124.46" y2="86.36" width="0.1524" layer="91"/>
<pinref part="R3" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$38" class="0">
<segment>
<pinref part="L8" gate="G$1" pin="1"/>
<wire x1="93.98" y1="139.7" x2="93.98" y2="134.62" width="0.1524" layer="91"/>
<pinref part="C14" gate="G$1" pin="2"/>
<wire x1="93.98" y1="134.62" x2="93.98" y2="132.08" width="0.1524" layer="91"/>
<wire x1="93.98" y1="134.62" x2="104.14" y2="134.62" width="0.1524" layer="91"/>
<junction x="93.98" y="134.62"/>
<wire x1="104.14" y1="134.62" x2="137.16" y2="134.62" width="0.1524" layer="91"/>
<pinref part="CON8" gate="G$1" pin="CT_T"/>
<wire x1="139.7" y1="101.6" x2="137.16" y2="101.6" width="0.1524" layer="91"/>
<wire x1="137.16" y1="101.6" x2="137.16" y2="121.92" width="0.1524" layer="91"/>
<pinref part="CON8" gate="G$1" pin="CT_R"/>
<wire x1="137.16" y1="121.92" x2="139.7" y2="121.92" width="0.1524" layer="91"/>
<wire x1="137.16" y1="121.92" x2="137.16" y2="134.62" width="0.1524" layer="91"/>
<junction x="137.16" y="121.92"/>
<pinref part="CON8" gate="G$1" pin="L1A"/>
<wire x1="137.16" y1="134.62" x2="139.7" y2="134.62" width="0.1524" layer="91"/>
<pinref part="CON8" gate="G$1" pin="L2A"/>
<wire x1="139.7" y1="71.12" x2="137.16" y2="71.12" width="0.1524" layer="91"/>
<wire x1="137.16" y1="71.12" x2="137.16" y2="101.6" width="0.1524" layer="91"/>
<junction x="137.16" y="101.6"/>
<junction x="137.16" y="134.62"/>
<pinref part="C63" gate="G$1" pin="1"/>
<wire x1="104.14" y1="132.08" x2="104.14" y2="134.62" width="0.1524" layer="91"/>
<junction x="104.14" y="134.62"/>
</segment>
</net>
<net name="N$41" class="0">
<segment>
<pinref part="IC1" gate="ETH" pin="ETH_RXI+"/>
<pinref part="CON8" gate="G$1" pin="R+"/>
<wire x1="139.7" y1="129.54" x2="134.62" y2="129.54" width="0.1524" layer="91"/>
<wire x1="134.62" y1="129.54" x2="134.62" y2="96.52" width="0.1524" layer="91"/>
<wire x1="134.62" y1="96.52" x2="71.12" y2="96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$32" class="0">
<segment>
<pinref part="IC1" gate="ETH" pin="ETH_TXO-"/>
<wire x1="71.12" y1="106.68" x2="127" y2="106.68" width="0.1524" layer="91"/>
<wire x1="127" y1="106.68" x2="127" y2="93.98" width="0.1524" layer="91"/>
<pinref part="CON8" gate="G$1" pin="T-"/>
<wire x1="127" y1="93.98" x2="139.7" y2="93.98" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$33" class="0">
<segment>
<pinref part="IC1" gate="ETH" pin="ETH_TXO+"/>
<wire x1="71.12" y1="104.14" x2="129.54" y2="104.14" width="0.1524" layer="91"/>
<wire x1="129.54" y1="104.14" x2="129.54" y2="109.22" width="0.1524" layer="91"/>
<pinref part="CON8" gate="G$1" pin="T+"/>
<wire x1="129.54" y1="109.22" x2="139.7" y2="109.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$34" class="0">
<segment>
<pinref part="IC1" gate="ETH" pin="ETH_RXI-"/>
<wire x1="71.12" y1="99.06" x2="132.08" y2="99.06" width="0.1524" layer="91"/>
<wire x1="132.08" y1="99.06" x2="132.08" y2="114.3" width="0.1524" layer="91"/>
<pinref part="CON8" gate="G$1" pin="R-"/>
<wire x1="132.08" y1="114.3" x2="139.7" y2="114.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$39" class="0">
<segment>
<pinref part="CON8" gate="G$1" pin="L2K"/>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="139.7" y1="66.04" x2="124.46" y2="66.04" width="0.1524" layer="91"/>
<wire x1="124.46" y1="66.04" x2="124.46" y2="76.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$40" class="0">
<segment>
<pinref part="CON8" gate="G$1" pin="L1K"/>
<pinref part="R4" gate="G$1" pin="2"/>
<wire x1="139.7" y1="139.7" x2="124.46" y2="139.7" width="0.1524" layer="91"/>
<wire x1="124.46" y1="139.7" x2="124.46" y2="129.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="POWER_24V" class="0">
<segment>
<pinref part="P+23" gate="1" pin="+24V"/>
<wire x1="10.16" y1="147.32" x2="10.16" y2="144.78" width="0.1524" layer="91"/>
<wire x1="10.16" y1="144.78" x2="17.78" y2="144.78" width="0.1524" layer="91"/>
<label x="17.78" y="144.78" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="POWER_5V" class="0">
<segment>
<pinref part="P+22" gate="1" pin="+5V"/>
<wire x1="10.16" y1="157.48" x2="10.16" y2="154.94" width="0.1524" layer="91"/>
<wire x1="10.16" y1="154.94" x2="17.78" y2="154.94" width="0.1524" layer="91"/>
<label x="17.78" y="154.94" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<text x="200.66" y="25.4" size="6.4516" layer="94">RTC</text>
<text x="10.16" y="73.66" size="1.778" layer="91">+ MELE0121 - Baterie knoflíková CR2032 lithiová, Vinnic (542-006)</text>
<text x="7.62" y="78.74" size="1.778" layer="91">MELE1065 - Držák baterie CR2032; BHSD-2032-SM</text>
<text x="10.16" y="76.2" size="1.778" layer="91">+ MELE1066 - Krytka držáku baterie CR2032;  BHSD-2032-COVER</text>
<wire x1="5.08" y1="83.82" x2="99.06" y2="83.82" width="0.1524" layer="94" style="shortdash"/>
<wire x1="99.06" y1="83.82" x2="99.06" y2="66.04" width="0.1524" layer="94" style="shortdash"/>
<wire x1="99.06" y1="66.04" x2="5.08" y2="66.04" width="0.1524" layer="94" style="shortdash"/>
<wire x1="5.08" y1="66.04" x2="5.08" y2="83.82" width="0.1524" layer="94" style="shortdash"/>
</plain>
<instances>
<instance part="IC7" gate="G$1" x="147.32" y="88.9"/>
<instance part="R1" gate="G$1" x="121.92" y="104.14" rot="R90"/>
<instance part="R2" gate="G$1" x="165.1" y="104.14" rot="R90"/>
<instance part="GND1" gate="1" x="165.1" y="63.5"/>
<instance part="+3V2" gate="G$3" x="121.92" y="114.3"/>
<instance part="+3V3" gate="G$3" x="165.1" y="114.3"/>
<instance part="BAT1" gate="G$1" x="93.98" y="73.66"/>
<instance part="GND2" gate="1" x="93.98" y="63.5"/>
<instance part="C1" gate="G$1" x="121.92" y="73.66" rot="R90"/>
<instance part="GND3" gate="1" x="121.92" y="63.5"/>
<instance part="D1" gate="G$1" x="106.68" y="88.9" smashed="yes" rot="MR90">
<attribute name="NAME" x="107.188" y="93.7006" size="1.524" layer="95"/>
<attribute name="VALUE" x="107.442" y="91.7956" size="1.524" layer="96"/>
</instance>
<instance part="+3V4" gate="G$3" x="99.06" y="114.3"/>
<instance part="FRAME6" gate="G$1" x="0" y="0"/>
<instance part="FRAME6" gate="G$2" x="162.56" y="0"/>
<instance part="+3V28" gate="G$3" x="10.16" y="172.72"/>
<instance part="P+24" gate="1" x="10.16" y="162.56"/>
<instance part="P+25" gate="1" x="10.16" y="152.4"/>
<instance part="CHASSIS29" gate="G$1" x="10.16" y="124.46"/>
<instance part="GND78" gate="1" x="10.16" y="137.16"/>
<instance part="GND96" gate="1" x="132.08" y="63.5"/>
</instances>
<busses>
</busses>
<nets>
<net name="POWER_3V3" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="2"/>
<pinref part="+3V2" gate="G$3" pin="POWER_3V3"/>
<wire x1="121.92" y1="111.76" x2="121.92" y2="109.22" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R2" gate="G$1" pin="2"/>
<pinref part="+3V3" gate="G$3" pin="POWER_3V3"/>
<wire x1="165.1" y1="111.76" x2="165.1" y2="109.22" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="+3V4" gate="G$3" pin="POWER_3V3"/>
<wire x1="99.06" y1="91.44" x2="99.06" y2="111.76" width="0.1524" layer="91"/>
<pinref part="D1" gate="G$1" pin="A2"/>
<wire x1="99.06" y1="91.44" x2="101.6" y2="91.44" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="+3V28" gate="G$3" pin="POWER_3V3"/>
<wire x1="10.16" y1="170.18" x2="10.16" y2="167.64" width="0.1524" layer="91"/>
<wire x1="10.16" y1="167.64" x2="17.78" y2="167.64" width="0.1524" layer="91"/>
<label x="17.78" y="167.64" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="POWER_GND" class="0">
<segment>
<pinref part="IC7" gate="G$1" pin="GND"/>
<wire x1="160.02" y1="91.44" x2="165.1" y2="91.44" width="0.1524" layer="91"/>
<pinref part="GND1" gate="1" pin="GND"/>
<wire x1="165.1" y1="91.44" x2="165.1" y2="66.04" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="BAT1" gate="G$1" pin="-"/>
<pinref part="GND2" gate="1" pin="GND"/>
<wire x1="93.98" y1="66.04" x2="93.98" y2="68.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C1" gate="G$1" pin="1"/>
<pinref part="GND3" gate="1" pin="GND"/>
<wire x1="121.92" y1="66.04" x2="121.92" y2="71.12" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND78" gate="1" pin="GND"/>
<wire x1="10.16" y1="139.7" x2="10.16" y2="142.24" width="0.1524" layer="91"/>
<wire x1="10.16" y1="142.24" x2="17.78" y2="142.24" width="0.1524" layer="91"/>
<label x="17.78" y="142.24" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="IC7" gate="G$1" pin="FOE"/>
<wire x1="137.16" y1="86.36" x2="132.08" y2="86.36" width="0.1524" layer="91"/>
<wire x1="132.08" y1="86.36" x2="132.08" y2="66.04" width="0.1524" layer="91"/>
<pinref part="GND96" gate="1" pin="GND"/>
</segment>
</net>
<net name="I2C_SCL" class="0">
<segment>
<pinref part="IC7" gate="G$1" pin="SCL"/>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="137.16" y1="96.52" x2="121.92" y2="96.52" width="0.1524" layer="91"/>
<wire x1="121.92" y1="96.52" x2="121.92" y2="99.06" width="0.1524" layer="91"/>
<wire x1="121.92" y1="96.52" x2="93.98" y2="96.52" width="0.1524" layer="91"/>
<junction x="121.92" y="96.52"/>
<label x="93.98" y="96.52" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="I2C_SDA" class="0">
<segment>
<pinref part="IC7" gate="G$1" pin="SDA"/>
<wire x1="160.02" y1="96.52" x2="165.1" y2="96.52" width="0.1524" layer="91"/>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="165.1" y1="96.52" x2="165.1" y2="99.06" width="0.1524" layer="91"/>
<wire x1="165.1" y1="96.52" x2="170.18" y2="96.52" width="0.1524" layer="91"/>
<junction x="165.1" y="96.52"/>
<label x="170.18" y="96.52" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="IC7" gate="G$1" pin="VDD"/>
<wire x1="137.16" y1="88.9" x2="121.92" y2="88.9" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="2"/>
<wire x1="121.92" y1="88.9" x2="121.92" y2="76.2" width="0.1524" layer="91"/>
<pinref part="D1" gate="G$1" pin="K"/>
<wire x1="111.76" y1="88.9" x2="121.92" y2="88.9" width="0.1524" layer="91"/>
<junction x="121.92" y="88.9"/>
</segment>
</net>
<net name="+" class="0">
<segment>
<pinref part="BAT1" gate="G$1" pin="+"/>
<wire x1="101.6" y1="86.36" x2="93.98" y2="86.36" width="0.1524" layer="91"/>
<wire x1="93.98" y1="86.36" x2="93.98" y2="78.74" width="0.1524" layer="91"/>
<pinref part="D1" gate="G$1" pin="A1"/>
</segment>
</net>
<net name="POWER_+5V" class="0">
<segment>
<pinref part="P+24" gate="1" pin="+5V"/>
<wire x1="10.16" y1="160.02" x2="10.16" y2="157.48" width="0.1524" layer="91"/>
<wire x1="10.16" y1="157.48" x2="17.78" y2="157.48" width="0.1524" layer="91"/>
<label x="17.78" y="157.48" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="POWER_24V" class="0">
<segment>
<pinref part="P+25" gate="1" pin="+24V"/>
<wire x1="10.16" y1="149.86" x2="10.16" y2="147.32" width="0.1524" layer="91"/>
<wire x1="10.16" y1="147.32" x2="17.78" y2="147.32" width="0.1524" layer="91"/>
<label x="17.78" y="147.32" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="CHASSIS" class="0">
<segment>
<pinref part="CHASSIS29" gate="G$1" pin="CHASSIS"/>
<wire x1="10.16" y1="127" x2="10.16" y2="129.54" width="0.1524" layer="91"/>
<wire x1="10.16" y1="129.54" x2="17.78" y2="129.54" width="0.1524" layer="91"/>
<label x="17.78" y="129.54" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<text x="223.52" y="160.274" size="1.27" layer="95" align="bottom-center">A</text>
<text x="223.52" y="155.194" size="1.27" layer="95" align="bottom-center">B</text>
<text x="223.266" y="150.114" size="1.27" layer="95" align="bottom-center">CGND</text>
<text x="223.52" y="145.034" size="1.27" layer="95" align="bottom-center">24V</text>
<text x="223.52" y="139.954" size="1.27" layer="95" align="bottom-center">0V</text>
<text x="223.52" y="81.534" size="1.27" layer="95" align="bottom-center">A</text>
<text x="223.52" y="76.454" size="1.27" layer="95" align="bottom-center">B</text>
<text x="223.266" y="71.374" size="1.27" layer="95" align="bottom-center">CGND</text>
<text x="223.52" y="66.294" size="1.27" layer="95" align="bottom-center">24V</text>
<text x="223.52" y="61.214" size="1.27" layer="95" align="bottom-center">0V</text>
<text x="177.8" y="25.4" size="6.4516" layer="94">RS485 + DEBUG</text>
<wire x1="245.11" y1="160.02" x2="246.38" y2="160.02" width="0.1524" layer="94"/>
<wire x1="245.11" y1="154.94" x2="246.38" y2="154.94" width="0.1524" layer="94"/>
<wire x1="245.11" y1="144.78" x2="246.38" y2="144.78" width="0.1524" layer="94"/>
<wire x1="245.11" y1="139.7" x2="246.38" y2="139.7" width="0.1524" layer="94"/>
<wire x1="246.38" y1="144.78" x2="251.46" y2="148.59" width="0.1524" layer="94"/>
<wire x1="246.38" y1="139.7" x2="251.46" y2="147.32" width="0.1524" layer="94"/>
<wire x1="246.38" y1="154.94" x2="251.46" y2="151.13" width="0.1524" layer="94"/>
<wire x1="246.38" y1="160.02" x2="251.46" y2="152.4" width="0.1524" layer="94"/>
<wire x1="245.11" y1="149.86" x2="251.46" y2="149.86" width="0.1524" layer="94"/>
<wire x1="251.46" y1="152.4" x2="251.46" y2="147.32" width="0.1524" layer="94"/>
<wire x1="251.46" y1="147.32" x2="261.62" y2="147.32" width="0.1524" layer="94"/>
<wire x1="251.46" y1="152.4" x2="261.62" y2="152.4" width="0.1524" layer="94"/>
<wire x1="261.62" y1="152.4" x2="261.62" y2="150.368" width="0.1524" layer="94"/>
<wire x1="261.62" y1="150.368" x2="260.35" y2="150.114" width="0.1524" layer="94"/>
<wire x1="260.35" y1="150.114" x2="262.89" y2="149.606" width="0.1524" layer="94"/>
<wire x1="262.89" y1="149.606" x2="261.62" y2="149.352" width="0.1524" layer="94"/>
<wire x1="261.62" y1="149.352" x2="261.62" y2="147.32" width="0.1524" layer="94"/>
<text x="256.54" y="149.86" size="1.778" layer="94" align="center">kabel</text>
<wire x1="245.11" y1="81.28" x2="246.38" y2="81.28" width="0.1524" layer="94"/>
<wire x1="245.11" y1="76.2" x2="246.38" y2="76.2" width="0.1524" layer="94"/>
<wire x1="245.11" y1="66.04" x2="246.38" y2="66.04" width="0.1524" layer="94"/>
<wire x1="245.11" y1="60.96" x2="246.38" y2="60.96" width="0.1524" layer="94"/>
<wire x1="246.38" y1="66.04" x2="251.46" y2="69.85" width="0.1524" layer="94"/>
<wire x1="246.38" y1="60.96" x2="251.46" y2="68.58" width="0.1524" layer="94"/>
<wire x1="246.38" y1="76.2" x2="251.46" y2="72.39" width="0.1524" layer="94"/>
<wire x1="246.38" y1="81.28" x2="251.46" y2="73.66" width="0.1524" layer="94"/>
<wire x1="245.11" y1="71.12" x2="251.46" y2="71.12" width="0.1524" layer="94"/>
<wire x1="251.46" y1="73.66" x2="251.46" y2="68.58" width="0.1524" layer="94"/>
<wire x1="251.46" y1="68.58" x2="261.62" y2="68.58" width="0.1524" layer="94"/>
<wire x1="251.46" y1="73.66" x2="261.62" y2="73.66" width="0.1524" layer="94"/>
<wire x1="261.62" y1="73.66" x2="261.62" y2="71.628" width="0.1524" layer="94"/>
<wire x1="261.62" y1="71.628" x2="260.35" y2="71.374" width="0.1524" layer="94"/>
<wire x1="260.35" y1="71.374" x2="262.89" y2="70.866" width="0.1524" layer="94"/>
<wire x1="262.89" y1="70.866" x2="261.62" y2="70.612" width="0.1524" layer="94"/>
<wire x1="261.62" y1="70.612" x2="261.62" y2="68.58" width="0.1524" layer="94"/>
<text x="256.54" y="71.12" size="1.778" layer="94" align="center">kabel</text>
</plain>
<instances>
<instance part="IC1" gate="SERIAL" x="25.4" y="104.14" smashed="yes" rot="MR0">
<attribute name="NAME" x="25.4" y="137.16" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="27.94" y="139.7" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="IC10" gate="G$1" x="109.22" y="152.4"/>
<instance part="C5" gate="G$1" x="129.54" y="167.64" rot="R90"/>
<instance part="P+3" gate="1" x="124.46" y="177.8"/>
<instance part="GND5" gate="1" x="124.46" y="132.08"/>
<instance part="GND6" gate="1" x="129.54" y="162.56"/>
<instance part="GND7" gate="1" x="114.3" y="132.08"/>
<instance part="CHASSIS2" gate="G$1" x="170.18" y="119.38"/>
<instance part="CHASSIS3" gate="G$1" x="180.34" y="119.38"/>
<instance part="R10" gate="G$1" x="144.78" y="167.64" rot="R90"/>
<instance part="R11" gate="G$1" x="142.24" y="142.24" rot="R90"/>
<instance part="P+4" gate="1" x="144.78" y="177.8"/>
<instance part="GND8" gate="1" x="142.24" y="132.08"/>
<instance part="TVS3" gate="G$1" x="190.5" y="134.62"/>
<instance part="CHASSIS5" gate="G$1" x="190.5" y="119.38"/>
<instance part="GND9" gate="1" x="200.66" y="119.38"/>
<instance part="L1" gate="G$1" x="200.66" y="134.62" smashed="yes" rot="MR0">
<attribute name="NAME" x="198.374" y="133.604" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="202.946" y="122.682" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="L2" gate="G$1" x="203.2" y="162.56" smashed="yes" rot="R90">
<attribute name="NAME" x="195.326" y="164.592" size="1.778" layer="95" rot="MR180"/>
<attribute name="VALUE" x="195.58" y="166.878" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="L3" gate="G$1" x="208.28" y="134.62" smashed="yes" rot="MR0">
<attribute name="NAME" x="205.994" y="133.604" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="210.566" y="122.682" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND10" gate="1" x="208.28" y="119.38"/>
<instance part="IC11" gate="G$1" x="109.22" y="73.66"/>
<instance part="C6" gate="G$1" x="129.54" y="88.9" rot="R90"/>
<instance part="P+6" gate="1" x="124.46" y="99.06"/>
<instance part="GND11" gate="1" x="124.46" y="53.34"/>
<instance part="GND12" gate="1" x="129.54" y="83.82"/>
<instance part="GND13" gate="1" x="114.3" y="53.34"/>
<instance part="CHASSIS4" gate="G$1" x="170.18" y="40.64"/>
<instance part="CHASSIS6" gate="G$1" x="180.34" y="40.64"/>
<instance part="R14" gate="G$1" x="144.78" y="88.9" rot="R90"/>
<instance part="R15" gate="G$1" x="142.24" y="63.5" rot="R90"/>
<instance part="P+7" gate="1" x="144.78" y="99.06"/>
<instance part="GND14" gate="1" x="142.24" y="53.34"/>
<instance part="TVS6" gate="G$1" x="190.5" y="55.88"/>
<instance part="CHASSIS7" gate="G$1" x="190.5" y="40.64"/>
<instance part="GND15" gate="1" x="200.66" y="40.64"/>
<instance part="L4" gate="G$1" x="200.66" y="55.88" smashed="yes" rot="MR0">
<attribute name="NAME" x="198.374" y="54.864" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="202.946" y="43.942" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="L5" gate="G$1" x="203.2" y="83.82" smashed="yes" rot="R90">
<attribute name="NAME" x="195.326" y="85.852" size="1.778" layer="95" rot="MR180"/>
<attribute name="VALUE" x="195.58" y="88.138" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="L6" gate="G$1" x="208.28" y="55.88" smashed="yes" rot="MR0">
<attribute name="NAME" x="205.994" y="54.864" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="210.566" y="43.942" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND16" gate="1" x="208.28" y="40.64"/>
<instance part="CON9" gate="A" x="111.76" y="114.3" smashed="yes" rot="MR180">
<attribute name="NAME" x="110.49" y="122.555" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="114.3" y="125.73" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND18" gate="1" x="101.6" y="99.06" rot="MR0"/>
<instance part="FRAME1" gate="G$1" x="0" y="0"/>
<instance part="FRAME1" gate="G$2" x="162.56" y="0"/>
<instance part="L9" gate="G$1" x="154.94" y="154.94" smashed="yes" rot="R90">
<attribute name="NAME" x="147.066" y="162.052" size="1.778" layer="95" rot="MR180"/>
<attribute name="VALUE" x="147.32" y="159.258" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="L22" gate="G$1" x="154.94" y="149.86" smashed="yes" rot="R90">
<attribute name="NAME" x="147.066" y="149.352" size="1.778" layer="95" rot="MR180"/>
<attribute name="VALUE" x="147.32" y="146.558" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="L23" gate="G$1" x="154.94" y="76.2" smashed="yes" rot="R90">
<attribute name="NAME" x="147.066" y="83.312" size="1.778" layer="95" rot="MR180"/>
<attribute name="VALUE" x="147.32" y="80.518" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="L24" gate="G$1" x="154.94" y="71.12" smashed="yes" rot="R90">
<attribute name="NAME" x="147.066" y="70.612" size="1.778" layer="95" rot="MR180"/>
<attribute name="VALUE" x="147.32" y="67.818" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="+3V29" gate="G$3" x="12.7" y="55.88"/>
<instance part="P+26" gate="1" x="12.7" y="45.72"/>
<instance part="P+27" gate="1" x="12.7" y="35.56"/>
<instance part="CHASSIS30" gate="G$1" x="12.7" y="7.62"/>
<instance part="GND79" gate="1" x="12.7" y="20.32"/>
<instance part="C42" gate="G$1" x="73.66" y="15.24" smashed="yes" rot="R90">
<attribute name="NAME" x="76.2" y="15.24" size="1.778" layer="95"/>
<attribute name="VALUE" x="76.2" y="13.335" size="1.778" layer="96"/>
</instance>
<instance part="IC12" gate="G$1" x="53.34" y="170.18"/>
<instance part="IC14" gate="G$1" x="53.34" y="152.4"/>
<instance part="IC15" gate="G$1" x="60.96" y="45.72"/>
<instance part="IC16" gate="G$1" x="60.96" y="25.4"/>
<instance part="GND84" gate="1" x="48.26" y="10.16"/>
<instance part="C58" gate="G$1" x="27.94" y="152.4" smashed="yes" rot="MR90">
<attribute name="NAME" x="25.4" y="152.4" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="25.4" y="150.495" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="GND85" gate="1" x="27.94" y="147.32"/>
<instance part="R52" gate="G$1" x="104.14" y="137.16"/>
<instance part="R53" gate="G$1" x="104.14" y="58.42"/>
<instance part="J5" gate="G$1" x="86.36" y="137.16"/>
<instance part="J6" gate="G$1" x="86.36" y="58.42"/>
<instance part="+3V33" gate="G$3" x="27.94" y="175.26"/>
<instance part="C59" gate="G$1" x="86.36" y="172.72"/>
<instance part="GND86" gate="1" x="73.66" y="10.16"/>
<instance part="P+32" gate="1" x="78.74" y="177.8" smashed="yes">
<attribute name="VALUE" x="74.422" y="176.022" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="GND87" gate="1" x="71.12" y="147.32"/>
<instance part="GND88" gate="1" x="35.56" y="147.32"/>
<instance part="GND89" gate="1" x="93.98" y="167.64"/>
<instance part="GND90" gate="1" x="35.56" y="165.1"/>
<instance part="GND91" gate="1" x="71.12" y="165.1"/>
<instance part="+3V31" gate="G$3" x="73.66" y="55.88"/>
<instance part="R54" gate="G$1" x="132.08" y="152.4" smashed="yes">
<attribute name="NAME" x="132.08" y="155.448" size="1.778" layer="95" align="bottom-center"/>
<attribute name="VALUE" x="132.08" y="149.225" size="1.778" layer="96" rot="R180" align="bottom-center"/>
</instance>
<instance part="R55" gate="G$1" x="132.08" y="73.66" smashed="yes">
<attribute name="NAME" x="132.08" y="76.708" size="1.778" layer="95" align="bottom-center"/>
<attribute name="VALUE" x="132.08" y="70.485" size="1.778" layer="96" rot="R180" align="bottom-center"/>
</instance>
<instance part="CON2" gate="G$1" x="236.22" y="149.86" rot="R180"/>
<instance part="CON4" gate="G$1" x="236.22" y="71.12" rot="R180"/>
<instance part="TVS2" gate="G$1" x="170.18" y="134.62"/>
<instance part="TVS1" gate="G$1" x="180.34" y="134.62"/>
<instance part="TVS4" gate="G$1" x="180.34" y="55.88"/>
<instance part="TVS5" gate="G$1" x="170.18" y="55.88"/>
</instances>
<busses>
</busses>
<nets>
<net name="POWER_GND" class="0">
<segment>
<pinref part="C5" gate="G$1" pin="1"/>
<pinref part="GND6" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="IC10" gate="G$1" pin="GND"/>
<wire x1="121.92" y1="144.78" x2="124.46" y2="144.78" width="0.1524" layer="91"/>
<pinref part="GND5" gate="1" pin="GND"/>
<wire x1="124.46" y1="144.78" x2="124.46" y2="134.62" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R11" gate="G$1" pin="1"/>
<pinref part="GND8" gate="1" pin="GND"/>
<wire x1="142.24" y1="134.62" x2="142.24" y2="137.16" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="L3" gate="G$1" pin="2"/>
<wire x1="208.28" y1="121.92" x2="208.28" y2="127" width="0.1524" layer="91"/>
<pinref part="GND10" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND9" gate="1" pin="GND"/>
<pinref part="L1" gate="G$1" pin="2"/>
<wire x1="200.66" y1="127" x2="200.66" y2="121.92" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C6" gate="G$1" pin="1"/>
<pinref part="GND12" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="IC11" gate="G$1" pin="GND"/>
<wire x1="121.92" y1="66.04" x2="124.46" y2="66.04" width="0.1524" layer="91"/>
<pinref part="GND11" gate="1" pin="GND"/>
<wire x1="124.46" y1="66.04" x2="124.46" y2="55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R15" gate="G$1" pin="1"/>
<pinref part="GND14" gate="1" pin="GND"/>
<wire x1="142.24" y1="55.88" x2="142.24" y2="58.42" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="L6" gate="G$1" pin="2"/>
<wire x1="208.28" y1="43.18" x2="208.28" y2="48.26" width="0.1524" layer="91"/>
<pinref part="GND16" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND15" gate="1" pin="GND"/>
<pinref part="L4" gate="G$1" pin="2"/>
<wire x1="200.66" y1="48.26" x2="200.66" y2="43.18" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="CON9" gate="A" pin="1"/>
<wire x1="109.22" y1="106.68" x2="101.6" y2="106.68" width="0.1524" layer="91"/>
<pinref part="GND18" gate="1" pin="GND"/>
<wire x1="101.6" y1="106.68" x2="101.6" y2="101.6" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND79" gate="1" pin="GND"/>
<wire x1="12.7" y1="22.86" x2="12.7" y2="25.4" width="0.1524" layer="91"/>
<wire x1="12.7" y1="25.4" x2="20.32" y2="25.4" width="0.1524" layer="91"/>
<label x="20.32" y="25.4" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="GND84" gate="1" pin="GND"/>
<wire x1="48.26" y1="12.7" x2="48.26" y2="20.32" width="0.1524" layer="91"/>
<pinref part="IC15" gate="G$1" pin="GND"/>
<wire x1="48.26" y1="20.32" x2="48.26" y2="40.64" width="0.1524" layer="91"/>
<wire x1="48.26" y1="40.64" x2="53.34" y2="40.64" width="0.1524" layer="91"/>
<pinref part="IC16" gate="G$1" pin="GND"/>
<wire x1="48.26" y1="20.32" x2="53.34" y2="20.32" width="0.1524" layer="91"/>
<junction x="48.26" y="20.32"/>
</segment>
<segment>
<pinref part="C58" gate="G$1" pin="1"/>
<pinref part="GND85" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C42" gate="G$1" pin="1"/>
<pinref part="GND86" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="IC12" gate="G$1" pin="DIR"/>
<wire x1="60.96" y1="170.18" x2="71.12" y2="170.18" width="0.1524" layer="91"/>
<pinref part="GND91" gate="1" pin="GND"/>
<wire x1="71.12" y1="170.18" x2="71.12" y2="167.64" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C59" gate="G$1" pin="2"/>
<wire x1="88.9" y1="172.72" x2="93.98" y2="172.72" width="0.1524" layer="91"/>
<pinref part="GND89" gate="1" pin="GND"/>
<wire x1="93.98" y1="172.72" x2="93.98" y2="170.18" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND13" gate="1" pin="GND"/>
<wire x1="114.3" y1="58.42" x2="114.3" y2="55.88" width="0.1524" layer="91"/>
<wire x1="109.22" y1="58.42" x2="114.3" y2="58.42" width="0.1524" layer="91"/>
<pinref part="R53" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="GND7" gate="1" pin="GND"/>
<wire x1="114.3" y1="137.16" x2="114.3" y2="134.62" width="0.1524" layer="91"/>
<pinref part="R52" gate="G$1" pin="2"/>
<wire x1="109.22" y1="137.16" x2="114.3" y2="137.16" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC14" gate="G$1" pin="GND"/>
<wire x1="35.56" y1="152.4" x2="45.72" y2="152.4" width="0.1524" layer="91"/>
<pinref part="GND88" gate="1" pin="GND"/>
<wire x1="35.56" y1="149.86" x2="35.56" y2="152.4" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND87" gate="1" pin="GND"/>
<pinref part="IC14" gate="G$1" pin="DIR"/>
<wire x1="60.96" y1="152.4" x2="71.12" y2="152.4" width="0.1524" layer="91"/>
<wire x1="71.12" y1="152.4" x2="71.12" y2="149.86" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC12" gate="G$1" pin="GND"/>
<wire x1="35.56" y1="167.64" x2="35.56" y2="170.18" width="0.1524" layer="91"/>
<wire x1="35.56" y1="170.18" x2="45.72" y2="170.18" width="0.1524" layer="91"/>
<pinref part="GND90" gate="1" pin="GND"/>
</segment>
</net>
<net name="I2C_SCL" class="0">
<segment>
<pinref part="IC1" gate="SERIAL" pin="GPIO-C.04/I2C_CLK"/>
<wire x1="38.1" y1="132.08" x2="48.26" y2="132.08" width="0.1524" layer="91"/>
<label x="48.26" y="132.08" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="I2C_SDA" class="0">
<segment>
<pinref part="IC1" gate="SERIAL" pin="GPIO-C.05/I2C_DATA"/>
<wire x1="38.1" y1="129.54" x2="48.26" y2="129.54" width="0.1524" layer="91"/>
<label x="48.26" y="129.54" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="POWER_+5V" class="0">
<segment>
<pinref part="IC10" gate="G$1" pin="VCC"/>
<wire x1="121.92" y1="160.02" x2="124.46" y2="160.02" width="0.1524" layer="91"/>
<wire x1="124.46" y1="160.02" x2="124.46" y2="172.72" width="0.1524" layer="91"/>
<pinref part="P+3" gate="1" pin="+5V"/>
<wire x1="124.46" y1="172.72" x2="124.46" y2="175.26" width="0.1524" layer="91"/>
<wire x1="124.46" y1="172.72" x2="129.54" y2="172.72" width="0.1524" layer="91"/>
<junction x="124.46" y="172.72"/>
<pinref part="C5" gate="G$1" pin="2"/>
<wire x1="129.54" y1="172.72" x2="129.54" y2="170.18" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R10" gate="G$1" pin="2"/>
<pinref part="P+4" gate="1" pin="+5V"/>
<wire x1="144.78" y1="175.26" x2="144.78" y2="172.72" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC11" gate="G$1" pin="VCC"/>
<wire x1="121.92" y1="81.28" x2="124.46" y2="81.28" width="0.1524" layer="91"/>
<wire x1="124.46" y1="81.28" x2="124.46" y2="93.98" width="0.1524" layer="91"/>
<pinref part="P+6" gate="1" pin="+5V"/>
<wire x1="124.46" y1="93.98" x2="124.46" y2="96.52" width="0.1524" layer="91"/>
<wire x1="124.46" y1="93.98" x2="129.54" y2="93.98" width="0.1524" layer="91"/>
<junction x="124.46" y="93.98"/>
<pinref part="C6" gate="G$1" pin="2"/>
<wire x1="129.54" y1="93.98" x2="129.54" y2="91.44" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R14" gate="G$1" pin="2"/>
<pinref part="P+7" gate="1" pin="+5V"/>
<wire x1="144.78" y1="96.52" x2="144.78" y2="93.98" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="P+26" gate="1" pin="+5V"/>
<wire x1="12.7" y1="43.18" x2="12.7" y2="40.64" width="0.1524" layer="91"/>
<wire x1="12.7" y1="40.64" x2="20.32" y2="40.64" width="0.1524" layer="91"/>
<label x="20.32" y="40.64" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="IC12" gate="G$1" pin="VCCB"/>
<pinref part="P+32" gate="1" pin="+5V"/>
<wire x1="78.74" y1="175.26" x2="78.74" y2="172.72" width="0.1524" layer="91"/>
<pinref part="IC14" gate="G$1" pin="VCCB"/>
<wire x1="78.74" y1="172.72" x2="78.74" y2="154.94" width="0.1524" layer="91"/>
<wire x1="78.74" y1="154.94" x2="60.96" y2="154.94" width="0.1524" layer="91"/>
<wire x1="60.96" y1="172.72" x2="78.74" y2="172.72" width="0.1524" layer="91"/>
<junction x="78.74" y="172.72"/>
<pinref part="C59" gate="G$1" pin="1"/>
<wire x1="78.74" y1="172.72" x2="83.82" y2="172.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="IC10" gate="G$1" pin="B"/>
<wire x1="121.92" y1="154.94" x2="139.7" y2="154.94" width="0.1524" layer="91"/>
<wire x1="139.7" y1="154.94" x2="142.24" y2="154.94" width="0.1524" layer="91"/>
<wire x1="142.24" y1="154.94" x2="142.24" y2="147.32" width="0.1524" layer="91"/>
<pinref part="R11" gate="G$1" pin="2"/>
<pinref part="L9" gate="G$1" pin="1"/>
<wire x1="142.24" y1="154.94" x2="147.32" y2="154.94" width="0.1524" layer="91"/>
<junction x="142.24" y="154.94"/>
<pinref part="R54" gate="G$1" pin="2"/>
<wire x1="137.16" y1="152.4" x2="139.7" y2="152.4" width="0.1524" layer="91"/>
<wire x1="139.7" y1="152.4" x2="139.7" y2="154.94" width="0.1524" layer="91"/>
<junction x="139.7" y="154.94"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="IC10" gate="G$1" pin="A"/>
<wire x1="121.92" y1="149.86" x2="124.46" y2="149.86" width="0.1524" layer="91"/>
<pinref part="R10" gate="G$1" pin="1"/>
<wire x1="124.46" y1="149.86" x2="144.78" y2="149.86" width="0.1524" layer="91"/>
<wire x1="144.78" y1="162.56" x2="144.78" y2="149.86" width="0.1524" layer="91"/>
<pinref part="L22" gate="G$1" pin="1"/>
<wire x1="144.78" y1="149.86" x2="147.32" y2="149.86" width="0.1524" layer="91"/>
<junction x="144.78" y="149.86"/>
<pinref part="R54" gate="G$1" pin="1"/>
<wire x1="127" y1="152.4" x2="124.46" y2="152.4" width="0.1524" layer="91"/>
<wire x1="124.46" y1="152.4" x2="124.46" y2="149.86" width="0.1524" layer="91"/>
<junction x="124.46" y="149.86"/>
</segment>
</net>
<net name="CHASSIS" class="0">
<segment>
<pinref part="CHASSIS2" gate="G$1" pin="CHASSIS"/>
<wire x1="170.18" y1="121.92" x2="170.18" y2="129.54" width="0.1524" layer="91"/>
<pinref part="TVS2" gate="G$1" pin="A"/>
</segment>
<segment>
<pinref part="CHASSIS3" gate="G$1" pin="CHASSIS"/>
<pinref part="TVS1" gate="G$1" pin="A"/>
<wire x1="180.34" y1="129.54" x2="180.34" y2="121.92" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="TVS3" gate="G$1" pin="A"/>
<pinref part="CHASSIS5" gate="G$1" pin="CHASSIS"/>
<wire x1="190.5" y1="129.54" x2="190.5" y2="121.92" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="CHASSIS4" gate="G$1" pin="CHASSIS"/>
<pinref part="TVS5" gate="G$1" pin="A"/>
<wire x1="170.18" y1="43.18" x2="170.18" y2="50.8" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="CHASSIS6" gate="G$1" pin="CHASSIS"/>
<pinref part="TVS4" gate="G$1" pin="A"/>
<wire x1="180.34" y1="43.18" x2="180.34" y2="50.8" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="TVS6" gate="G$1" pin="A"/>
<pinref part="CHASSIS7" gate="G$1" pin="CHASSIS"/>
<wire x1="190.5" y1="50.8" x2="190.5" y2="43.18" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="CHASSIS30" gate="G$1" pin="CHASSIS"/>
<wire x1="12.7" y1="10.16" x2="12.7" y2="12.7" width="0.1524" layer="91"/>
<wire x1="12.7" y1="12.7" x2="20.32" y2="12.7" width="0.1524" layer="91"/>
<label x="20.32" y="12.7" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="L1" gate="G$1" pin="1"/>
<wire x1="200.66" y1="149.86" x2="200.66" y2="142.24" width="0.1524" layer="91"/>
<pinref part="CON2" gate="G$1" pin="3"/>
<wire x1="200.66" y1="149.86" x2="228.6" y2="149.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="L3" gate="G$1" pin="1"/>
<wire x1="218.44" y1="144.78" x2="208.28" y2="144.78" width="0.1524" layer="91"/>
<wire x1="208.28" y1="144.78" x2="208.28" y2="142.24" width="0.1524" layer="91"/>
<pinref part="CON2" gate="G$1" pin="1"/>
<wire x1="218.44" y1="144.78" x2="218.44" y2="139.7" width="0.1524" layer="91"/>
<wire x1="218.44" y1="139.7" x2="228.6" y2="139.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="TVS3" gate="G$1" pin="B"/>
<pinref part="L2" gate="G$1" pin="1"/>
<wire x1="190.5" y1="147.32" x2="190.5" y2="139.7" width="0.1524" layer="91"/>
<junction x="190.5" y="147.32"/>
<wire x1="218.44" y1="147.32" x2="190.5" y2="147.32" width="0.1524" layer="91"/>
<wire x1="195.58" y1="162.56" x2="190.5" y2="162.56" width="0.1524" layer="91"/>
<wire x1="190.5" y1="162.56" x2="190.5" y2="147.32" width="0.1524" layer="91"/>
<pinref part="CON2" gate="G$1" pin="2"/>
<wire x1="218.44" y1="147.32" x2="220.98" y2="147.32" width="0.1524" layer="91"/>
<wire x1="220.98" y1="147.32" x2="220.98" y2="144.78" width="0.1524" layer="91"/>
<wire x1="220.98" y1="144.78" x2="228.6" y2="144.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="IC11" gate="G$1" pin="B"/>
<wire x1="121.92" y1="76.2" x2="139.7" y2="76.2" width="0.1524" layer="91"/>
<wire x1="139.7" y1="76.2" x2="142.24" y2="76.2" width="0.1524" layer="91"/>
<wire x1="142.24" y1="76.2" x2="147.32" y2="76.2" width="0.1524" layer="91"/>
<wire x1="142.24" y1="76.2" x2="142.24" y2="68.58" width="0.1524" layer="91"/>
<junction x="142.24" y="76.2"/>
<pinref part="R15" gate="G$1" pin="2"/>
<pinref part="L23" gate="G$1" pin="1"/>
<pinref part="R55" gate="G$1" pin="2"/>
<wire x1="137.16" y1="73.66" x2="139.7" y2="73.66" width="0.1524" layer="91"/>
<wire x1="139.7" y1="73.66" x2="139.7" y2="76.2" width="0.1524" layer="91"/>
<junction x="139.7" y="76.2"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="IC11" gate="G$1" pin="A"/>
<wire x1="121.92" y1="71.12" x2="124.46" y2="71.12" width="0.1524" layer="91"/>
<pinref part="R14" gate="G$1" pin="1"/>
<wire x1="124.46" y1="71.12" x2="144.78" y2="71.12" width="0.1524" layer="91"/>
<wire x1="144.78" y1="71.12" x2="147.32" y2="71.12" width="0.1524" layer="91"/>
<wire x1="144.78" y1="83.82" x2="144.78" y2="71.12" width="0.1524" layer="91"/>
<junction x="144.78" y="71.12"/>
<pinref part="L24" gate="G$1" pin="1"/>
<pinref part="R55" gate="G$1" pin="1"/>
<wire x1="127" y1="73.66" x2="124.46" y2="73.66" width="0.1524" layer="91"/>
<wire x1="124.46" y1="73.66" x2="124.46" y2="71.12" width="0.1524" layer="91"/>
<junction x="124.46" y="71.12"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="L4" gate="G$1" pin="1"/>
<wire x1="228.6" y1="71.12" x2="200.66" y2="71.12" width="0.1524" layer="91"/>
<wire x1="200.66" y1="71.12" x2="200.66" y2="63.5" width="0.1524" layer="91"/>
<pinref part="CON4" gate="G$1" pin="3"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="L6" gate="G$1" pin="1"/>
<wire x1="208.28" y1="66.04" x2="208.28" y2="63.5" width="0.1524" layer="91"/>
<pinref part="CON4" gate="G$1" pin="1"/>
<wire x1="208.28" y1="66.04" x2="218.44" y2="66.04" width="0.1524" layer="91"/>
<wire x1="218.44" y1="66.04" x2="218.44" y2="60.96" width="0.1524" layer="91"/>
<wire x1="218.44" y1="60.96" x2="228.6" y2="60.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="TVS6" gate="G$1" pin="B"/>
<wire x1="190.5" y1="68.58" x2="190.5" y2="60.96" width="0.1524" layer="91"/>
<pinref part="L5" gate="G$1" pin="1"/>
<wire x1="218.44" y1="68.58" x2="190.5" y2="68.58" width="0.1524" layer="91"/>
<wire x1="195.58" y1="83.82" x2="190.5" y2="83.82" width="0.1524" layer="91"/>
<wire x1="190.5" y1="83.82" x2="190.5" y2="68.58" width="0.1524" layer="91"/>
<junction x="190.5" y="68.58"/>
<pinref part="CON4" gate="G$1" pin="2"/>
<wire x1="218.44" y1="68.58" x2="220.98" y2="68.58" width="0.1524" layer="91"/>
<wire x1="220.98" y1="68.58" x2="220.98" y2="66.04" width="0.1524" layer="91"/>
<wire x1="220.98" y1="66.04" x2="228.6" y2="66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="TXD" class="0">
<segment>
<pinref part="CON9" gate="A" pin="5"/>
<pinref part="IC1" gate="SERIAL" pin="GPIO-O.01/UART_A_TXD"/>
<wire x1="38.1" y1="104.14" x2="86.36" y2="104.14" width="0.1524" layer="91"/>
<wire x1="86.36" y1="104.14" x2="86.36" y2="116.84" width="0.1524" layer="91"/>
<wire x1="86.36" y1="116.84" x2="109.22" y2="116.84" width="0.1524" layer="91"/>
<label x="48.26" y="104.394" size="1.27" layer="95"/>
<label x="99.06" y="117.094" size="1.27" layer="95"/>
</segment>
</net>
<net name="RXD" class="0">
<segment>
<pinref part="IC1" gate="SERIAL" pin="GPIO-O.02/UART_A_RXD"/>
<pinref part="CON9" gate="A" pin="4"/>
<wire x1="109.22" y1="114.3" x2="88.9" y2="114.3" width="0.1524" layer="91"/>
<wire x1="88.9" y1="114.3" x2="88.9" y2="101.6" width="0.1524" layer="91"/>
<wire x1="88.9" y1="101.6" x2="38.1" y2="101.6" width="0.1524" layer="91"/>
<label x="48.26" y="101.854" size="1.27" layer="95"/>
<label x="99.06" y="114.554" size="1.27" layer="95"/>
</segment>
</net>
<net name="RTS" class="0">
<segment>
<pinref part="IC1" gate="SERIAL" pin="GPIO-O.04/UART_A_RTS"/>
<pinref part="CON9" gate="A" pin="2"/>
<wire x1="38.1" y1="99.06" x2="91.44" y2="99.06" width="0.1524" layer="91"/>
<wire x1="91.44" y1="99.06" x2="91.44" y2="109.22" width="0.1524" layer="91"/>
<wire x1="91.44" y1="109.22" x2="109.22" y2="109.22" width="0.1524" layer="91"/>
<label x="48.26" y="99.314" size="1.27" layer="95"/>
<label x="99.06" y="109.474" size="1.27" layer="95"/>
</segment>
</net>
<net name="CTS" class="0">
<segment>
<pinref part="CON9" gate="A" pin="6"/>
<pinref part="IC1" gate="SERIAL" pin="GPIO-O.03/UART_A_CTS"/>
<wire x1="109.22" y1="119.38" x2="93.98" y2="119.38" width="0.1524" layer="91"/>
<wire x1="93.98" y1="119.38" x2="93.98" y2="96.52" width="0.1524" layer="91"/>
<wire x1="93.98" y1="96.52" x2="38.1" y2="96.52" width="0.1524" layer="91"/>
<label x="48.26" y="96.774" size="1.27" layer="95"/>
<label x="99.06" y="119.634" size="1.27" layer="95"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<wire x1="43.18" y1="167.64" x2="43.18" y2="109.22" width="0.1524" layer="91"/>
<wire x1="43.18" y1="109.22" x2="38.1" y2="109.22" width="0.1524" layer="91"/>
<pinref part="IC1" gate="SERIAL" pin="GPIO-C.03/UART_C_RXD"/>
<pinref part="IC12" gate="G$1" pin="A"/>
<wire x1="43.18" y1="167.64" x2="45.72" y2="167.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="IC10" gate="G$1" pin="DI"/>
<wire x1="96.52" y1="144.78" x2="76.2" y2="144.78" width="0.1524" layer="91"/>
<wire x1="76.2" y1="144.78" x2="76.2" y2="111.76" width="0.1524" layer="91"/>
<pinref part="IC1" gate="SERIAL" pin="GPIO-C.02/UART_C_TXD"/>
<wire x1="76.2" y1="111.76" x2="38.1" y2="111.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="IC11" gate="G$1" pin="RO"/>
<wire x1="96.52" y1="81.28" x2="66.04" y2="81.28" width="0.1524" layer="91"/>
<wire x1="66.04" y1="81.28" x2="66.04" y2="149.86" width="0.1524" layer="91"/>
<pinref part="IC14" gate="G$1" pin="B"/>
<wire x1="66.04" y1="149.86" x2="60.96" y2="149.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="IC1" gate="SERIAL" pin="GPIO-J.07/UART_B_TXD"/>
<wire x1="38.1" y1="81.28" x2="60.96" y2="81.28" width="0.1524" layer="91"/>
<wire x1="60.96" y1="81.28" x2="60.96" y2="66.04" width="0.1524" layer="91"/>
<pinref part="IC11" gate="G$1" pin="DI"/>
<wire x1="60.96" y1="66.04" x2="96.52" y2="66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="IC10" gate="G$1" pin="DE"/>
<wire x1="96.52" y1="149.86" x2="78.74" y2="149.86" width="0.1524" layer="91"/>
<pinref part="IC15" gate="G$1" pin="Y"/>
<wire x1="68.58" y1="40.64" x2="78.74" y2="40.64" width="0.1524" layer="91"/>
<pinref part="J5" gate="G$1" pin="1"/>
<wire x1="78.74" y1="40.64" x2="78.74" y2="137.16" width="0.1524" layer="91"/>
<wire x1="78.74" y1="137.16" x2="78.74" y2="149.86" width="0.1524" layer="91"/>
<wire x1="81.28" y1="137.16" x2="78.74" y2="137.16" width="0.1524" layer="91"/>
<junction x="78.74" y="137.16"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<pinref part="IC1" gate="SERIAL" pin="GPIO-K.07/UART_B_RTS"/>
<pinref part="IC16" gate="G$1" pin="A"/>
<wire x1="40.64" y1="76.2" x2="38.1" y2="76.2" width="0.1524" layer="91"/>
<wire x1="40.64" y1="76.2" x2="40.64" y2="25.4" width="0.1524" layer="91"/>
<wire x1="40.64" y1="25.4" x2="53.34" y2="25.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="RS485_PWR" class="0">
<segment>
<pinref part="L2" gate="G$1" pin="2"/>
<wire x1="210.82" y1="162.56" x2="215.9" y2="162.56" width="0.1524" layer="91"/>
<wire x1="215.9" y1="162.56" x2="215.9" y2="83.82" width="0.1524" layer="91"/>
<pinref part="L5" gate="G$1" pin="2"/>
<wire x1="215.9" y1="83.82" x2="210.82" y2="83.82" width="0.1524" layer="91"/>
<wire x1="215.9" y1="162.56" x2="215.9" y2="175.26" width="0.1524" layer="91"/>
<wire x1="215.9" y1="175.26" x2="220.98" y2="175.26" width="0.1524" layer="91"/>
<junction x="215.9" y="162.56"/>
<label x="220.98" y="175.26" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<wire x1="218.44" y1="154.94" x2="180.34" y2="154.94" width="0.1524" layer="91"/>
<wire x1="180.34" y1="154.94" x2="180.34" y2="149.86" width="0.1524" layer="91"/>
<pinref part="L22" gate="G$1" pin="2"/>
<wire x1="162.56" y1="149.86" x2="180.34" y2="149.86" width="0.1524" layer="91"/>
<junction x="180.34" y="149.86"/>
<pinref part="CON2" gate="G$1" pin="5"/>
<wire x1="218.44" y1="154.94" x2="218.44" y2="160.02" width="0.1524" layer="91"/>
<wire x1="218.44" y1="160.02" x2="228.6" y2="160.02" width="0.1524" layer="91"/>
<pinref part="TVS1" gate="G$1" pin="B"/>
<wire x1="180.34" y1="149.86" x2="180.34" y2="139.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<wire x1="220.98" y1="152.4" x2="170.18" y2="152.4" width="0.1524" layer="91"/>
<pinref part="L9" gate="G$1" pin="2"/>
<wire x1="162.56" y1="154.94" x2="170.18" y2="154.94" width="0.1524" layer="91"/>
<wire x1="170.18" y1="154.94" x2="170.18" y2="152.4" width="0.1524" layer="91"/>
<junction x="170.18" y="152.4"/>
<pinref part="CON2" gate="G$1" pin="4"/>
<wire x1="220.98" y1="152.4" x2="220.98" y2="154.94" width="0.1524" layer="91"/>
<wire x1="220.98" y1="154.94" x2="228.6" y2="154.94" width="0.1524" layer="91"/>
<pinref part="TVS2" gate="G$1" pin="B"/>
<wire x1="170.18" y1="139.7" x2="170.18" y2="152.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="L23" gate="G$1" pin="2"/>
<wire x1="162.56" y1="76.2" x2="170.18" y2="76.2" width="0.1524" layer="91"/>
<wire x1="170.18" y1="76.2" x2="170.18" y2="73.66" width="0.1524" layer="91"/>
<junction x="170.18" y="73.66"/>
<pinref part="CON4" gate="G$1" pin="4"/>
<wire x1="220.98" y1="73.66" x2="170.18" y2="73.66" width="0.1524" layer="91"/>
<wire x1="220.98" y1="73.66" x2="220.98" y2="76.2" width="0.1524" layer="91"/>
<wire x1="220.98" y1="76.2" x2="228.6" y2="76.2" width="0.1524" layer="91"/>
<pinref part="TVS5" gate="G$1" pin="B"/>
<wire x1="170.18" y1="60.96" x2="170.18" y2="73.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="L24" gate="G$1" pin="2"/>
<wire x1="162.56" y1="71.12" x2="180.34" y2="71.12" width="0.1524" layer="91"/>
<wire x1="218.44" y1="76.2" x2="180.34" y2="76.2" width="0.1524" layer="91"/>
<wire x1="180.34" y1="76.2" x2="180.34" y2="71.12" width="0.1524" layer="91"/>
<junction x="180.34" y="71.12"/>
<pinref part="CON4" gate="G$1" pin="5"/>
<wire x1="218.44" y1="76.2" x2="218.44" y2="81.28" width="0.1524" layer="91"/>
<wire x1="218.44" y1="81.28" x2="228.6" y2="81.28" width="0.1524" layer="91"/>
<pinref part="TVS4" gate="G$1" pin="B"/>
<wire x1="180.34" y1="60.96" x2="180.34" y2="71.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="POWER_3V3" class="0">
<segment>
<pinref part="+3V29" gate="G$3" pin="POWER_3V3"/>
<wire x1="12.7" y1="53.34" x2="12.7" y2="50.8" width="0.1524" layer="91"/>
<wire x1="12.7" y1="50.8" x2="20.32" y2="50.8" width="0.1524" layer="91"/>
<label x="20.32" y="50.8" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="IC14" gate="G$1" pin="VCCA"/>
<pinref part="C58" gate="G$1" pin="2"/>
<wire x1="45.72" y1="154.94" x2="27.94" y2="154.94" width="0.1524" layer="91"/>
<pinref part="IC12" gate="G$1" pin="VCCA"/>
<junction x="27.94" y="154.94"/>
<pinref part="+3V33" gate="G$3" pin="POWER_3V3"/>
<wire x1="45.72" y1="172.72" x2="27.94" y2="172.72" width="0.1524" layer="91"/>
<wire x1="27.94" y1="154.94" x2="27.94" y2="172.72" width="0.1524" layer="91"/>
<junction x="27.94" y="172.72"/>
</segment>
<segment>
<pinref part="+3V31" gate="G$3" pin="POWER_3V3"/>
<pinref part="C42" gate="G$1" pin="2"/>
<wire x1="73.66" y1="53.34" x2="73.66" y2="50.8" width="0.1524" layer="91"/>
<pinref part="IC15" gate="G$1" pin="VCC"/>
<wire x1="73.66" y1="50.8" x2="73.66" y2="30.48" width="0.1524" layer="91"/>
<wire x1="73.66" y1="30.48" x2="73.66" y2="17.78" width="0.1524" layer="91"/>
<wire x1="68.58" y1="50.8" x2="73.66" y2="50.8" width="0.1524" layer="91"/>
<junction x="73.66" y="50.8"/>
<pinref part="IC16" gate="G$1" pin="VCC"/>
<wire x1="68.58" y1="30.48" x2="73.66" y2="30.48" width="0.1524" layer="91"/>
<junction x="73.66" y="30.48"/>
</segment>
</net>
<net name="POWER_24V" class="0">
<segment>
<pinref part="P+27" gate="1" pin="+24V"/>
<wire x1="12.7" y1="33.02" x2="12.7" y2="30.48" width="0.1524" layer="91"/>
<wire x1="12.7" y1="30.48" x2="20.32" y2="30.48" width="0.1524" layer="91"/>
<label x="20.32" y="30.48" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="J5" gate="G$1" pin="2"/>
<pinref part="R52" gate="G$1" pin="1"/>
<wire x1="91.44" y1="137.16" x2="93.98" y2="137.16" width="0.1524" layer="91"/>
<pinref part="IC10" gate="G$1" pin="!RE"/>
<wire x1="93.98" y1="137.16" x2="99.06" y2="137.16" width="0.1524" layer="91"/>
<wire x1="96.52" y1="154.94" x2="93.98" y2="154.94" width="0.1524" layer="91"/>
<wire x1="93.98" y1="154.94" x2="93.98" y2="137.16" width="0.1524" layer="91"/>
<junction x="93.98" y="137.16"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="J6" gate="G$1" pin="2"/>
<pinref part="R53" gate="G$1" pin="1"/>
<wire x1="91.44" y1="58.42" x2="93.98" y2="58.42" width="0.1524" layer="91"/>
<pinref part="IC11" gate="G$1" pin="!RE"/>
<wire x1="93.98" y1="58.42" x2="99.06" y2="58.42" width="0.1524" layer="91"/>
<wire x1="96.52" y1="76.2" x2="93.98" y2="76.2" width="0.1524" layer="91"/>
<wire x1="93.98" y1="76.2" x2="93.98" y2="58.42" width="0.1524" layer="91"/>
<junction x="93.98" y="58.42"/>
</segment>
</net>
<net name="N$120" class="0">
<segment>
<pinref part="IC11" gate="G$1" pin="DE"/>
<wire x1="96.52" y1="71.12" x2="81.28" y2="71.12" width="0.1524" layer="91"/>
<pinref part="J6" gate="G$1" pin="1"/>
<wire x1="81.28" y1="58.42" x2="81.28" y2="71.12" width="0.1524" layer="91"/>
<junction x="81.28" y="58.42"/>
<wire x1="81.28" y1="20.32" x2="81.28" y2="58.42" width="0.1524" layer="91"/>
<pinref part="IC16" gate="G$1" pin="Y"/>
<wire x1="68.58" y1="20.32" x2="81.28" y2="20.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$121" class="0">
<segment>
<pinref part="IC15" gate="G$1" pin="A"/>
<pinref part="IC1" gate="SERIAL" pin="GPIO-O.00/UART_A_DTR"/>
<wire x1="43.18" y1="91.44" x2="38.1" y2="91.44" width="0.1524" layer="91"/>
<wire x1="43.18" y1="91.44" x2="43.18" y2="45.72" width="0.1524" layer="91"/>
<wire x1="43.18" y1="45.72" x2="53.34" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$122" class="0">
<segment>
<pinref part="IC12" gate="G$1" pin="B"/>
<wire x1="66.04" y1="167.64" x2="60.96" y2="167.64" width="0.1524" layer="91"/>
<wire x1="66.04" y1="160.02" x2="66.04" y2="167.64" width="0.1524" layer="91"/>
<pinref part="IC10" gate="G$1" pin="RO"/>
<wire x1="96.52" y1="160.02" x2="66.04" y2="160.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$123" class="0">
<segment>
<pinref part="IC1" gate="SERIAL" pin="GPIO-B.00/UART_B_RXD"/>
<pinref part="IC14" gate="G$1" pin="A"/>
<wire x1="40.64" y1="149.86" x2="45.72" y2="149.86" width="0.1524" layer="91"/>
<wire x1="40.64" y1="149.86" x2="40.64" y2="78.74" width="0.1524" layer="91"/>
<wire x1="40.64" y1="78.74" x2="38.1" y2="78.74" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<text x="314.96" y="25.4" size="6.4516" layer="94">USB HUB</text>
</plain>
<instances>
<instance part="IC1" gate="USB" x="35.56" y="111.76" smashed="yes" rot="MR0">
<attribute name="NAME" x="35.56" y="129.54" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="38.1" y="132.08" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="CON7" gate="G$1" x="381" y="76.2" rot="MR270"/>
<instance part="IC5" gate="G$1" x="340.36" y="73.66" rot="MR90"/>
<instance part="IC6" gate="G$1" x="165.1" y="213.36"/>
<instance part="P+9" gate="1" x="167.64" y="251.46"/>
<instance part="GND32" gate="1" x="167.64" y="187.96"/>
<instance part="L12" gate="G$1" x="330.2" y="55.88" smashed="yes" rot="MR90">
<attribute name="NAME" x="329.184" y="58.166" size="1.778" layer="95"/>
<attribute name="VALUE" x="318.262" y="53.594" size="1.778" layer="96"/>
</instance>
<instance part="CHASSIS12" gate="G$1" x="340.36" y="50.8"/>
<instance part="GND33" gate="1" x="320.04" y="50.8"/>
<instance part="CHASSIS13" gate="G$1" x="381" y="60.96"/>
<instance part="CHASSIS14" gate="G$1" x="381" y="88.9" rot="R180"/>
<instance part="L10" gate="G$1" x="330.2" y="93.98" smashed="yes" rot="MR90">
<attribute name="NAME" x="329.184" y="96.266" size="1.778" layer="95"/>
<attribute name="VALUE" x="318.262" y="91.694" size="1.778" layer="96"/>
</instance>
<instance part="CON1" gate="G$1" x="381" y="127" rot="MR270"/>
<instance part="IC2" gate="G$1" x="340.36" y="124.46" rot="MR90"/>
<instance part="L11" gate="G$1" x="330.2" y="106.68" smashed="yes" rot="MR90">
<attribute name="NAME" x="329.184" y="108.966" size="1.778" layer="95"/>
<attribute name="VALUE" x="318.262" y="104.394" size="1.778" layer="96"/>
</instance>
<instance part="CHASSIS9" gate="G$1" x="340.36" y="101.6"/>
<instance part="GND34" gate="1" x="320.04" y="101.6"/>
<instance part="CHASSIS10" gate="G$1" x="381" y="111.76"/>
<instance part="CHASSIS11" gate="G$1" x="381" y="139.7" rot="R180"/>
<instance part="L13" gate="G$1" x="330.2" y="144.78" smashed="yes" rot="MR90">
<attribute name="NAME" x="329.184" y="147.066" size="1.778" layer="95"/>
<attribute name="VALUE" x="318.262" y="142.494" size="1.778" layer="96"/>
</instance>
<instance part="CON3" gate="G$1" x="381" y="177.8" rot="MR270"/>
<instance part="IC3" gate="G$1" x="340.36" y="175.26" rot="MR90"/>
<instance part="L14" gate="G$1" x="330.2" y="157.48" smashed="yes" rot="MR90">
<attribute name="NAME" x="329.184" y="159.766" size="1.778" layer="95"/>
<attribute name="VALUE" x="318.262" y="155.194" size="1.778" layer="96"/>
</instance>
<instance part="CHASSIS15" gate="G$1" x="340.36" y="152.4"/>
<instance part="GND35" gate="1" x="320.04" y="152.4"/>
<instance part="CHASSIS16" gate="G$1" x="381" y="162.56"/>
<instance part="CHASSIS17" gate="G$1" x="381" y="190.5" rot="R180"/>
<instance part="L15" gate="G$1" x="330.2" y="195.58" smashed="yes" rot="MR90">
<attribute name="NAME" x="329.184" y="197.866" size="1.778" layer="95"/>
<attribute name="VALUE" x="318.262" y="193.294" size="1.778" layer="96"/>
</instance>
<instance part="CON5" gate="G$1" x="381" y="228.6" rot="MR270"/>
<instance part="IC4" gate="G$1" x="340.36" y="226.06" rot="MR90"/>
<instance part="L16" gate="G$1" x="330.2" y="208.28" smashed="yes" rot="MR90">
<attribute name="NAME" x="329.184" y="210.566" size="1.778" layer="95"/>
<attribute name="VALUE" x="318.262" y="205.994" size="1.778" layer="96"/>
</instance>
<instance part="CHASSIS18" gate="G$1" x="340.36" y="203.2"/>
<instance part="GND36" gate="1" x="320.04" y="203.2"/>
<instance part="CHASSIS19" gate="G$1" x="381" y="213.36"/>
<instance part="CHASSIS20" gate="G$1" x="381" y="241.3" rot="R180"/>
<instance part="L17" gate="G$1" x="330.2" y="246.38" smashed="yes" rot="MR90">
<attribute name="NAME" x="329.184" y="248.666" size="1.778" layer="95"/>
<attribute name="VALUE" x="318.262" y="244.094" size="1.778" layer="96"/>
</instance>
<instance part="IC8" gate="G$1" x="144.78" y="88.9"/>
<instance part="GND37" gate="1" x="124.46" y="50.8"/>
<instance part="+3V18" gate="G$3" x="162.56" y="134.62"/>
<instance part="+3V19" gate="G$3" x="124.46" y="134.62"/>
<instance part="R26" gate="G$1" x="119.38" y="73.66" smashed="yes" rot="R180">
<attribute name="NAME" x="117.348" y="70.104" size="1.778" layer="95"/>
<attribute name="VALUE" x="117.602" y="72.771" size="1.6764" layer="96"/>
</instance>
<instance part="R27" gate="G$1" x="111.76" y="76.2" smashed="yes" rot="R180">
<attribute name="NAME" x="109.474" y="71.628" size="1.778" layer="95"/>
<attribute name="VALUE" x="109.22" y="75.311" size="1.6764" layer="96"/>
</instance>
<instance part="C17" gate="G$1" x="119.38" y="60.96" rot="R90"/>
<instance part="C18" gate="G$1" x="109.22" y="60.96" rot="R90"/>
<instance part="C19" gate="G$1" x="96.52" y="60.96" rot="R90"/>
<instance part="C20" gate="G$1" x="86.36" y="60.96" rot="R90"/>
<instance part="C21" gate="G$1" x="119.38" y="127" rot="R90"/>
<instance part="C22" gate="G$1" x="109.22" y="127" rot="R90"/>
<instance part="C23" gate="G$1" x="99.06" y="127" rot="R90"/>
<instance part="C24" gate="G$1" x="88.9" y="127" rot="R90"/>
<instance part="C25" gate="G$1" x="78.74" y="127" rot="R90"/>
<instance part="GND38" gate="1" x="119.38" y="119.38"/>
<instance part="C26" gate="G$1" x="187.96" y="127" rot="R90"/>
<instance part="C28" gate="G$1" x="167.64" y="127" rot="R90"/>
<instance part="C27" gate="G$1" x="177.8" y="127" rot="R90"/>
<instance part="GND39" gate="1" x="177.8" y="119.38"/>
<instance part="R28" gate="G$1" x="60.96" y="111.76" smashed="yes" rot="R180">
<attribute name="NAME" x="55.88" y="116.84" size="1.778" layer="95"/>
<attribute name="VALUE" x="56.134" y="113.665" size="1.27" layer="96"/>
</instance>
<instance part="R29" gate="G$1" x="68.58" y="119.38" smashed="yes" rot="R270">
<attribute name="NAME" x="73.152" y="117.094" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="69.469" y="116.84" size="1.6764" layer="96" rot="R90"/>
</instance>
<instance part="+3V20" gate="G$3" x="68.58" y="129.54"/>
<instance part="C31" gate="G$1" x="190.5" y="198.12" rot="R90"/>
<instance part="C32" gate="G$1" x="210.82" y="198.12" rot="R90"/>
<instance part="C33" gate="G$1" x="231.14" y="198.12" rot="R90"/>
<instance part="C34" gate="G$1" x="251.46" y="198.12" rot="R90"/>
<instance part="GND40" gate="1" x="190.5" y="190.5"/>
<instance part="GND41" gate="1" x="210.82" y="190.5"/>
<instance part="GND42" gate="1" x="231.14" y="190.5"/>
<instance part="GND43" gate="1" x="251.46" y="190.5"/>
<instance part="GND44" gate="1" x="261.62" y="190.5"/>
<instance part="GND45" gate="1" x="241.3" y="190.5"/>
<instance part="GND46" gate="1" x="220.98" y="190.5"/>
<instance part="GND47" gate="1" x="200.66" y="190.5"/>
<instance part="C35" gate="G$1" x="200.66" y="198.12" rot="R90"/>
<instance part="C36" gate="G$1" x="220.98" y="198.12" rot="R90"/>
<instance part="C37" gate="G$1" x="241.3" y="198.12" rot="R90"/>
<instance part="C38" gate="G$1" x="261.62" y="198.12" rot="R90"/>
<instance part="C53" gate="G$1" x="160.02" y="241.3" rot="R90"/>
<instance part="GND57" gate="1" x="160.02" y="233.68"/>
<instance part="FRAME2" gate="G$1" x="0" y="0"/>
<instance part="FRAME2" gate="G$2" x="287.02" y="0"/>
<instance part="LED1" gate="G$1" x="368.3" y="215.9" smashed="yes" rot="R270">
<attribute name="NAME" x="370.84" y="208.28" size="1.778" layer="95"/>
<attribute name="VALUE" x="363.22" y="205.74" size="1.778" layer="96"/>
</instance>
<instance part="LED2" gate="G$1" x="368.3" y="165.1" smashed="yes" rot="R270">
<attribute name="NAME" x="370.84" y="157.48" size="1.778" layer="95"/>
<attribute name="VALUE" x="363.22" y="154.94" size="1.778" layer="96"/>
</instance>
<instance part="LED3" gate="G$1" x="368.3" y="114.3" smashed="yes" rot="R270">
<attribute name="NAME" x="370.84" y="106.68" size="1.778" layer="95"/>
<attribute name="VALUE" x="363.22" y="104.14" size="1.778" layer="96"/>
</instance>
<instance part="LED4" gate="G$1" x="368.3" y="63.5" smashed="yes" rot="R270">
<attribute name="NAME" x="370.84" y="55.88" size="1.778" layer="95"/>
<attribute name="VALUE" x="363.22" y="53.34" size="1.778" layer="96"/>
</instance>
<instance part="R39" gate="G$1" x="368.3" y="238.76" rot="R90"/>
<instance part="R41" gate="G$1" x="368.3" y="187.96" rot="R90"/>
<instance part="R42" gate="G$1" x="368.3" y="137.16" rot="R90"/>
<instance part="R43" gate="G$1" x="368.3" y="86.36" rot="R90"/>
<instance part="XTAL1" gate="G$1" x="99.06" y="83.82" smashed="yes">
<attribute name="NAME" x="101.6" y="91.44" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="101.6" y="77.47" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="GND72" gate="1" x="83.82" y="83.82"/>
<instance part="+3V30" gate="G$3" x="20.32" y="66.04"/>
<instance part="P+28" gate="1" x="20.32" y="55.88"/>
<instance part="P+29" gate="1" x="20.32" y="45.72"/>
<instance part="CHASSIS31" gate="G$1" x="20.32" y="17.78"/>
<instance part="GND80" gate="1" x="20.32" y="30.48"/>
<instance part="L26" gate="G$1" x="320.04" y="76.2" rot="MR0"/>
<instance part="L27" gate="G$1" x="320.04" y="127" rot="MR0"/>
<instance part="L28" gate="G$1" x="320.04" y="177.8" rot="MR0"/>
<instance part="L29" gate="G$1" x="322.58" y="228.6" rot="MR0"/>
<instance part="C30" gate="G$1" x="360.68" y="215.9" rot="R90"/>
<instance part="C39" gate="G$1" x="360.68" y="165.1" rot="R90"/>
<instance part="C40" gate="G$1" x="360.68" y="114.3" rot="R90"/>
<instance part="C41" gate="G$1" x="360.68" y="63.5" rot="R90"/>
<instance part="R12" gate="G$1" x="60.96" y="91.44" smashed="yes" rot="R90">
<attribute name="NAME" x="56.388" y="93.726" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="60.071" y="93.98" size="1.6764" layer="96" rot="R270"/>
</instance>
<instance part="R13" gate="G$1" x="68.58" y="91.44" smashed="yes" rot="R90">
<attribute name="NAME" x="64.008" y="93.726" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="67.691" y="93.98" size="1.6764" layer="96" rot="R270"/>
</instance>
<instance part="R16" gate="G$1" x="76.2" y="91.44" smashed="yes" rot="R90">
<attribute name="NAME" x="71.628" y="93.726" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="75.311" y="93.98" size="1.6764" layer="96" rot="R270"/>
</instance>
<instance part="GND82" gate="1" x="76.2" y="83.82"/>
<instance part="GND81" gate="1" x="68.58" y="83.82"/>
<instance part="GND83" gate="1" x="60.96" y="83.82"/>
<instance part="R17" gate="G$1" x="124.46" y="238.76" smashed="yes" rot="R90">
<attribute name="NAME" x="119.888" y="241.046" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="123.571" y="241.3" size="1.6764" layer="96" rot="R270"/>
</instance>
<instance part="R48" gate="G$1" x="132.08" y="238.76" smashed="yes" rot="R90">
<attribute name="NAME" x="127.508" y="241.046" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="131.191" y="241.3" size="1.6764" layer="96" rot="R270"/>
</instance>
<instance part="R49" gate="G$1" x="139.7" y="238.76" smashed="yes" rot="R90">
<attribute name="NAME" x="135.128" y="241.046" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="138.811" y="241.3" size="1.6764" layer="96" rot="R270"/>
</instance>
<instance part="R50" gate="G$1" x="147.32" y="238.76" smashed="yes" rot="R90">
<attribute name="NAME" x="142.748" y="241.046" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="146.431" y="241.3" size="1.6764" layer="96" rot="R270"/>
</instance>
<instance part="+3V32" gate="G$3" x="124.46" y="248.92"/>
<instance part="R56" gate="G$1" x="203.2" y="48.26" smashed="yes" rot="R90">
<attribute name="NAME" x="198.628" y="50.546" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="202.311" y="50.8" size="1.6764" layer="96" rot="R270"/>
</instance>
<instance part="R57" gate="G$1" x="215.9" y="48.26" smashed="yes" rot="R90">
<attribute name="NAME" x="211.328" y="50.546" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="215.011" y="50.8" size="1.6764" layer="96" rot="R270"/>
</instance>
<instance part="R58" gate="G$1" x="228.6" y="48.26" smashed="yes" rot="R90">
<attribute name="NAME" x="224.028" y="50.546" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="227.711" y="50.8" size="1.6764" layer="96" rot="R270"/>
</instance>
<instance part="R59" gate="G$1" x="241.3" y="48.26" smashed="yes" rot="R90">
<attribute name="NAME" x="236.728" y="50.546" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="240.411" y="50.8" size="1.6764" layer="96" rot="R270"/>
</instance>
<instance part="GND92" gate="1" x="203.2" y="38.1"/>
<instance part="GND93" gate="1" x="215.9" y="38.1"/>
<instance part="GND94" gate="1" x="228.6" y="38.1"/>
<instance part="GND95" gate="1" x="241.3" y="38.1"/>
</instances>
<busses>
</busses>
<nets>
<net name="POWER_3V3" class="0">
<segment>
<pinref part="IC8" gate="G$1" pin="VDDA33@3"/>
<pinref part="+3V19" gate="G$3" pin="POWER_3V3"/>
<wire x1="127" y1="114.3" x2="124.46" y2="114.3" width="0.1524" layer="91"/>
<wire x1="124.46" y1="114.3" x2="124.46" y2="116.84" width="0.1524" layer="91"/>
<pinref part="IC8" gate="G$1" pin="VDDA33"/>
<wire x1="124.46" y1="116.84" x2="124.46" y2="119.38" width="0.1524" layer="91"/>
<wire x1="124.46" y1="119.38" x2="124.46" y2="121.92" width="0.1524" layer="91"/>
<wire x1="124.46" y1="121.92" x2="124.46" y2="132.08" width="0.1524" layer="91"/>
<wire x1="127" y1="121.92" x2="124.46" y2="121.92" width="0.1524" layer="91"/>
<junction x="124.46" y="121.92"/>
<pinref part="IC8" gate="G$1" pin="VDDA33@1"/>
<wire x1="127" y1="119.38" x2="124.46" y2="119.38" width="0.1524" layer="91"/>
<junction x="124.46" y="119.38"/>
<pinref part="IC8" gate="G$1" pin="VDDA33@2"/>
<wire x1="127" y1="116.84" x2="124.46" y2="116.84" width="0.1524" layer="91"/>
<junction x="124.46" y="116.84"/>
<pinref part="C21" gate="G$1" pin="2"/>
<wire x1="119.38" y1="132.08" x2="119.38" y2="129.54" width="0.1524" layer="91"/>
<pinref part="C22" gate="G$1" pin="2"/>
<wire x1="119.38" y1="132.08" x2="109.22" y2="132.08" width="0.1524" layer="91"/>
<wire x1="109.22" y1="132.08" x2="109.22" y2="129.54" width="0.1524" layer="91"/>
<junction x="119.38" y="132.08"/>
<pinref part="C23" gate="G$1" pin="2"/>
<wire x1="109.22" y1="132.08" x2="99.06" y2="132.08" width="0.1524" layer="91"/>
<wire x1="99.06" y1="132.08" x2="99.06" y2="129.54" width="0.1524" layer="91"/>
<junction x="109.22" y="132.08"/>
<pinref part="C24" gate="G$1" pin="2"/>
<wire x1="99.06" y1="132.08" x2="88.9" y2="132.08" width="0.1524" layer="91"/>
<wire x1="88.9" y1="132.08" x2="88.9" y2="129.54" width="0.1524" layer="91"/>
<junction x="99.06" y="132.08"/>
<pinref part="C25" gate="G$1" pin="2"/>
<wire x1="88.9" y1="132.08" x2="78.74" y2="132.08" width="0.1524" layer="91"/>
<wire x1="78.74" y1="132.08" x2="78.74" y2="129.54" width="0.1524" layer="91"/>
<junction x="88.9" y="132.08"/>
<wire x1="119.38" y1="132.08" x2="124.46" y2="132.08" width="0.1524" layer="91"/>
<junction x="124.46" y="132.08"/>
</segment>
<segment>
<pinref part="+3V18" gate="G$3" pin="POWER_3V3"/>
<pinref part="C28" gate="G$1" pin="2"/>
<wire x1="162.56" y1="132.08" x2="167.64" y2="132.08" width="0.1524" layer="91"/>
<wire x1="167.64" y1="132.08" x2="167.64" y2="129.54" width="0.1524" layer="91"/>
<pinref part="C27" gate="G$1" pin="2"/>
<wire x1="167.64" y1="132.08" x2="177.8" y2="132.08" width="0.1524" layer="91"/>
<wire x1="177.8" y1="132.08" x2="177.8" y2="129.54" width="0.1524" layer="91"/>
<junction x="167.64" y="132.08"/>
<pinref part="C26" gate="G$1" pin="2"/>
<wire x1="177.8" y1="132.08" x2="187.96" y2="132.08" width="0.1524" layer="91"/>
<wire x1="187.96" y1="132.08" x2="187.96" y2="129.54" width="0.1524" layer="91"/>
<junction x="177.8" y="132.08"/>
<pinref part="IC8" gate="G$1" pin="VDD33@2"/>
<wire x1="160.02" y1="119.38" x2="162.56" y2="119.38" width="0.1524" layer="91"/>
<wire x1="162.56" y1="119.38" x2="162.56" y2="121.92" width="0.1524" layer="91"/>
<pinref part="IC8" gate="G$1" pin="VDD33@3"/>
<wire x1="160.02" y1="121.92" x2="162.56" y2="121.92" width="0.1524" layer="91"/>
<wire x1="162.56" y1="121.92" x2="162.56" y2="132.08" width="0.1524" layer="91"/>
<junction x="162.56" y="121.92"/>
<junction x="162.56" y="132.08"/>
</segment>
<segment>
<pinref part="R29" gate="G$1" pin="1"/>
<wire x1="68.58" y1="124.46" x2="68.58" y2="127" width="0.1524" layer="91"/>
<pinref part="+3V20" gate="G$3" pin="POWER_3V3"/>
</segment>
<segment>
<pinref part="+3V30" gate="G$3" pin="POWER_3V3"/>
<wire x1="20.32" y1="63.5" x2="20.32" y2="60.96" width="0.1524" layer="91"/>
<wire x1="20.32" y1="60.96" x2="27.94" y2="60.96" width="0.1524" layer="91"/>
<label x="27.94" y="60.96" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R17" gate="G$1" pin="2"/>
<wire x1="124.46" y1="243.84" x2="124.46" y2="246.38" width="0.1524" layer="91"/>
<wire x1="124.46" y1="246.38" x2="132.08" y2="246.38" width="0.1524" layer="91"/>
<pinref part="R48" gate="G$1" pin="2"/>
<wire x1="132.08" y1="246.38" x2="132.08" y2="243.84" width="0.1524" layer="91"/>
<junction x="132.08" y="246.38"/>
<wire x1="132.08" y1="246.38" x2="139.7" y2="246.38" width="0.1524" layer="91"/>
<pinref part="R49" gate="G$1" pin="2"/>
<wire x1="139.7" y1="246.38" x2="139.7" y2="243.84" width="0.1524" layer="91"/>
<wire x1="139.7" y1="246.38" x2="147.32" y2="246.38" width="0.1524" layer="91"/>
<junction x="139.7" y="246.38"/>
<pinref part="R50" gate="G$1" pin="2"/>
<wire x1="147.32" y1="246.38" x2="147.32" y2="243.84" width="0.1524" layer="91"/>
<pinref part="+3V32" gate="G$3" pin="POWER_3V3"/>
<junction x="124.46" y="246.38"/>
</segment>
</net>
<net name="POWER_GND" class="0">
<segment>
<pinref part="IC6" gate="G$1" pin="GND@2"/>
<pinref part="GND32" gate="1" pin="GND"/>
<wire x1="167.64" y1="190.5" x2="167.64" y2="193.04" width="0.1524" layer="91"/>
<wire x1="167.64" y1="193.04" x2="167.64" y2="195.58" width="0.1524" layer="91"/>
<wire x1="167.64" y1="193.04" x2="165.1" y2="193.04" width="0.1524" layer="91"/>
<junction x="167.64" y="193.04"/>
<pinref part="IC6" gate="G$1" pin="GND@1"/>
<wire x1="165.1" y1="193.04" x2="165.1" y2="195.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="L12" gate="G$1" pin="2"/>
<wire x1="322.58" y1="55.88" x2="320.04" y2="55.88" width="0.1524" layer="91"/>
<pinref part="GND33" gate="1" pin="GND"/>
<wire x1="320.04" y1="55.88" x2="320.04" y2="53.34" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="L11" gate="G$1" pin="2"/>
<wire x1="322.58" y1="106.68" x2="320.04" y2="106.68" width="0.1524" layer="91"/>
<pinref part="GND34" gate="1" pin="GND"/>
<wire x1="320.04" y1="106.68" x2="320.04" y2="104.14" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="L14" gate="G$1" pin="2"/>
<wire x1="322.58" y1="157.48" x2="320.04" y2="157.48" width="0.1524" layer="91"/>
<pinref part="GND35" gate="1" pin="GND"/>
<wire x1="320.04" y1="157.48" x2="320.04" y2="154.94" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="L16" gate="G$1" pin="2"/>
<wire x1="322.58" y1="208.28" x2="320.04" y2="208.28" width="0.1524" layer="91"/>
<pinref part="GND36" gate="1" pin="GND"/>
<wire x1="320.04" y1="208.28" x2="320.04" y2="205.74" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC8" gate="G$1" pin="VSS"/>
<wire x1="127" y1="55.88" x2="124.46" y2="55.88" width="0.1524" layer="91"/>
<pinref part="GND37" gate="1" pin="GND"/>
<wire x1="124.46" y1="55.88" x2="124.46" y2="53.34" width="0.1524" layer="91"/>
<pinref part="IC8" gate="G$1" pin="TEST"/>
<wire x1="127" y1="63.5" x2="124.46" y2="63.5" width="0.1524" layer="91"/>
<wire x1="124.46" y1="63.5" x2="124.46" y2="55.88" width="0.1524" layer="91"/>
<junction x="124.46" y="55.88"/>
<pinref part="C17" gate="G$1" pin="1"/>
<wire x1="119.38" y1="58.42" x2="119.38" y2="55.88" width="0.1524" layer="91"/>
<wire x1="119.38" y1="55.88" x2="109.22" y2="55.88" width="0.1524" layer="91"/>
<pinref part="C20" gate="G$1" pin="1"/>
<wire x1="109.22" y1="55.88" x2="104.14" y2="55.88" width="0.1524" layer="91"/>
<wire x1="104.14" y1="55.88" x2="96.52" y2="55.88" width="0.1524" layer="91"/>
<wire x1="96.52" y1="55.88" x2="86.36" y2="55.88" width="0.1524" layer="91"/>
<wire x1="86.36" y1="55.88" x2="86.36" y2="58.42" width="0.1524" layer="91"/>
<pinref part="C19" gate="G$1" pin="1"/>
<wire x1="96.52" y1="55.88" x2="96.52" y2="58.42" width="0.1524" layer="91"/>
<junction x="96.52" y="55.88"/>
<pinref part="C18" gate="G$1" pin="1"/>
<wire x1="109.22" y1="55.88" x2="109.22" y2="58.42" width="0.1524" layer="91"/>
<junction x="109.22" y="55.88"/>
<wire x1="119.38" y1="55.88" x2="124.46" y2="55.88" width="0.1524" layer="91"/>
<junction x="119.38" y="55.88"/>
<pinref part="R26" gate="G$1" pin="2"/>
<wire x1="114.3" y1="73.66" x2="104.14" y2="73.66" width="0.1524" layer="91"/>
<wire x1="104.14" y1="73.66" x2="104.14" y2="55.88" width="0.1524" layer="91"/>
<junction x="104.14" y="55.88"/>
<pinref part="R27" gate="G$1" pin="2"/>
<wire x1="106.68" y1="76.2" x2="104.14" y2="76.2" width="0.1524" layer="91"/>
<wire x1="104.14" y1="76.2" x2="104.14" y2="73.66" width="0.1524" layer="91"/>
<junction x="104.14" y="73.66"/>
</segment>
<segment>
<pinref part="C25" gate="G$1" pin="1"/>
<wire x1="78.74" y1="124.46" x2="78.74" y2="121.92" width="0.1524" layer="91"/>
<pinref part="C24" gate="G$1" pin="1"/>
<wire x1="78.74" y1="121.92" x2="88.9" y2="121.92" width="0.1524" layer="91"/>
<wire x1="88.9" y1="121.92" x2="88.9" y2="124.46" width="0.1524" layer="91"/>
<pinref part="C23" gate="G$1" pin="1"/>
<wire x1="88.9" y1="121.92" x2="99.06" y2="121.92" width="0.1524" layer="91"/>
<wire x1="99.06" y1="121.92" x2="99.06" y2="124.46" width="0.1524" layer="91"/>
<junction x="88.9" y="121.92"/>
<pinref part="C22" gate="G$1" pin="1"/>
<wire x1="99.06" y1="121.92" x2="109.22" y2="121.92" width="0.1524" layer="91"/>
<wire x1="109.22" y1="121.92" x2="109.22" y2="124.46" width="0.1524" layer="91"/>
<junction x="99.06" y="121.92"/>
<pinref part="C21" gate="G$1" pin="1"/>
<wire x1="109.22" y1="121.92" x2="119.38" y2="121.92" width="0.1524" layer="91"/>
<wire x1="119.38" y1="121.92" x2="119.38" y2="124.46" width="0.1524" layer="91"/>
<junction x="109.22" y="121.92"/>
<pinref part="GND38" gate="1" pin="GND"/>
<junction x="119.38" y="121.92"/>
</segment>
<segment>
<pinref part="C28" gate="G$1" pin="1"/>
<wire x1="167.64" y1="124.46" x2="167.64" y2="121.92" width="0.1524" layer="91"/>
<wire x1="167.64" y1="121.92" x2="177.8" y2="121.92" width="0.1524" layer="91"/>
<pinref part="C27" gate="G$1" pin="1"/>
<pinref part="C26" gate="G$1" pin="1"/>
<wire x1="177.8" y1="121.92" x2="187.96" y2="121.92" width="0.1524" layer="91"/>
<wire x1="187.96" y1="121.92" x2="187.96" y2="124.46" width="0.1524" layer="91"/>
<junction x="177.8" y="121.92"/>
<pinref part="GND39" gate="1" pin="GND"/>
<wire x1="177.8" y1="121.92" x2="177.8" y2="124.46" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C31" gate="G$1" pin="1"/>
<pinref part="GND40" gate="1" pin="GND"/>
<wire x1="190.5" y1="193.04" x2="190.5" y2="195.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C32" gate="G$1" pin="1"/>
<pinref part="GND41" gate="1" pin="GND"/>
<wire x1="210.82" y1="193.04" x2="210.82" y2="195.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C33" gate="G$1" pin="1"/>
<pinref part="GND42" gate="1" pin="GND"/>
<wire x1="231.14" y1="193.04" x2="231.14" y2="195.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C34" gate="G$1" pin="1"/>
<pinref part="GND43" gate="1" pin="GND"/>
<wire x1="251.46" y1="193.04" x2="251.46" y2="195.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND44" gate="1" pin="GND"/>
<wire x1="261.62" y1="193.04" x2="261.62" y2="195.58" width="0.1524" layer="91"/>
<pinref part="C38" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="GND45" gate="1" pin="GND"/>
<wire x1="241.3" y1="193.04" x2="241.3" y2="195.58" width="0.1524" layer="91"/>
<pinref part="C37" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="GND46" gate="1" pin="GND"/>
<wire x1="220.98" y1="193.04" x2="220.98" y2="195.58" width="0.1524" layer="91"/>
<pinref part="C36" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="GND47" gate="1" pin="GND"/>
<wire x1="200.66" y1="193.04" x2="200.66" y2="195.58" width="0.1524" layer="91"/>
<pinref part="C35" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="C53" gate="G$1" pin="1"/>
<pinref part="GND57" gate="1" pin="GND"/>
<wire x1="160.02" y1="236.22" x2="160.02" y2="238.76" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="XTAL1" gate="G$1" pin="2"/>
<wire x1="106.68" y1="81.28" x2="109.22" y2="81.28" width="0.1524" layer="91"/>
<wire x1="109.22" y1="81.28" x2="109.22" y2="88.9" width="0.1524" layer="91"/>
<wire x1="109.22" y1="88.9" x2="86.36" y2="88.9" width="0.1524" layer="91"/>
<wire x1="86.36" y1="88.9" x2="86.36" y2="86.36" width="0.1524" layer="91"/>
<pinref part="XTAL1" gate="G$1" pin="4"/>
<wire x1="86.36" y1="86.36" x2="83.82" y2="86.36" width="0.1524" layer="91"/>
<wire x1="91.44" y1="86.36" x2="86.36" y2="86.36" width="0.1524" layer="91"/>
<junction x="86.36" y="86.36"/>
<pinref part="GND72" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND80" gate="1" pin="GND"/>
<wire x1="20.32" y1="33.02" x2="20.32" y2="35.56" width="0.1524" layer="91"/>
<wire x1="20.32" y1="35.56" x2="27.94" y2="35.56" width="0.1524" layer="91"/>
<label x="27.94" y="35.56" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R16" gate="G$1" pin="1"/>
<pinref part="GND82" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R13" gate="G$1" pin="1"/>
<pinref part="GND81" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R12" gate="G$1" pin="1"/>
<pinref part="GND83" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="203.2" y1="40.64" x2="203.2" y2="43.18" width="0.1524" layer="91"/>
<pinref part="R56" gate="G$1" pin="1"/>
<pinref part="GND92" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="215.9" y1="40.64" x2="215.9" y2="43.18" width="0.1524" layer="91"/>
<pinref part="R57" gate="G$1" pin="1"/>
<pinref part="GND93" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="228.6" y1="40.64" x2="228.6" y2="43.18" width="0.1524" layer="91"/>
<pinref part="R58" gate="G$1" pin="1"/>
<pinref part="GND94" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="241.3" y1="40.64" x2="241.3" y2="43.18" width="0.1524" layer="91"/>
<pinref part="R59" gate="G$1" pin="1"/>
<pinref part="GND95" gate="1" pin="GND"/>
</segment>
</net>
<net name="POWER_+5V" class="0">
<segment>
<pinref part="IC6" gate="G$1" pin="IN@2"/>
<pinref part="P+9" gate="1" pin="+5V"/>
<wire x1="167.64" y1="248.92" x2="167.64" y2="246.38" width="0.1524" layer="91"/>
<wire x1="167.64" y1="246.38" x2="167.64" y2="233.68" width="0.1524" layer="91"/>
<wire x1="167.64" y1="233.68" x2="167.64" y2="231.14" width="0.1524" layer="91"/>
<wire x1="167.64" y1="233.68" x2="165.1" y2="233.68" width="0.1524" layer="91"/>
<junction x="167.64" y="233.68"/>
<pinref part="IC6" gate="G$1" pin="IN@1"/>
<wire x1="165.1" y1="233.68" x2="165.1" y2="231.14" width="0.1524" layer="91"/>
<pinref part="C53" gate="G$1" pin="2"/>
<wire x1="167.64" y1="246.38" x2="160.02" y2="246.38" width="0.1524" layer="91"/>
<wire x1="160.02" y1="246.38" x2="160.02" y2="243.84" width="0.1524" layer="91"/>
<junction x="167.64" y="246.38"/>
</segment>
<segment>
<pinref part="P+28" gate="1" pin="+5V"/>
<wire x1="20.32" y1="53.34" x2="20.32" y2="50.8" width="0.1524" layer="91"/>
<wire x1="20.32" y1="50.8" x2="27.94" y2="50.8" width="0.1524" layer="91"/>
<label x="27.94" y="50.8" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="CHASSIS" class="0">
<segment>
<pinref part="IC5" gate="G$1" pin="GND"/>
<wire x1="340.36" y1="53.34" x2="340.36" y2="55.88" width="0.1524" layer="91"/>
<pinref part="CHASSIS12" gate="G$1" pin="CHASSIS"/>
<pinref part="L12" gate="G$1" pin="1"/>
<wire x1="340.36" y1="55.88" x2="340.36" y2="58.42" width="0.1524" layer="91"/>
<wire x1="340.36" y1="58.42" x2="340.36" y2="60.96" width="0.1524" layer="91"/>
<wire x1="337.82" y1="55.88" x2="340.36" y2="55.88" width="0.1524" layer="91"/>
<junction x="340.36" y="55.88"/>
<wire x1="353.06" y1="58.42" x2="353.06" y2="71.12" width="0.1524" layer="91"/>
<pinref part="CON7" gate="G$1" pin="4"/>
<wire x1="353.06" y1="71.12" x2="375.92" y2="71.12" width="0.1524" layer="91"/>
<wire x1="340.36" y1="55.88" x2="353.06" y2="55.88" width="0.1524" layer="91"/>
<wire x1="353.06" y1="55.88" x2="353.06" y2="58.42" width="0.1524" layer="91"/>
<wire x1="353.06" y1="55.88" x2="358.14" y2="55.88" width="0.1524" layer="91"/>
<junction x="353.06" y="55.88"/>
<pinref part="LED4" gate="G$1" pin="K"/>
<wire x1="368.3" y1="55.88" x2="368.3" y2="58.42" width="0.1524" layer="91"/>
<wire x1="368.3" y1="55.88" x2="360.68" y2="55.88" width="0.1524" layer="91"/>
<pinref part="C41" gate="G$1" pin="1"/>
<wire x1="360.68" y1="55.88" x2="358.14" y2="55.88" width="0.1524" layer="91"/>
<wire x1="360.68" y1="60.96" x2="360.68" y2="55.88" width="0.1524" layer="91"/>
<junction x="360.68" y="55.88"/>
</segment>
<segment>
<pinref part="CON7" gate="G$1" pin="5"/>
<pinref part="CHASSIS13" gate="G$1" pin="CHASSIS"/>
<wire x1="381" y1="63.5" x2="381" y2="66.04" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="CON7" gate="G$1" pin="6"/>
<pinref part="CHASSIS14" gate="G$1" pin="CHASSIS"/>
<wire x1="381" y1="86.36" x2="381" y2="83.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="GND"/>
<wire x1="340.36" y1="104.14" x2="340.36" y2="106.68" width="0.1524" layer="91"/>
<pinref part="CHASSIS9" gate="G$1" pin="CHASSIS"/>
<pinref part="L11" gate="G$1" pin="1"/>
<wire x1="340.36" y1="106.68" x2="340.36" y2="109.22" width="0.1524" layer="91"/>
<wire x1="340.36" y1="109.22" x2="340.36" y2="111.76" width="0.1524" layer="91"/>
<wire x1="337.82" y1="106.68" x2="340.36" y2="106.68" width="0.1524" layer="91"/>
<junction x="340.36" y="106.68"/>
<wire x1="353.06" y1="109.22" x2="353.06" y2="121.92" width="0.1524" layer="91"/>
<pinref part="CON1" gate="G$1" pin="4"/>
<wire x1="353.06" y1="121.92" x2="375.92" y2="121.92" width="0.1524" layer="91"/>
<wire x1="340.36" y1="106.68" x2="353.06" y2="106.68" width="0.1524" layer="91"/>
<wire x1="353.06" y1="106.68" x2="353.06" y2="109.22" width="0.1524" layer="91"/>
<wire x1="353.06" y1="106.68" x2="358.14" y2="106.68" width="0.1524" layer="91"/>
<junction x="353.06" y="106.68"/>
<pinref part="LED3" gate="G$1" pin="K"/>
<wire x1="368.3" y1="106.68" x2="368.3" y2="109.22" width="0.1524" layer="91"/>
<wire x1="368.3" y1="106.68" x2="360.68" y2="106.68" width="0.1524" layer="91"/>
<pinref part="C40" gate="G$1" pin="1"/>
<wire x1="360.68" y1="106.68" x2="358.14" y2="106.68" width="0.1524" layer="91"/>
<wire x1="360.68" y1="111.76" x2="360.68" y2="106.68" width="0.1524" layer="91"/>
<junction x="360.68" y="106.68"/>
</segment>
<segment>
<pinref part="CON1" gate="G$1" pin="5"/>
<pinref part="CHASSIS10" gate="G$1" pin="CHASSIS"/>
<wire x1="381" y1="114.3" x2="381" y2="116.84" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="CON1" gate="G$1" pin="6"/>
<pinref part="CHASSIS11" gate="G$1" pin="CHASSIS"/>
<wire x1="381" y1="137.16" x2="381" y2="134.62" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC3" gate="G$1" pin="GND"/>
<wire x1="340.36" y1="154.94" x2="340.36" y2="157.48" width="0.1524" layer="91"/>
<pinref part="CHASSIS15" gate="G$1" pin="CHASSIS"/>
<pinref part="L14" gate="G$1" pin="1"/>
<wire x1="340.36" y1="157.48" x2="340.36" y2="160.02" width="0.1524" layer="91"/>
<wire x1="340.36" y1="160.02" x2="340.36" y2="162.56" width="0.1524" layer="91"/>
<wire x1="337.82" y1="157.48" x2="340.36" y2="157.48" width="0.1524" layer="91"/>
<junction x="340.36" y="157.48"/>
<wire x1="353.06" y1="160.02" x2="353.06" y2="172.72" width="0.1524" layer="91"/>
<pinref part="CON3" gate="G$1" pin="4"/>
<wire x1="353.06" y1="172.72" x2="375.92" y2="172.72" width="0.1524" layer="91"/>
<wire x1="340.36" y1="157.48" x2="353.06" y2="157.48" width="0.1524" layer="91"/>
<wire x1="353.06" y1="157.48" x2="353.06" y2="160.02" width="0.1524" layer="91"/>
<wire x1="353.06" y1="157.48" x2="358.14" y2="157.48" width="0.1524" layer="91"/>
<junction x="353.06" y="157.48"/>
<pinref part="LED2" gate="G$1" pin="K"/>
<wire x1="368.3" y1="157.48" x2="368.3" y2="160.02" width="0.1524" layer="91"/>
<wire x1="358.14" y1="157.48" x2="360.68" y2="157.48" width="0.1524" layer="91"/>
<pinref part="C39" gate="G$1" pin="1"/>
<wire x1="360.68" y1="157.48" x2="368.3" y2="157.48" width="0.1524" layer="91"/>
<wire x1="360.68" y1="162.56" x2="360.68" y2="157.48" width="0.1524" layer="91"/>
<junction x="360.68" y="157.48"/>
</segment>
<segment>
<pinref part="CON3" gate="G$1" pin="5"/>
<pinref part="CHASSIS16" gate="G$1" pin="CHASSIS"/>
<wire x1="381" y1="165.1" x2="381" y2="167.64" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="CON3" gate="G$1" pin="6"/>
<pinref part="CHASSIS17" gate="G$1" pin="CHASSIS"/>
<wire x1="381" y1="187.96" x2="381" y2="185.42" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC4" gate="G$1" pin="GND"/>
<wire x1="340.36" y1="205.74" x2="340.36" y2="208.28" width="0.1524" layer="91"/>
<pinref part="CHASSIS18" gate="G$1" pin="CHASSIS"/>
<pinref part="L16" gate="G$1" pin="1"/>
<wire x1="340.36" y1="208.28" x2="340.36" y2="210.82" width="0.1524" layer="91"/>
<wire x1="340.36" y1="210.82" x2="340.36" y2="213.36" width="0.1524" layer="91"/>
<wire x1="337.82" y1="208.28" x2="340.36" y2="208.28" width="0.1524" layer="91"/>
<junction x="340.36" y="208.28"/>
<wire x1="353.06" y1="210.82" x2="353.06" y2="223.52" width="0.1524" layer="91"/>
<pinref part="CON5" gate="G$1" pin="4"/>
<wire x1="353.06" y1="223.52" x2="375.92" y2="223.52" width="0.1524" layer="91"/>
<wire x1="340.36" y1="208.28" x2="353.06" y2="208.28" width="0.1524" layer="91"/>
<wire x1="353.06" y1="208.28" x2="353.06" y2="210.82" width="0.1524" layer="91"/>
<wire x1="353.06" y1="208.28" x2="358.14" y2="208.28" width="0.1524" layer="91"/>
<junction x="353.06" y="208.28"/>
<pinref part="LED1" gate="G$1" pin="K"/>
<wire x1="368.3" y1="208.28" x2="368.3" y2="210.82" width="0.1524" layer="91"/>
<wire x1="358.14" y1="208.28" x2="360.68" y2="208.28" width="0.1524" layer="91"/>
<pinref part="C30" gate="G$1" pin="1"/>
<wire x1="360.68" y1="208.28" x2="368.3" y2="208.28" width="0.1524" layer="91"/>
<wire x1="360.68" y1="213.36" x2="360.68" y2="208.28" width="0.1524" layer="91"/>
<junction x="360.68" y="208.28"/>
</segment>
<segment>
<pinref part="CON5" gate="G$1" pin="5"/>
<pinref part="CHASSIS19" gate="G$1" pin="CHASSIS"/>
<wire x1="381" y1="215.9" x2="381" y2="218.44" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="CON5" gate="G$1" pin="6"/>
<pinref part="CHASSIS20" gate="G$1" pin="CHASSIS"/>
<wire x1="381" y1="238.76" x2="381" y2="236.22" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="CHASSIS31" gate="G$1" pin="CHASSIS"/>
<wire x1="20.32" y1="20.32" x2="20.32" y2="22.86" width="0.1524" layer="91"/>
<wire x1="20.32" y1="22.86" x2="27.94" y2="22.86" width="0.1524" layer="91"/>
<label x="27.94" y="22.86" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$50" class="0">
<segment>
<pinref part="IC5" gate="G$1" pin="VCC"/>
<wire x1="340.36" y1="88.9" x2="340.36" y2="91.44" width="0.1524" layer="91"/>
<pinref part="CON7" gate="G$1" pin="1"/>
<wire x1="370.84" y1="78.74" x2="375.92" y2="78.74" width="0.1524" layer="91"/>
<wire x1="370.84" y1="78.74" x2="353.06" y2="78.74" width="0.1524" layer="91"/>
<wire x1="353.06" y1="78.74" x2="353.06" y2="91.44" width="0.1524" layer="91"/>
<pinref part="L10" gate="G$1" pin="1"/>
<wire x1="337.82" y1="93.98" x2="340.36" y2="93.98" width="0.1524" layer="91"/>
<wire x1="340.36" y1="93.98" x2="340.36" y2="91.44" width="0.1524" layer="91"/>
<wire x1="340.36" y1="93.98" x2="353.06" y2="93.98" width="0.1524" layer="91"/>
<wire x1="353.06" y1="93.98" x2="353.06" y2="91.44" width="0.1524" layer="91"/>
<junction x="340.36" y="93.98"/>
<wire x1="353.06" y1="93.98" x2="358.14" y2="93.98" width="0.1524" layer="91"/>
<junction x="353.06" y="93.98"/>
<pinref part="R43" gate="G$1" pin="2"/>
<wire x1="358.14" y1="93.98" x2="360.68" y2="93.98" width="0.1524" layer="91"/>
<wire x1="360.68" y1="93.98" x2="368.3" y2="93.98" width="0.1524" layer="91"/>
<wire x1="368.3" y1="93.98" x2="368.3" y2="91.44" width="0.1524" layer="91"/>
<pinref part="C41" gate="G$1" pin="2"/>
<wire x1="360.68" y1="66.04" x2="360.68" y2="93.98" width="0.1524" layer="91"/>
<junction x="360.68" y="93.98"/>
</segment>
</net>
<net name="N$51" class="0">
<segment>
<pinref part="IC5" gate="G$1" pin="CH2OUT"/>
<wire x1="345.44" y1="88.9" x2="350.52" y2="88.9" width="0.1524" layer="91"/>
<wire x1="350.52" y1="88.9" x2="350.52" y2="76.2" width="0.1524" layer="91"/>
<pinref part="CON7" gate="G$1" pin="2"/>
<wire x1="350.52" y1="76.2" x2="375.92" y2="76.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$52" class="0">
<segment>
<pinref part="CON7" gate="G$1" pin="3"/>
<wire x1="375.92" y1="73.66" x2="350.52" y2="73.66" width="0.1524" layer="91"/>
<wire x1="350.52" y1="73.66" x2="350.52" y2="60.96" width="0.1524" layer="91"/>
<pinref part="IC5" gate="G$1" pin="CH1OUT"/>
<wire x1="350.52" y1="60.96" x2="345.44" y2="60.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$53" class="0">
<segment>
<pinref part="IC8" gate="G$1" pin="USBDM_DN4"/>
<wire x1="302.26" y1="60.96" x2="160.02" y2="60.96" width="0.1524" layer="91"/>
<wire x1="302.26" y1="78.74" x2="302.26" y2="60.96" width="0.1524" layer="91"/>
<pinref part="L26" gate="G$1" pin="2"/>
<wire x1="312.42" y1="78.74" x2="302.26" y2="78.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$54" class="0">
<segment>
<pinref part="IC8" gate="G$1" pin="USBDP_DN4"/>
<wire x1="160.02" y1="63.5" x2="304.8" y2="63.5" width="0.1524" layer="91"/>
<wire x1="304.8" y1="63.5" x2="304.8" y2="73.66" width="0.1524" layer="91"/>
<pinref part="L26" gate="G$1" pin="3"/>
<wire x1="304.8" y1="73.66" x2="312.42" y2="73.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$55" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="VCC"/>
<wire x1="340.36" y1="139.7" x2="340.36" y2="142.24" width="0.1524" layer="91"/>
<pinref part="CON1" gate="G$1" pin="1"/>
<wire x1="370.84" y1="129.54" x2="375.92" y2="129.54" width="0.1524" layer="91"/>
<wire x1="370.84" y1="129.54" x2="353.06" y2="129.54" width="0.1524" layer="91"/>
<wire x1="353.06" y1="129.54" x2="353.06" y2="142.24" width="0.1524" layer="91"/>
<pinref part="L13" gate="G$1" pin="1"/>
<wire x1="337.82" y1="144.78" x2="340.36" y2="144.78" width="0.1524" layer="91"/>
<wire x1="340.36" y1="144.78" x2="340.36" y2="142.24" width="0.1524" layer="91"/>
<wire x1="340.36" y1="144.78" x2="353.06" y2="144.78" width="0.1524" layer="91"/>
<wire x1="353.06" y1="144.78" x2="353.06" y2="142.24" width="0.1524" layer="91"/>
<junction x="340.36" y="144.78"/>
<wire x1="353.06" y1="144.78" x2="358.14" y2="144.78" width="0.1524" layer="91"/>
<junction x="353.06" y="144.78"/>
<pinref part="R42" gate="G$1" pin="2"/>
<wire x1="358.14" y1="144.78" x2="360.68" y2="144.78" width="0.1524" layer="91"/>
<wire x1="360.68" y1="144.78" x2="368.3" y2="144.78" width="0.1524" layer="91"/>
<wire x1="368.3" y1="144.78" x2="368.3" y2="142.24" width="0.1524" layer="91"/>
<pinref part="C40" gate="G$1" pin="2"/>
<wire x1="360.68" y1="116.84" x2="360.68" y2="144.78" width="0.1524" layer="91"/>
<junction x="360.68" y="144.78"/>
</segment>
</net>
<net name="N$56" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="CH2OUT"/>
<wire x1="345.44" y1="139.7" x2="350.52" y2="139.7" width="0.1524" layer="91"/>
<wire x1="350.52" y1="139.7" x2="350.52" y2="127" width="0.1524" layer="91"/>
<pinref part="CON1" gate="G$1" pin="2"/>
<wire x1="350.52" y1="127" x2="375.92" y2="127" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$57" class="0">
<segment>
<pinref part="CON1" gate="G$1" pin="3"/>
<wire x1="375.92" y1="124.46" x2="350.52" y2="124.46" width="0.1524" layer="91"/>
<wire x1="350.52" y1="124.46" x2="350.52" y2="111.76" width="0.1524" layer="91"/>
<pinref part="IC2" gate="G$1" pin="CH1OUT"/>
<wire x1="350.52" y1="111.76" x2="345.44" y2="111.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$58" class="0">
<segment>
<pinref part="IC8" gate="G$1" pin="USBDM_DN3"/>
<wire x1="292.1" y1="129.54" x2="292.1" y2="76.2" width="0.1524" layer="91"/>
<wire x1="292.1" y1="76.2" x2="160.02" y2="76.2" width="0.1524" layer="91"/>
<pinref part="L27" gate="G$1" pin="2"/>
<wire x1="312.42" y1="129.54" x2="292.1" y2="129.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$59" class="0">
<segment>
<pinref part="IC8" gate="G$1" pin="USBDP_DN3"/>
<wire x1="294.64" y1="124.46" x2="312.42" y2="124.46" width="0.1524" layer="91"/>
<wire x1="160.02" y1="78.74" x2="294.64" y2="78.74" width="0.1524" layer="91"/>
<wire x1="294.64" y1="78.74" x2="294.64" y2="124.46" width="0.1524" layer="91"/>
<pinref part="L27" gate="G$1" pin="3"/>
</segment>
</net>
<net name="N$60" class="0">
<segment>
<pinref part="IC3" gate="G$1" pin="VCC"/>
<wire x1="340.36" y1="190.5" x2="340.36" y2="193.04" width="0.1524" layer="91"/>
<pinref part="CON3" gate="G$1" pin="1"/>
<wire x1="370.84" y1="180.34" x2="375.92" y2="180.34" width="0.1524" layer="91"/>
<wire x1="370.84" y1="180.34" x2="353.06" y2="180.34" width="0.1524" layer="91"/>
<wire x1="353.06" y1="180.34" x2="353.06" y2="193.04" width="0.1524" layer="91"/>
<pinref part="L15" gate="G$1" pin="1"/>
<wire x1="337.82" y1="195.58" x2="340.36" y2="195.58" width="0.1524" layer="91"/>
<wire x1="340.36" y1="195.58" x2="340.36" y2="193.04" width="0.1524" layer="91"/>
<wire x1="340.36" y1="195.58" x2="353.06" y2="195.58" width="0.1524" layer="91"/>
<wire x1="353.06" y1="195.58" x2="353.06" y2="193.04" width="0.1524" layer="91"/>
<junction x="340.36" y="195.58"/>
<wire x1="353.06" y1="195.58" x2="358.14" y2="195.58" width="0.1524" layer="91"/>
<junction x="353.06" y="195.58"/>
<pinref part="R41" gate="G$1" pin="2"/>
<wire x1="358.14" y1="195.58" x2="360.68" y2="195.58" width="0.1524" layer="91"/>
<wire x1="360.68" y1="195.58" x2="368.3" y2="195.58" width="0.1524" layer="91"/>
<wire x1="368.3" y1="195.58" x2="368.3" y2="193.04" width="0.1524" layer="91"/>
<pinref part="C39" gate="G$1" pin="2"/>
<wire x1="360.68" y1="167.64" x2="360.68" y2="195.58" width="0.1524" layer="91"/>
<junction x="360.68" y="195.58"/>
</segment>
</net>
<net name="N$61" class="0">
<segment>
<pinref part="IC3" gate="G$1" pin="CH2OUT"/>
<wire x1="345.44" y1="190.5" x2="350.52" y2="190.5" width="0.1524" layer="91"/>
<wire x1="350.52" y1="190.5" x2="350.52" y2="177.8" width="0.1524" layer="91"/>
<pinref part="CON3" gate="G$1" pin="2"/>
<wire x1="350.52" y1="177.8" x2="375.92" y2="177.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$62" class="0">
<segment>
<pinref part="CON3" gate="G$1" pin="3"/>
<wire x1="375.92" y1="175.26" x2="350.52" y2="175.26" width="0.1524" layer="91"/>
<wire x1="350.52" y1="175.26" x2="350.52" y2="162.56" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="CH1OUT"/>
<wire x1="350.52" y1="162.56" x2="345.44" y2="162.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$63" class="0">
<segment>
<pinref part="IC8" gate="G$1" pin="USBDM_DN2"/>
<wire x1="160.02" y1="91.44" x2="281.94" y2="91.44" width="0.1524" layer="91"/>
<wire x1="281.94" y1="180.34" x2="281.94" y2="91.44" width="0.1524" layer="91"/>
<pinref part="L28" gate="G$1" pin="2"/>
<wire x1="312.42" y1="180.34" x2="281.94" y2="180.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$64" class="0">
<segment>
<pinref part="IC8" gate="G$1" pin="USBDP_DN2"/>
<wire x1="160.02" y1="93.98" x2="284.48" y2="93.98" width="0.1524" layer="91"/>
<wire x1="284.48" y1="93.98" x2="284.48" y2="175.26" width="0.1524" layer="91"/>
<pinref part="L28" gate="G$1" pin="3"/>
<wire x1="312.42" y1="175.26" x2="284.48" y2="175.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$65" class="0">
<segment>
<pinref part="IC4" gate="G$1" pin="VCC"/>
<wire x1="340.36" y1="241.3" x2="340.36" y2="243.84" width="0.1524" layer="91"/>
<pinref part="CON5" gate="G$1" pin="1"/>
<wire x1="370.84" y1="231.14" x2="375.92" y2="231.14" width="0.1524" layer="91"/>
<wire x1="370.84" y1="231.14" x2="353.06" y2="231.14" width="0.1524" layer="91"/>
<wire x1="353.06" y1="231.14" x2="353.06" y2="243.84" width="0.1524" layer="91"/>
<pinref part="L17" gate="G$1" pin="1"/>
<wire x1="337.82" y1="246.38" x2="340.36" y2="246.38" width="0.1524" layer="91"/>
<wire x1="340.36" y1="246.38" x2="340.36" y2="243.84" width="0.1524" layer="91"/>
<wire x1="340.36" y1="246.38" x2="353.06" y2="246.38" width="0.1524" layer="91"/>
<wire x1="353.06" y1="246.38" x2="353.06" y2="243.84" width="0.1524" layer="91"/>
<junction x="340.36" y="246.38"/>
<wire x1="353.06" y1="246.38" x2="358.14" y2="246.38" width="0.1524" layer="91"/>
<junction x="353.06" y="246.38"/>
<pinref part="R39" gate="G$1" pin="2"/>
<wire x1="358.14" y1="246.38" x2="360.68" y2="246.38" width="0.1524" layer="91"/>
<wire x1="360.68" y1="246.38" x2="368.3" y2="246.38" width="0.1524" layer="91"/>
<wire x1="368.3" y1="246.38" x2="368.3" y2="243.84" width="0.1524" layer="91"/>
<pinref part="C30" gate="G$1" pin="2"/>
<wire x1="360.68" y1="218.44" x2="360.68" y2="246.38" width="0.1524" layer="91"/>
<junction x="360.68" y="246.38"/>
</segment>
</net>
<net name="N$66" class="0">
<segment>
<pinref part="IC4" gate="G$1" pin="CH2OUT"/>
<wire x1="345.44" y1="241.3" x2="350.52" y2="241.3" width="0.1524" layer="91"/>
<wire x1="350.52" y1="241.3" x2="350.52" y2="228.6" width="0.1524" layer="91"/>
<pinref part="CON5" gate="G$1" pin="2"/>
<wire x1="350.52" y1="228.6" x2="375.92" y2="228.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$67" class="0">
<segment>
<pinref part="CON5" gate="G$1" pin="3"/>
<wire x1="375.92" y1="226.06" x2="350.52" y2="226.06" width="0.1524" layer="91"/>
<wire x1="350.52" y1="226.06" x2="350.52" y2="213.36" width="0.1524" layer="91"/>
<pinref part="IC4" gate="G$1" pin="CH1OUT"/>
<wire x1="350.52" y1="213.36" x2="345.44" y2="213.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$68" class="0">
<segment>
<pinref part="IC8" gate="G$1" pin="USBDM_DN1"/>
<wire x1="271.78" y1="106.68" x2="160.02" y2="106.68" width="0.1524" layer="91"/>
<wire x1="271.78" y1="106.68" x2="271.78" y2="231.14" width="0.1524" layer="91"/>
<pinref part="L29" gate="G$1" pin="2"/>
<wire x1="314.96" y1="231.14" x2="271.78" y2="231.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$69" class="0">
<segment>
<pinref part="IC8" gate="G$1" pin="USBDP_DN1"/>
<wire x1="160.02" y1="109.22" x2="274.32" y2="109.22" width="0.1524" layer="91"/>
<wire x1="274.32" y1="109.22" x2="274.32" y2="226.06" width="0.1524" layer="91"/>
<pinref part="L29" gate="G$1" pin="3"/>
<wire x1="314.96" y1="226.06" x2="274.32" y2="226.06" width="0.1524" layer="91"/>
</segment>
</net>
<net name="OUT1" class="0">
<segment>
<pinref part="IC6" gate="G$1" pin="OUT1"/>
<wire x1="180.34" y1="226.06" x2="190.5" y2="226.06" width="0.1524" layer="91"/>
<wire x1="190.5" y1="226.06" x2="200.66" y2="226.06" width="0.1524" layer="91"/>
<pinref part="L17" gate="G$1" pin="2"/>
<pinref part="C31" gate="G$1" pin="2"/>
<wire x1="190.5" y1="200.66" x2="190.5" y2="226.06" width="0.1524" layer="91"/>
<junction x="190.5" y="226.06"/>
<wire x1="200.66" y1="200.66" x2="200.66" y2="226.06" width="0.1524" layer="91"/>
<junction x="200.66" y="226.06"/>
<pinref part="C35" gate="G$1" pin="2"/>
<wire x1="200.66" y1="226.06" x2="200.66" y2="246.38" width="0.1524" layer="91"/>
<wire x1="200.66" y1="246.38" x2="322.58" y2="246.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="OUT2" class="0">
<segment>
<pinref part="IC6" gate="G$1" pin="OUT2"/>
<wire x1="180.34" y1="218.44" x2="210.82" y2="218.44" width="0.1524" layer="91"/>
<wire x1="210.82" y1="218.44" x2="220.98" y2="218.44" width="0.1524" layer="91"/>
<wire x1="307.34" y1="195.58" x2="322.58" y2="195.58" width="0.1524" layer="91"/>
<pinref part="L15" gate="G$1" pin="2"/>
<pinref part="C32" gate="G$1" pin="2"/>
<wire x1="210.82" y1="200.66" x2="210.82" y2="218.44" width="0.1524" layer="91"/>
<junction x="210.82" y="218.44"/>
<wire x1="220.98" y1="200.66" x2="220.98" y2="218.44" width="0.1524" layer="91"/>
<junction x="220.98" y="218.44"/>
<pinref part="C36" gate="G$1" pin="2"/>
<wire x1="220.98" y1="218.44" x2="220.98" y2="243.84" width="0.1524" layer="91"/>
<wire x1="220.98" y1="243.84" x2="307.34" y2="243.84" width="0.1524" layer="91"/>
<wire x1="307.34" y1="243.84" x2="307.34" y2="195.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="OUT3" class="0">
<segment>
<pinref part="L13" gate="G$1" pin="2"/>
<wire x1="322.58" y1="144.78" x2="304.8" y2="144.78" width="0.1524" layer="91"/>
<wire x1="304.8" y1="144.78" x2="304.8" y2="241.3" width="0.1524" layer="91"/>
<pinref part="IC6" gate="G$1" pin="OUT3"/>
<pinref part="C33" gate="G$1" pin="2"/>
<wire x1="241.3" y1="210.82" x2="231.14" y2="210.82" width="0.1524" layer="91"/>
<wire x1="231.14" y1="210.82" x2="180.34" y2="210.82" width="0.1524" layer="91"/>
<wire x1="231.14" y1="200.66" x2="231.14" y2="210.82" width="0.1524" layer="91"/>
<junction x="231.14" y="210.82"/>
<wire x1="241.3" y1="200.66" x2="241.3" y2="210.82" width="0.1524" layer="91"/>
<junction x="241.3" y="210.82"/>
<pinref part="C37" gate="G$1" pin="2"/>
<wire x1="304.8" y1="241.3" x2="241.3" y2="241.3" width="0.1524" layer="91"/>
<wire x1="241.3" y1="241.3" x2="241.3" y2="210.82" width="0.1524" layer="91"/>
</segment>
</net>
<net name="OUT4" class="0">
<segment>
<pinref part="IC6" gate="G$1" pin="OUT4"/>
<wire x1="180.34" y1="203.2" x2="251.46" y2="203.2" width="0.1524" layer="91"/>
<wire x1="251.46" y1="203.2" x2="261.62" y2="203.2" width="0.1524" layer="91"/>
<wire x1="302.26" y1="238.76" x2="302.26" y2="93.98" width="0.1524" layer="91"/>
<pinref part="L10" gate="G$1" pin="2"/>
<wire x1="302.26" y1="93.98" x2="322.58" y2="93.98" width="0.1524" layer="91"/>
<pinref part="C34" gate="G$1" pin="2"/>
<wire x1="251.46" y1="200.66" x2="251.46" y2="203.2" width="0.1524" layer="91"/>
<junction x="251.46" y="203.2"/>
<wire x1="261.62" y1="200.66" x2="261.62" y2="203.2" width="0.1524" layer="91"/>
<junction x="261.62" y="203.2"/>
<pinref part="C38" gate="G$1" pin="2"/>
<wire x1="261.62" y1="203.2" x2="261.62" y2="238.76" width="0.1524" layer="91"/>
<wire x1="261.62" y1="238.76" x2="302.26" y2="238.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$70" class="0">
<segment>
<pinref part="IC8" gate="G$1" pin="PRTPWR1"/>
<wire x1="160.02" y1="104.14" x2="203.2" y2="104.14" width="0.1524" layer="91"/>
<wire x1="203.2" y1="104.14" x2="203.2" y2="142.24" width="0.1524" layer="91"/>
<wire x1="203.2" y1="142.24" x2="121.92" y2="142.24" width="0.1524" layer="91"/>
<pinref part="IC6" gate="G$1" pin="EN1"/>
<wire x1="121.92" y1="142.24" x2="121.92" y2="226.06" width="0.1524" layer="91"/>
<wire x1="121.92" y1="226.06" x2="152.4" y2="226.06" width="0.1524" layer="91"/>
<pinref part="R56" gate="G$1" pin="2"/>
<wire x1="203.2" y1="104.14" x2="203.2" y2="53.34" width="0.1524" layer="91"/>
<junction x="203.2" y="104.14"/>
</segment>
</net>
<net name="N$71" class="0">
<segment>
<pinref part="IC6" gate="G$1" pin="!OC1"/>
<wire x1="152.4" y1="223.52" x2="124.46" y2="223.52" width="0.1524" layer="91"/>
<pinref part="IC8" gate="G$1" pin="OCS_N1"/>
<wire x1="124.46" y1="223.52" x2="124.46" y2="144.78" width="0.1524" layer="91"/>
<wire x1="124.46" y1="144.78" x2="205.74" y2="144.78" width="0.1524" layer="91"/>
<wire x1="205.74" y1="144.78" x2="205.74" y2="101.6" width="0.1524" layer="91"/>
<wire x1="205.74" y1="101.6" x2="160.02" y2="101.6" width="0.1524" layer="91"/>
<pinref part="R17" gate="G$1" pin="1"/>
<wire x1="124.46" y1="233.68" x2="124.46" y2="223.52" width="0.1524" layer="91"/>
<junction x="124.46" y="223.52"/>
</segment>
</net>
<net name="N$72" class="0">
<segment>
<pinref part="IC8" gate="G$1" pin="PRTPWR2"/>
<wire x1="160.02" y1="88.9" x2="215.9" y2="88.9" width="0.1524" layer="91"/>
<wire x1="215.9" y1="88.9" x2="215.9" y2="154.94" width="0.1524" layer="91"/>
<wire x1="215.9" y1="154.94" x2="129.54" y2="154.94" width="0.1524" layer="91"/>
<wire x1="129.54" y1="154.94" x2="129.54" y2="218.44" width="0.1524" layer="91"/>
<pinref part="IC6" gate="G$1" pin="EN2"/>
<wire x1="152.4" y1="218.44" x2="129.54" y2="218.44" width="0.1524" layer="91"/>
<pinref part="R57" gate="G$1" pin="2"/>
<wire x1="215.9" y1="88.9" x2="215.9" y2="53.34" width="0.1524" layer="91"/>
<junction x="215.9" y="88.9"/>
</segment>
</net>
<net name="N$73" class="0">
<segment>
<pinref part="IC6" gate="G$1" pin="!OC2"/>
<wire x1="152.4" y1="215.9" x2="132.08" y2="215.9" width="0.1524" layer="91"/>
<pinref part="IC8" gate="G$1" pin="OCS_N2"/>
<wire x1="132.08" y1="215.9" x2="132.08" y2="157.48" width="0.1524" layer="91"/>
<wire x1="132.08" y1="157.48" x2="218.44" y2="157.48" width="0.1524" layer="91"/>
<wire x1="218.44" y1="157.48" x2="218.44" y2="86.36" width="0.1524" layer="91"/>
<wire x1="218.44" y1="86.36" x2="160.02" y2="86.36" width="0.1524" layer="91"/>
<pinref part="R48" gate="G$1" pin="1"/>
<wire x1="132.08" y1="233.68" x2="132.08" y2="215.9" width="0.1524" layer="91"/>
<junction x="132.08" y="215.9"/>
</segment>
</net>
<net name="N$74" class="0">
<segment>
<pinref part="IC8" gate="G$1" pin="PRTPWR3"/>
<wire x1="160.02" y1="73.66" x2="228.6" y2="73.66" width="0.1524" layer="91"/>
<wire x1="228.6" y1="73.66" x2="228.6" y2="167.64" width="0.1524" layer="91"/>
<wire x1="228.6" y1="167.64" x2="137.16" y2="167.64" width="0.1524" layer="91"/>
<wire x1="137.16" y1="167.64" x2="137.16" y2="210.82" width="0.1524" layer="91"/>
<pinref part="IC6" gate="G$1" pin="EN3"/>
<wire x1="137.16" y1="210.82" x2="152.4" y2="210.82" width="0.1524" layer="91"/>
<pinref part="R58" gate="G$1" pin="2"/>
<wire x1="228.6" y1="73.66" x2="228.6" y2="53.34" width="0.1524" layer="91"/>
<junction x="228.6" y="73.66"/>
</segment>
</net>
<net name="N$75" class="0">
<segment>
<pinref part="IC6" gate="G$1" pin="!OC3"/>
<wire x1="152.4" y1="208.28" x2="139.7" y2="208.28" width="0.1524" layer="91"/>
<wire x1="139.7" y1="208.28" x2="139.7" y2="170.18" width="0.1524" layer="91"/>
<pinref part="IC8" gate="G$1" pin="OCS_N3"/>
<wire x1="139.7" y1="170.18" x2="231.14" y2="170.18" width="0.1524" layer="91"/>
<wire x1="231.14" y1="170.18" x2="231.14" y2="71.12" width="0.1524" layer="91"/>
<wire x1="231.14" y1="71.12" x2="160.02" y2="71.12" width="0.1524" layer="91"/>
<pinref part="R49" gate="G$1" pin="1"/>
<wire x1="139.7" y1="233.68" x2="139.7" y2="208.28" width="0.1524" layer="91"/>
<junction x="139.7" y="208.28"/>
</segment>
</net>
<net name="N$76" class="0">
<segment>
<pinref part="IC8" gate="G$1" pin="PRTPWR4"/>
<wire x1="160.02" y1="58.42" x2="241.3" y2="58.42" width="0.1524" layer="91"/>
<wire x1="241.3" y1="58.42" x2="241.3" y2="180.34" width="0.1524" layer="91"/>
<wire x1="241.3" y1="180.34" x2="144.78" y2="180.34" width="0.1524" layer="91"/>
<pinref part="IC6" gate="G$1" pin="EN4"/>
<wire x1="144.78" y1="203.2" x2="152.4" y2="203.2" width="0.1524" layer="91"/>
<wire x1="144.78" y1="203.2" x2="144.78" y2="180.34" width="0.1524" layer="91"/>
<pinref part="R59" gate="G$1" pin="2"/>
<wire x1="241.3" y1="58.42" x2="241.3" y2="53.34" width="0.1524" layer="91"/>
<junction x="241.3" y="58.42"/>
</segment>
</net>
<net name="N$77" class="0">
<segment>
<pinref part="IC6" gate="G$1" pin="!OC4"/>
<pinref part="IC8" gate="G$1" pin="OCS_N4"/>
<wire x1="147.32" y1="182.88" x2="243.84" y2="182.88" width="0.1524" layer="91"/>
<wire x1="243.84" y1="182.88" x2="243.84" y2="55.88" width="0.1524" layer="91"/>
<wire x1="243.84" y1="55.88" x2="160.02" y2="55.88" width="0.1524" layer="91"/>
<wire x1="152.4" y1="200.66" x2="147.32" y2="200.66" width="0.1524" layer="91"/>
<pinref part="R50" gate="G$1" pin="1"/>
<wire x1="147.32" y1="200.66" x2="147.32" y2="182.88" width="0.1524" layer="91"/>
<wire x1="147.32" y1="233.68" x2="147.32" y2="200.66" width="0.1524" layer="91"/>
<junction x="147.32" y="200.66"/>
</segment>
</net>
<net name="N$82" class="0">
<segment>
<pinref part="IC8" gate="G$1" pin="RBIAS"/>
<pinref part="R26" gate="G$1" pin="1"/>
<wire x1="124.46" y1="73.66" x2="127" y2="73.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$83" class="0">
<segment>
<pinref part="IC8" gate="G$1" pin="SUSP_IND"/>
<pinref part="R27" gate="G$1" pin="1"/>
<wire x1="116.84" y1="76.2" x2="127" y2="76.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$49" class="0">
<segment>
<pinref part="IC8" gate="G$1" pin="CRFILT"/>
<pinref part="C19" gate="G$1" pin="2"/>
<wire x1="127" y1="68.58" x2="96.52" y2="68.58" width="0.1524" layer="91"/>
<wire x1="96.52" y1="68.58" x2="96.52" y2="63.5" width="0.1524" layer="91"/>
<pinref part="C20" gate="G$1" pin="2"/>
<wire x1="96.52" y1="68.58" x2="86.36" y2="68.58" width="0.1524" layer="91"/>
<wire x1="86.36" y1="68.58" x2="86.36" y2="63.5" width="0.1524" layer="91"/>
<junction x="96.52" y="68.58"/>
</segment>
</net>
<net name="N$78" class="0">
<segment>
<pinref part="IC8" gate="G$1" pin="PLLFILT"/>
<pinref part="C18" gate="G$1" pin="2"/>
<wire x1="127" y1="66.04" x2="119.38" y2="66.04" width="0.1524" layer="91"/>
<wire x1="119.38" y1="66.04" x2="109.22" y2="66.04" width="0.1524" layer="91"/>
<wire x1="109.22" y1="66.04" x2="109.22" y2="63.5" width="0.1524" layer="91"/>
<pinref part="C17" gate="G$1" pin="2"/>
<wire x1="119.38" y1="63.5" x2="119.38" y2="66.04" width="0.1524" layer="91"/>
<junction x="119.38" y="66.04"/>
</segment>
</net>
<net name="N$80" class="0">
<segment>
<pinref part="IC8" gate="G$1" pin="USBDP_UP"/>
<wire x1="127" y1="109.22" x2="48.26" y2="109.22" width="0.1524" layer="91"/>
<pinref part="IC1" gate="USB" pin="USBH_P"/>
</segment>
</net>
<net name="N$81" class="0">
<segment>
<pinref part="IC1" gate="USB" pin="USBH_N"/>
<pinref part="IC8" gate="G$1" pin="USBDM_UP"/>
<wire x1="48.26" y1="106.68" x2="127" y2="106.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="!RESET_OUT" class="0">
<segment>
<pinref part="IC8" gate="G$1" pin="!RESET"/>
<wire x1="127" y1="91.44" x2="124.46" y2="91.44" width="0.1524" layer="91"/>
<label x="124.46" y="91.44" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$85" class="0">
<segment>
<pinref part="IC1" gate="USB" pin="GPIO-W.03/USBH_OC"/>
<wire x1="48.26" y1="119.38" x2="53.34" y2="119.38" width="0.1524" layer="91"/>
<wire x1="53.34" y1="119.38" x2="53.34" y2="111.76" width="0.1524" layer="91"/>
<pinref part="R28" gate="G$1" pin="2"/>
<wire x1="53.34" y1="111.76" x2="55.88" y2="111.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$87" class="0">
<segment>
<pinref part="IC8" gate="G$1" pin="VBUS_DET"/>
<pinref part="R28" gate="G$1" pin="1"/>
<wire x1="127" y1="111.76" x2="68.58" y2="111.76" width="0.1524" layer="91"/>
<pinref part="R29" gate="G$1" pin="2"/>
<wire x1="68.58" y1="111.76" x2="66.04" y2="111.76" width="0.1524" layer="91"/>
<wire x1="68.58" y1="114.3" x2="68.58" y2="111.76" width="0.1524" layer="91"/>
<junction x="68.58" y="111.76"/>
</segment>
</net>
<net name="N$99" class="0">
<segment>
<pinref part="R43" gate="G$1" pin="1"/>
<pinref part="LED4" gate="G$1" pin="A"/>
<wire x1="368.3" y1="81.28" x2="368.3" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$100" class="0">
<segment>
<pinref part="R42" gate="G$1" pin="1"/>
<pinref part="LED3" gate="G$1" pin="A"/>
<wire x1="368.3" y1="132.08" x2="368.3" y2="119.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$101" class="0">
<segment>
<pinref part="LED2" gate="G$1" pin="A"/>
<pinref part="R41" gate="G$1" pin="1"/>
<wire x1="368.3" y1="170.18" x2="368.3" y2="182.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$102" class="0">
<segment>
<pinref part="R39" gate="G$1" pin="1"/>
<pinref part="LED1" gate="G$1" pin="A"/>
<wire x1="368.3" y1="233.68" x2="368.3" y2="220.98" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$79" class="0">
<segment>
<pinref part="IC8" gate="G$1" pin="XTALOUT"/>
<wire x1="127" y1="78.74" x2="88.9" y2="78.74" width="0.1524" layer="91"/>
<wire x1="88.9" y1="78.74" x2="88.9" y2="81.28" width="0.1524" layer="91"/>
<pinref part="XTAL1" gate="G$1" pin="1"/>
<wire x1="88.9" y1="81.28" x2="91.44" y2="81.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$84" class="0">
<segment>
<pinref part="XTAL1" gate="G$1" pin="3"/>
<wire x1="106.68" y1="86.36" x2="124.46" y2="86.36" width="0.1524" layer="91"/>
<wire x1="124.46" y1="86.36" x2="124.46" y2="88.9" width="0.1524" layer="91"/>
<pinref part="IC8" gate="G$1" pin="XTALIN"/>
<wire x1="124.46" y1="88.9" x2="127" y2="88.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="POWER_24V" class="0">
<segment>
<pinref part="P+29" gate="1" pin="+24V"/>
<wire x1="20.32" y1="43.18" x2="20.32" y2="40.64" width="0.1524" layer="91"/>
<wire x1="20.32" y1="40.64" x2="27.94" y2="40.64" width="0.1524" layer="91"/>
<label x="27.94" y="40.64" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$90" class="0">
<segment>
<pinref part="IC4" gate="G$1" pin="CH2IN"/>
<wire x1="335.28" y1="241.3" x2="330.2" y2="241.3" width="0.1524" layer="91"/>
<wire x1="330.2" y1="241.3" x2="330.2" y2="231.14" width="0.1524" layer="91"/>
<pinref part="L29" gate="G$1" pin="1"/>
<wire x1="327.66" y1="231.14" x2="330.2" y2="231.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$105" class="0">
<segment>
<pinref part="IC4" gate="G$1" pin="CH1IN"/>
<wire x1="335.28" y1="213.36" x2="330.2" y2="213.36" width="0.1524" layer="91"/>
<wire x1="330.2" y1="213.36" x2="330.2" y2="226.06" width="0.1524" layer="91"/>
<pinref part="L29" gate="G$1" pin="4"/>
<wire x1="327.66" y1="226.06" x2="330.2" y2="226.06" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$107" class="0">
<segment>
<pinref part="IC3" gate="G$1" pin="CH2IN"/>
<wire x1="335.28" y1="190.5" x2="330.2" y2="190.5" width="0.1524" layer="91"/>
<wire x1="330.2" y1="190.5" x2="330.2" y2="180.34" width="0.1524" layer="91"/>
<pinref part="L28" gate="G$1" pin="1"/>
<wire x1="325.12" y1="180.34" x2="330.2" y2="180.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$109" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="CH2IN"/>
<wire x1="335.28" y1="139.7" x2="330.2" y2="139.7" width="0.1524" layer="91"/>
<wire x1="330.2" y1="139.7" x2="330.2" y2="129.54" width="0.1524" layer="91"/>
<pinref part="L27" gate="G$1" pin="1"/>
<wire x1="325.12" y1="129.54" x2="330.2" y2="129.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$111" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="CH1IN"/>
<wire x1="335.28" y1="111.76" x2="330.2" y2="111.76" width="0.1524" layer="91"/>
<wire x1="330.2" y1="111.76" x2="330.2" y2="124.46" width="0.1524" layer="91"/>
<pinref part="L27" gate="G$1" pin="4"/>
<wire x1="325.12" y1="124.46" x2="330.2" y2="124.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$112" class="0">
<segment>
<pinref part="IC5" gate="G$1" pin="CH1IN"/>
<wire x1="335.28" y1="60.96" x2="330.2" y2="60.96" width="0.1524" layer="91"/>
<wire x1="330.2" y1="60.96" x2="330.2" y2="73.66" width="0.1524" layer="91"/>
<pinref part="L26" gate="G$1" pin="4"/>
<wire x1="325.12" y1="73.66" x2="330.2" y2="73.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$113" class="0">
<segment>
<pinref part="IC5" gate="G$1" pin="CH2IN"/>
<wire x1="335.28" y1="88.9" x2="330.2" y2="88.9" width="0.1524" layer="91"/>
<wire x1="330.2" y1="88.9" x2="330.2" y2="78.74" width="0.1524" layer="91"/>
<pinref part="L26" gate="G$1" pin="1"/>
<wire x1="325.12" y1="78.74" x2="330.2" y2="78.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$114" class="0">
<segment>
<pinref part="IC3" gate="G$1" pin="CH1IN"/>
<wire x1="335.28" y1="162.56" x2="330.2" y2="162.56" width="0.1524" layer="91"/>
<wire x1="330.2" y1="162.56" x2="330.2" y2="175.26" width="0.1524" layer="91"/>
<pinref part="L28" gate="G$1" pin="4"/>
<wire x1="325.12" y1="175.26" x2="330.2" y2="175.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$115" class="0">
<segment>
<pinref part="IC8" gate="G$1" pin="SDA"/>
<wire x1="127" y1="101.6" x2="60.96" y2="101.6" width="0.1524" layer="91"/>
<wire x1="60.96" y1="101.6" x2="60.96" y2="96.52" width="0.1524" layer="91"/>
<pinref part="R12" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$116" class="0">
<segment>
<pinref part="IC8" gate="G$1" pin="SCL"/>
<wire x1="127" y1="99.06" x2="68.58" y2="99.06" width="0.1524" layer="91"/>
<wire x1="68.58" y1="99.06" x2="68.58" y2="96.52" width="0.1524" layer="91"/>
<pinref part="R13" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$117" class="0">
<segment>
<pinref part="IC8" gate="G$1" pin="HS_IND"/>
<wire x1="127" y1="96.52" x2="76.2" y2="96.52" width="0.1524" layer="91"/>
<pinref part="R16" gate="G$1" pin="2"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
<errors>
<approved hash="102,1,25.4,154.94,+5V,POWER_+5V,,,,"/>
<approved hash="102,1,25.4,144.78,+24V,POWER_24V,,,,"/>
<approved hash="102,1,25.4,134.62,GND,POWER_GND,,,,"/>
<approved hash="102,2,152.4,60.96,GND,POWER_GND,,,,"/>
<approved hash="102,2,152.4,50.8,GND,POWER_GND,,,,"/>
<approved hash="102,2,152.4,40.64,GND,POWER_GND,,,,"/>
<approved hash="102,2,152.4,30.48,GND,POWER_GND,,,,"/>
<approved hash="102,2,119.38,33.02,GND,POWER_GND,,,,"/>
<approved hash="102,2,12.7,78.74,GND,POWER_GND,,,,"/>
<approved hash="102,2,12.7,7.62,GND,POWER_GND,,,,"/>
<approved hash="102,2,63.5,12.7,GND,POWER_GND,,,,"/>
<approved hash="102,2,63.5,83.82,GND,POWER_GND,,,,"/>
<approved hash="102,2,104.14,114.3,+5V,POWER_+5V,,,,"/>
<approved hash="102,2,99.06,147.32,GND,POWER_GND,,,,"/>
<approved hash="102,2,63.5,139.7,+24V,POWER_24V,,,,"/>
<approved hash="102,2,63.5,68.58,+24V,POWER_24V,,,,"/>
<approved hash="102,2,119.38,73.66,+24V,POWER_24V,,,,"/>
<approved hash="102,2,99.06,170.18,+24V,POWER_24V,,,,"/>
<approved hash="102,2,203.2,165.1,+24V,POWER_24V,,,,"/>
<approved hash="102,2,170.18,129.54,GND,POWER_GND,,,,"/>
<approved hash="102,2,157.48,129.54,GND,POWER_GND,,,,"/>
<approved hash="102,2,109.22,63.5,GND,POWER_GND,,,,"/>
<approved hash="102,2,73.66,63.5,GND,POWER_GND,,,,"/>
<approved hash="102,2,73.66,134.62,GND,POWER_GND,,,,"/>
<approved hash="102,2,218.44,91.44,+24V,POWER_24V,,,,"/>
<approved hash="102,2,218.44,63.5,GND,POWER_GND,,,,"/>
<approved hash="102,2,228.6,63.5,GND,POWER_GND,,,,"/>
<approved hash="102,2,238.76,63.5,GND,POWER_GND,,,,"/>
<approved hash="102,2,248.92,63.5,GND,POWER_GND,,,,"/>
<approved hash="102,2,238.76,91.44,+5V,POWER_+5V,,,,"/>
<approved hash="102,2,213.36,109.22,GND,POWER_GND,,,,"/>
<approved hash="102,2,187.96,76.2,+5V,POWER_+5V,,,,"/>
<approved hash="102,2,187.96,66.04,+24V,POWER_24V,,,,"/>
<approved hash="102,2,187.96,55.88,GND,POWER_GND,,,,"/>
<approved hash="102,2,157.48,165.1,+5V,POWER_+5V,,,,"/>
<approved hash="102,3,205.74,86.36,GND,POWER_GND,,,,"/>
<approved hash="102,3,154.94,81.28,GND,POWER_GND,,,,"/>
<approved hash="102,3,226.06,91.44,GND,POWER_GND,,,,"/>
<approved hash="102,3,236.22,91.44,GND,POWER_GND,,,,"/>
<approved hash="102,3,246.38,91.44,GND,POWER_GND,,,,"/>
<approved hash="102,3,256.54,91.44,GND,POWER_GND,,,,"/>
<approved hash="102,3,121.92,99.06,GND,POWER_GND,,,,"/>
<approved hash="102,3,226.06,116.84,GND,POWER_GND,,,,"/>
<approved hash="102,3,236.22,116.84,GND,POWER_GND,,,,"/>
<approved hash="102,3,246.38,116.84,GND,POWER_GND,,,,"/>
<approved hash="102,3,256.54,116.84,GND,POWER_GND,,,,"/>
<approved hash="102,3,7.62,160.02,+5V,POWER_+5V,,,,"/>
<approved hash="102,3,7.62,149.86,+24V,POWER_24V,,,,"/>
<approved hash="102,3,7.62,139.7,GND,POWER_GND,,,,"/>
<approved hash="102,4,208.28,88.9,VSS1,POWER_GND,,,,"/>
<approved hash="102,4,208.28,86.36,VSS2,POWER_GND,,,,"/>
<approved hash="102,4,220.98,83.82,GND,POWER_GND,,,,"/>
<approved hash="102,4,175.26,63.5,GND,POWER_GND,,,,"/>
<approved hash="102,4,22.86,129.54,GND,POWER_GND,,,,"/>
<approved hash="102,4,228.6,60.96,GND,POWER_GND,,,,"/>
<approved hash="102,4,20.32,53.34,+5V,POWER_+5V,,,,"/>
<approved hash="102,4,20.32,43.18,+24V,POWER_24V,,,,"/>
<approved hash="102,4,20.32,33.02,GND,POWER_GND,,,,"/>
<approved hash="102,5,73.66,88.9,GND,POWER_GND,,,,"/>
<approved hash="102,5,104.14,124.46,GND,POWER_GND,,,,"/>
<approved hash="102,5,93.98,124.46,GND,POWER_GND,,,,"/>
<approved hash="102,5,10.16,157.48,+5V,POWER_5V,,,,"/>
<approved hash="102,5,10.16,147.32,+24V,POWER_24V,,,,"/>
<approved hash="102,5,10.16,137.16,GND,POWER_GND,,,,"/>
<approved hash="102,6,165.1,66.04,GND,POWER_GND,,,,"/>
<approved hash="102,6,93.98,66.04,GND,POWER_GND,,,,"/>
<approved hash="102,6,121.92,66.04,GND,POWER_GND,,,,"/>
<approved hash="102,6,10.16,160.02,+5V,POWER_+5V,,,,"/>
<approved hash="102,6,10.16,149.86,+24V,POWER_24V,,,,"/>
<approved hash="102,6,10.16,139.7,GND,POWER_GND,,,,"/>
<approved hash="102,6,132.08,66.04,GND,POWER_GND,,,,"/>
<approved hash="102,7,124.46,175.26,+5V,POWER_+5V,,,,"/>
<approved hash="102,7,124.46,134.62,GND,POWER_GND,,,,"/>
<approved hash="102,7,129.54,165.1,GND,POWER_GND,,,,"/>
<approved hash="102,7,114.3,134.62,GND,POWER_GND,,,,"/>
<approved hash="102,7,144.78,175.26,+5V,POWER_+5V,,,,"/>
<approved hash="102,7,142.24,134.62,GND,POWER_GND,,,,"/>
<approved hash="102,7,200.66,121.92,GND,POWER_GND,,,,"/>
<approved hash="102,7,208.28,121.92,GND,POWER_GND,,,,"/>
<approved hash="102,7,124.46,96.52,+5V,POWER_+5V,,,,"/>
<approved hash="102,7,124.46,55.88,GND,POWER_GND,,,,"/>
<approved hash="102,7,129.54,86.36,GND,POWER_GND,,,,"/>
<approved hash="102,7,114.3,55.88,GND,POWER_GND,,,,"/>
<approved hash="102,7,144.78,96.52,+5V,POWER_+5V,,,,"/>
<approved hash="102,7,142.24,55.88,GND,POWER_GND,,,,"/>
<approved hash="102,7,200.66,43.18,GND,POWER_GND,,,,"/>
<approved hash="102,7,208.28,43.18,GND,POWER_GND,,,,"/>
<approved hash="102,7,101.6,101.6,GND,POWER_GND,,,,"/>
<approved hash="102,7,12.7,43.18,+5V,POWER_+5V,,,,"/>
<approved hash="102,7,12.7,33.02,+24V,POWER_24V,,,,"/>
<approved hash="102,7,12.7,22.86,GND,POWER_GND,,,,"/>
<approved hash="102,7,48.26,12.7,GND,POWER_GND,,,,"/>
<approved hash="102,7,27.94,149.86,GND,POWER_GND,,,,"/>
<approved hash="102,7,73.66,12.7,GND,POWER_GND,,,,"/>
<approved hash="102,7,78.74,175.26,+5V,POWER_+5V,,,,"/>
<approved hash="102,7,71.12,149.86,GND,POWER_GND,,,,"/>
<approved hash="102,7,35.56,149.86,GND,POWER_GND,,,,"/>
<approved hash="102,7,93.98,170.18,GND,POWER_GND,,,,"/>
<approved hash="102,7,35.56,167.64,GND,POWER_GND,,,,"/>
<approved hash="102,7,71.12,167.64,GND,POWER_GND,,,,"/>
<approved hash="102,8,167.64,248.92,+5V,POWER_+5V,,,,"/>
<approved hash="102,8,167.64,190.5,GND,POWER_GND,,,,"/>
<approved hash="102,8,320.04,53.34,GND,POWER_GND,,,,"/>
<approved hash="102,8,320.04,104.14,GND,POWER_GND,,,,"/>
<approved hash="102,8,320.04,154.94,GND,POWER_GND,,,,"/>
<approved hash="102,8,320.04,205.74,GND,POWER_GND,,,,"/>
<approved hash="102,8,124.46,53.34,GND,POWER_GND,,,,"/>
<approved hash="102,8,119.38,121.92,GND,POWER_GND,,,,"/>
<approved hash="102,8,177.8,121.92,GND,POWER_GND,,,,"/>
<approved hash="102,8,190.5,193.04,GND,POWER_GND,,,,"/>
<approved hash="102,8,210.82,193.04,GND,POWER_GND,,,,"/>
<approved hash="102,8,231.14,193.04,GND,POWER_GND,,,,"/>
<approved hash="102,8,251.46,193.04,GND,POWER_GND,,,,"/>
<approved hash="102,8,261.62,193.04,GND,POWER_GND,,,,"/>
<approved hash="102,8,241.3,193.04,GND,POWER_GND,,,,"/>
<approved hash="102,8,220.98,193.04,GND,POWER_GND,,,,"/>
<approved hash="102,8,200.66,193.04,GND,POWER_GND,,,,"/>
<approved hash="102,8,160.02,236.22,GND,POWER_GND,,,,"/>
<approved hash="102,8,83.82,86.36,GND,POWER_GND,,,,"/>
<approved hash="102,8,20.32,53.34,+5V,POWER_+5V,,,,"/>
<approved hash="102,8,20.32,43.18,+24V,POWER_24V,,,,"/>
<approved hash="102,8,20.32,33.02,GND,POWER_GND,,,,"/>
<approved hash="102,8,76.2,86.36,GND,POWER_GND,,,,"/>
<approved hash="102,8,68.58,86.36,GND,POWER_GND,,,,"/>
<approved hash="102,8,60.96,86.36,GND,POWER_GND,,,,"/>
<approved hash="102,8,203.2,40.64,GND,POWER_GND,,,,"/>
<approved hash="102,8,215.9,40.64,GND,POWER_GND,,,,"/>
<approved hash="102,8,228.6,40.64,GND,POWER_GND,,,,"/>
<approved hash="102,8,241.3,40.64,GND,POWER_GND,,,,"/>
<approved hash="104,8,375.92,78.74,CON7,1,N$50,,,"/>
<approved hash="104,8,375.92,71.12,CON7,4,CHASSIS,,,"/>
<approved hash="104,6,137.16,88.9,IC7,VDD,N$1,,,"/>
<approved hash="104,6,160.02,91.44,IC7,GND,POWER_GND,,,"/>
<approved hash="104,7,121.92,144.78,IC10,GND,POWER_GND,,,"/>
<approved hash="104,7,121.92,160.02,IC10,VCC,POWER_+5V,,,"/>
<approved hash="104,7,121.92,66.04,IC11,GND,POWER_GND,,,"/>
<approved hash="104,7,121.92,81.28,IC11,VCC,POWER_+5V,,,"/>
<approved hash="104,8,340.36,60.96,IC5,GND,CHASSIS,,,"/>
<approved hash="104,8,340.36,88.9,IC5,VCC,N$50,,,"/>
<approved hash="104,8,165.1,195.58,IC6,GND,POWER_GND,,,"/>
<approved hash="104,8,165.1,231.14,IC6,IN,POWER_+5V,,,"/>
<approved hash="104,8,167.64,195.58,IC6,GND,POWER_GND,,,"/>
<approved hash="104,8,167.64,231.14,IC6,IN,POWER_+5V,,,"/>
<approved hash="104,8,375.92,129.54,CON1,1,N$55,,,"/>
<approved hash="104,8,375.92,121.92,CON1,4,CHASSIS,,,"/>
<approved hash="104,8,340.36,111.76,IC2,GND,CHASSIS,,,"/>
<approved hash="104,8,340.36,139.7,IC2,VCC,N$55,,,"/>
<approved hash="104,8,375.92,180.34,CON3,1,N$60,,,"/>
<approved hash="104,8,375.92,172.72,CON3,4,CHASSIS,,,"/>
<approved hash="104,8,340.36,162.56,IC3,GND,CHASSIS,,,"/>
<approved hash="104,8,340.36,190.5,IC3,VCC,N$60,,,"/>
<approved hash="104,8,375.92,231.14,CON5,1,N$65,,,"/>
<approved hash="104,8,375.92,223.52,CON5,4,CHASSIS,,,"/>
<approved hash="104,8,340.36,213.36,IC4,GND,CHASSIS,,,"/>
<approved hash="104,8,340.36,241.3,IC4,VCC,N$65,,,"/>
<approved hash="104,8,127,119.38,IC8,VDDA33,POWER_3V3,,,"/>
<approved hash="104,8,160.02,119.38,IC8,VDD33,POWER_3V3,,,"/>
<approved hash="104,8,160.02,121.92,IC8,VDD33,POWER_3V3,,,"/>
<approved hash="104,8,127,116.84,IC8,VDDA33,POWER_3V3,,,"/>
<approved hash="104,8,127,114.3,IC8,VDDA33,POWER_3V3,,,"/>
<approved hash="104,8,127,121.92,IC8,VDDA33,POWER_3V3,,,"/>
<approved hash="104,8,127,55.88,IC8,VSS,POWER_GND,,,"/>
<approved hash="104,2,20.32,149.86,CON6,1,CHASSIS,,,"/>
<approved hash="104,2,20.32,154.94,CON6,2,CHASSIS,,,"/>
<approved hash="104,2,20.32,160.02,CON6,3,N$86,,,"/>
<approved hash="104,2,17.78,160.02,CON6,4,N$86,,,"/>
<approved hash="104,2,121.92,66.04,IC9_LDO,VIN,POWER_24V,,,"/>
<approved hash="104,2,121.92,38.1,IC9_LDO,GND,POWER_GND,,,"/>
<approved hash="104,2,121.92,35.56,IC9_LDO,PAD,POWER_GND,,,"/>
<approved hash="104,2,40.64,93.98,IC9_5V,PGND,POWER_GND,,,"/>
<approved hash="104,2,40.64,22.86,IC9_3\,3V,PGND,POWER_GND,,,"/>
<approved hash="104,2,180.34,149.86,IC13,V+,POWER_24V,,,"/>
<approved hash="104,2,170.18,134.62,IC13,GND,POWER_GND,,,"/>
<approved hash="104,8,106.68,81.28,XTAL1,2,POWER_GND,,,"/>
<approved hash="104,8,91.44,86.36,XTAL1,4,POWER_GND,,,"/>
<approved hash="104,7,45.72,172.72,IC12,VCCA,POWER_3V3,,,"/>
<approved hash="104,7,60.96,172.72,IC12,VCCB,POWER_+5V,,,"/>
<approved hash="104,7,45.72,170.18,IC12,GND,POWER_GND,,,"/>
<approved hash="104,7,45.72,154.94,IC14,VCCA,POWER_3V3,,,"/>
<approved hash="104,7,60.96,154.94,IC14,VCCB,POWER_+5V,,,"/>
<approved hash="104,7,45.72,152.4,IC14,GND,POWER_GND,,,"/>
<approved hash="104,7,53.34,40.64,IC15,GND,POWER_GND,,,"/>
<approved hash="104,7,68.58,50.8,IC15,VCC,POWER_3V3,,,"/>
<approved hash="104,7,53.34,20.32,IC16,GND,POWER_GND,,,"/>
<approved hash="104,7,68.58,30.48,IC16,VCC,POWER_3V3,,,"/>
<approved hash="105,5,10.16,156.21,POWER_5V,,,,,"/>
<approved hash="106,3,60.96,157.48,USB_HS_IND,,,,,"/>
<approved hash="113,7,131.976,90.066,FRAME1,,,,,"/>
<approved hash="113,8,194.206,131.976,FRAME2,,,,,"/>
<approved hash="113,5,131.976,90.066,FRAME3,,,,,"/>
<approved hash="113,2,131.976,90.066,FRAME4,,,,,"/>
<approved hash="113,4,131.976,90.066,FRAME5,,,,,"/>
<approved hash="113,6,131.976,90.066,FRAME6,,,,,"/>
<approved hash="113,3,131.976,90.066,FRAME7,,,,,"/>
<approved hash="113,1,131.976,90.066,FRAME8,,,,,"/>
</errors>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
</compatibility>
</eagle>
